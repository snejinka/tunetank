import React from 'react';
import ReactDOM from 'react-dom';
import createStore from './store/createStore';
import AppContainer from './containers/AppContainer';
import { setAuthorizationHeader } from './utils/AuthService';
import { auth } from 'routes/index';

// ========================================================
// Store Instantiation
// ========================================================
const initialState = window.__INITIAL_STATE__;
const store = createStore(initialState);

const token = auth.getToken();
if (token) {
  setAuthorizationHeader(token);
}

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root');

let render = () => {
  const routes = require('./routes/index').default(store);

  if (process.env.NODE_ENV === 'production') {
    Raven.config('https://7b9e52e02c8545ca9ac8fcf549f983a8@sentry.io/218234',{
      release: '2da95dfb052f477380608d59d32b4ab9',
    }).install()
  }

  ReactDOM.render(
    <AppContainer store={store} routes={routes} />,
    MOUNT_NODE,
  );
};

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    const renderApp = render;
    const renderError = (error) => {
      const RedBox = require('redbox-react').default;

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE);
    };

    // Wrap render in try/catch
    render = () => {
      try {
        renderApp();
      } catch (error) {
        renderError(error);
      }
    };

    // Setup hot module replacement
    module.hot.accept('./routes/index', () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE);
        render();
      })
    );
  }
}

// ========================================================
// Go!
// ========================================================
render();
