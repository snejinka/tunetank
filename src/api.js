import axios from 'axios';

const config = { api_server_url: process.env.API_SERVER_URL };
export const API_SERVER_URL = config.api_server_url;
export const API_URL = `${API_SERVER_URL}/api/v1/graphql`;

export function fetchSignIn(auth0UserUrl, data) {
  return axios.post(`https://${auth0UserUrl}/oauth/token`, data).then(resp => resp.data);
}

export function fetchSignUp(auth0UserUrl, data) {
  return axios.post(`https://${auth0UserUrl}/dbconnections/signup`, data, { validateStatus: status => status < 500 });
}

export function fetchUpdateUser(userInfoCopy) {
  return axios.post(API_URL, {
    query: 'mutation UpdateUser($form:UpdateUserInput!) { update_user (input:$form) { id } }',
    variables: { form: userInfoCopy },
  });
}

export function fetchUpdateExactField(id, fields) {
  return axios.post(API_URL, {
    query: 'mutation UpdateUser($form:UpdateUserInput!) { update_user (input:$form) { id } }',
    variables: { form: { id, ...fields } },
  });
}

export function fetchGetCurrentUser() {
  return axios.post(API_URL, {
    query: '{ current_user () { id name email biography role created_at is_current balance_in_cents paypal_account followings { id } } }',
  });
}

export function fetchGetCurrentUserBillingInfo() {
  return axios.post(API_URL, {
    query: '{ current_user () { email name address_first_line address_second_line country city state zip vat } }',
  });
}

export function fetchFollowAuthor(authorId) {
  return axios.post(API_URL, {
    query: 'mutation FollowUser($input:FollowUserInput!) { follow_user (input:$input) { id } }',
    variables: { input: { id: authorId } },
  });
}

export function fetchUnfollowAuthor(authorId) {
  return axios.post(API_URL, {
    query: 'mutation UnfollowUser($input:UnfollowUserInput!) { unfollow_user (input:$input) { id } }',
    variables: { input: { id: authorId } },
  });
}

export function removeTrack(id) {
  return axios.post(API_URL, {
    query: `mutation DeleteTrack($input:DeleteTrackInput!) {
      delete_track (input:$input) {
        id
      }
    }`,
    variables: {
      input: {id: id}
    },
  })
}

export function fetchCatalogSearchResults(filters, tracksCount = 10, offset = 0) {
  return axios.post(API_URL, {
    query: `query FilterTracks($input:FilterTracksInput!)
                { filter_tracks(input: $input)
                    { tracks { id
                               name
                               created_at
                               logo
                               main_track
                               duration
                               is_favorite
                               wave
                               status
                               user { id name avatar total_tracks }
                               versions { main_track { url duration }
                                          short_version { url duration }
                                          medium_version { url duration }
                                          long_version { url duration }
                                        }
                              }
                        total_count
                     }
                }`,
    variables: { input: { ...filters, tracks_count: tracksCount, offset } },
  }).then(resp => resp.data.data.filter_tracks);
}

export function fetchSearchSuggestions(searchString, tracksCount) {
  return axios.post(API_URL, {
    query: `query Entity($search_string:String!, $requested_types:[String], $results_count:Int)
                { find_entity_by_name(search_string: $search_string, requested_types: $requested_types, results_count: $results_count)
                    { id name type }
                }`,
    variables: { search_string: searchString, requested_types: ['moods', 'genres', 'instruments', 'tracks', 'users'], results_count: tracksCount },
  }).then(resp => resp.data.data.find_entity_by_name);
}

export function fetchCategories() {
  return axios.post(API_URL, {
    query: `{ track_categories () { genres { id, name, tracks_count },
                                      moods { id, name, tracks_count },
                                      instruments { id, name, tracks_count}
                                    }
              }`,
  }).then(resp => resp.data.data.track_categories);
}

export function fetchNewTracks() {
  return axios.post(API_URL, {
    query: `query NewTracks($tracks_count:Int)
                { newest_tracks(tracks_count:$tracks_count)
                    { genre { id name }
                      tracks { id name }
                    }
                }`,
    variables: { tracks_count: 20 },
  }).then(resp => resp.data.data.newest_tracks);
}

export function fetchSimilarArtists(userId) {
  return axios.post(API_URL, {
    query: `query User($id:ID!,
                        $similar_authors_count:Int)
                            { user(id:$id)
                                { id
                                similar_authors(similar_authors_count:$similar_authors_count)  { id name avatar }
                                }
                            }`,
    variables: { id: userId, similar_authors_count: 6 },
  }).then(resp => resp.data.data.user.similar_authors);
}

export function fetchCurrentTrack(trackId) {
  return axios.post(API_URL, {
    query: `query Track($id:ID!, $similar_tracks_count:Int)
                    { find_track_by_id(id: $id) { id
                                                  name
                                                  is_favorite
                                                  organization
                                                  created_at
                                                  composer
                                                  publisher
                                                  bit_rate
                                                  sample_rate
                                                  status
                                                  main_track
                                                  duration
                                                  wave
                                                  logo_medium
                                                  versions { main_track { url duration }
                                                             short_version { url duration }
                                                             medium_version { url duration }
                                                             long_version { url duration }
                                                            }
                                                  more_tracks {   id
                                                                  name
                                                                  is_favorite
                                                                  duration
                                                                  logo
                                                                  created_at
                                                                  main_track
                                                                  wave
                                                                  user {id name}
                                                                  versions { main_track { url duration }
                                                                              short_version { url duration }
                                                                              medium_version { url duration }
                                                                              long_version { url duration }
                                                                            }
                                                  }          
                                                  user { id
                                                         name
                                                         avatar
                                                         city
                                                         country
                                                         email
                                                         created_at
                                                         is_current
                                                         tracks {   id
                                                                    name
                                                                    is_favorite
                                                                    duration
                                                                    featured
                                                                    logo
                                                                    created_at
                                                                    main_track
                                                                    wave
                                                                    user {id name}
                                                                    versions { main_track { url duration }
                                                                                short_version { url duration }
                                                                                medium_version { url duration }
                                                                                long_version { url duration }
                                                                              }
                                                                }
                                                        }
                                                  tempo
                                                  looped
                                                  genres {id name}
                                                  moods {id name}
                                                  instruments {id name}
                                                  youtube_id_administrated_by
                                                  similar_tracks(similar_tracks_count:$similar_tracks_count)
                                                        { id
                                                          name
                                                          is_favorite
                                                          logo
                                                          duration
                                                          main_track
                                                          wave
                                                          versions { main_track { url duration }
                                                                     short_version { url duration }
                                                                     medium_version { url duration }
                                                                     long_version { url duration }
                                                                    }
                                                          user { id
                                                                 name
                                                                 avatar
                                                                 city
                                                                 country
                                                                 email
                                                                 biography
                                                                 created_at
                                                                 total_tracks
                                                                 total_featured_tracks
                                                                 is_current
                                                                }
                                                        }
                                                }
                        }`,
    variables: { id: trackId, similar_tracks_count: 16 },
  }).then(resp => resp.data.data.find_track_by_id);
}

export function fetchUserInfo(userId) {
  return axios.post(API_URL, {
    query: `query User($id:ID!,
                         $tracks_input:TrackPaginationInput,
                         $favorite_tracks_input:TrackPaginationInput,
                         $bought_tracks_input:TrackPaginationInput)
                        {user(id:$id) { id
                                        name
                                        email
                                        avatar
                                        role
                                        social_links { type url }
                                        banner
                                        biography
                                        address_first_line
                                        address_second_line
                                        city
                                        state
                                        zip
                                        vat
                                        country
                                        secure_id
                                        created_at
                                        is_current
                                        total_tracks
                                        total_featured_tracks
                                        tracks(tracks_input:$tracks_input) {id
                                                                            name
                                                                            is_favorite
                                                                            logo
                                                                            created_at
                                                                            composer
                                                                            duration
                                                                            status
                                                                            publisher
                                                                            featured
                                                                            main_track
                                                                            wave
                                                                            user {id name}
                                                                            versions { main_track { url duration }
                                                                                       short_version { url duration }
                                                                                       medium_version { url duration }
                                                                                       long_version { url duration }
                                                                                     }
                                                                            }
                                        favorite_tracks(favorite_tracks_input:$favorite_tracks_input) { id
                                                                                                        name
                                                                                                        logo
                                                                                                        created_at
                                                                                                        main_track
                                                                                                        wave
                                                                                                        duration
                                                                                                        is_favorite
                                                                                                        versions { main_track { url duration }
                                                                                                                   short_version { url duration }
                                                                                                                   medium_version { url duration }
                                                                                                                   long_version { url duration }
                                                                                                        }
                                                                                                        user { id
                                                                                                               name
                                                                                                               avatar
                                                                                                               total_tracks
                                                                                                               total_featured_tracks
                                                                                                               is_current
                                                                                                               country
                                                                                                               city}
                                                                                                        }
                                        bought_tracks(bought_tracks_input:$bought_tracks_input) { id
                                                                                                  name
                                                                                                  logo
                                                                                                  created_at
                                                                                                  main_track
                                                                                                  wave
                                                                                                  duration
                                                                                                  is_favorite
                                                                                                  versions { main_track { url duration }
                                                                                                             short_version { url duration }
                                                                                                             medium_version { url duration }
                                                                                                             long_version { url duration }
                                                                                                  }
                                                                                                  user { id
                                                                                                         name
                                                                                                         avatar
                                                                                                         total_tracks
                                                                                                         total_featured_tracks
                                                                                                         is_current
                                                                                                         country
                                                                                                         city}
                                                                                                  }
                                        followers{ id name avatar total_tracks total_followers_tracks }
                                        followings{ id name avatar total_tracks total_followings_tracks }
                                        }
                            }`,
    variables: { id: userId,
      tracks_input: { count: 150, offset: 0, sorting: 1 },
      favorite_tracks_input: { count: 100, offset: 0, sorting: 1 },
      followers_tracks_input: { count: 100, offset: 0, sorting: 1 },
      followings_tracks_input: { count: 100, offset: 0, sorting: 1 },
      bought_tracks_input: { count: 100, offset: 0, sorting: 1 } },
  }).then(resp => resp.data.data.user);
}

export function fetchTrackByUser(userId, count, offset, sorting) {
  return axios.post(API_URL, {
    query: `query User($id:ID!,
                         $tracks_input:TrackPaginationInput)
                        {user(id:$id) { id
                                        total_tracks
                                        total_featured_tracks
                                        role
                                        tracks(tracks_input:$tracks_input) {id
                                                                            name
                                                                            is_favorite
                                                                            logo
                                                                            created_at
                                                                            status
                                                                            composer
                                                                            duration
                                                                            publisher
                                                                            main_track
                                                                            wave
                                                                            status
                                                                            user {id name}
                                                                            versions { main_track { url duration }
                                                                                        short_version { url duration }
                                                                                        medium_version { url duration }
                                                                                        long_version { url duration }
                                                                                     }
                                                                            }
                                                                            }
                            }`,
    variables: { id: userId,
      tracks_input: { count, offset, sorting },
    },
  }).then(resp => resp.data.data.user);
}

export function fetchCreateNewTrack(form) {
  return axios.post(API_URL, {
    query: 'mutation CreateTrack($form:CreateTrackInput!) { create_track (input:$form) { id } }',
    variables: { form },
  }).then(resp => resp);
}

export function fetchContactForm(authorId, subject, message) {
  return axios.post(API_URL, {
    author_id: authorId,
    subject,
    message,
  });
}

export function fetchAddTrackToFavorites(trackId) {
  return axios.post(API_URL, {
    query: 'mutation AddTrackToFavorites($input:AddTrackToFavoritesInput!) { add_track_to_favorites (input:$input) { id } }',
    variables: { input: { id: trackId } },
  }).then(resp => resp.data.data.add_track_to_favorites);
}

export function fetchRemoveTrackFromFavorites(trackId) {
  return axios.post(API_URL, {
    query: 'mutation RemoveTrackFromFavorites($input:RemoveTrackFromFavoritesInput!) {remove_track_from_favorites (input:$input) { id }}',
    variables: { input: { id: trackId } },
  }).then(resp => resp.data.data.remove_track_from_favorites);
}

export function fetchFeaturedTracks() {
  return axios.post(API_URL, {
    query: '{ featured_tracks (featured: true) { id name is_favorite created_at logo main_track duration wave user { id name avatar is_current total_tracks total_featured_tracks country city} versions { main_track { url duration } short_version { url duration } medium_version { url duration } long_version { url duration } } } }',
    variables: { featured: true },
  }).then(resp => resp.data.data.featured_tracks);
}

export function fetchGenres() {
  return axios.post(API_URL, {
    query: '{ genres () { id, name }}',
  }).then(resp => resp.data.data.genres);
}

export function fetchFindTrackByGenre(id) {
  return axios.post(API_URL, {
    query: 'query Genre($id:ID!, $tracks_count:Int, $offset:Int) { find_track_by_genre(genre_id: $id, tracks_count: $tracks_count, offset: $offset) { id name is_favorite created_at logo main_track duration wave featured user { id name avatar is_current total_tracks total_featured_tracks country city } versions { main_track { url duration } short_version { url duration } medium_version { url duration } long_version { url duration } } } }',
    variables: { id, tracks_count: 20, offset: 0 },
  }).then(resp => resp.data.data.find_track_by_genre);
}

export function fetchTrackTemplates(userId) {
  return axios.post(API_URL, {
    query: 'query TrackTemplate($user_id:ID!) { track_templates_by_user(user_id: $user_id) { id name looped vocals tempo bit_rate sample_rate organization composer publisher youtube_id_administrated_by genres { id name } moods { id name } instruments { id name } }}',
    variables: { user_id: userId },
  }).then(resp => resp.data.data.track_templates_by_user);
}

export function fetchCreateTrackTemplate(trackTemplate) {
  return axios.post(API_URL, {
    query: 'mutation CreateTrackTemplate($input:CreateTrackTemplateInput!) { create_track_template(input: $input) { id name } }',
    variables: { input: trackTemplate },
  });
}

export function fetchAddNewsletterEmail(email) {
  return axios.post(API_URL, {
    query: 'mutation AddNewsletterEmail($form:AddNewsletterEmailInput!) { add_newsletter_email (input:$form) { id } }',
    variables: { form: { email } },
  });
}

export function fetchBecomeASeller(id) {
  return axios.post(API_URL, {
    query: 'mutation MakeUserSeller($input:MakeUserSellerInput!) { make_user_seller (input:$input) { id } }',
    variables: { input: { id } },
  });
}

export function fetchDownloadTrack(id) {
  return axios.post(API_URL, {
    query: `query Track($id:ID!)
                    { find_track_by_id(id: $id) { id
                                                  originals_archive
                                                }
                    }`,
    variables: { id },
  });
}
