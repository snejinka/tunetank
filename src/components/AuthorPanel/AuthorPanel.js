import React, { PropTypes } from 'react';
import { followAuthor, unfollowAuthor, openModal } from 'store/user';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { countries } from 'common';
import NoAvatar from 'assets/img/no-avatar.png';
import _ from 'lodash';

class AuthorPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      followingIds: props.loggedIn ? props.currentUser.followings.map(i => i.id) : [],
    };
  }

  static propTypes = {
    user: PropTypes.object,
    openAuthorPanel: PropTypes.bool,
    toggleAuthorPanel: PropTypes.func,
    hoverTrack: PropTypes.bool,
    isFollowingItem: PropTypes.bool,
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.loggedIn) {
      if (!_.isEqual(nextProps.currentUser.followings, this.props.currentUser.followings)) {
        this.setState({
          followingIds: nextProps.currentUser.followings.map(i => i.id),
        });
      }
    }
  }

  setStateFollow(following_id) {
    const newFollowingIds = _.clone(this.state.followingIds);
    newFollowingIds.push(following_id);
    this.setState({
      followingIds: newFollowingIds,
    });
  }

  setStateUnfollow(following_id) {
    const newFollowingIds = _.clone(this.state.followingIds);
    newFollowingIds.splice(this.state.followingIds.indexOf(following_id), 1);
    this.setState({
      followingIds: newFollowingIds,
    });
  }

  follow(following_id) {
    if (!this.props.loggedIn) {
      this.props.actions.openModal('sign_up');
    } else {
      this.setStateFollow(following_id);
      this.props.actions.followAuthor(following_id).catch((err => this.setStateUnfollow(following_id)));
    }
  }

  unfollow(following_id) {
    this.setStateUnfollow(following_id);
    this.props.actions.unfollowAuthor(following_id).catch((err => this.setStateFollow(following_id)));
  }

  handleBlur(e) {
    if (!this.el.contains(e.relatedTarget)) {
      _.delay(() => {
        this.props.toggleAuthorPanel();
      }, 200);
    }
  }

  renderCountry() {
    for (let i = 0; i < countries.length; i++) {
      if (countries[i].value === this.props.user.country) {
        return countries[i].label;
      }
    }
  }

  render() {
    const avatar = this.props.user.avatar ? this.props.user.avatar : NoAvatar;
    return (
      <div
        className={classNames('author-panel', { 'author-panel_visible': this.props.openAuthorPanel, 'author-panel_follower': this.props.isFollower })}
        onMouseOut={e => this.handleBlur(e)}
        style={{left: this.props.left}}
        ref={(el) => { this.el = el; }}
      >
        <span className="author-panel__area-target" />
        <div className="author-panel__image" style={{ backgroundImage: `url(${avatar})` }} />
        <div className="author-panel__name">
          {this.props.user.name}
        </div>
        <div className="author-panel__description">
          {this.props.user.city && <p>{this.props.user.city}</p> }
          <p>{this.renderCountry()}</p>
        </div>
        <div className="author-panel__count">
          <div className="music-icon music-icon__indent-right" />
          {this.props.user.total_tracks}
        </div>
        {this.props.loggedIn && !this.props.user.is_current && this.state.followingIds.includes(this.props.user.id) &&
          <button
            className="btn btn-action btn-action__role-follow"
            onClick={() => this.unfollow(this.props.user.id)}
          >
                  Unfollow
                </button>
              }
        {this.props.loggedIn && !this.props.user.is_current && !this.state.followingIds.includes(this.props.user.id) &&
          <button
            className="btn btn-action btn-action__role-follow"
            onClick={() => this.follow(this.props.user.id)}
          >
                Follow
              </button>
              }
        {!this.props.loggedIn &&
          <button
            className="btn btn-action btn-action__role-follow"
            onClick={() => this.follow(this.props.user.id)}
          >
                Follow
              </button>
              }
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  loggedIn: state.user.loggedIn,
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    followAuthor,
    unfollowAuthor,
    openModal,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(AuthorPanel);
