import React, { PropTypes } from 'react';
import TrackListItem from '../TrackListItem';
import 'styles/variables.scss';

class TrackList extends React.Component {
  static defaultProps = {
    isBought: false,
  };

  static propTypes = {
    tracks: PropTypes.array,
    userInfo: PropTypes.object,
    hasTrackMenu: PropTypes.bool,
    viewAuthorTracks: PropTypes.bool,
    showVersions: PropTypes.bool,
    setTracks: PropTypes.func,
    isBought: PropTypes.bool,
    isDownload: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      hoverTrack: false,
    };
  }

  onMouseOverTrack(e) {
    e.stopPropagation();
    this.setState({
      hoverTrack: true,
    });
  }

  onMouseOutTrack(e) {
    e.stopPropagation();
    this.setState({
      hoverTrack: false,
    });
  }

  renderList() {
    const nodes = [];
    for (let i = 0; i < this.props.tracks.length; i++) {
      nodes.push(
        <TrackListItem
          track={this.props.tracks[i]}
          userInfo={this.props.userInfo}
          hasTrackMenu={false}
          viewAuthorTracks={this.props.viewAuthorTracks}
          showVersions={this.props.showVersions}
          setTracks={this.props.setTracks}
          isBought={this.props.isBought}
          isDownload={this.props.isDownload}
          key={this.props.tracks[i].id}
          removeTrack={this.props.removeTrack}
        />
      );
    }

    return nodes;
  }

  render() {
    return (
      <div>
        <div className="track-list">
          {this.renderList()}
        </div>
      </div>
    );
  }
}

export default TrackList;
