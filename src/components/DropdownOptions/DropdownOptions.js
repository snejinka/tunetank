import React from 'react';
import { Row, Col, DropdownButton, MenuItem } from 'react-bootstrap';

export const DropdownOptions = () => (
  <DropdownButton title="Download" id="bg-nested-dropdown" className="dropdown-options dropdown-download">
    <MenuItem eventKey="1">Download ZIP</MenuItem>
    <MenuItem eventKey="2">Download BSD License</MenuItem>
  </DropdownButton>
     );

export default DropdownOptions;
