import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import 'styles/variables.scss';
import { DropdownButton, MenuItem, Fade } from 'react-bootstrap';
import { setTab } from 'store/user.js';
import { tabs } from 'common';
import { browserHistory } from 'react-router';
import slug from 'slug';
import _ from 'lodash';


class DropdownMenuHeader extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickItem = this.handleClickItem.bind(this);
    this.handleClickUpload = this.handleClickUpload.bind(this);
    this.state = {
      dropdownOpen: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.openDropDown === false) {
      _.delay(()=>{
        this.setState({
          dropdownOpen: false,
        });
      }, 300);
    } else {
      this.setState({
        dropdownOpen: true,
      });
    }
  }

  onHover() {
    this.setState({
      dropdownOpen: true,
    }, () => {
      this.props.open();      
    })
  }

  onMouseOut() {
    this.props.close();
  }

  handleClickItem(tab) {
    if (tab) {
      this.props.actions.setTab(tab);
    }
    this.setState({ openDropdown: false });
    browserHistory.push(`/author/${slug(this.props.currentUser.name, { lower: true })}/${this.props.currentUser.id}`);
  }

  handleClickUpload() {
    this.setState({ openDropdown: false });
    browserHistory.push('/uploadItem');
  }

  renderSellerItems(isSeller) {
    if (!isSeller) { return false; }

    return (
      <div>
        <MenuItem eventKey="1" onClick={() => this.handleClickUpload()}>Upload</MenuItem>
        <MenuItem eventKey="1" onClick={() => this.handleClickItem(tabs.earnings)}>Earnings</MenuItem>
        <MenuItem eventKey="1" onClick={() => this.handleClickItem(tabs.statements)}>Statements</MenuItem>
        <MenuItem eventKey="1" onClick={() => this.handleClickItem(tabs.withdrawals)}>Withdrawals</MenuItem>
        <MenuItem
          eventKey="1"
          className="dropdown-menu__separator"
          onClick={() => this.handleClickItem(tabs.referralUrl)}
        >
          Referal URL
        </MenuItem>
        <li />
      </div>
    );
  }

  renderMenuItems(mainPage,isSeller, logout) {
    return <div>
      <MenuItem eventKey="1" onClick={() => this.handleClickItem(mainPage)}>My Page</MenuItem>
      <MenuItem eventKey="2" onClick={() => this.handleClickItem(tabs.downloads)}>Downloads</MenuItem>
      <MenuItem eventKey="1" onClick={() => this.handleClickItem(tabs.favourites)}>Favourites</MenuItem>
      <MenuItem eventKey="2" className="dropdown-menu__separator" onClick={() => this.handleClickItem(tabs.settings)}>Settings</MenuItem>
      <li />      
      {this.renderSellerItems(isSeller)}
      <div className="menu-item__hack">
        <MenuItem
          eventKey="1"
          onClick={() => logout()}
        >
          <button className="btn btn-sign-out">Sign Out</button>            
        </MenuItem>
      </div>
    </div>
  }

  render() {
    const { logout, currentUser, openDropDown } = this.props;
    const isSeller = currentUser.role === 'seller' || currentUser.role === 'admin';
    const mainPage = isSeller ? tabs.main : tabs.followingFeed;
    return (
      <div className="user-options">
        <div className="user-options">
          <button className="dropdown-options dropdown-options__icon-plus dropdown-toggle btn btn-default"> </button>
          <Fade in={openDropDown} timeout="50">          
            <div className={`dropdown ${this.state.dropdownOpen ? 'open' : ''}`}>
              <ul className="dropdown-menu hotfixforsergey">
                {this.renderMenuItems(mainPage,isSeller, logout)}
              </ul>
            </div>
          </Fade>          
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTab,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(DropdownMenuHeader);
