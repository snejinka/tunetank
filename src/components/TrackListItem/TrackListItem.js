import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col } from 'react-bootstrap';
import { showBuyTrack, openModal, downloadTrack, deleteTrack } from 'store/user';
import * as common from 'common';
import Like from 'components/Like';
import _ from 'lodash';
import { TrackSvg } from 'styles/svgs/svgs.js';
import 'styles/variables.scss';
import classNames from 'classnames';
import TrackItem from '../TrackItem';
import WaveSurfer from '../WaveSurfer';
import DropdownOptions from 'components/DropdownOptions';
import moment from 'moment';
import confirm from 'components/confirm/confirm';

const STATUSES = {
  0: 'Moderating',
  1: 'Approved',
  2: 'Rejected'
}

class TrackListItem extends React.Component {
  static defaultProps = {
    isBought: false,
  };

  static propTypes = {
    track: PropTypes.object.isRequired,
    userInfo: PropTypes.object,
    hasTrackMenu: PropTypes.bool,
    viewAuthorTracks: PropTypes.bool,
    showVersions: PropTypes.bool,
    setTracks: PropTypes.func,
    isBought: PropTypes.bool,
  };

  constructor(props) {
    super(props);



    this.state = {
      likedTrack: this.props.track.is_favorite,
      hover: false,
      showVersionsDropdown: false,
    };

    this.showVersions = this.showVersions.bind(this);
    this.hideVersions = this.hideVersions.bind(this);
    this.setImageHover = this.setImageHover.bind(this);
    this.unsetImageHover = this.unsetImageHover.bind(this);
    this.handleClickWave = this.handleClickWave.bind(this);
  }

  setImageHover() {
    this.setState({ hover: true });
  }

  hideVersions() {
    this.setState({ showVersionsDropdown: false });
  }

  showVersions() {
    this.setState({ showVersionsDropdown: true });
  }

  unsetImageHover() {
    this.setState({ hover: false });
  }

  handleClickWave() {
    this.refs.track_item.getWrappedInstance().handleTrackButton();
  }

  renderVersionsTooltip() {
    const props = this.props;
    return (
      <div className="tooltip_versions tooltip_versions__list show">
        <span className="tooltip_versions__text tooltip_versions__title">Versions by lenght</span>
        <span className={classNames('tooltip_versions__text', { hidden: props.track.versions[common.TrackVersions.main_track].url === null })}>Full Track - {common.timeFormatter(props.track.versions[common.TrackVersions.main_track].duration)}</span>
        <span className={classNames('tooltip_versions__text', { hidden: props.track.versions[common.TrackVersions.short_version].url === null })}>Short Track - {common.timeFormatter(props.track.versions[common.TrackVersions.short_version].duration)}</span>
        <span className={classNames('tooltip_versions__text', { hidden: props.track.versions[common.TrackVersions.medium_version].url === null })}>Medium Track - {common.timeFormatter(props.track.versions[common.TrackVersions.medium_version].duration)}</span>
        <span className={classNames('tooltip_versions__text', { hidden: props.track.versions[common.TrackVersions.long_version].url === null })}>Long Track - {common.timeFormatter(props.track.versions[common.TrackVersions.long_version].duration)}</span>
      </div>
    );
  }

  deleteTrack(id) {
    confirm(`Are you sure you want to delete this track?`,
      { title: 'Delete confrimation',
        btnText: 'Delete',
      }
    ).then(() => {
      this.props.actions.deleteTrack(id);
      this.props.removeTrack(id);
    })
  }

  renderVersions() {
    const versionUrls = [];
    _.forEach(this.props.track.versions, (version) => {
      if (version.url != null) {
        versionUrls.push(version.url);
      }
    });

    return (
      <div className="versions-circle btn_version__short" onMouseEnter={this.showVersions} onMouseLeave={this.hideVersions} style={{ marginRight: 21 }}>
        <span className="versions-circle__text">{versionUrls.length}</span>
      </div>
    );
  }

  renderDownloadButton() {
    if (this.props.loggedIn) {
      if (this.props.isBought) {
        return (
          <button
            onClick={() => this.props.actions.downloadTrack(this.props.track.id)}
            className="btn btn-download"
            dangerouslySetInnerHTML={{ __html: TrackSvg.downloadIcon }}
          />
        );
      }
      return (
        <a
          href={this.props.track.versions.main_track.url}
          download
          className="btn btn-download"
          dangerouslySetInnerHTML={{ __html: TrackSvg.downloadIcon }}
        />
      );
    }
    return (
      <a
        onClick={() => this.props.actions.openModal('sign_up')}
        className="btn btn-download"
        dangerouslySetInnerHTML={{ __html: TrackSvg.downloadIcon }}
      />
    );
  }

  render() {
    const width = this.props.showVersions ? 390 : 360;
    const height = this.props.showVersions ? 44 : 39;

    return (
      <div className="track-list__item" onMouseEnter={this.setImageHover} onMouseLeave={this.unsetImageHover}>
        <Row className="flexbox align-center">
          <Col md={3} sm={6} xs={12}>
            <TrackItem
              ref="track_item"
              track={this.props.track}
              userInfo={this.props.userInfo}
              hasTrackMenu={this.props.hasTrackMenu}
              viewAuthorTracks={this.props.viewAuthorTracks}
              hover={this.state.hover}
              setTracks={this.props.setTracks}
              isBought={this.props.isBought}
            />
          </Col>
          <Col md={6}>
            <WaveSurfer
              track={this.props.track}
              width={width}
              height={height}
              selectedVersion={common.TrackVersions.main_track}
            />
          </Col>
          <div className="track-duration track-duration__left">
            {common.timeFormatter(~~(this.props.track.duration))}
          </div>
          {this.props.isDownload ? (
            <Col md={3} className="track-list__item-options track-list__item-options-download flexbox align-center justify-center">
              <DropdownOptions/>
              <div className="track-list__item-type">Professional - $95</div>
            </Col>
          ) : (
          <Col md={3} className={classNames('track-list__item-options flexbox align-center', { 'track-list__item-with-versions nopadding-left': this.props.showVersions })}>
            {this.state.showVersionsDropdown && this.props.showVersions && this.renderVersionsTooltip()}
            {this.props.showVersions && this.renderVersions()}
            <div className="player__btn-actions-container player__btn-actions-container_reverse">
              {!this.props.isBought && !(this.props.loggedIn && this.props.track.user.id === this.props.currentUser.id) && this.props.track &&
                <button className={`btn btn-action ${this.props.loggedIn && this.props.track.user.id === this.props.currentUser.id ? 'disabledDiv': ''}`} onClick={() => { this.props.actions.showBuyTrack(this.props.track); }}>License</button>
              }
              {this.props.loggedIn && this.props.track.user.id === this.props.currentUser.id &&
                <div className="author-info">
                  <div>
                    Created at: {moment(this.props.track.created_at).format('DD MM YYYY')}                
                  </div>
                  <div>
                    Status: {STATUSES[this.props.track.status]}
                  </div>
                  {this.props.track.status !== 1 &&
                    <div className="author-info__delete" onClick={() => (this.deleteTrack(this.props.track.id))}>
                      Delete
                    </div>
                  }
                </div>
              } 
              {!(this.props.loggedIn && this.props.track.user.id === this.props.currentUser.id) && <Like track={this.props.track} /> }
              {!(this.props.loggedIn && this.props.track.user.id === this.props.currentUser.id) && this.renderDownloadButton()}
            </div>
          </Col>
          )}
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loggedIn: state.user.loggedIn,
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    showBuyTrack,
    openModal,
    downloadTrack,
    deleteTrack,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackListItem);
