import React, { PropTypes, Component } from 'react';
import { SimpleSelect } from 'react-selectize';
import 'react-selectize/themes/index.css';

class SelectizeSimpleSelectTheme extends Component {
  static propTypes = {
    options: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
    ]).isRequired,
    onSelect: PropTypes.func.isRequired,
    defaultValue: PropTypes.object,
    placeholder: PropTypes.string,
  }

  componentWillMount() {
    const options = this.props.options;
    if (typeof options === 'array') {
      this.options = options.map(fruit => ({ label: fruit, value: fruit }));
    }

    if (typeof options === 'object') {
      this.options = Object.keys(options).map((key) => {
        const value = this.props.options[key];
        return { label: value, value: key };
      });
    }
  }

  render() {
    const props = this.props;
    return (<SimpleSelect
      {...props}
      options={this.options}
      defaultValue={this.props.defaultValue}
      placeholder={this.props.placeholder || ''}
      onValueChange={(newValue, callback) => this.props.onSelect(newValue)}
      className="selectize-select-theme react-default-template"
    />);
  }
}
export default SelectizeSimpleSelectTheme;
