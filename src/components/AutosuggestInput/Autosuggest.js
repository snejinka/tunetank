import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import 'styles/variables.scss';
import Autosuggest from 'react-autosuggest';
import { requestSearchSuggestions,
        clearSearchSuggestions,
        changeFilterValue,
        FilterFields } from 'store/filters';
import _ from 'lodash';
import { browserHistory } from 'react-router';

function getSuggestions(value) {
  return [];
}

function getSuggestionValue(suggestion) {
  return suggestion.name;
}

const renderSuggestion = (suggestion, query) => (
  <div className="form-control input-primary input_search">
    {suggestion.name}
  </div>
  );

class AutosuggestInput extends React.Component {
  constructor(props) {
    super(props);

    this.delayedCallback = _.debounce((value) => {
      this.props.actions.changeFilterValue(FilterFields.search_string, value);
      this.props.actions.requestSearchSuggestions(value, 5);
    }, 300);

    this.state = {
      value: props.search_string,
      suggestions: [],
    };
  }

  componentWillReceiveProps(newProps) {
    if (this.props.suggestions !== newProps.suggestions) {
      this.setState({
        suggestions: newProps.suggestions,
      });
    }

    if (newProps.search_string == '') {
      this.setState({
        value: '',
      });
    }
  }

  onChange = (event, { newValue, method }) => {
    this.setState({
      value: newValue,
    });
    this.props.actions.changeFilterValue(FilterFields.search_string, newValue);
  };

  onKeyDown = (e) => {
    if (e.keyCode == 13) {
      browserHistory.push({ pathname: '/catalog', query: { q: this.state.value } });
    }
  }

  onSuggestionsFetchRequested = ({ value }) => {
    this.delayedCallback(value);
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: 'Search for music by genres, moods, etc.',
      value,
      onChange: this.onChange,
      onKeyDown: this.onKeyDown,
    };

    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
      />
    );
  }
}

const mapStateToProps = (state, props) => ({
  suggestions: state.catalogFilters.suggestions,
  search_string: state.catalogFilters.filters.search_string,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    requestSearchSuggestions,
    clearSearchSuggestions,
    changeFilterValue,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(AutosuggestInput);
