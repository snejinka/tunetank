import React, { PropTypes } from 'react';
import { Pagination } from 'react-bootstrap';

class PaginationAdvanced extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
    };
  }

  static propTypes = {
    changePage: PropTypes.func,
    allPages: PropTypes.number,
  }

  handleSelect(eventKey) {
    this.setState({
      activePage: eventKey,
    });
    this.props.changePage(eventKey);
  }

  render() {
    const props = this.props;
    const paginationProps = { ...props };
    delete paginationProps.changePage;
    delete paginationProps.allPages;

    return (
      <Pagination
        prev
        next
        ellipsis
        boundaryLinks
        items={this.props.allPages}
        maxButtons={4}
        activePage={this.state.activePage}
        onSelect={this.handleSelect.bind(this)}
        className="pagination-defaul"
        {...paginationProps}
      />
    );
  }
}


export default PaginationAdvanced;
