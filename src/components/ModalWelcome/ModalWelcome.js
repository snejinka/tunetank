import React, { PropTypes } from 'react';
import AuthService from 'utils/AuthService';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  Modal,
  Button,
  ButtonToolbar,
} from 'react-bootstrap';
import { browserHistory } from 'react-router';
import ClassNames from 'classnames';
import * as common from 'common';

class ModalWelcome extends React.Component {
  constructor() {
    super();
    this.state = {
      submitted: false,
      email: '',
      password: '',
      spinner: false,
      authError: false,
      errorMessage: '',
    };
    this.emailRegexp = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-?\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.handleSignUp = this.handleSignUp.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  static propTypes = {
    show: PropTypes.bool,
    auth: PropTypes.instanceOf(AuthService),
    getCurrentUser: PropTypes.func,
    open: PropTypes.func,
    close: PropTypes.func,
    setLoggedIn: PropTypes.func,
  }

  handleChangeInput(e) {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({ [name]: value });
  }

  handleLogin() {
    this.props.open('sign_in');
  }

  handleSubmit() {
    this.setState({ submitted: true });
    const isValidEmail = common.validateEmail(this.state.email);
    const isValidPass = common.validatePass(this.state.password);
    if (isValidEmail && isValidPass) {
      this.setState({ spinner: true });
      this.props.auth._doAuthentication(this.state.email, this.state.password).then(() => {
        this.props.getCurrentUser().then(() => {
          this.props.close('sign_in');
          this.props.setLoggedIn(true);
          browserHistory.replace({ pathname: location.pathname });
        }).catch(() => {
          this.setState({ spinner: false });
        });
      }).catch((error) => {
        if (error.message === 'Request failed with status code 403') {
          this.setState({ authError: true, errorMessage: 'Invalid credentials! Please, try again.' });
        }
        if (error.message === 'Request failed with status code 429') {
          this.setState({ authError: true, errorMessage: 'Too many attempts! Please, try again later.' });
        }
        this.setState({ spinner: false });
      });
    }
  }

  handleClose = () => {
    this.props.close('sign_in');
  }

  handleSignUp() {
    this.props.close('sign_in');
    this.props.open('sign_up');
  }

  handleKeyPress(e) {
    if (e.charCode === 13) {
      this.handleSubmit();
    }
  }

  render() {
    const props = this.props;
    const backdropStyle = {
      zIndex: '100',
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
    };

    const isValidEmail = common.validateEmail(this.state.email);
    const isValidPass = common.validatePass(this.state.password);

    const modalProps = {
      ...props,
    };
    delete modalProps.auth;
    delete modalProps.close;
    delete modalProps.getCurrentUser;
    delete modalProps.setLoggedIn;

    return (
      <ButtonToolbar>
        <button className="header__nav-item" onClick={this.handleLogin}>Login</button>
        <Modal {...modalProps} backdropStyle={backdropStyle} show={this.props.show} onHide={this.handleClose} onKeyPress={this.handleKeyPress} dialogClassName="modal-default">
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg" className="title_primary title_primary__inner">Welcome Back!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form action="" className="form-default">
              <FormGroup className="form-group__indent-bot-big">
                {this.state.submitted && this.state.authError && <HelpBlock className="not-valid">
                  {this.state.errorMessage}
                </HelpBlock>
}
                <ControlLabel>
                  E-mail *
                </ControlLabel>
                <FormControl type="email" name="email" value={this.state.email} onChange={this.handleChangeInput} /> {this.state.submitted && this.state.email === '' && <HelpBlock className="not-valid">
                  E-mail can't be blank
                </HelpBlock>
              }
                {
                this.state.submitted && !isValidEmail && this.state.email !== '' && <HelpBlock className="not-valid">
                  Invalid format
                </HelpBlock>
              } </FormGroup>
              <FormGroup>
                <ControlLabel>
                  Password *
                </ControlLabel>
                <FormControl type="password" name="password" value={this.state.password} onChange={this.handleChangeInput} />
                {
                this.state.submitted && this.state.password === '' && <HelpBlock className="not-valid">
                  Password can't be blank
                </HelpBlock>
}
                {this.state.submitted && !isValidPass && this.state.password !== '' && <HelpBlock className="not-valid">
                  The password does not match with the minimum password length.
                </HelpBlock>
}
                {/* <HelpBlock>Forgot password?</HelpBlock>*/}
              </FormGroup>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <div className="modal-default__submit-wrap">
              <Button onClick={this.handleSubmit} disabled={!this.state.email || !this.state.password} className={ClassNames('btn-primary btn-primary__form-submit', { 'btn-primary__form-submit_spinner': this.state.spinner })}>
                <span className="btn-primary__text">Submit</span>
                {this.state.spinner && <i
                  className="fa fa-spinner fa-spin" style={{ padding: 3 }}
                />}
              </Button>
            </div>

            <div className="flexbox justify-center align-center">
              <div className="title title-note">Don’t have an account?</div>
              &nbsp;
              <a
                className="action-link"
                style={{ cursor: 'pointer' }}
                onClick={this.handleSignUp}
              >
                Sign Up, It’s Free
              </a>!
            </div>
          </Modal.Footer>
        </Modal>
      </ButtonToolbar>
    );
  }
}
export default ModalWelcome;
