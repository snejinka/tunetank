import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as common from 'common';
import classNames from 'classnames';
import { setTrackPath, setPlayStatus, setNextTrack, setPreviousTrack, setCurrentVersion } from 'store/audioPlayer';
import { Row, Col, Grid } from 'react-bootstrap';
import TrackImage1 from '../../assets/img/track-img1.png';
import DropdownMenuUp from '../DropdownMenuUp';
import WaveSlider from '../WaveSlider';
import { CROWN_SVG } from 'assets/js/crown';
import { SHORT_NOTE_SVG } from 'assets/js/short_note';
import { MEDIUM_NOTE_SVG } from 'assets/js/medium_note';
import { LONG_NOTE_SVG } from 'assets/js/long_note';
import { Link } from 'react-router';
import { TrackSvg } from 'styles/svgs/svgs.js';
import { showBuyTrack, openModal } from 'store/user';
import Like from 'components/Like';
import slug from 'slug';

class AudioPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showVersionsDropdown: false,
      selectedVersion: common.TrackVersions.main_track,
    };

    this.onClickOutside = this.clickOutsideVersions.bind(this);
  }

  handlePlay() {
    this.props.actions.setPlayStatus(common.AudioPlayerStatuses.PLAY);
  }

  handlePause() {
    this.props.actions.setPlayStatus(common.AudioPlayerStatuses.PAUSE);
  }

  handleNext() {
    this.props.actions.setNextTrack();
  }

  handlePrevious() {
    this.props.actions.setPreviousTrack();
  }

  clickOutsideVersions() {
    this.setState({showVersionsDropdown: false});
    document.removeEventListener('click', this.onClickOutside);
  }

  toggleVersionsDropdown() {
    this.setState({ showVersionsDropdown: !this.state.showVersionsDropdown });    
    document.addEventListener('click', this.onClickOutside)
  }

  handleClickVersionsDropdown(key) {
    this.props.actions.setCurrentVersion(key);
    this.setState({ showVersionsDropdown: false, selectedVersion: key });
  }

  renderTrackButton() {
    if (this.props.play_status === common.AudioPlayerStatuses.PAUSE) {
      return (
        <button onClick={() => this.handlePlay()} className="btn btn-track-control btn-track-control__play" dangerouslySetInnerHTML={{ __html: TrackSvg.playIcon }} />
      );
    } else if (this.props.play_status === common.AudioPlayerStatuses.PLAY) {
      return (
        <button onClick={() => this.handlePause()} className="btn btn-track-control btn-track-control__play" dangerouslySetInnerHTML={{ __html: TrackSvg.pauseIcon }} />
      );
    } else if (this.props.play_status === common.AudioPlayerStatuses.STOP) {
      return (
        <button onClick={() => this.handlePlay()} className="btn btn-track-control btn-track-control__play" dangerouslySetInnerHTML={{ __html: TrackSvg.playIcon }} />
      );
    }
  }

  renderDownloadButton() {
    if (this.props.loggedIn) {
      return (
        <a
          href={this.props.track_playing.versions.main_track.url}
          download
          className="btn btn-download"
          dangerouslySetInnerHTML={{ __html: TrackSvg.downloadIcon }}
        />
      );
    }
    return (
      <a
        onClick={() => this.props.actions.openModal('sign_up')}
        className="btn btn-download"
        dangerouslySetInnerHTML={{ __html: TrackSvg.downloadIcon }}
      />
    );
  }

  renderVersions() {
    const props = this.props;
    const selectedType = (props.track_playing.versions[this.state.selectedVersion].url === null) ? common.TrackVersions.main_track : this.state.selectedVersion;
    return (
      <div className="dropdown-menu show">
        <div
          className={classNames('dropdown-item', { selected: selectedType === common.TrackVersions.main_track, hidden: !props.track_playing.versions[common.TrackVersions.main_track].url })}
          onClick={() => this.handleClickVersionsDropdown(common.TrackVersions.main_track)}
        >
          <div className="dropdown-item__text" dangerouslySetInnerHTML={{ __html: CROWN_SVG }} />
          <div className="dropdown-item__text">{common.timeFormatter(props.track_playing.versions[common.TrackVersions.main_track].duration)}</div>
        </div>
        <div
          className={classNames('dropdown-item', { selected: selectedType === common.TrackVersions.short_version, hidden: !props.track_playing.versions[common.TrackVersions.short_version].url })}
          onClick={() => this.handleClickVersionsDropdown(common.TrackVersions.short_version)}
        >
          <div className="dropdown-item__text" dangerouslySetInnerHTML={{ __html: SHORT_NOTE_SVG }} />
          <div className="dropdown-item__text">{common.timeFormatter(props.track_playing.versions[common.TrackVersions.short_version].duration)}</div>
        </div>
        <div
          className={classNames('dropdown-item', { selected: selectedType === common.TrackVersions.medium_version, hidden: !props.track_playing.versions[common.TrackVersions.medium_version].url })}
          onClick={() => this.handleClickVersionsDropdown(common.TrackVersions.medium_version)}
        >
          <div className="dropdown-item__text" dangerouslySetInnerHTML={{ __html: MEDIUM_NOTE_SVG }} />
          <div className="dropdown-item__text">{common.timeFormatter(props.track_playing.versions[common.TrackVersions.medium_version].duration)}</div>
        </div>
        <div
          className={classNames('dropdown-item', { selected: selectedType === common.TrackVersions.long_version, hidden: !props.track_playing.versions[common.TrackVersions.long_version].url })}
          onClick={() => this.handleClickVersionsDropdown(common.TrackVersions.long_version)}
        >
          <div className="dropdown-item__text" dangerouslySetInnerHTML={{ __html: LONG_NOTE_SVG }} />
          <div className="dropdown-item__text">{common.timeFormatter(props.track_playing.versions[common.TrackVersions.long_version].duration)}</div>
        </div>
      </div>
    );
  }

  render() {
    if (!this.props.track_path) { return false; }

    let logo = '';
    if (this.props.track_playing.logo) {
      logo = this.props.track_playing.logo;
    }

    if (this.props.track_playing.logo_medium) {
      logo = this.props.track_playing.logo_medium;
    }

    return (
      <div className="sticky_footer">
        <Grid className="container_player">
          <Row>
            <Col md={12}>
              <div className="player-panel-wrapper">
                <div className="player-panel">
                  <div className="track-control-box">
                    <button className="btn btn-track-control btn-track-control__prev" onClick={this.handlePrevious.bind(this)} dangerouslySetInnerHTML={{ __html: TrackSvg.prevIcon }} />
                    {this.renderTrackButton()}
                    <button className="btn btn-track-control btn-track-control__next" onClick={this.handleNext.bind(this)} dangerouslySetInnerHTML={{ __html: TrackSvg.nextIcon }} />
                  </div>
                  <WaveSlider track={this.props.track_playing} selectedVersion={this.state.selectedVersion} />
                  <div className="btn-group dropup player-dropup">
                    {this.state.showVersionsDropdown ?
                      <button className={`btn_version__true btn btn_version`} onClick={this.toggleVersionsDropdown.bind(this)}>
                        <i className="fa fa-times" aria-hidden="true" />
                      </button> :
                      <button className="btn btn_version" onClick={this.toggleVersionsDropdown.bind(this)}>Versions</button>}
                    {this.state.showVersionsDropdown && this.renderVersions()}
                  </div>
                </div>
                {this.props.track_playing &&
                <div className="track-current">
                  <div className="track track__separate">
                    <div className="track__img" style={{ backgroundImage: `url(${logo})` }} />
                    <div className="track__info">
                      <Link to={`/track/${slug(this.props.track_playing.name, { lower: true })}/${this.props.track_playing.id}`} className="track__name">{this.props.track_playing.name && this.props.track_playing.name}</Link>
                      <Link to={`/author/${slug((this.props.track_playing.user && this.props.track_playing.user.name), { lower: true })}/${this.props.track_playing.user.id}`} className="track__author">{this.props.track_playing.user && this.props.track_playing.user.name}</Link>
                    </div>
                    <div className="player__btn-actions-container">
                      <button className="btn btn-action" onClick={() => this.props.actions.showBuyTrack(this.props.track_playing)}>License</button>
                      <Like track={this.props.track_playing} />
                      {this.renderDownloadButton()}
                    </div>
                  </div>
                </div>
                }
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  track_path: state.audioPlayer.track_path,
  play_status: state.audioPlayer.play_status,
  track_playing: state.audioPlayer.track_playing,
  selectedType: state.audioPlayer.selectedType,
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackPath,
    setPlayStatus,
    setNextTrack,
    setPreviousTrack,
    setCurrentVersion,
    showBuyTrack,
    openModal,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(AudioPlayer);
