import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as common from 'common';
import WaveSurfer from 'utils/wavesurfer.js';
import { setTrackPath, setPlayStatus, setPlayingPosition, setTrackPlaying, setCurrentVersion } from 'store/audioPlayer';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import classNames from 'classnames';
import Volume from 'assets/img/volume.png';
import Mute from 'assets/img/mute.png';
import VolumeCaret from 'assets/img/volume-caret.png';
import { TrackSvg, wave } from 'styles/svgs/svgs.js';

class Wave extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      durationInSec: common.zeroTime,
      showWave: false,
      showVolume: false,
      volume: 100,
      muteVolume: 100,
    };

    this.uniqID = common.generateRandomHash();
    this.height = this.props.height || 52;
    this.width = this.props.width || 494;

    if (typeof WaveSurfer === undefined) {
      throw new Error('WaveSurfer is undefined!');
    }

    this.waveOptions = {
      container: '#player_' + this.uniqID,
      backend: 'MediaElement',
      mediaType: 'audio',
      waveColor: '#656363',
      progressColor: '#ffcc00',
      height: this.height,
      normalize: true,
      barWidth: 2,
      forceDecode: false
    };
  }

  static propTypes = {
    track: PropTypes.object,
    selectedVersion: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number
  }

  componentDidMount() {
    this._wavesurfer = WaveSurfer.create(this.waveOptions);
    this.wave = JSON.parse(this.props.track.wave);
    this._wavesurfer.backend.peaks = this.wave.data;
    this._wavesurfer.drawBuffer();

    this._wavesurfer.drawer.handlers.click[0] = function() {
      return false;
    }

    this._wavesurfer.drawer.on('click', (event, pos) => {
      if (this.props.track_plaing_id != this.props.track.id) {
        this.props.actions.setTrackPlaying(this.props.track);
        this.props.actions.setCurrentVersion(common.TrackVersions.main_track);
      } else {
        const position = pos * this.props.track.duration;
        const positionInSec = common.timeFormatter(~~(position));
        this.props.actions.setPlayingPosition(Math.round(position), positionInSec);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.track_plaing_id == this.props.track.id && nextProps.selectedType == common.TrackVersions.main_track) {
      if(this.props.pos != nextProps.pos){
        let position = nextProps.pos / this.props.track.duration;
        this._wavesurfer.drawer.progress(position);
      }
    } else {
      this._wavesurfer.drawer.progress(0);
    }
  }

  componentWillUnmount() {
    this._wavesurfer.un('ready');
    this._wavesurfer.un('seek');
    this._wavesurfer.destroy();
  }

  render() {
    return (
      <div className="track-progress">
        <div className="track-progress-wrapper">
          <div style={{ width: this.width, height: this.height }}>
            <div
              id={'player_' + this.uniqID}
              className='track-progress-wrapper__wave'
              style={{ width: this.width, height: this.height }}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  play_status: state.audioPlayer.play_status,
  pos: state.audioPlayer.pos,
  track_plaing_id: state.audioPlayer.trackId,
  selectedType: state.audioPlayer.selectedType
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackPath,
    setPlayStatus,
    setPlayingPosition,
    setTrackPlaying,
    setCurrentVersion
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(Wave);
