import React from 'react';
import { connect } from 'react-redux';
import { IndexLink, Link } from 'react-router';
import 'styles/variables.scss';
import { Grid, Row, Col } from 'react-bootstrap';
import classNames from 'classnames';


class Footer extends React.Component {
  render() {
    return (
      <div className={classNames('footer', { 'footer_with-margin': this.props.track_path })}>
        <Grid>
          <Row>
            <Col md={2} sm={6}>
              <Link to="/" className="footer__logo"><svg xmlns="http://www.w3.org/2000/svg" width="157" height="18" viewBox="0 0 157 18"><g fill="#3e3e3e" fillRule="evenodd"><path d="M145.128 7.46l4.442-4.429h-1.207V.383h7.183V3.03h-1.875l-4.91 4.746 5.73 6.575h2.086V17h-7.172v-2.648h1.149l-3.95-4.512-1.476 1.453v3.059h2.063V17h-7.172v-2.648h2.062V3.032h-2.062V.381h7.172v2.65h-2.063zM129.848 3.031h-2.145V.383h7.172V3.03h-2.074v11.32h1.547V17h-7.16v-2.648h.972l-5.707-8.555v8.555h2.156V17h-7.171v-2.648h2.062V3.032h-2.063V.381h4.934l7.477 11.169zM113.348 14.352V17h-5.285v-2.648h.692l-.809-2.098h-5.355l-.75 2.098h.726V17h-5.285v-2.648h1.36l4.113-11.32h-.668V.381h4.558l5.473 13.97h1.23zm-6.363-4.758l-1.7-5.344-1.769 5.344h3.47zM87.322 3.184h-2.508v1.898h-2.648v-4.7h13.37v4.7h-2.648V3.184h-2.531v11.168h2.074V17h-7.16v-2.648h2.05zM68.186 3.184v3.832h6.844v2.8h-6.844v4.371h6.176v-1.886h2.66V17H63.077v-2.648h2.062V3.032h-2.062V.381h13.945v4.7h-2.66V3.184zM52.905 3.031h-2.144V.383h7.172V3.03h-2.074v11.32h1.546V17h-7.16v-2.648h.973l-5.707-8.555v8.555h2.156V17h-7.172v-2.648h2.063V3.032h-2.063V.381h4.934l7.476 11.169zM23.047 3.043V9.91c0 1.5.219 2.598.656 3.293.54.828 1.469 1.242 2.79 1.242 1.437 0 2.437-.382 3-1.148.5-.695.75-1.824.75-3.387V3.043H28.18V.395h7.172v2.648h-2.075v6.879c0 4.898-2.199 7.348-6.597 7.348-2.336 0-4.059-.665-5.168-1.993C20.504 14.067 20 12.281 20 9.922V3.043h-2.062V.395h7.171v2.648h-2.062zM8.491 7.6V3.184h2.532v1.898h2.648v-4.7H.3v4.7h2.648V3.184h2.508V7.6h3.035zm0 3.8v3.15h2.075v2.65h-7.16v-2.65h2.05V11.4h3.035z" /></g></svg></Link>
            </Col>
            <Col md={2} mdOffset={2} sm={6} xs={12}>
              <div className="title title_secondary">Browse</div>
              <ul className="footer__menu">
                <li className="footer__menu-item"><Link to="/catalog?featured=true" className="title title_small">Featured Tunes</Link></li>
                <li className="footer__menu-item"><Link to="/catalog?sorting=1" className="title title_small">Top new files</Link></li>
                <li className="footer__menu-item"><Link to="/catalog?sorting=2" className="title title_small">Popular files</Link></li>
                <li className="footer__menu-item"><Link to="/catalog?sorting=3" className="title title_small">Recently added</Link></li>
              </ul>

            </Col>
            <Col md={2} sm={6} xs={12}>
              <div className="title title_secondary">About Service</div>
              <ul className="footer__menu">
                <li className="footer__menu-item"><Link to="#" className="title title_small">About</Link></li>
                <li className="footer__menu-item"><Link to="/faq" className="title title_small">FAQ</Link></li>
                <li className="footer__menu-item"><Link to="/become-an-artist" className="title title_small">Become an Artist</Link></li>
                <li className="footer__menu-item"><Link to="/license-terms" className="title title_small">License terms</Link></li>
                <li className="footer__menu-item"><Link to="#" className="title title_small">Contacts</Link></li>
              </ul>
            </Col>
            <Col md={2} mdOffset={1} sm={6} smOffset={0} xs={12}>
              <div className="title title_secondary">Follow Us</div>
              <div className="footer__social">
                <Link to="#" className="btn btn_social">
                  <i className="fa fa-twitter" aria-hidden="true" />
                </Link>
                <Link to="#" className="btn btn_social">
                  <i className="fa fa-facebook-official" aria-hidden="true" />
                </Link>
                <Link to="#" className="btn btn_social">
                  <i className="fa fa-instagram" aria-hidden="true" />
                </Link>
                <Link to="#" className="btn btn_social">
                  <i className="fa fa-youtube" aria-hidden="true" />
                </Link>
              </div>

            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <div className="copy">© 2017 Trademarks and brands are the property of their respective owners.</div>
            </Col>
          </Row>
        </Grid>

      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  track_path: state.audioPlayer.track_path,
});


export default connect(mapStateToProps)(Footer);
