import React, { PropTypes } from 'react';
import axios from 'axios';
import { IndexLink, Link } from 'react-router';
import 'styles/variables.scss';
import { Row, Col, DropdownButton, MenuItem } from 'react-bootstrap';
import classNames from 'classnames';

class DropdownMenuUp extends React.Component {
  static propTypes = {
    hoverTrack: PropTypes.bool,
    track: PropTypes.object,
  }

  AddTrackToFavorites = (event) => {
    api.fetchAddTrackToFavorites(this.props.currentTrack.id).then((res) => {});
  }

  RemoveTrackFromFavorites = (event) => {
    api.fetchRemoveTrackFromFavorites(this.props.currentTrack.id).then((res) => {});
  }

  render() {
    return (
      <div className={classNames('track__options', { hidden: !this.props.hoverTrack })}>
        <DropdownButton title="" dropup id="bg-nested-dropdown" className="dropdown-options dropdown-menu__icon dropdown-menu__icon-dark">
          <MenuItem eventKey="1" onClick={this.AddTrackToFavorites}>Add to Favs</MenuItem>
          <MenuItem eventKey="2">Download Preview</MenuItem>
          <MenuItem eventKey="1">License Track</MenuItem>
          <MenuItem eventKey="2">View Artist</MenuItem>
          <MenuItem eventKey="1">View Details</MenuItem>
        </DropdownButton>
      </div>
    );
  }
}

export default DropdownMenuUp;
