import React from 'react';
import PropTypes from 'prop-types';
import { confirmable } from 'react-confirm';
import {Modal} from 'react-bootstrap';

const ConfirmDialog = ({
  show,
  confirmation,
  options,
  proceed,
  dismiss,
  cancel,
}) => {
  
  const {
    title,
    btnText
  } = options;

  return (
    <Modal
      show={true}
      dialogClassName="modal-default"
    >
    <Modal.Header closeButton onHide={dismiss}>
      <Modal.Title id="contained-modal-title-lg" className="title_primary title_primary__inner">{title}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <div className="modal-note">
         {confirmation}
      </div>
    </Modal.Body>
    <Modal.Footer>
      <div className="modal-default__submit-wrap">
        <button onClick={proceed} className="btn-primary btn-primary__form-submit btn btn-default">
          <span className="btn-primary__text">{btnText}</span>
        </button>
      </div>
    </Modal.Footer>
    </Modal>
  )
}

export default confirmable(ConfirmDialog);