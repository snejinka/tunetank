import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import 'styles/variables.scss';
import DropdownMenu from '../DropdownMenu';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as common from 'common';
import AuthorPanel from '../AuthorPanel';
import { setTrackPlaying, setPlayStatus } from 'store/audioPlayer';
import { TrackSvg } from 'styles/svgs/svgs';
import _ from 'lodash';
import classnames from 'classnames';
import slug from 'slug';

class TrackItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hoverTrack: false,
      hoverImage: false,
      instantHoverTrack: false,
      trackStatus: 'stop',
      track_path: this.props.track.main_track,
      openAuthorPanel: false,
      timeOutId: null,
      left: 0,
    };

    this.onMouseEnterTrack = this.onMouseEnterTrack.bind(this);
    this.onMouseLeaveTrack = this.onMouseLeaveTrack.bind(this);
  }

  static propTypes = {
    track: PropTypes.object.isRequired,
    userInfo: PropTypes.object,
    hasTrackMenu: PropTypes.bool,
    viewAuthorTracks: PropTypes.bool,
    setTracks: PropTypes.func,
    dropDownStyle: PropTypes.string,
    hover: PropTypes.bool,
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.hover !== nextProps.hover) {
      this.setState({ hoverImage: nextProps.hover });
    }
  }

  onMouseEnterTrack() {
    this.setState({ hoverTrack: true, instantHoverTrack: true, hoverImage: true });
  }

  onMouseLeaveTrack() {
    this.setState({ instantHoverTrack: false, hoverImage: false });

    if (this.state.timeOutId) {
      clearTimeout(this.state.timeOutId);
      this.setState({ timeOutId: null });
    }
    this.setState({ hoverTrack: false, openAuthorPanel: false });
  }

  onFocusUserName(e) {
    const left = Math.floor(e.currentTarget.offsetWidth / 2 ) - 5;
    const timeoutId = setTimeout(() => {
      this.setState({
        hoverTrack: true,
        openAuthorPanel: true,
        timeOutId: null,
        left: left,
      });
    }, 750);
    this.setState({ timeOutId: timeoutId });
  }
  
  onFocuseLeave() {
    if (this.state.timeOutId) {
      clearTimeout(this.state.timeOutId)
      this.setState({
        timeOutId: null,
      })
    }
  }

  toggleAuthorPanel() {
    this.setState({
      openAuthorPanel: false,
    });
  }

  handleTrackButton() {
    if (this.props.track_plaing_id == this.props.track.id) {
      if (this.props.play_status == common.AudioPlayerStatuses.PLAY && this.props.track_plaing_id === this.props.track.id) {
        this.props.actions.setPlayStatus(common.AudioPlayerStatuses.PAUSE);
      } else {
        this.props.actions.setPlayStatus(common.AudioPlayerStatuses.PLAY);
      }
    } else {
      this.props.setTracks(this.props.track);
      this.props.actions.setTrackPlaying(this.props.track);
    }
  }

  renderUserName() {
    if (this.props.viewAuthorTracks === true) {
      return (
        <Link
          to={`/author/${slug(this.props.userInfo.name, { lower: true })}/${this.props.userInfo.id}`}
          className="track__author"
          onMouseEnter={(e) => { this.onFocusUserName(e); }}
          onMouseLeave={(e) => {this.onFocuseLeave(e);}}
        >{ this.props.userInfo.name }</Link>
      );
    }
    return (
      <Link
        to={`/author/${slug(this.props.track.user.name, { lower: true })}/${this.props.track.user.id}`}
        className="track__author"
        onMouseEnter={(e) => { this.onFocusUserName(e); }}
        onMouseLeave={(e) => {this.onFocuseLeave(e);}}
      >{ this.props.track.user.name }</Link>
    );
  }


  renderAuthorPanel() {
    if (this.props.viewAuthorTracks === true) {
      return (
        <AuthorPanel
          user={this.props.userInfo} openAuthorPanel={this.state.openAuthorPanel}
          toggleAuthorPanel={this.toggleAuthorPanel.bind(this)} hoverTrack={this.state.hoverTrack}
          left={this.state.left}
        />
      );
    }
    return (
      <AuthorPanel
        user={this.props.track.user} openAuthorPanel={this.state.openAuthorPanel}
        toggleAuthorPanel={this.toggleAuthorPanel.bind(this)}
        left={this.state.left}        
      />
    );
  }

  renderTrackButton() {
    let hoverImage;
    if (this.props.track_plaing_id === this.props.track.id && this.props.play_status === common.AudioPlayerStatuses.PLAY) {
      hoverImage = (<div
        className={classnames('track__img-hover', { hidden: !this.state.hoverImage })}
        dangerouslySetInnerHTML={{ __html: TrackSvg.pauseBtn }}
      />);
    } else {
      hoverImage = (<div
        className={classnames('track__img-hover', { hidden: !this.state.hoverImage })}
        dangerouslySetInnerHTML={{ __html: TrackSvg.playIcon }}
      />);
    }

    return (
      <button
        onClick={this.handleTrackButton.bind(this)}
        className="btn btn-play-track"
      >
        {hoverImage}
      </button>
    );
  }


  render() {
    return (
      <div className="track" onMouseEnter={this.onMouseEnterTrack} onMouseLeave={this.onMouseLeaveTrack}>
        <div
          className="track__img"
          style={this.props.track.logo !== null && { backgroundImage: `url(${this.props.track.logo})` }}
        >
          {this.renderTrackButton()}
        </div>
        <div className="track__info">
          <Link to={`/track/${slug(this.props.track.name, { lower: true })}/${this.props.track.id}`} className="track__name">{ this.props.track.name }</Link>
          {this.renderUserName()}
          {this.props.hasTrackMenu && <DropdownMenu dropDownStyle={this.props.dropDownStyle} hoverTrack={this.state.instantHoverTrack} track={this.props.track} />}
          {this.renderAuthorPanel()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  track_path: state.audioPlayer.track_path,
  play_status: state.audioPlayer.play_status,
  track_plaing_id: state.audioPlayer.trackId,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackPlaying,
    setPlayStatus,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(TrackItem);
