import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import * as api from 'api';
import { openModal } from 'store/user.js';

class Like extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_favorite: props.track.is_favorite,
      hovered: false,
    };
    this.addTrackToFavorites = this.addTrackToFavorites.bind(this);
    this.removeTrackFromFavorites = this.removeTrackFromFavorites.bind(this);
  }

  static propTypes = {
    track: PropTypes.object,
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.track.is_favorite !== nextProps.track.is_favorite) {
      this.setState({
        is_favorite: nextProps.track.is_favorite,
      });
    }
  }

  addTrackToFavorites(event) {
    if (this.props.loggedIn) {
      this.setState({ is_favorite: true });
      api.fetchAddTrackToFavorites(this.props.track.id).then((res) => {
        if (res.id != this.props.track.id) {
          this.setState({ is_favorite: false });
        }
      });
    } else {
      this.props.actions.openModal('sign_up');
    }
  }

  removeTrackFromFavorites(event) {
    if (this.props.loggedIn) {
      this.setState({ is_favorite: false });
      api.fetchRemoveTrackFromFavorites(this.props.track.id).then((res) => {
        if (res.id != this.props.track.id) {
          this.setState({ is_favorite: true });
        }
      });
    } else {
      this.props.actions.openModal('sign_up');
    }
  }

  onMouseEnter() {
    if (!this.state.is_favorite) {
      this.setState({ hovered: true })      
    }
  }

  render() {
    if (this.state.is_favorite) {
      var clickEvent = this.removeTrackFromFavorites;
      var color = '#BC200C';
    } else {
      var clickEvent = this.addTrackToFavorites;
      var color = '#BC200C';
    }

    return (<button
      className="btn btn-like"
      onMouseEnter={() => this.onMouseEnter()}
      onMouseLeave={() => this.setState({ hovered: false })}
      onClick={clickEvent}
    >
      {this.state.hovered || this.state.is_favorite ?
                (
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 22 20" enableBackground="new 0 0 22 20" fill="#BC200C">
                    <path xmlns="http://www.w3.org/2000/svg" id="a" d="M13.182.764c.82-.48 1.632-.734 2.432-.76.8-.027 1.591.11 2.372.41.78.3 1.465.727 2.052 1.28a6.377 6.377 0 0 1 1.42 2.013c.362.787.542 1.608.542 2.462 0 .76-.224 1.544-.671 2.352-.447.807-1.074 1.638-1.882 2.492-.807.854-7.773 8.352-7.773 8.352a.921.921 0 0 1-1.366 0s-6.948-7.498-7.755-8.352C1.746 10.159 1.118 9.328.67 8.52.223 7.713 0 6.929 0 6.169c0-.854.187-1.675.56-2.462a6.493 6.493 0 0 1 1.462-2.012A6.38 6.38 0 0 1 4.114.414a6.103 6.103 0 0 1 2.382-.41C7.29.03 8.09.286 8.898.773c.807.488 1.504 1.184 2.092 2.092.64-.921 1.372-1.622 2.192-2.102z" />
                  </svg>
                ) :
                (
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 22 20" enableBackground="new 0 0 22 20" fill="#BC200C">
                    <path id="path-1_2_" className="Graphic_x0020_Style_x0020_2" d="M15.81,2c0.49,0,0.96,0.09,1.45,0.28c0.54,0.21,1,0.5,1.4,0.88c0.4,0.39,0.73,0.85,0.98,1.42C19.88,5.13,20,5.69,20,6.27c0,0.43-0.14,0.91-0.43,1.44c-0.37,0.67-0.9,1.39-1.59,2.13c-0.65,0.7-5.13,5.6-6.99,7.64c-1.85-2.03-6.33-6.94-6.97-7.64C3.33,9.09,2.79,8.38,2.43,7.71C2.14,7.19,2,6.71,2,6.27c0-0.58,0.12-1.12,0.37-1.66c0.27-0.57,0.6-1.04,1.02-1.43C3.8,2.79,4.28,2.5,4.83,2.29C5.33,2.09,5.82,2,6.3,2l0.13,0c0.44,0.02,0.92,0.18,1.42,0.49C8.4,2.83,8.88,3.32,9.3,3.99l1.63,2.56l1.71-2.5c0.47-0.69,1-1.21,1.56-1.55c0.52-0.31,1.02-0.48,1.48-0.49L15.81,2 M15.81,0c-0.07,0-0.13,0-0.2,0c-0.8,0.03-1.61,0.28-2.43,0.77c-0.82,0.49-1.55,1.2-2.19,2.14C10.4,1.99,9.71,1.28,8.9,0.79C8.09,0.29,7.29,0.03,6.5,0C6.43,0,6.37,0,6.3,0C5.57,0,4.84,0.14,4.11,0.42c-0.79,0.31-1.49,0.74-2.09,1.3c-0.6,0.56-1.09,1.25-1.46,2.05C0.19,4.57,0,5.4,0,6.27c0,0.77,0.22,1.57,0.67,2.39c0.45,0.82,1.07,1.67,1.88,2.53c0.81,0.87,7.76,8.49,7.76,8.49c0.19,0.2,0.43,0.31,0.68,0.31c0.25,0,0.49-0.1,0.68-0.31c0,0,6.97-7.62,7.77-8.49c0.81-0.87,1.43-1.71,1.88-2.53C21.78,7.84,22,7.05,22,6.27c0-0.87-0.18-1.7-0.54-2.5c-0.36-0.8-0.83-1.48-1.42-2.05c-0.59-0.56-1.27-1-2.05-1.3C17.27,0.14,16.54,0,15.81,0L15.81,0z" />
                  </svg>
                )}
    </button>);
  }
}

const mapStateToProps = (state, props) => ({
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    openModal,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(Like);
