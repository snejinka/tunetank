import React, { PropTypes } from 'react';
import AuthService from 'utils/AuthService';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock, Modal, Button, Popover, Tooltip, ButtonToolbar } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import ClassNames from 'classnames';
import * as common from 'common';
import { Link } from 'react-router';

class ModalSignUp extends React.Component {
  constructor() {
    super();
    this.state = {
      submitted: false,
      email: '',
      password: '',
      name: '',
      authError: false,
      errorMessage: '',
    };
    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCurrentPage = this.handleCurrentPage.bind(this);
    this.handleProfile = this.handleProfile.bind(this);
    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }


  static propTypes = {
    show: PropTypes.bool,
    auth: PropTypes.instanceOf(AuthService),
    getCurrentUser: PropTypes.func,
    open: PropTypes.func,
    close: PropTypes.func,
  }

  handleChangeInput(e) {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({ [name]: value });
  }

  showModal = () => {
    this.props.open('sign_up');
  }

  hideModal = () => {
    this.props.close('sign_up');
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true, authError: false });
    const isValidEmail = common.validateEmail(this.state.email);
    const isValidPass = common.validatePass(this.state.password);
    const isValidName = common.validateName(this.state.name);
    if (isValidEmail && isValidPass && isValidName) {
      this.setState({ spinner: true });
      this.props.auth._doRegistration(this.state.email, this.state.password, this.state.name).then(() => {
        this.props.getCurrentUser().then((user) => {
          const location = browserHistory.getCurrentLocation();
          sessionStorage.setItem('welcomeTunetankPath', location.pathname);
          sessionStorage.setItem('welcomeTunetankAuthor', user.id);
          sessionStorage.setItem('welcomeTunetankAuthorName', user.name);
          this.setState({ spinner: false });
          this.props.close('sign_up');
          browserHistory.replace({ pathname: sessionStorage.welcomeTunetankPath });
        });
      }).catch((error) => {
        if (error.statusCode === 400 && error.code === 'user_exists') {
          this.setState({ authError: true, errorMessage: error.description });
        }
        this.setState({ spinner: false });
      });
    }
  }

  handleCurrentPage() {
    browserHistory.replace({ pathname: '/' });
  }

  handleProfile() {
    browserHistory.replace({ pathname: '/' });
  }

  handleSignIn() {
    this.props.close('sign_up');
    this.props.open('sign_in');
  }

  handleKeyPress(e) {
    if (e.charCode === 13 && this.state.terms) {
      this.handleSubmit();
    }
  }

  render() {
    const props = this.props;
    const backdropStyle = {
      zIndex: '100',
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
    };
    const isValidEmail = common.validateEmail(this.state.email);
    const isValidPass = common.validatePass(this.state.password);
    const isValidName = common.validateName(this.state.name);
    const authError = this.state.authError;

    const modalProps = { ...props };
    delete modalProps.auth;
    delete modalProps.close;
    delete modalProps.getCurrentUser;

    return (
      <ButtonToolbar>
        <Link className="header__nav-item" onClick={this.showModal}>
          <button className="btn__sign-up">
              Sign Up
            </button>
        </Link>
        <Modal
          {...modalProps}
          backdropStyle={backdropStyle}
          show={this.props.show}
          onHide={this.hideModal}
          onKeyPress={this.handleKeyPress}
          dialogClassName="modal-default"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg" className="title_primary title_primary__inner">Sign Up</Modal.Title>
          </Modal.Header>
            <form className="form-default">
            <Modal.Body>  
              <FormGroup className="form-group__indent-bot-medium">
                <ControlLabel>
                    E-mail *
                  </ControlLabel>
                <FormControl type="email" name="email" value={this.state.email} onChange={this.handleChangeInput} />
                {this.state.submitted && this.state.email === '' &&
                <HelpBlock className="not-valid">
                      E-mail can't be blank
                    </HelpBlock>
                  }
                {this.state.submitted && !isValidEmail && this.state.email !== '' &&
                <HelpBlock className="not-valid">
                      Invalid format
                    </HelpBlock>
                  }
                {this.state.submitted && authError &&
                <HelpBlock className="not-valid">
                  {this.state.errorMessage}
                </HelpBlock>
                  }
              </FormGroup>
              <FormGroup className="form-group__indent-bot-medium">
                <ControlLabel>
                    Name *
                  </ControlLabel>
                <FormControl type="text" maxLength="160" name="name" value={this.state.name} onChange={this.handleChangeInput} />
                {this.state.submitted && this.state.name === '' &&
                  <HelpBlock className="not-valid">
                    Name can't be blank
                  </HelpBlock>
                  }
                {this.state.submitted && !isValidName && this.state.name !== '' &&
                  <HelpBlock className="not-valid">
                    Invalid format
                  </HelpBlock>
                  }
                {this.state.submitted && !isValidName &&
                  <HelpBlock className="not-valid">
                    The name must be at least 3 characters long.
                  </HelpBlock>
                  }
              </FormGroup>
              <FormGroup>
                <ControlLabel>
                    Password *
                  </ControlLabel>
                <FormControl type="password" name="password" value={this.state.password} onChange={this.handleChangeInput} />
                {this.state.submitted && this.state.password === '' &&
                <HelpBlock className="not-valid">
                      Password can't be blank
                    </HelpBlock>
                  }
                {this.state.submitted && !isValidPass && this.state.password !== '' &&
                <HelpBlock className="not-valid">
                      The password does not match with the minimum password length.
                    </HelpBlock>
                  }
                <HelpBlock className="flexbox align-center">
                  <div className="title title-note">
                      By creating an account you agree to our <a href="" className="link-conditionals">Terms & Conditions</a>
                  </div>
                </HelpBlock>
              </FormGroup>
              </Modal.Body>
              <Modal.Footer>
                <div className="modal-default__submit-wrap">
                  <Button
                    onClick={this.handleSubmit}
                    disabled={!this.state.email || !this.state.password || !this.state.name}
                    className={ClassNames('btn-primary btn-primary__form-submit', { 'btn-primary__form-submit_spinner': this.state.spinner })}
                    type="submit"
                  >
                    <span className="btn-primary__text">Submit</span>
                    {this.state.spinner && <i className="fa fa-spinner fa-spin" style={{ padding: 3 }} />}
                  </Button>
                </div>
                <div className="flexbox justify-center">
                  <div className="title title-note">Already got an account?</div>&nbsp;<a className="action-link" style={{ cursor: 'pointer' }} onClick={this.handleSignIn}> Sign In</a>!
                </div>
              </Modal.Footer> 
            </form>
        </Modal>
      </ButtonToolbar>
    );
  }
}
export default ModalSignUp;
