import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as common from 'common';
import WaveSurfer from 'utils/wavesurfer.js';
import { setTrackPath, setPlayStatus, setPlayingPosition } from 'store/audioPlayer';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import classNames from 'classnames';
import Volume from 'assets/img/volume.png';
import Mute from 'assets/img/mute.png';
import VolumeCaret from 'assets/img/volume-caret.png';
import { TrackSvg, wave } from 'styles/svgs/svgs.js';
import { Fade } from 'react-bootstrap';

class Wave extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      durationInSec: common.zeroTime,
      durationInPoint: 0,
      showWave: false,
      showVolume: false,
      volume: 100,
      muteVolume: 100,
    };

    this.handleMouseLeave = this.handleMouseLeave.bind(this);
    this.handleChangeVolume = this.handleChangeVolume.bind(this);
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.showVolume = this.showVolume.bind(this);
    this.hideVolume = this.hideVolume.bind(this);
    this.toggleMute = this.toggleMute.bind(this);
    this.height = 52;

    if (typeof WaveSurfer === undefined) {
      throw new Error('WaveSurfer is undefined!');
    }

    this.waveOptions = {
      container: '#player',
      backend: 'MediaElement',
      mediaType: 'audio',
      waveColor: '#656363',
      progressColor: '#ffcc00',
      height: this.height,
      normalize: true,
      barWidth: 2,
      forceDecode: false
    };
  }

  static propTypes = {
    track: PropTypes.object,
    selectedVersion: PropTypes.string,
  }

  componentDidMount() {
    this._wavesurfer = WaveSurfer.create(this.waveOptions);
    this.wave = JSON.parse(this.props.track.wave);
    this._wavesurfer.backend.peaks = this.wave.data;
    this._wavesurfer.drawBuffer();
    this._wavesurfer.load(this.props.track.versions[this.props.selectedVersion].url, this._wavesurfer.backend.peaks);

    this._wavesurfer.on('ready', () => {
      if(this.props.play_status == common.AudioPlayerStatuses.PLAY) {
        this._wavesurfer.play();
      }
      this.getTrackDuration().then((duration) => {
        this.setState({ durationInSec: common.timeFormatter(~~(duration)),
          durationInPoint: Math.round(duration) });
      });
    });

    this._wavesurfer.on('seek', (pos) => {
      const position = pos * this._wavesurfer.getDuration();
      const positionInSec = common.timeFormatter(~~(position));
      if (positionInSec !== this.props.posInSec) {
        this.props.actions.setPlayingPosition(Math.round(position), positionInSec);
      }
    });

    this._wavesurfer.on('audioprocess', (pos) => {
      const positionInSec = common.timeFormatter(pos);
      if (positionInSec !== this.props.posInSec) {
        this.props.actions.setPlayingPosition(Math.round(pos), positionInSec);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.play_status !== nextProps.play_status) {
      if (nextProps.play_status === common.AudioPlayerStatuses.PAUSE) {
        this._wavesurfer.pause();
      } else if (nextProps.play_status === common.AudioPlayerStatuses.PLAY) {
        this._wavesurfer.play();
      } else if (nextProps.play_status === common.AudioPlayerStatuses.STOP) {
        this._wavesurfer.stop();
      }
    }

    if (this.props.selectedVersion !== nextProps.selectedVersion) {
      this.reload(nextProps.track, nextProps.selectedVersion);
    }

    if (this.props.track.id !== nextProps.track.id) {
      this.reload(nextProps.track, common.TrackVersions.main_track);
    }

    if(this.props.pos != nextProps.pos && Math.abs(nextProps.pos - this.props.pos) > 1){
      let position = nextProps.pos / nextProps.track.duration;
      this._wavesurfer.backend.seekTo(position * this.props.track.duration);
      this._wavesurfer.drawer.progress(position);
    }
  }

  reload(track, version) {
    this._wavesurfer.destroy();
    this._wavesurfer = WaveSurfer.create(this.waveOptions);
    this.wave = JSON.parse(track.wave);
    this._wavesurfer.load(track.versions[version].url, this.wave.data);
    this._wavesurfer.on('ready', () => {
      this.handleChangeVolume(this.state.volume);

      if(this.props.play_status == common.AudioPlayerStatuses.PLAY) {
        this._wavesurfer.play();
      }

      this.getTrackDuration().then((duration) => {
        this.setState({ durationInSec: common.timeFormatter(~~(duration)),
          durationInPoint: Math.round(duration) });
      });
    });

    this._wavesurfer.on('seek', (pos) => {
      const position = pos * this._wavesurfer.getDuration();
      const positionInSec = common.timeFormatter(~~(position));
      if (positionInSec !== this.props.posInSec) {
        this.props.actions.setPlayingPosition(Math.round(position), positionInSec);
      }
    });

    this._wavesurfer.on('audioprocess', (pos) => {
      const positionInSec = common.timeFormatter(pos);
      if (positionInSec !== this.props.posInSec) {
        this.props.actions.setPlayingPosition(Math.round(pos), positionInSec);
      }
    });
  }

  getTrackDuration() {
    return new Promise((resolve, reject) => {
      const duration = this._wavesurfer.backend.getDuration();
      resolve(duration);
    });
  }

  handleChangeVolume(value) {
    let float = 100;
    if (value == 0) {
      float = 0;
    } else {
      float = value / 100;
    }
    this.setState({ volume: value, muteVolume: value });
    this._wavesurfer.setVolume(float);
  }

  componentWillUnmount() {
    this._wavesurfer.un('ready');
    this._wavesurfer.un('seek');
    this._wavesurfer.un('audioprocess');
    this._wavesurfer.destroy();
  }

  handleMouseLeave() {
    this.setState({ showWave: false });
  }

  handleMouseEnter() {
    this.setState({ showWave: true });
  }

  showVolume() {
    this.setState({ showVolume: true });
  }

  hideVolume() {
    this.setState({ showVolume: false });
  }

  toggleMute() {
    if (!this._wavesurfer.isMuted) {
      const currentVolume = this.state.volume;
      this.setState({ muteVolume: currentVolume, volume: 0 });
      this._wavesurfer.toggleMute();
    } else {
      const beforeVolume = this.state.muteVolume;
      this.setState({ volume: beforeVolume });
      this._wavesurfer.toggleMute();
    }
  }

  renderVolume() {
    return (
      <div className={classNames('track-volume', { 'track-volume__active': this.state.showVolume })} onMouseEnter={this.showVolume} onMouseLeave={this.hideVolume}>
        {this.state.volume == 0 &&
        <button
          className="btn btn-volume btn-volume__mute"
          dangerouslySetInnerHTML={{ __html: TrackSvg.volumeMuteIcon }}
          onClick={this.toggleMute}
        />
          }
        {this.state.volume == 100 &&
        <button
          className="btn btn-volume btn-volume__max"
          dangerouslySetInnerHTML={{ __html: TrackSvg.volumeMaxIcon }}
          onClick={this.toggleMute}
        />
          }
        {this.state.volume > 0 && this.state.volume < 100 &&
        <button
          className="btn btn-volume btn-volume__normal"
          dangerouslySetInnerHTML={{ __html: TrackSvg.volumeNormalIcon }}
          onClick={this.toggleMute}
        />
          }
        <Fade in={this.state.showVolume} duration={300}>
          <Slider
            style={{ position: 'absolute' }}
            vertical
            min={0}
            max={100}
            value={this.state.volume}
            onChange={this.handleChangeVolume}
            defaultValue={this.state.volume}
          />
        </Fade>
      </div>
    );
  }

  render() {
    return (
      <div className="track-progress">
        <div className="track-duration track-duration__current">
          {this.props.posInSec}
        </div>
        <div className="track-progress-wrapper">
          <div onMouseLeave={this.handleMouseLeave} onMouseEnter={this.handleMouseEnter} style={{ width: 494, height: this.height }}>
            <Slider
              className={classNames('track-progress-wrapper__slider', { hidden: this.state.showWave })}
              value={this.props.pos}
              min={0}
              max={this.state.durationInPoint}
              defaultValue={0}
            />
            <div
              id="player"
              className={classNames('track-progress-wrapper__player', { invisible: !this.state.showWave })}
              style={{ width: 494, height: this.height }}
            />
          </div>
        </div>
        <div className="track-duration track-duration__left">
          {this.state.durationInSec}
        </div>
        <div className="track-volume-panel">
          {this.renderVolume()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  track_plaing_id: state.audioPlayer.trackId,
  play_status: state.audioPlayer.play_status,
  pos: state.audioPlayer.pos,
  posInSec: state.audioPlayer.posInSec,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackPath,
    setPlayStatus,
    setPlayingPosition,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(Wave);