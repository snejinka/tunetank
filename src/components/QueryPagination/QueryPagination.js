import React, { PropTypes } from 'react';
import { Pagination } from 'react-bootstrap';
import { browserHistory } from 'react-router';

class QueryPagination extends React.Component {

  componentWillMount() {
    const location = browserHistory.getCurrentLocation();
    const query = location.query;
    if (query.page) {
      this.props.switchPage(Number(query.page));
    }
  }

  static propTypes = {
    changePage: PropTypes.func,
    allPages: PropTypes.number,
  }

  handleSelect(eventKey) {
    const location = browserHistory.getCurrentLocation();
    const query = location.query;
    query.page = eventKey;
    this.props.switchPage(Number(eventKey));
    browserHistory.push({ pathname: location.pathname, query });
  }

  render() {
    const props = this.props;
    const paginationProps = { ...props };
    delete paginationProps.changePage;
    delete paginationProps.allPages;
    delete paginationProps.query;
    delete paginationProps.switchPage;
    return (
      <Pagination
        prev
        next
        ellipsis
        boundaryLinks
        items={this.props.allPages}
        maxButtons={4}
        activePage={this.props.activePage}
        onSelect={this.handleSelect.bind(this)}
        className="pagination-defaul"
        {...paginationProps}
      />
    );
  }
}


export default QueryPagination;
