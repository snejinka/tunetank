import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import { IndexLink, Link, browserHistory } from 'react-router';
import 'styles/variables.scss';
import { Row, Col, DropdownButton, MenuItem } from 'react-bootstrap';
import classNames from 'classnames';
import * as api from 'api';
import { openModal, showBuyTrack } from 'store/user.js';
import slug from 'slug';

class DropdownMenu extends React.Component {
  static propTypes = {
    hoverTrack: PropTypes.bool,
    track: PropTypes.object,
    sign_up: PropTypes.bool,
    openModal: PropTypes.func,
    dropDownStyle: PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      liked: props.track.is_favorite,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.track.is_favorite !== nextProps.track.is_favorite) {
      this.setState({
        liked: nextProps.track.is_favorite,
      });
    }
  }

  AddTrackToFavorites = (e) => {
    e.stopPropagation();
    if (this.props.loggedIn) {
      this.setState({
        liked: true,
        isOpen: true,
      });
      api.fetchAddTrackToFavorites(this.props.track.id).catch(() => {
        this.setState({
          liked: false,
        });
      });
    } else {
      this.props.actions.openModal('sign_up');
    }
  }

  RemoveTrackFromFavorites = (e) => {
    e.stopPropagation();
    if (this.props.loggedIn) {
      this.setState({
        liked: false,
      });
      api.fetchRemoveTrackFromFavorites(this.props.track.id).catch(() => {
        this.setState({
          liked: true,
        });
      });
    } else {
      this.props.actions.openModal('sign_up');
    }
  }

  handleDownload = (e) => {
    e.stopPropagation();
    if (this.props.loggedIn) {
      const filePath = this.props.track.main_track;
      const link = document.createElement('a');
      link.href = filePath;
      link.download = filePath;
      link.click();
    } else {
      this.props.actions.openModal('sign_up');
    }
  }

  handleViewArtist = (e) => {
    browserHistory.push(`/author/${slug(this.props.track.user.name, { lower: true })}/${this.props.track.user.id}`);
  }

  handleViewDetails = (e) => {
    browserHistory.push(`/track/${slug(this.props.track.name, { lower: true })}/${this.props.track.id}`);
  }

  handleBlur(e) {
    if (!e.currentTarget.contains(document.activeElement)) {
      this.setState({ isOpen: false });
    }
  }

  render() {
    return (
      <div className={classNames('track__options', { hidden: !this.props.hoverTrack })}>
        <DropdownButton
          onToggle={(isOpen) => { this.setState({ isOpen }); }}
          open={this.state.isOpen}
          title=""
          id="bg-nested-dropdown"
          onBlur={(e) => { this.handleBlur(e); }}
          className={`dropdown-options dropdown-menu__icon dropdown-menu__icon-${this.props.dropDownStyle}`}
        >
          {(!this.state.liked ?
            <MenuItem eventKey="1" onMouseDown={this.AddTrackToFavorites}>Add to Favs</MenuItem>
            :
            <MenuItem eventKey="1" onMouseDown={this.RemoveTrackFromFavorites}>Delete from Favs</MenuItem>) }
          <MenuItem eventKey="2" onMouseDown={this.handleDownload}>Download Preview</MenuItem>
          <MenuItem eventKey="1" onMouseDown={() => (this.props.actions.showBuyTrack(this.props.track))}>License Track</MenuItem>
          <MenuItem eventKey="2" onMouseDown={this.handleViewArtist}>View Artist</MenuItem>
          <MenuItem eventKey="1" onMouseDown={this.handleViewDetails}>View Details</MenuItem>
        </DropdownButton>
      </div>
    );
  }
}


const mapStateToProps = (state, props) => ({
  favorite_tracks: state.user.favorite_tracks,
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    openModal,
    showBuyTrack
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(DropdownMenu);
