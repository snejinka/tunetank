import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Modal, ControlLabel, FormControl, Fade } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { hideBuyTrack } from 'store/user.js';
import { licenses, addItemToCart } from 'store/cart';
import accounting from 'accounting';

class BuyTrack extends Component {
  constructor(props) {
    super(props);
    this.state = {
      licenseId: licenses[0].id,
    };
  }

  getLicenseRadio() {
    return licenses.map(license => (
      <div key={`license-${license.id}-${license.name}`}>
        <div className="radio-box">
          <FormControl
            type="radio"
            id={`license-${license.price_in_cents}`}
            name="licenseRadioGroup"
            checked={this.state.licenseId === license.id}
            onChange={() => this.toggleRadio(license.id)}
          />
          <ControlLabel htmlFor={`license-${license.price_in_cents}`} className="radio-box__pseudo-radio" />
          <ControlLabel htmlFor={`license-${license.price_in_cents}`} className="radio-box__note">
            <span className="buy-track__license-price">{accounting.formatMoney(license.price)}</span> - {license.name}
          </ControlLabel>
        </div>
        { this.state.licenseId === license.id ?
          ( 
            <Fade in={true} timeout={500}>
              <div className="buy-track__license-description">
                {license.description}
              </div>
            </Fade>
          ) : null
        }
      </div>
    ));
  }

  toggleRadio(value) {
    this.setState({ licenseId: value });
  }

  addItemToCart(track) {
    this.props.actions.addItemToCart({ ...track, quantity: 1, license: licenses.find(license => license.id === this.state.licenseId) });
    this.props.actions.hideBuyTrack();
    this.setState({ licenseId: licenses[0].id });
    browserHistory.push({ pathname: '/cart' });
  }

  render() {
    const backdropStyle = {
      zIndex: '100',
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
    };

    const { show, track, actions } = this.props;
    const logo = track.logo_medium || track.logo;
    return (
      <Modal
        backdropStyle={backdropStyle}
        show={show}
        onHide={actions.hideBuyTrack}
        dialogClassName="modal-default modal-message"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg" className="title_primary title_primary__inner">
            License Guide!
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="buy-track__selected">
            <div className="buy-track__selected-image" style={{ backgroundImage: `url(${logo})` }}></div>
            <div className="buy-track__selected-description">
              <div className="buy-track__selected-track">{track.name}</div>
              <div className="buy-track__selected-artist">{track.user.name}</div>
              <div className="buy-track__selected-extension">(includes .MP3 and .WAV files)</div>
            </div>
          </div>
          <div className="buy-track__license">
            {this.getLicenseRadio()}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <div className="track-info-actions__item track-info-actions__item-buy">
            <button
              onClick={() => this.addItemToCart(track)}
              className="btn btn-primary"
            >
              Buy License
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

BuyTrack.propTypes = {
  show: PropTypes.bool.isRequired,
  track: PropTypes.shape().isRequired,
};

const mapStateToProps = state => ({
  show: state.user.buyTrackShow,
  track: state.user.buyTrack,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    addItemToCart,
    hideBuyTrack,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(BuyTrack);
