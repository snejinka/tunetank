import React, { PropTypes } from 'react';
import { IndexLink, Link } from 'react-router';
import 'styles/variables.scss';
import { Grid, Row, Col } from 'react-bootstrap';
import classNames from 'classnames';
import spinnerUrl from '../../assets/img/spinner.gif';

class Spinner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  static propTypes = {
  }
  render() {
    return (
      <div className="sk-wave">
        <div className="sk-rect sk-rect1" />
        <div className="sk-rect sk-rect2" />
        <div className="sk-rect sk-rect3" />
        <div className="sk-rect sk-rect4" />
        <div className="sk-rect sk-rect5" />
      </div>
    );
  }
}

export default Spinner;
