import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import 'styles/variables.scss';
import classNames from 'classnames';
import AuthService from 'utils/AuthService';
import { getCurrentUser, openModal, hideModal, setTab, setLoggedIn } from 'store/user.js';
import { tabs } from 'common';
import ModalAccountCreated from 'components/ModalAccountCreated';
import { IndexLink, Link, browserHistory } from 'react-router';
import { TrackSvg } from 'styles/svgs/svgs.js';
import Transition from 'react-transition-group/Transition';
import slug from 'slug';
import DropdownMenuHeader from '../DropdownMenuHeader';
import ModalWelcome from '../ModalWelcome';
import ModalSignUp from '../ModalSignUp';
import accounting from 'accounting';
import _ from 'lodash';

const duration = 300;
const defaultStyleLogo = {
  transition: `all ${duration}ms linear`,
  top: '-150px',
};

const transitionLogoStyles = {
  entering: { opacity: 1, top: 0 },
  entered: { opacity: 1, top: 0 },
};

const defaultStyleSimpleLogo = {
  transition: `all ${duration}ms linear`,
  top: '150px',
};

const transitionSimpleLogoStyles = {
  entering: { opacity: 1, top: 0 },
  entered: { opacity: 1, top: 0 },
};

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openBurger: false,
      simpleLogo: false,
      openDropDown: false,
    };
    this.handleScroll = this.handleScroll.bind(this);
    this.closeDropDown = this.closeDropDown.bind(this);
    this.handleClickBalance = this.handleClickBalance.bind(this);
  }

  componentWillMount() {
    this.props.actions.getCurrentUser();
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  animateBurger() {
    this.setState({
      openBurger: !this.state.openBurger,
    });
  }

  openDropDown() {
    if (this.state.openDropDown !== true) {
      _.delay(()=>{
        this.setState({
          openDropDown: true,
        });
      }, 100)
     
    }
  }

  closeDropDown() {
    this.setState({
      openDropDown: false,
    });
  }

  handleScroll() {
    if (window.scrollY > 150) {
      if (this.state.simpleLogo !== true) {
        this.setState({ simpleLogo: true });
      }
    } else if (this.state.simpleLogo !== false) {
      this.setState({ simpleLogo: false });
    }
  }

  handleClickBalance() {
    this.props.actions.setTab(tabs.earnings);
    browserHistory.push(`/author/${slug(this.props.currentUser.name, { lower: true })}/${this.props.currentUser.id}`);
  }

  renderUsername() {
    if (this.props.currentUser) {
      const profile = this.props.auth.getProfile();
      let userName = this.props.currentUser.name;
      if (!userName || userName === '') {
        userName = profile.nickname !== undefined ? profile.nickname : profile.name;
      }
      return (
        <div
          className="header__nav-item header__user-name-wrap header__nav-item__logged"
          onMouseEnter={this.openDropDown.bind(this)}

        >
          <Link
            to={`/author/${slug(userName, { lower: true })}/${this.props.currentUser.id}`}
            className="header__nav-item__user-name"
          >
            {userName}
          </Link>

          <DropdownMenuHeader
            logout={this.props.logout}
            openDropDown={this.state.openDropDown}
            close={this.closeDropDown.bind(this)}
            open={this.openDropDown.bind(this)}
          />

        </div>
      );
    }
    return false;
  }

  render() {
    let { currentUser } = this.props;

    return (
      <div className="header header_fixed">
        <div className="container">
          <div className="row flexbox align-center">
            <div className="col-md-4">
              <div className="header__nav">
                <Link to="/catalog" className={classNames('header__nav-item', { 'header__nav-item_active': this.state.openBurger === true })}>Browse</Link>
                <Link to="#" className={classNames('header__nav-item', { 'header__nav-item_active': this.state.openBurger === true })}>About</Link>
                <Link to="https://blog.tunetank.com/" className={classNames('header__nav-item')}>Blog</Link>
                <Link to="/become-a-seller" className={classNames('header__nav-item header__nav-item--highlited', { 'header__nav-item_active': this.state.openBurger === true })}>Become a seller</Link>
              </div>
            </div>
            <div className="col-md-4">
              <Link to="/" className="logo header__logo">
                <Transition in={!this.state.simpleLogo} timeout={duration}>
                  {state => (
                    <div
                      dangerouslySetInnerHTML={{ __html: TrackSvg.logo }} style={{
                        ...defaultStyleLogo,
                        ...transitionLogoStyles[state],
                      }}
                    />
                  )}
                </Transition>
                <Transition in={this.state.simpleLogo} timeout={duration}>
                  {state => (
                    <div
                      dangerouslySetInnerHTML={{ __html: TrackSvg.logoLetter }} style={{
                        ...defaultStyleSimpleLogo,
                        ...transitionSimpleLogoStyles[state],
                      }}
                    />

                  )}
                </Transition>
              </Link>
            </div>
            <div className="col-md-4">
              {this.props.loggedIn &&
              <div className="header__nav header__nav_user header__nav_user-logged"  onMouseLeave={this.closeDropDown}>
                {(currentUser.role === 'seller' || currentUser.role === 'admin') && (
                <a onClick={this.handleClickBalance} className="header__nav-item header__nav-item__current-invoice header__nav-item__logged">
                  {accounting.formatMoney(currentUser.balance_in_cents / 100)}
                </a>)}
                {this.renderUsername()}
                <div className={`cart`} onClick={() => browserHistory.push({ pathname: '/cart' })}>
                  <div
                    className="header__nav-btn cart-btn"
                    dangerouslySetInnerHTML={{ __html: '<?xml version="1.0" encoding="UTF-8"?><svg width="24px" height="28px" viewBox="0 0 24 29" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs></defs><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="card-border" transform="translate(-103.000000, -294.000000)" stroke="#FFFFFF" stroke-width="2"><g id="backet" transform="translate(103.000000, 295.000000)"><path d="M8,10.975 L8,4.893 C8.00165512,2.68470879 9.79170844,0.895102974 12,0.894 C14.21,0.894 16,2.68 16,4.899 L16,10.882" id="Shape" stroke-linecap="round"></path><path d="M2.9131755,7 L1.26693276,25.1000996 C1.17066472,26.1642168 1.93045012,27 2.992,27 L21.008,27 C22.0698598,27 22.8289093,26.1660088 22.7321059,25.1005263 L21.0867767,7 L2.9131755,7 Z" id="a"></path></g></g></g></svg>' }}
                  />
                  <div className="cart__count">{this.props.cartItems.length}</div>
                </div>
              </div>
              }
              {!this.props.loggedIn &&
                <div className="header__nav header__nav_user">
                  <ModalSignUp
                    show={this.props.sign_up}
                    auth={this.props.auth}
                    open={type => this.props.actions.openModal(type)}
                    close={type => this.props.actions.hideModal(type)}
                    getCurrentUser={this.props.actions.getCurrentUser}
                  />
                  <ModalWelcome
                    show={this.props.sign_in}
                    auth={this.props.auth}
                    open={type => this.props.actions.openModal(type)}
                    close={type => this.props.actions.hideModal(type)}
                    getCurrentUser={this.props.actions.getCurrentUser}
                    setLoggedIn={this.props.actions.setLoggedIn}
                  />
                  <div className="cart disabled-cart">
                    <div className="header__nav-btn" dangerouslySetInnerHTML={{ __html: '<?xml version="1.0" encoding="UTF-8"?><svg width="24px" height="28px" viewBox="0 0 24 29" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs></defs><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="card-border" transform="translate(-103.000000, -294.000000)" stroke="#FFFFFF" stroke-width="2"><g id="backet" transform="translate(103.000000, 295.000000)"><path d="M8,10.975 L8,4.893 C8.00165512,2.68470879 9.79170844,0.895102974 12,0.894 C14.21,0.894 16,2.68 16,4.899 L16,10.882" id="Shape" stroke-linecap="round"></path><path d="M2.9131755,7 L1.26693276,25.1000996 C1.17066472,26.1642168 1.93045012,27 2.992,27 L21.008,27 C22.0698598,27 22.8289093,26.1660088 22.7321059,25.1005263 L21.0867767,7 L2.9131755,7 Z" id="a"></path></g></g></g></svg>' }} />
                    <div className="cart__count">{this.props.cartItems.length}</div>
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
        <ModalAccountCreated />
      </div>
    );
  }
}

Header.propTypes = {
  loggedIn: PropTypes.bool,
  logout: PropTypes.func,
  auth: PropTypes.instanceOf(AuthService),
  sign_up: PropTypes.bool,
  sign_in: PropTypes.bool,
  openModal: PropTypes.func,
  hideModal: PropTypes.func,
};

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
  sign_in: state.user.sign_in,
  sign_up: state.user.sign_up,
  cartItems: state.cart.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getCurrentUser,
    openModal,
    hideModal,
    setTab,
    setLoggedIn
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
