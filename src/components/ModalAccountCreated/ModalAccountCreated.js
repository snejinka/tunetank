import React, { PropTypes } from 'react';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock, Modal, Button, ButtonToolbar } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import slug from 'slug';

class ModalAccountCreated extends React.Component {
  constructor(props) {
    super();
    this.state = {
      show: false,
    };
  }

  componentWillUpdate() {
    if (sessionStorage.welcomeTunetankPath && sessionStorage.welcomeTunetankAuthor && !this.state.show) {
      this.setState({ show: true });
    }
  }

  handleProfile() {
    let path = `/author/${slug(sessionStorage.welcomeTunetankAuthorName, { lower: true })}/${sessionStorage.welcomeTunetankAuthor}`;
    if (!path) {
      path = '/';
    }
    this.hideModal();
    browserHistory.push({ pathname: path });
  }

  hideModal = () => {
    sessionStorage.removeItem('welcomeTunetankPath');
    sessionStorage.removeItem('welcomeTunetankAuthor');
    sessionStorage.removeItem('welcomeTunetankAuthorName');
    this.setState({ show: false });
  };

  render() {
    const backdropStyle = {
      zIndex: 'auto',
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
    };

    return (
      <ButtonToolbar>
        <Modal
          {...this.props}
          backdropStyle={backdropStyle}
          show={this.state.show}
          onHide={this.hideModal}
          dialogClassName="modal-default modal-message"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg" className="title_primary title_primary__inner">Account is created!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="modal-note">Great, your account is created.</div>
            <div className="modal-note">Now you can adjust your profile settings.</div>
          </Modal.Body>
          <Modal.Footer>
            <div className="modal-note">
                    Go to <a className="modal-note__link" onClick={() => this.handleProfile()} style={{ cursor: 'pointer' }}>your profile</a>.
                </div>
          </Modal.Footer>
        </Modal>
      </ButtonToolbar>
    );
  }
}
export default ModalAccountCreated;
