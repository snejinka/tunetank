import _ from 'lodash';
import slug from 'slug';
import * as api from 'api';
import multiDownload from 'multi-download';
import toastr from 'toastr';

export const GET_USER_INFO = 'GET_USER_INFO';
export const GET_CURRENT_USER = 'GET_CURRENT_USER';
export const SET_USER_AVATAR = 'SET_USER_AVATAR';
export const SET_USER_BANNER = 'SET_USER_BANNER';
export const SET_USER_SECURE_ID = 'SET_USER_SECURE_ID';
export const SET_USER_NAME = 'SET_USER_NAME';
export const SET_COUNTRY = 'SET_COUNTRY';
export const SET_ADDRESS_FIRST_LINE = 'SET_ADDRESS_FIRST_LINE';
export const SET_ADDRESS_SECOND_LINE = 'SET_ADDRESS_SECOND_LINE';
export const SET_CITY = 'SET_CITY';
export const SET_STATE = 'SET_STATE';
export const SET_ZIP = 'SET_ZIP';
export const SET_VAT = 'SET_VAT';
export const SET_BIOGRAPHY = 'SET_BIOGRAPHY';
export const ADD_TRACK_TO_FAVORITES = 'ADD_TRACK_TO_FAVORITES';
export const SET_SOCIAL_LINKS = 'SET_SOCIAL_LINKS';
export const SET_EMAIL = 'SET_EMAIL';
export const SET_PASSWORD = 'SET_PASSWORD';
export const SET_TAB = 'SET_TAB';
export const FOLLOW_AUTHOR = 'FOLLOW_AUTHOR';
export const UNFOLLOW_AUTHOR = 'UNFOLLOW_AUTHOR';
export const SET_LOGGED_IN = 'SET_LOGGED_IN';
export const USERS_GET_SIMILAR_ARTISTS = 'USERS_GET_SIMILAR_ARTISTS';
export const USERS_CLEAR_USER_INFO = 'USERS_CLEAR_USER_INFO';
export const OPEN_MODAL = 'OPEN_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const SHOW_BUY_TRACK = 'SHOW_BUY_TRACK';
export const HIDE_BUY_TRACK = 'HIDE_BUY_TRACK';
export const REMOVE_TRACK = 'REMOVE TRACK'

export const SOCIAL_LINK_TYPE = {
  reserved: 0,
  sound_cloud: 1,
  twitter: 2,
  instagram: 3,
  facebook: 4,
  google_plus: 5,
  linked_in: 6,
  youtube: 7,
  vimeo: 8,
};

const initialDynamicUserInfo = {
  id: '',
  secure_id: '',
  name: '',
  avatar: '',
  banner: '',
  country: '',
  address_first_line: '',
  address_second_line: '',
  city: '',
  state: '',
  zip: '',
  vat: '',
  biography: '',
  social_links: [],
};

const initialStaticUserInfo = {
  tracks: [],
  social_links: [],
  followers: [],
  followings: [],
  created_at: '',
  is_current: false,
  favorite_tracks: [],
  bought_tracks: [],
  total_tracks: 0,
};

const initialState = {
  userInfo: {
    ...initialDynamicUserInfo,
    ...initialStaticUserInfo,
  },
  currentUser: {
    followings: [],
  },
  favorite_tracks: [],
  social_links: [],
  email: '',
  password: '',
  sign_in: false,
  sign_up: false,
  tab: 'followingFeed',
  loggedIn: false,
  buyTrackShow: false,
  buyTrack: {
    user: {},
  },
  similarArtists: [],
};

export const downloadTrack = trackId => () => api.fetchDownloadTrack(trackId).then((res) => {
  const archiveUrl = res.data.data.find_track_by_id.originals_archive;
  multiDownload([archiveUrl]);
}).catch(() => {
  toastr.error('Something went wrong. Please try again.');
});

export const getUserInfo = (userId, userSlug = null) => (dispatch) => {
  dispatch({ type: USERS_CLEAR_USER_INFO });
  return api.fetchUserInfo(userId).then((res) => {
    const dbSlug = slug(res.name, { lower: true });
    if (userSlug !== dbSlug) {
      const err = new Error('Not Found');
      err.status = 404;
      res = err;
    }
    dispatch({ type: GET_USER_INFO, data: res });
  });
};

export const updateUserInfo = data => () => {
  const userInfo = data;
  const userInfoCopy = _.clone(userInfo);
  Object.keys(initialStaticUserInfo).forEach((key) => {
    delete userInfoCopy[key];
  });
  delete userInfoCopy.total_featured_tracks;
  return api.fetchUpdateUser(userInfoCopy);
};

export const updateExactFealds = (id, fields) => () => new Promise((resolve, reject) => api.fetchUpdateExactField(id, fields).then((res) => {
  resolve(res);
}).catch((err) => {
  reject();
}));

export const getCurrentUser = () => dispatch => new Promise((resolve, reject) => api.fetchGetCurrentUser().then((res) => {
  dispatch({ type: GET_CURRENT_USER, data: res.data.data.current_user });
  resolve(res.data.data.current_user);
}).catch(() => {
  reject();
}));

export const getSimilarArtists = userId => dispatch => api.fetchSimilarArtists(userId).then((res) => {
  dispatch({ type: USERS_GET_SIMILAR_ARTISTS, data: res });
});

export const AddTrackToFavorites = id => () => api.fetchAddTrackToFavorites(id).then(() => {});

export const RemoveTrackFromFavorites = id => () => api.fetchRemoveTrackFromFavorites(id).then(() => {
  // todo go to track page
});

export const deleteTrack = id => (dispatch) => api.removeTrack(id).then((response) => {
  dispatch({ type: REMOVE_TRACK, data: response.data.data.delete_track.id});
});

export const followAuthor = authorId => dispatch => new Promise((resolve, reject) => api.fetchFollowAuthor(authorId).then((res) => {
  resolve(res);
  dispatch({ type: FOLLOW_AUTHOR, data: res.data.data.follow_user });
}).catch((err) => {
  reject(err);
}));

export const unfollowAuthor = authorId => dispatch => api.fetchUnfollowAuthor(authorId).then((res) => {
  dispatch({ type: UNFOLLOW_AUTHOR, data: res.data.data.unfollow_user });
});

export const setLoggedIn = value => (dispatch) => {
  dispatch({ type: SET_LOGGED_IN, data: value });
};

export const setUserAvatar = avatar => (dispatch) => {
  dispatch({ type: SET_USER_AVATAR, data: avatar });
};

export const setUserBanner = banner => (dispatch) => {
  dispatch({ type: SET_USER_BANNER, data: banner });
};

export const setUserName = name => (dispatch) => {
  dispatch({ type: SET_USER_NAME, data: name });
};

export const setCountry = country => (dispatch) => {
  dispatch({ type: SET_COUNTRY, data: country });
};

export const setAddressFirstLine = address => (dispatch) => {
  dispatch({ type: SET_ADDRESS_FIRST_LINE, data: address });
};

export const setAddressSecondLine = address => (dispatch) => {
  dispatch({ type: SET_ADDRESS_SECOND_LINE, data: address });
};

export const setCity = city => (dispatch) => {
  dispatch({ type: SET_CITY, data: city });
};

export const setState = state => (dispatch) => {
  dispatch({ type: SET_STATE, data: state });
};

export const setEmail = email => (dispatch) => {
  dispatch({ type: SET_EMAIL, data: email });
};

export const setPassword = password => (dispatch) => {
  dispatch({ type: SET_PASSWORD, data: password });
};

export const setZip = zip => (dispatch) => {
  dispatch({ type: SET_ZIP, data: zip });
};

export const setVat = vat => (dispatch) => {
  dispatch({ type: SET_VAT, data: vat });
};

export const setBiography = biography => (dispatch) => {
  dispatch({ type: SET_BIOGRAPHY, data: biography });
};

export const setSocialLinks = links => (dispatch) => {
  dispatch({ type: SET_SOCIAL_LINKS, data: links });
};

export const setUserSecureId = value => (dispatch) => {
  dispatch({ type: SET_USER_SECURE_ID, data: value });
};

export const setTab = value => (dispatch) => {
  dispatch({ type: SET_TAB, data: value });
};

export const showBuyTrack = track => (dispatch) => {
  dispatch({ type: SHOW_BUY_TRACK, data: track });
};

export const hideBuyTrack = () => (dispatch) => {
  dispatch({ type: HIDE_BUY_TRACK });
};

export const openModal = type => (dispatch) => {
  dispatch({ type: OPEN_MODAL, data: type });
};

export const hideModal = type => (dispatch) => {
  dispatch({ type: HIDE_MODAL, data: type });
};

const ACTION_HANDLERS = {
  [GET_USER_INFO]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      id: action.data.id,
      created_at: action.data.created_at,
      is_current: action.data.is_current,
      tracks: action.data.tracks,
      followers: action.data.followers,
      followings: action.data.followings,
      favorite_tracks: action.data.favorite_tracks,
      bought_tracks: action.data.bought_tracks,
      secure_id: action.data.secure_id,
      name: action.data.name,
      role: action.data.role,
      avatar: action.data.avatar,
      banner: action.data.banner,
      country: action.data.country,
      address_first_line: action.data.address_first_line,
      address_second_line: action.data.address_second_line,
      city: action.data.city,
      state: action.data.state,
      zip: action.data.zip,
      vat: action.data.vat,
      total_tracks: action.data.total_tracks,
      total_featured_tracks: action.data.total_featured_tracks,
      biography: action.data.biography,
      social_links: action.data.social_links,
    },
    favorite_tracks: action.data.favorite_tracks,
    social_links: action.data.social_links,
    email: action.data.email,
  }),
  [USERS_CLEAR_USER_INFO]: state => ({
    ...state,
    userInfo: {},
    favorite_tracks: [],
    bought_tracks: [],
    social_links: [],
    email: '',
  }),
  [GET_CURRENT_USER]: (state, action) => ({
    ...state,
    currentUser: action.data,
  }),
  [SET_USER_AVATAR]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      avatar: action.data,
    },
  }),
  [SET_USER_BANNER]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      banner: action.data,
    },
  }),
  [FOLLOW_AUTHOR]: (state, action) => {
    const follow = action.data;
    const followings = state.currentUser.followings;
    followings.push(follow);
    return ({
      ...state,
      currentUser: {
        ...state.currentUser,
        followings,
      },
    });
  },
  [UNFOLLOW_AUTHOR]: (state, action) => {
    const unfollow = action.data;
    const followings = state.currentUser.followings;
    const i = followings.findIndex(item => item.id === unfollow.id);
    if (i !== -1) {
      followings.splice(i, 1);
    }
    return ({
      ...state,
      currentUser: {
        ...state.currentUser,
        followings,
      },
    });
  },
  [REMOVE_TRACK]: (state, action) => {
    const removedId = action.data;
    const tracks = state.userInfo.tracks;
    const i = tracks.findIndex(item => item.id === removedId);
    if (i !== -1) {
      tracks.splice(i,1);
    }
    return ({
      ...state,
      userInfo: {
        ...state.userInfo,
        tracks
      }
    })
  },
  [SET_USER_NAME]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      name: action.data,
    },
  }),
  [SET_COUNTRY]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      country: action.data,
    },
  }),
  [SET_ADDRESS_FIRST_LINE]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      address_first_line: action.data,
    },
  }),
  [SET_ADDRESS_SECOND_LINE]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      address_second_line: action.data,
    },
  }),
  [SET_CITY]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      city: action.data,
    },
  }),
  [SET_STATE]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      state: action.data,
    },
  }),
  [SET_ZIP]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      zip: action.data,
    },
  }),
  [SET_VAT]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      vat: action.data,
    },
  }),
  [SET_BIOGRAPHY]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      biography: action.data,
    },
  }),
  [SET_USER_SECURE_ID]: (state, action) => ({
    ...state,
    userInfo: {
      ...state.userInfo,
      secure_id: action.data,
    },
  }),
  [SET_EMAIL]: (state, action) => ({
    ...state,
    email: action.data,
  }),
  [SET_PASSWORD]: (state, action) => ({
    ...state,
    password: action.data,
  }),
  [ADD_TRACK_TO_FAVORITES]: (state, action) => ({
    ...state,
    favorite_tracks: action.data,
  }),
  [SET_SOCIAL_LINKS]: (state, action) => ({
    ...state,
    social_links: action.data,
  }),
  [SET_TAB]: (state, action) => ({
    ...state,
    tab: action.data,
  }),
  [SET_LOGGED_IN]: (state, action) => ({
    ...state,
    loggedIn: action.data,
  }),
  [USERS_GET_SIMILAR_ARTISTS]: (state, action) => ({
    ...state,
    similarArtists: action.data,
  }),
  [OPEN_MODAL]: (state, action) => ({
    ...state,
    [action.data]: true,
  }),
  [HIDE_MODAL]: (state, action) => ({
    ...state,
    [action.data]: false,
  }),
  [SHOW_BUY_TRACK]: (state, action) => ({
    ...state,
    buyTrackShow: true,
    buyTrack: action.data,
  }),
  [HIDE_BUY_TRACK]: state => ({
    ...state,
    buyTrackShow: false,
  }),
};

export default function userReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler
    ? handler(state, action)
    : state;
}
