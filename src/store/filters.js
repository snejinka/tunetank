import axios from 'axios';
import * as api from 'api';
import update from 'react-addons-update';
import _ from 'lodash';

export const FilterFields = {
  search_string: 'search_string',
  time_selection: 'time_selection',
  sorting: 'sorting',
  genres: 'genres',
  moods: 'moods',
  instruments: 'instruments',
  vocals: 'vocals',
  length: 'length',
  tempo: 'tempo',
};

export const SORTING_TYPE = {
  0: 'Best match',
  1: 'Recently added',
  2: 'Best sellers',
  3: 'Longest',
  4: 'Shortest',
};

export const TIME_SELECTION_TYPE = {
  0: 'All time',
  1: 'Last year',
  2: 'Last six months',
  3: 'Last month',
  4: 'Last week',
  5: 'Today',
};

export const CATALOG_REQUEST_RESULTS = 'CATALOG_REQUEST_RESULTS',
  CATALOG_RECEIVE_RESULTS = 'CATALOG_RECEIVE_RESULTS',
  CATALOG_SET_FILTER_QUERY = 'CATALOG_SET_FILTER_QUERY',
  CATALOG_RECEIVE_CATEGORIES = 'CATALOG_RECEIVE_CATEGORIES',
  CATALOG_ADD_SEARCH_SUGGESTIONS = 'CATALOG_ADD_SEARCH_SUGGESTIONS',
  CATALOG_CLEAR_SEARCH_SUGGESTIONS = 'CATALOG_CLEAR_SEARCH_SUGGESTIONS',
  CATALOG_CHANGE_SEARCH_VALUE = 'CATALOG_CHANGE_SEARCH_VALUE',
  CATALOG_CHANGE_FILTER_VALUE = 'CATALOG_CHANGE_FILTER_VALUE',
  CATALOG_CLEAR_ALL_FILTERS = 'CATALOG_CLEAR_ALL_FILTERS',
  CATALOG_SET_SPINNER = 'CATALOG_SET_SPINNER',
  CATALOG_CLEAR_SPINNER = 'CATALOG_CLEAR_SPINNER';


export const changeFilterValue = (field, data) => (dispatch) => {
  dispatch({
    type: CATALOG_CHANGE_FILTER_VALUE,
    field,
    data,
  });
};

export const getFilterCategories = () => dispatch => new Promise((resolve, reject) => {
  api.fetchCategories().then((res) => {
    dispatch({
      type: CATALOG_RECEIVE_CATEGORIES,
      data: res,
    });
    resolve();
  });
});

export const requestResults = (query = {}, difference = []) => {
  const page = query.page;
  delete query.page;

  return (dispatch, getState) => {
    dispatch(setCatalogSpinner());

    if (difference.length > 0) {
      difference.forEach((key) => {
        query[key] = initialFilterState[key];
      });
    }

    const preparedQuery = prepareQuery(query);
    dispatch(setFilterQuery(preparedQuery));

    Object.keys(preparedQuery).forEach((key) => {
      if (preparedQuery[key] == initialFilterState[key]) {
        delete preparedQuery[key];
      }
    });

    const numberOfResultsPerPage = getState().catalogFilters.numberOfResultsPerPage;
    const offset = numberOfResultsPerPage * (page - 1);
    api.fetchCatalogSearchResults(preparedQuery, numberOfResultsPerPage, offset).then((res) => {
      dispatch(receiveResults(res, page));
    });
  };
};

export const prepareQuery = (query = {}) => {
  if (!_.isEmpty(query)) {
    if (query.tempo) {
      query.tempo = query.tempo.map(Number);
    }
    if (query.length) {
      query.length = query.length.map(Number);
    }
    if (query.time_selection) {
      query.time_selection = parseInt(query.time_selection);
    }
    if (query.sorting) {
      query.sorting = parseInt(query.sorting);
    }
    if (query.vocals) {
      query.vocals = JSON.parse(query.vocals);
    }
    if (query.featured) {
      query.featured = JSON.parse(query.featured);
    }
    if (query.looped) {
      query.looped = JSON.parse(query.looped);
    }
    if (query.affilated) {
      query.affilated = JSON.parse(query.affilated);
    }
    if (query.multiple_versions) {
      query.multiple_versions = JSON.parse(query.multiple_versions);
    }
    if (query.genres && (typeof (query.genres) === 'string')) {
      query.genres = [query.genres];
    }
    if (query.moods && (typeof (query.moods) === 'string')) {
      query.moods = [query.moods];
    }
    if (query.instruments && (typeof (query.instruments) === 'string')) {
      query.instruments = [query.instruments];
    }

    if (query.q || query.q == '') {
      query.search_string = query.q;
      delete query.q;
    }
  }

  return (
      query
  );
};

export const setFilterQuery = fields => (dispatch) => {
  dispatch({
    type: CATALOG_SET_FILTER_QUERY,
    fields,
  });
};

export const switchCurrentPage = page => (dispatch, getState) => {
  const currentPage = getState().catalogFilters.currentPage;
  if (currentPage !== page) {
    dispatch(setCatalogSpinner());
    const query = getState().catalogFilters.query;
    const numberOfResultsPerPage = getState().catalogFilters.numberOfResultsPerPage;
    const offset = numberOfResultsPerPage * (page - 1);

    api.fetchCatalogSearchResults(query, numberOfResultsPerPage, offset).then((res) => {
      dispatch(receiveResults(res, page));
    });
  }
};

export const receiveResults = (results, page = 1) => (dispatch) => {
  dispatch({
    type: CATALOG_RECEIVE_RESULTS,
    data: results,
    page: Number(page),
  });

  dispatch(clearCatalogSpinner());
};

export const requestSearchSuggestions = (search_string, tracks_count) => (dispatch) => {
  api.fetchSearchSuggestions(search_string, tracks_count).then((res) => {
    dispatch({
      type: CATALOG_ADD_SEARCH_SUGGESTIONS,
      data: res,
    });
  });
};

export const clearSearchSuggestions = results => (dispatch) => {
  dispatch({
    type: CATALOG_CLEAR_SEARCH_SUGGESTIONS,
  });
};

export const clearAllCatalogFilters = () => (dispatch) => {
  dispatch({
    type: CATALOG_CLEAR_ALL_FILTERS,
  });

  dispatch(requestResults());
};

export const setCatalogSpinner = () => (dispatch) => {
  dispatch({
    type: CATALOG_SET_SPINNER,
  });
};

export const clearCatalogSpinner = () => (dispatch) => {
  dispatch({
    type: CATALOG_CLEAR_SPINNER,
  });
};

const ACTION_HANDLERS = {
  [CATALOG_RECEIVE_CATEGORIES]: (state, action) => {
    const categories = action.data;
    return ({
      ...state,
      ...categories,
    });
  },
  [CATALOG_CHANGE_FILTER_VALUE]: (state, action) => {
    const newState = update(state, { filters: { $merge: { [action.field]: action.data } } });
    return newState;
  },
  [CATALOG_RECEIVE_RESULTS]: (state, action) => ({
    ...state,
    resultTracks: action.data.tracks,
    total: action.data.total_count,
    currentPage: action.page,
  }),
  [CATALOG_SET_FILTER_QUERY]: (state, action) => {
    const query = action.fields;
    const newFilters = { ...state.filters, ...query };
    return ({
      ...state,
      filters: newFilters,
      query,
    });
  },
  [CATALOG_ADD_SEARCH_SUGGESTIONS]: (state, action) => ({
    ...state,
    suggestions: action.data,
  }),
  [CATALOG_CLEAR_SEARCH_SUGGESTIONS]: (state, action) => ({
    ...state,
    suggestions: [],
  }),
  [CATALOG_CLEAR_ALL_FILTERS]: (state, action) => ({
    ...state,
    filters: { ...initialFilterState },
    query: {},
    currentPage: 0,
  }),
  [CATALOG_SET_SPINNER]: (state, action) => ({
    ...state,
    spinner: true,
  }),
  [CATALOG_CLEAR_SPINNER]: (state, action) => ({
    ...state,
    spinner: false,
  }),
};

export const initialFilterState = {
  time_selection: 0,
  sorting: 0,
  genres: [],
  moods: [],
  instruments: [],
  vocals: false,
  looped: false,
  multiple_versions: false,
  affilated: false,
  featured: false,
  search_string: '',
  length: [0, 1200],
  tempo: [0, 250],
};

const initialState = {
  filters: initialFilterState,
  resultTracks: [],
  suggestions: [],
  spinner: false,
  genres: [],
  moods: [],
  instruments: [],
  total: 0,
  query: {},
  numberOfResultsPerPage: 20,
  currentPage: 0,
};

export default function filtersReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
