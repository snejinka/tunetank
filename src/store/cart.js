const MIN_LICENES_COUNT = 1;
const MAX_LICENES_COUNT = 30;
export const licenses = [
  {
    id: 1,
    name: 'Individual',
    price: 25,
    price_in_cents: 2500,
    description: 'For making content on your Youtube channel, Homevideos and Slideshows. Suitable for non profit apps and games',
  },
  {
    id: 2,
    name: 'Professional',
    price: 95,
    price_in_cents: 9500,
    description: 'Covers wedding day films, TV and Radio shows. Additionaly, can be tied to low budget crowdfunding goals, reels, tradeshows and games',
  },
  {
    id: 3,
    name: 'Marketing',
    price: 315,
    price_in_cents: 31500,
    description: 'For use in Internet, TV and Radio commercials',
  },
  {
    id: 4,
    name: 'Enterprise',
    price: 695,
    price_in_cents: 69500,
    description: 'The intended use is for high budget crowdfunding goals, reels and tradeshows. Also lets you use tracks for feature films with low budget',
  },
  {
    id: 5,
    name: 'Universal',
    price: 985,
    price_in_cents: 98500,
    description: 'Covers all use cases and scenarios. No budget and audience limitations',
  },
];
export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const INCREASE_ITEM_QUANTITY = 'INCREASE_ITEM_QUANTITY';
export const REDUCE_ITEM_QUANTITY = 'REDUCE_ITEM_QUANTITY';
export const SET_QUANTITY = 'SET_QUANTITY';
export const SET_LICENSE = 'SET_LICENSE';
export const CLEAR_CART = 'CLEAR_CART';

export const calcTotalAmount = () => (dispatch, getState) => {
  const { cart } = getState();
  let totalAmount = 0;

  cart.items.forEach((item) => {
    totalAmount += item.license.price_in_cents * item.quantity;
  });
  return totalAmount / 100;
};

export const isCartEmpty = () => (dispatch, getState) => {
  const { cart } = getState();
  return cart.items.filter(item => item.quantity > 0).length === 0;
};

export const addItemToCart = item => (dispatch) => {
  dispatch({ type: ADD_ITEM, item });
};

export const removeItemFromCart = itemId => (dispatch) => {
  dispatch({ type: REMOVE_ITEM, itemId });
};

export const increaseItemQuantity = itemId => (dispatch) => {
  dispatch({ type: INCREASE_ITEM_QUANTITY, itemId });
};

export const reduceItemQuantity = itemId => (dispatch) => {
  dispatch({ type: REDUCE_ITEM_QUANTITY, itemId });
};

export const setQuantity = (value, itemId) => (dispatch) => {
  dispatch({ type: SET_QUANTITY, value, itemId });
};

export const setLicense = (licenseId, itemId) => (dispatch) => {
  dispatch({ type: SET_LICENSE, licenseId, itemId });
};

export const clearCart = () => (dispatch) => {
  dispatch({ type: CLEAR_CART });
};

const initialState = {
  items: [],
};

const ACTION_HANDLERS = {
  [ADD_ITEM]: (state, action) => {
    const items = state.items.filter(item => item.id !== action.item.id);
    return {
      ...state,
      items: [...items, action.item],
    };
  },
  [REMOVE_ITEM]: (state, action) => ({
    ...state,
    items: state.items.filter(item => item.id !== action.itemId),
  }),
  [INCREASE_ITEM_QUANTITY]: (state, action) => ({
    ...state,
    items: state.items.map(item => item.id === action.itemId ?
         { ...item, quantity: item.quantity >= MAX_LICENES_COUNT ? MAX_LICENES_COUNT : item.quantity += 1 } :
         item),
  }),
  [REDUCE_ITEM_QUANTITY]: (state, action) => ({
    ...state,
    items: state.items.map(item => item.id === action.itemId ?
           { ...item, quantity: item.quantity <= MIN_LICENES_COUNT ? MIN_LICENES_COUNT : item.quantity -= 1 } :
           item),
  }),
  [SET_QUANTITY]: (state, action) => {
    let newQuantity = parseInt(action.value, 10);
    if (newQuantity >= MAX_LICENES_COUNT) {
      newQuantity = MAX_LICENES_COUNT;
    } else if (newQuantity <= MIN_LICENES_COUNT) {
      newQuantity = MIN_LICENES_COUNT;
    }
    return {
      ...state,
      items: state.items.map(item => item.id === action.itemId ?
             { ...item, quantity: newQuantity || MIN_LICENES_COUNT } :
             item),
    };
  },
  [SET_LICENSE]: (state, action) => ({
    ...state,
    items: state.items.map(item => item.id === action.itemId ?
           { ...item, license: licenses.find(license => license.id === action.licenseId) } :
           item),
  }),
  [CLEAR_CART]: state => ({
    ...state,
    items: initialState.items,
  }),
};

export default function cartReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
