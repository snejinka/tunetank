import * as api from 'api';
import { setTracks, setInitialTrackPlaying, setPlayStatus } from 'store/audioPlayer';
import * as common from 'common';

export const GET_FEATURED_TRACKS = 'GET_FEATURED_TRACKS',
  GET_GENRES = 'GET_GENRES',
  FIND_TRACK_BY_GENRE = 'FIND_TRACK_BY_GENRE',
  GET_NEW_TRACKS = 'GET_NEW_TRACKS';


const initialState = {
  featured: [],
  genres: [],
  newTracks: [],
  selectedGenreTracks: [],
};

export const getFeaturedTracks = () =>
  (dispatch, getState) => api.fetchFeaturedTracks().then((res) => {
    var state = getState();
    var firstLoad = state.tracks.featured.length == 0;

    dispatch({
      type: GET_FEATURED_TRACKS,
      data: res,
    });
    
    if(firstLoad) {
      dispatch(setPlayStatus(common.AudioPlayerStatuses.STOP))
      dispatch(setTracks(res));
      dispatch(setInitialTrackPlaying(res[0]));
    }
  });

export const getGenres = () =>
  (dispatch, getState) => api.fetchGenres().then((res) => {
    dispatch({
      type: GET_GENRES,
      data: res,
    });
  });

export const findTrackByGenre = (selectedGenre = null) =>
  (dispatch, getState) => {
    const state = getState();
    if (selectedGenre == null) {
      selectedGenre = state.tracks.genres[0];
    }
    return api.fetchFindTrackByGenre(selectedGenre.id).then((res) => {
      if (res) {
        dispatch({
          type: FIND_TRACK_BY_GENRE,
          data: res,
        });
      }
    });
  };

export const fetchNewTracks = () => (dispatch, getState) => {
  api.fetchCatalogSearchResults({ sorting: 1 }, 20).then((res) => {
    dispatch({
      type: GET_NEW_TRACKS,
      data: res.tracks,
    });
  });
};

const ACTION_HANDLERS = {
  [GET_FEATURED_TRACKS]: (state, action) => ({
    ...state,
    featured: action.data,
  }),
  [GET_GENRES]: (state, action) => ({
    ...state,
    genres: action.data,
  }),
  [FIND_TRACK_BY_GENRE]: (state, action) => ({
    ...state,
    selectedGenreTracks: action.data,
  }),
  [GET_NEW_TRACKS]: (state, action) => ({
    ...state,
    newTracks: action.data,
  }),
};

export default function tracksReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
