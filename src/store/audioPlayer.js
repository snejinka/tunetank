import * as common from 'common';

const SET_TRACK_PATH = 'SET_TRACK_PATH';
const SET_PLAY_STATUS = 'SET_PLAY_STATUS';
const SET_TRACK_ID = 'SET_TRACK_ID';
const SET_TRACKS = 'SET_TRACKS';
const SET_TRACK_PLAYING = 'SET_TRACK_PLAYING';
const SET_NEXT_TRACK = 'SET_NEXT_TRACK';
const SET_PREVIOUS_TRACK = 'SET_PREVIOUS_TRACK';
const SET_CURRENT_VERSION = 'SET_CURRENT_VERSION';
const SET_PLAYING_POSITION = 'SET_PLAYING_POSITION';
const SET_INITIAL_TRACK_PLAYING = 'SET_INITIAL_TRACK_PLAYING';

const initialState = {
  track_path: '',
  play_status: 'stop',
  trackId: '',
  selectedType: 'main_track',
  tracks: [],
  track_playing: {},
  pos: 0,
  posInSec: common.zeroTime,
};

export const setTrackPath = path =>
  (dispatch) => {
    dispatch({
      type: SET_TRACK_PATH,
      data: path,
    });
  };

export const setPlayStatus = status =>
  (dispatch) => {
    dispatch({
      type: SET_PLAY_STATUS,
      data: status,
    });
  };

export const setTrackId = id =>
  (dispatch) => {
    dispatch({
      type: SET_TRACK_ID,
      data: id,
    });
  };

export const setTracks = tracks =>
  (dispatch) => {
    dispatch({
      type: SET_TRACKS,
      data: tracks,
    });
  };

export const setTrackPlaying = track =>
  (dispatch) => {
    dispatch({
      type: SET_TRACK_PLAYING,
      data: track,
    });
  };

export const setInitialTrackPlaying = track =>
  (dispatch) => {
    dispatch({
      type: SET_INITIAL_TRACK_PLAYING,
      data: track,
    });
  };  

export const setNextTrack = track =>
  (dispatch) => {
    dispatch({
      type: SET_NEXT_TRACK,
    });
  };

export const setPreviousTrack = track =>
    (dispatch) => {
      dispatch({
        type: SET_PREVIOUS_TRACK,
      });
    };

export const setCurrentVersion = version =>
  (dispatch) => {
    dispatch({
      type: SET_CURRENT_VERSION,
      data: version,
    });
  };

export const setPlayingPosition = (pos, posInSec) =>
  (dispatch) => {
    dispatch({
      type: SET_PLAYING_POSITION,
      pos,
      posInSec,
    });
  };

const ACTION_HANDLERS = {
  [SET_TRACK_PATH]: (state, action) => ({
    ...state,
    track_path: action.data,
  }),
  [SET_PLAY_STATUS]: (state, action) => ({
    ...state,
    play_status: action.data,
  }),
  [SET_TRACK_ID]: (state, action) => ({
    ...state,
    trackId: action.data,
  }),
  [SET_TRACKS]: (state, action) => ({
    ...state,
    tracks: action.data,
  }),
  [SET_TRACK_PLAYING]: (state, action) => {
    const selectedType = action.data.versions[state.selectedType].url === null ? common.TrackVersions.main_track : state.selectedType;
    return ({
      ...state,
      track_playing: action.data,
      trackId: action.data.id,
      track_path: action.data.versions[selectedType].url,
      play_status: 'play',
      pos: 0,
      posInSec: common.zeroTime,
    });
  },
  [SET_INITIAL_TRACK_PLAYING]: (state, action) => {
    return ({
      ...state,
      track_playing: action.data,
      trackId: action.data.id,
      track_path: action.data.versions[common.TrackVersions.main_track].url,
      play_status: 'pause',
      pos: 0,
      posInSec: common.zeroTime,
    });
  },
  [SET_TRACK_PLAYING]: (state, action) => {
    const selectedType = action.data.versions[state.selectedType].url === null ? common.TrackVersions.main_track : state.selectedType;
    return ({
      ...state,
      track_playing: action.data,
      trackId: action.data.id,
      track_path: action.data.versions[selectedType].url,
      play_status: 'play',
      pos: 0,
      posInSec: common.zeroTime,
    });
  },
  [SET_NEXT_TRACK]: (state, action) => {
    const index = state.tracks.findIndex(track => track.id == state.trackId);
    let newTrack = state.tracks[index + 1];
    if (!newTrack) {
      newTrack = state.tracks[0];
    }

    const selectedType = newTrack.versions[state.selectedType].url === null ? common.TrackVersions.main_track : state.selectedType;

    return ({
      ...state,
      track_playing: newTrack,
      trackId: newTrack.id,
      track_path: newTrack.versions[selectedType].url,
      play_status: 'play',
      pos: 0,
      posInSec: common.zeroTime,
    });
  },
  [SET_PREVIOUS_TRACK]: (state, action) => {
    const index = state.tracks.findIndex(track => track.id == state.trackId);
    let newTrack = state.tracks[index - 1];
    if (!newTrack) {
      newTrack = state.tracks[state.tracks.length - 1];
    }

    const selectedType = newTrack.versions[state.selectedType].url === null ? common.TrackVersions.main_track : state.selectedType;

    return ({
      ...state,
      track_playing: newTrack,
      trackId: newTrack.id,
      track_path: newTrack.versions[selectedType].url,
      play_status: 'play',
      pos: 0,
      posInSec: common.zeroTime,
    });
  },
  [SET_CURRENT_VERSION]: (state, action) => {
    const track = state.tracks.length > 0 ? state.tracks.find(track => track.id == state.trackId) : state.track_playing;

    if (track) {
      return ({
        ...state,
        selectedType: action.data,
        track_path: track.versions[action.data].url,
        pos: 0,
        posInSec: common.zeroTime,
      });
    }
    return ({
      ...state,
      selectedType: action.data,
      play_status: 'play'
    });
  },
  [SET_PLAYING_POSITION]: (state, action) => ({
    ...state,
    pos: action.pos,
    posInSec: action.posInSec,
  }),
};

export default function audioPlayerReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
