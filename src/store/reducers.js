import { combineReducers } from 'redux';
import locationReducer from './location';
import followingFeedReducer from './followingFeed';
import userReducer from './user';
import audioPlayerReducer from './audioPlayer';
import filtersReducer from './filters';
import cartReducer from './cart';
import tracksReducer from './tracks';

export const makeRootReducer = asyncReducers => combineReducers({
  location: locationReducer,
  followingFeed: followingFeedReducer,
  user: userReducer,
  audioPlayer: audioPlayerReducer,
  catalogFilters: filtersReducer,
  cart: cartReducer,
  tracks: tracksReducer,
  ...asyncReducers,
});

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return;

  store.asyncReducers[key] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
