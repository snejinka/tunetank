import axios from 'axios';
import { API_URL } from 'api';
export const GET_FOLLOWING_TRACKS = 'GET_FOLLOWING_TRACKS';

const initialState = {
  followingTracks: [],
};

export const getFollowingTracks = () =>
  (dispatch, getState) => {
    const state = getState();
    return axios.post(API_URL, {
      query: `query { following_tracks() { tracks { id 
                                                    name 
                                                    is_favorite 
                                                    logo 
                                                    created_at 
                                                    duration 
                                                    main_track 
                                                    wave
                                                    versions { main_track { url duration } 
                                                                short_version { url duration } 
                                                                medium_version { url duration } 
                                                                long_version { url duration } 
                                                              }
                                                    user{ id name avatar is_current total_tracks country city} 
                                                  }, 
                                                  is_featured 
                                            } 
                    }`,
    }).then((res) => {
      dispatch({
        type: GET_FOLLOWING_TRACKS,
        data: res.data.data.following_tracks,
      });
    });
  };

const ACTION_HANDLERS = {
  [GET_FOLLOWING_TRACKS]: (state, action) => ({
    ...state,
    followingTracks: action.data.tracks,
    is_featured: action.data.is_featured,
  }),
};

export default function followingFeedReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
