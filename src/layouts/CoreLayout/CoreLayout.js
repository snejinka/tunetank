import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BuyTrack from 'components/BuyTrack';
import { setLoggedIn } from 'store/user';
import { clearCart } from 'store/cart';
import { getFeaturedTracks } from 'store/tracks';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Spinner from '../../components/Spinner';
import AudioPlayer from '../../components/AudioPlayer';
import './CoreLayout.scss';
import '../../styles/core.scss';
import { isTokenExpired } from '../../utils/jwtHelper';

class CoreLayout extends React.Component {
  constructor(props) {
    super(props);

    this.logout = this.logout.bind(this);
    this.state = {
      refreshTokenProcess: false
    }
  }

  componentWillMount() {
    var refreshToken = localStorage.getItem('authRefreshTokenTunetank');
    var token = localStorage.getItem('authTokenTunetank');
    if(refreshToken && isTokenExpired(token)){
      this.setState({refreshTokenProcess: true});
      this.props.route.auth._doRefreshAuthentication(refreshToken).then(() => {
        this.setState({refreshTokenProcess: false});
        const loggedIn = this.props.route.auth.loggedIn();
        this.props.actions.setLoggedIn(loggedIn);
      });
    }
    this.props.actions.getFeaturedTracks();
    const loggedIn = this.props.route.auth.loggedIn();
    this.props.actions.setLoggedIn(loggedIn);
  }

  logout() {
    this.props.actions.clearCart();
    this.props.actions.setLoggedIn(false);    
    this.props.route.auth.logout();
  }

  render() {
    let children = null;
    if (this.props.children) {
      children = React.cloneElement(this.props.children, {
        auth: this.props.route.auth,
        loggedIn: this.props.route.auth.loggedIn(),
      });
    }

    if(this.state.refreshTokenProcess){
      return (
        <div className="core-layout__viewport">
          <Spinner/>
        </div>
      )
    }

    return (
      <div className="text-center">
        <Header
          loggedIn={this.props.route.auth.loggedIn()}
          logout={this.logout}
          auth={this.props.route.auth}
        />
        <div className="core-layout__viewport top-offset">
          <BuyTrack />
          {children}
        </div>
        {(this.props.location.pathname !== '/catalog' && !this.props.location.pathname.includes('/author/')) && <Footer />}
        <AudioPlayer />
      </div>
    );
  }
}

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

CoreLayout.contextTypes = {
  router: PropTypes.object,
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setLoggedIn,
    clearCart,
    getFeaturedTracks
  }, dispatch),
});

export default connect(null, mapDispatchToProps)(CoreLayout);
