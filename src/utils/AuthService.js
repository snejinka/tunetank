import { EventEmitter } from 'events';
import { isTokenExpired } from './jwtHelper';
import auth0 from 'auth0-js';
import { browserHistory } from 'react-router';
import axios from 'axios';
import * as api from 'api';
export const auth0DatabaseConnection = 'Username-Password-Authentication';

export function setAuthorizationHeader(token) {
  if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common.Authorization;
  }
}

export default class AuthService extends EventEmitter {
  constructor(clientId, domain) {
    super();
    this.clientId = clientId;
    this.domain = domain;
    this._doAuthentication = this._doAuthentication.bind(this);
    this._doRegistration = this._doRegistration.bind(this);
    this.auth0 = new auth0.WebAuth({
      domain,
      clientID: clientId,
      scope: 'openid email profile offline_access',
      responseType: 'id_token token',
    });
  }

  _doRegistration(email, password, name) {
    return new Promise((resolve, reject) => {
      const authData = {
        client_id: this.clientId,
        email,
        password,
        name,
        connection: auth0DatabaseConnection,
        scope: 'openid email profile offline_access',
      };

      api.fetchSignUp(this.domain, authData).then((resp) => {
        if (resp.status === 200) {
          this._doAuthentication(email, password).then(() => {
            api.fetchGetCurrentUser().then((user) => {
              api.fetchUpdateExactField(user.data.data.current_user.id, { name }).then(() => resolve());
            });
          },
           );
        }
        if (resp.status === 400) {
          reject(resp.data);
        }
      }).catch((error) => {
        reject(error);
      });
    });
  }

  _doAuthentication(email, password) {
    return new Promise((resolve, reject) => {
      this.auth0.client.login({
        realm: auth0DatabaseConnection,
        username: email,
        password,
        scope: 'openid email profile offline_access',
        audience: `https://${this.domain}/api/v2/`,
      }, (err, authResult) => {
        if (err) return reject(err);
        setAuthorizationHeader(authResult.idToken);
        this.setToken(authResult.idToken);
        this.setRefreshToken(authResult.refreshToken);
        this.auth0.client.userInfo(authResult.accessToken, (error, profile) => {
          if (!error) {
            this.setProfile(profile);
            resolve();
          }
        });
      });
    });
  }

  _doRefreshAuthentication(token) {
    return new Promise((resolve, reject) => {
      const authData = {
        grant_type: 'refresh_token',
        refresh_token: token,
        audience: `https://${this.domain}/api/v2/`,
        scope: 'openid email profile offline_access',
        client_id: this.clientId,
      };

      api.fetchSignIn(this.domain, authData).then((resp) => {
        setAuthorizationHeader(resp.id_token);
        this.setToken(resp.id_token);
        resolve(resp);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  _doChangePassword(email) {
    return new Promise((resolve, reject) => {
      const authData = {
        client_id: this.clientId,
        connection: auth0DatabaseConnection,
        email,
      };

      api.fetchChangePassword(this.domain, authData).then((resp) => {
        resolve(resp);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  renew() {
    const token = this.getRefreshToken();
    if (token) {
      this._doRefreshAuthentication(token);
    }
  }

  loggedIn() {
    const token = this.getToken();
    return !!token && !isTokenExpired(token);
  }

  logout() {
    localStorage.removeItem('authTokenTunetank');
    localStorage.removeItem('profileTunetank');
    localStorage.removeItem('authRefreshTokenTunetank');
    setAuthorizationHeader();
    location.replace('/');
  }

  setProfile(profile) {
    localStorage.setItem('profileTunetank', JSON.stringify(profile));
    this.emit('profile_updated', profile);
  }

  setToken(idToken) {
    localStorage.setItem('authTokenTunetank', idToken);
  }

  setRefreshToken(refreshToken) {
    localStorage.setItem('authRefreshTokenTunetank', refreshToken);
  }

  getToken() {
    return localStorage.getItem('authTokenTunetank');
  }

  getRefreshToken() {
    return localStorage.getItem('authRefreshTokenTunetank');
  }

  getProfile() {
    const profile = localStorage.getItem('profileTunetank');
    return JSON.parse(profile);
  }
}
