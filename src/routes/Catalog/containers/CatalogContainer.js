import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid, Row, Col, AutoAffix, Affix } from 'react-bootstrap';
import FiltersSidebar from './../components/FiltersSidebar';
import FiltersTop from './../components/FiltersTop';
import Results from './../components/Results';
import { requestResults, getFilterCategories, FilterFields, clearAllCatalogFilters } from 'store/filters';
import Spinner from 'components/Spinner';
import _ from 'lodash';

class CatalogContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filtersHeight: 0,
    };
  }

  componentWillMount() {
    const query = this.props.location.query;
    this.props.actions.getFilterCategories();
    this.props.actions.requestResults(query);
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps.location.query) != JSON.stringify(this.props.location.query)) {
      const query = nextProps.location.query;
      const differenceValues = _.difference(_.keys(this.props.location.query), _.keys(nextProps.location.query));
      this.props.actions.requestResults(query, differenceValues);
    }
  }

  componentWillUnmount() {
    this.props.actions.clearAllCatalogFilters();
  }

  setFiltersHeight(height) {
    this.setState({ filtersHeight: height });
  }

  render() {
    return (
      <div className="catalog">
        <Grid>
       
              <FiltersSidebar spinner={this.props.spinner}/>
        
              <FiltersTop setHeight={this.setFiltersHeight.bind(this)} spinner={this.props.spinner} />
              {this.props.spinner ?
                <div className="spinner__wrap catalog__spinner-wrap">
                  <Spinner />
                </div>
                          :
                <Results filtersHeight={this.state.filtersHeight} />}

        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  spinner: state.catalogFilters.spinner,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    requestResults,
    getFilterCategories,
    clearAllCatalogFilters,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CatalogContainer);
