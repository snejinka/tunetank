import CatalogContainer from './containers/CatalogContainer';

export default () => ({
  path: 'catalog',
  getComponent(nextState, cb) {
    require.ensure([], () => {
      cb(null, CatalogContainer);
    }, 'CatalogView');
  },
});
