import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as common from 'common';
import { browserHistory } from 'react-router';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import PaginationAdvanced from 'components/Pagination';
import { DropdownButton, MenuItem, ButtonToolbar, Dropdown, Button } from 'react-bootstrap';
import { Link } from 'react-router';
import TrackList from 'components/TrackList';
import { Range } from 'rc-slider';
import { FilterFields, clearAllCatalogFilters, changeFilterValue } from 'store/filters';
import FilterGenres from './FilterGenres';
import FilterMoods from './FilterMoods';
import FilterInstruments from './FilterInstruments';

class FiltersSidebar extends React.Component {
  constructor(props) {
    super(props);

    this.delayedCallback = _.debounce((type, value) => {
      const location = browserHistory.getCurrentLocation();
      const query = location.query;
      browserHistory.push({ pathname: '/catalog', query: { ...query, [type]: value } });
    }, 500);
  }

  handleChangeCheckboxes(e) {
    const location = browserHistory.getCurrentLocation();
    const query = location.query;
    const fieldName = e.target.name;
    const fieldVal = e.target.checked;
    browserHistory.push({ pathname: '/catalog', query: { ...query, [fieldName]: fieldVal } });      
  }

  handleChangeRange(value, type) {
    this.props.actions.changeFilterValue(type, value);
    this.delayedCallback(type, value);
  }

  clearAllCatalogFilters() {
    this.props.actions.clearAllCatalogFilters();
    browserHistory.push({ pathname: '/catalog', query: '' });
  }

  render() {
    return (

      <aside className={`sidebar sidebar_left catalog__sidebar ${this.props.spinner ? 'disabledDiv' : ''}`}>
        <Row>
          <Col md={12}>
            <div className="catalog-options">
              <FilterGenres />
              <FilterMoods />
              <FilterInstruments />
            </div>
            <div className="filter__range filter__range--small-indent" >
              <span className="catalog__title catalog__title_active">Length <span className="text-regular">(min : sec)</span></span>
              <Range min={0} max={1200} defaultValue={[0, 1200]} value={this.props.length} onChange={(value) => { this.handleChangeRange(value, FilterFields.length); }} />
              <span className="filter__range-value filter__range-value_min">{common.timeFormatter(this.props.length[0])}</span>
              <span className="filter__range-value filter__range-value_max">{common.timeFormatter(this.props.length[1])}</span>
            </div>
            <div className="filter__range">
              <span className="catalog__title catalog__title_active">Tempo <span className="text-regular">(BPM)</span></span>
              <Range min={0} max={250} defaultValue={[0, 250]} value={this.props.tempo} onChange={(value) => { this.handleChangeRange(value, FilterFields.tempo); }} />
              <span className="filter__range-value filter__range-value_min">{this.props.tempo[0]}</span>
              <span className="filter__range-value filter__range-value_max">{this.props.tempo[1]}</span>
            </div>
            <div className="filter__checkbox-block">
              <FormGroup>
                <div className="radio-box radio-box__square radio-box__nogap">
                  <FormControl
                    type="checkbox"
                    id="vocals"
                    name="vocals"
                    checked={this.props.filters.vocals}
                    onChange={this.handleChangeCheckboxes.bind(this)}
                  />
                  <ControlLabel htmlFor="vocals" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="vocals" className="radio-box__note">Vocal</ControlLabel>
                </div>
              </FormGroup>
              <FormGroup>
                <div className="radio-box radio-box__square radio-box__nogap">
                  <FormControl
                    type="checkbox"
                    id="looped"
                    name="looped"
                    checked={this.props.filters.looped}
                    onChange={this.handleChangeCheckboxes.bind(this)}
                  />
                  <ControlLabel htmlFor="looped" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="looped" className="radio-box__note">Looped</ControlLabel>
                </div>
              </FormGroup>
              <FormGroup>
                <div className="radio-box radio-box__square radio-box__nogap">
                  <FormControl
                    type="checkbox"
                    id="multiple_versions"
                    name="multiple_versions"
                    checked={this.props.filters.multiple_versions}
                    onChange={this.handleChangeCheckboxes.bind(this)}
                  />
                  <ControlLabel htmlFor="multiple_versions" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="multiple_versions" className="radio-box__note">Multiple versions</ControlLabel>
                </div>
              </FormGroup>
              <FormGroup>
                <div className="radio-box radio-box__square radio-box__nogap">
                  <FormControl
                    type="checkbox"
                    id="featured"
                    name="featured"
                    checked={this.props.filters.featured}
                    onChange={this.handleChangeCheckboxes.bind(this)}
                  />
                  <ControlLabel htmlFor="featured" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="featured" className="radio-box__note">Featured</ControlLabel>
                </div>
              </FormGroup>
              {/*
                       <FormGroup>
                           <div className="radio-box radio-box__square radio-box__nogap">
                                <FormControl type="checkbox"
                                         id="affilated"
                                         name="affilated"
                                         checked={this.props.filters.affilated}
                                         onChange={this.handleChangeCheckboxes.bind(this)}/>
                                <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio"/>
                                <ControlLabel htmlFor="radioGroup121" className="radio-box__note">P.R.O Affilated</ControlLabel>
                           </div>
                       </FormGroup>
                      */}
            </div>

            <button
              className="btn btn_line btn-clear-filter"
              onClick={() => this.clearAllCatalogFilters()}
            >
                        Clear all filters
                    </button>
          </Col>
        </Row>
      </aside>
    );
  }
}

const mapStateToProps = (state, props) => ({
  filters: state.catalogFilters.filters,
  length: state.catalogFilters.filters.length,
  tempo: state.catalogFilters.filters.tempo,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    clearAllCatalogFilters,
    changeFilterValue,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FiltersSidebar);
