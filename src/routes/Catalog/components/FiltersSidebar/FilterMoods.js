import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { DropdownButton, MenuItem, ButtonToolbar, Dropdown, Button } from 'react-bootstrap';
import { Link } from 'react-router';
import { FilterFields, changeFilterValue } from 'store/filters';
import classNames from 'classnames';
import _ from 'lodash';

class FilterMoods extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.filters.moods,
      isOpen: false,

    };
  }
  
  componentWillMount() {
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.filters.moods != nextProps.filters.moods) {
      this.setState({ selected: nextProps.filters.moods });
    }
  }

  handleApplyFilter() {
    this.setCurrentValue(FilterFields.moods, this.state.selected);
    this.setState({ isOpen: false });
  }

  toggleClickMood(mood) {
    const selected = _.clone(this.state.selected);
    const index = selected.findIndex(id => mood.id == id);
    if (index == -1) {
      selected.push(mood.id);
    } else {
      selected.splice(index, 1);
    }
    this.setState({ selected });
  }

  toggleFilter(isOpen) {
    this.setState({ isOpen });
  }

  setCurrentValue(type, selected) {
    const location = browserHistory.getCurrentLocation();
    const query = location.query;
    const value = selected;
    browserHistory.push({ pathname: '/catalog', query: { ...query, [type]: value } });
  }

  renderNodes() {
    const nodes = [];
    this.props.moods.forEach((mood, i) => {
      const selected = this.state.selected.includes(mood.id);
      nodes.push(
        <div key={i} onClick={() => this.toggleClickMood(mood)}>
          <div className={classNames('catalog__menu-item', { active: selected })}>{mood.name} &nbsp; <span className="catalog__menu-item__count">{mood.tracks_count}</span></div>
        </div>,
          );
    });

    return (
      <div className="catalog__pop-up-filter">
        {nodes}
      </div>
    );
  }

  render() {
    const countMoods = this.props.moods.length;
    return (
      <div>
        <ButtonToolbar>
          <Dropdown id="dropdown-custom-1" open={this.state.isOpen} onToggle={(isOpen, e) => this.toggleFilter(isOpen)}>
            <Dropdown.Toggle className="catalog__title catalog__title_active" noCaret>
              <span>Mood</span>
            </Dropdown.Toggle>
            <Dropdown.Menu className="catalog__pop-up">
              <MenuItem header >
                {this.state.selected.length > 0 ?
                  (`Show ${this.state.selected.length} mood${this.state.selected.length > 1 ? 's' : ''}`) :
                  (`Show: All moods (${countMoods})`) 
                }
              </MenuItem>
              <MenuItem divider />
              <div className="catalog__scroll">
                {this.renderNodes()}
              </div>
              <div className="catalog__footer text-center">
                <Button type="button" className="btn btn-primary" onClick={this.handleApplyFilter.bind(this)}>Apply Filters</Button>
              </div>
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  filters: state.catalogFilters.filters,
  moods: state.catalogFilters.moods,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    changeFilterValue,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FilterMoods);
