import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { MenuItem, ButtonToolbar, Dropdown, Button } from 'react-bootstrap';
import { FilterFields, changeFilterValue } from 'store/filters';
import classNames from 'classnames';
import _ from 'lodash';

class FilterGenres extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.filters.genres,
      isOpen: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.filters.genres != nextProps.filters.genres) {
      this.setState({ selected: nextProps.filters.genres });
    }
  }

  handleApplyFilter() {
    this.setCurrentValue(FilterFields.genres, this.state.selected);
    this.setState({ isOpen: false });
  }

  toggleClickGanre(genre) {
    const selected = _.clone(this.state.selected);
    const index = selected.findIndex(id => genre.id == id);
    if (index == -1) {
      selected.push(genre.id);
    } else {
      selected.splice(index, 1);
    }
    this.setState({ selected });
  }

  toggleFilter(isOpen) {
    this.setState({ isOpen });
  }

  setCurrentValue(type, selected) {
    const location = browserHistory.getCurrentLocation();
    const query = location.query;
    const value = selected;
    browserHistory.push({ pathname: '/catalog', query: { ...query, [type]: value } });
  }

  renderNodes() {
    const nodes = [];
    this.props.genres.forEach((genre, i) => {
      const selected = this.state.selected.includes(genre.id);
      nodes.push(
        <div key={i}>
          <div
            className={classNames('catalog__menu-item', { active: selected })}
            onClick={() => this.toggleClickGanre(genre, true)}
          >
           {genre.name} &nbsp;
           <span className="catalog__menu-item__count">{genre.tracks_count}</span>
          </div>
        </div>,
          );
    });

    return (
      <div className="catalog__pop-up-filter">
        {nodes}
      </div>
    );
  }

  render() {
    const countGenres = this.props.genres.length;
    return (
      <div>
        <ButtonToolbar>
          <Dropdown id="dropdown-custom-1" open={this.state.isOpen} onToggle={(isOpen, e) => this.toggleFilter(isOpen)}>
            <Dropdown.Toggle className="catalog__title catalog__title_active" noCaret>
              <span>Genre</span>
            </Dropdown.Toggle>
            <Dropdown.Menu className="catalog__pop-up">
            <MenuItem header >
                {this.state.selected.length > 0 ?
                  (`Show ${this.state.selected.length} genre${this.state.selected.length > 1 ? 's' : ''}`) :
                  (`Show: All genres (${countGenres})`) 
                }
              </MenuItem>
              <MenuItem divider />
              <div className="catalog__scroll">
                {this.renderNodes()}
              </div>
              <div className="catalog__footer text-center">
                <Button type="button" className="btn btn-primary" onClick={this.handleApplyFilter.bind(this)}>Apply Filters</Button>
              </div>

            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  filters: state.catalogFilters.filters,
  genres: state.catalogFilters.genres,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    changeFilterValue,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FilterGenres);
