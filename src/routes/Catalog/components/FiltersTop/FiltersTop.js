import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import SelectizeSimpleSelectTheme from 'components/SelectizeSimpleSelectTheme';
import PaginationAdvanced from 'components/Pagination';
import AutosuggestInput from 'components/AutosuggestInput';
import { Link } from 'react-router';
import TrackList from 'components/TrackList';
import { Range } from 'rc-slider';
import classNames from 'classnames';
import { ReactHeight } from 'react-height';
import { FilterFields, SORTING_TYPE, TIME_SELECTION_TYPE, changeFilterValue, requestResults } from 'store/filters';

class FiltersTop extends React.Component {
  onChangeSortingType(object) {
    const location = browserHistory.getCurrentLocation();
    const query = location.query;
    const value = object.value;
    browserHistory.push({ pathname: '/catalog', query: { ...query, sorting: value } });
  }

  onChangeTimeSelection(object) {
    const location = browserHistory.getCurrentLocation();
    const query = location.query;
    const value = object.value;
    browserHistory.push({ pathname: '/catalog', query: { ...query, time_selection: value } });
  }

  onClickSearch() {
    const location = browserHistory.getCurrentLocation();
    const query = location.query;
    const value = this.props.filters.search_string;
    browserHistory.push({ pathname: '/catalog', query: { ...query, q: value } });
  }

  appliedFilters() {
    const query = browserHistory.getCurrentLocation().query;
    // TODO: hot fix. If you have time pls refactor this.
    let queryKeys = Object.entries(query);
    let filtredKeys = queryKeys.filter((item) => {
      const filter = item[0];
      if (filter === 'genres' || filter === 'instruments' || filter === 'moods') {
        return item;
      } else {
        return item[1] === 'true';        
      }
    });
    let filtredKeysObj = filtredKeys.reduce((p,c) => {
      p[c[0]] = c[1];
      return p;
    }, {});

    let keys = Object.keys(filtredKeysObj);
    let extractedFilters = [];
    
    keys.forEach((filter) => {
      if (filter === 'genres' || filter === 'instruments' || filter === 'moods') {
        let multipleFilter = [];
        if (!Array.isArray(query[filter])) {
          multipleFilter.push(query[filter]);
        } else {
          multipleFilter = [...query[filter]];
        }
        multipleFilter.forEach((id) => {
          this.props[filter].forEach((obj) => {
            if (obj.id === id) extractedFilters.push({ name: obj.name, filter, id });
          });
        });
      } else if (filter !== 'page' && filter !== 'q') {
        filter = filter.replace('_', ' ');
        filter = filter.charAt(0).toUpperCase() + filter.slice(1);
        extractedFilters.push({ name: filter });
      }
    });

    extractedFilters = extractedFilters.map((filter) => {
      if (filter.filter) {
        const dropFilter = {};
        if (!Array.isArray(query[filter.filter])) {
          dropFilter[filter.filter] = undefined;
        } else {
          dropFilter[filter.filter] = query[filter.filter].filter(id => id !== filter.id);
        }

        const cleanQuery = Object.assign({}, query, dropFilter);
        return Object.assign({}, filter, { query: cleanQuery });
      }
      let name = filter.name;
      name = name.replace(' ', '_');
      name = name.charAt(0).toLowerCase() + name.slice(1);
      const cleanQuery = Object.assign({}, query, { [name]: undefined });
      return Object.assign({}, filter, { query: cleanQuery });
    });

    if (extractedFilters.length) {
      return (
        <Row className="catalog__applied-filters">
          <div className="catalog__applied-title">Applied filters:</div>
          <div className="catalog__filters-wrapper">
            {extractedFilters.map((filter, index) => (<Link key={index} className="catalog__applied-filter" to={{ pathname: '/catalog', query: filter.query }}>{filter.name} <span>&times;</span></Link>))}
          </div>
        </Row>
      );
    }
    return null;
  }

  render() {
    const query = browserHistory.getCurrentLocation().query;
    delete query.page;
    delete query.q;
    return (
      <div className={classNames('catalog__header', { 'catalog__header--filters': Object.keys(query).length, 'disabledDiv': this.props.spinner })}>
        <Row className="flexbox align-center">
          <Col md={6}>
            <div className="catalog-filter-search">
              <div className="input_search-wrap input_search-wrap_bordered">
                <AutosuggestInput />
                <button className="btn btn_search" onClick={() => this.onClickSearch()}>
                  <i className="fa fa-search" aria-hidden="true" />
                </button>
              </div>
            </div>
          </Col>
          <Col md={6}>
          <div className="filter-limited-group">
          <Col md={12}>
              <Col md={6}>
                <SelectizeSimpleSelectTheme
                  options={SORTING_TYPE}
                  onBlurResetsInput={false}
                  onSelect={this.onChangeSortingType.bind(this)}
                  value={{ label: SORTING_TYPE[this.props.filters.sorting], value: this.props.filters.sorting }}
                  defaultValue={{ label: SORTING_TYPE[this.props.filters.sorting], value: this.props.filters.sorting }}
                />
              </Col>
              <Col md={6}>
                <SelectizeSimpleSelectTheme
                  options={TIME_SELECTION_TYPE}
                  onSelect={this.onChangeTimeSelection.bind(this)}
                  value={{ label: TIME_SELECTION_TYPE[this.props.filters.time_selection], value: this.props.filters.time_selection }}
                  defaultValue={{ label: TIME_SELECTION_TYPE[this.props.filters.time_selection], value: this.props.filters.time_selection }}
                />
              </Col>
            </Col>
          </div>
          </Col>
        </Row>
        <ReactHeight onHeightReady={height => this.props.setHeight(height)}>
          <div>
            {this.appliedFilters()}
          </div>
        </ReactHeight>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  filters: state.catalogFilters.filters,
  genres: state.catalogFilters.genres,
  moods: state.catalogFilters.moods,
  instruments: state.catalogFilters.instruments,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    changeFilterValue,
    requestResults,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FiltersTop);
