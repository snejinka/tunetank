import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import QueryPagination from 'components/QueryPagination';
import AutosuggestInput from 'components/AutosuggestInput';
import { DropdownButton, MenuItem, ButtonToolbar, Dropdown, Button } from 'react-bootstrap';
import { Link } from 'react-router';
import TrackList from 'components/TrackList';
import { Range } from 'rc-slider';
import { switchCurrentPage } from 'store/filters';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';

class Results extends React.Component {
  changePage(eventKey) {
    this.props.actions.switchCurrentPage(eventKey);
  }

  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.results);
    this.props.actions.setTrackPlaying(playTrack);
  }

  render() {
    let showPagination = true;
    if (this.props.filters.total == 0 || this.props.filters.currentPage == 0) { showPagination = false; }

    const pages = Math.ceil(this.props.filters.total / this.props.numberOfResultsPerPage);
    return (
      <div className="catalog__body " style={{ marginTop: this.props.filtersHeight }}>
        <TrackList
          userInfo={this.props.userInfo}
          updateUser={this.props.updateUser}
          tracks={this.props.results}
          showVersions
          setTracks={playTrack => this.setTracks(playTrack)}
          viewAuthorTracks={false}
        />

        {showPagination && <QueryPagination
          allPages={pages}
          changePage={eventKey => this.changePage(eventKey)}
          switchPage={this.props.actions.switchCurrentPage}
          activePage={this.props.currentPage}
        />}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  filters: state.catalogFilters,
  results: state.catalogFilters.resultTracks,
  numberOfResultsPerPage: state.catalogFilters.numberOfResultsPerPage,
  currentPage: state.catalogFilters.currentPage,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    switchCurrentPage,
    setTracks,
    setTrackPlaying,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Results);
