import * as api from 'api';
import slug from 'slug';

export const GET_CURRENT_TRACK = 'GET_CURRENT_TRACK',
  CLEAR_CURRENT_TRACK = 'CLEAR_CURRENT_TRACK';

const initialState = {
  currentTrack: [],
};


export const getCurrentTrack = (trackSlug, trackId) => (dispatch) => {
  dispatch({
    type: CLEAR_CURRENT_TRACK,
  });
  return api.fetchCurrentTrack(trackId).then((res) => {
    const dbSlug = slug(res.name, { lower: true });
    if (trackSlug !== dbSlug) {
      const err = new Error('Not Found');
      err.status = 404;
      res = err;
    }
    dispatch({
      type: GET_CURRENT_TRACK,
      data: res,
    });
  });
};

const ACTION_HANDLERS = {
  [GET_CURRENT_TRACK]: (state, action) => ({
    ...state,
    currentTrack: action.data,
  }),
  [CLEAR_CURRENT_TRACK]: (state, action) => ({
    ...state,
    currentTrack: [],
  }),
};


export default function tracksReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
