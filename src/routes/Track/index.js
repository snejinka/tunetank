import TrackView from './components/TrackView';
import { injectReducer } from '../../store/reducers';

export default store => ({
  path: 'track/:trackSlug/:id',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const reducer = require('./modules/currentTrack').default;
      injectReducer(store, { key: 'currentTrack', reducer });
      cb(null, TrackView);
    }, 'TrackView');
  },
});
