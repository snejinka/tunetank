import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Grid, Row, Col } from 'react-bootstrap';
import moment from 'moment';
import DropdownMenu from '../DropdownMenu';
import Achivments from '../Achivments';
import AuthorTrackAvatar from 'assets/img/avatar.png';
import NoAvatar from 'assets/img/no-avatar.png';
import slug from 'slug';

class AuthorCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: props.currentTrack.user,
      createdAt: moment(props.currentTrack.user.created_at, 'YYYY-MM-DD hh:mm:ss').format('MMMM YYYY'),
    };
  }

  static propTypes = {
    currentTrack: PropTypes.object,
  }

  render() {
    const user = this.props.currentTrack.user;
    const avatar = user.avatar ? user.avatar : NoAvatar;
    return (
      <div className="author-card">
        <Row>
          <Col md={12}>
            <div style={{ backgroundImage: `url(${avatar})` }} className="author-card__avatar" />
            <Link to={`/author/${slug(user.name, { lower: true })}/${user.id}`} className="title author-card__title-primary">{user.name}</Link>
            <div className="title author-card__title-secondary">Registered since {this.state.createdAt}</div>
            <Link to={`/author/${slug(user.name, { lower: true })}/${user.id}`} className="btn btn-action">View Profile</Link>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({

  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(AuthorCard);
