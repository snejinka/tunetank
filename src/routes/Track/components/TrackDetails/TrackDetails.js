import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import TrackInfoImage from 'assets/img/track-img-big1.png';
import MusicWave from 'assets/img/waves.png';
import { ShareButtons, ShareCounts } from 'react-share';
import CountHandler from './CountHandler';
import {Link} from 'react-router';

class TrackDetails extends React.Component {

  static propTypes = {
    currentTrack: PropTypes.object,
  }

  render() {
    const { currentTrack } = this.props;
    const shareUrl = window.location.href;
    const title = `${currentTrack.user.name} - ${currentTrack.name}`;
    const {
        FacebookShareButton,
        TwitterShareButton,
        TelegramShareButton,
        WhatsappShareButton,
        PinterestShareButton,
        RedditShareButton,
    } = ShareButtons;

    return (
      <div className="details">
        <Grid>
          <Row>
            <Col md={3}>
              <div className="details__item">
                <div className="details__item-title">Bit Rate :</div>
                <div className="details__item-text">{currentTrack.bit_rate === 0 && 320} kbps</div>
              </div>
              {/* <div className="details__item">
                <div className="details__item-title">P.R.O. Affiliated :</div>
                <div className="details__item-text">{currentTrack.organization}</div>
              </div>
              <div className="details__item">
                <div className="details__item-title">Publisher :</div>
                <div className="details__item-text">{currentTrack.publisher}</div>
              </div>
              <div className="details__item">
                <div className="details__item-title">Composer :</div>
                <div className="details__item-text">{currentTrack.composer}</div>
              </div>
               */}
              <div className="details__item">
                <div className="details__item-title">Sample Rate :</div>
                <div className="details__item-text">{currentTrack.sample_rate === 0 && '16-Bit Stereo, 44.1 kHz'}</div>
              </div>
              <div className="details__item">
                <div className="details__item-title">Looped Audio :</div>
                <div className="details__item-text">{currentTrack.looped ? 'Yes' : 'No'}</div>
              </div>
              <div className="details__item">
                <div className="details__item-title">Youtube Content ID Registered :</div>
                <div className="details__item-text">No</div>
              </div>
            </Col>
            <Col md={6}>
              <div className="details__item">
                <div className="details__item-title">Tempo (BPM) :</div>
                <div className="details__item-text">{currentTrack.tempo}</div>
              </div>
              <div className="details__item">
                <div className="details__item-title">Genre(s) :</div>
                <div className="details__item-text">
                  {currentTrack.genres.map((genre, i) => (
                    <Link to={`/catalog?genres=${genre.id}`} key={i}>{genre.name} </Link>
                    ))}
                </div>
              </div>
              <div className="details__item">
                <div className="details__item-title">Mood(s) :</div>
                <div className="details__item-text ">
                  {currentTrack.moods.map((mood, i) => (
                    <Link to={`/catalog?moods=${mood.id}`} key={i}> {mood.name} </Link>
                    ))}
                </div>
              </div>
              <div className="details__item">
                <div className="details__item-title">Instrument(s) :</div>
                <div className="details__item-text">
                  {currentTrack.instruments.map((instrument, i) => (
                    <Link to={`/catalog?instruments=${instrument.id}`} key={i}>{instrument.name} </Link>
                    ))}
                </div>
              </div>
            </Col>
            <Col md={3} className="social__share-column">
              <div className="social__wrap">
                <div className="social__title">Share author’s page</div>
                <div className="social">
                  <FacebookShareButton url={shareUrl} title={title} className="social__item">
                    <i className="fa fa-facebook" aria-hidden="true" />
                  </FacebookShareButton>
                  <TwitterShareButton url={shareUrl} title={title} className="social__item social__item--twitter">
                    <i className="fa fa-twitter" aria-hidden="true" />
                  </TwitterShareButton>
                  <TelegramShareButton url={shareUrl} title={title} className="social__item social__item--telegram">
                    <i className="fa fa-telegram" aria-hidden="true" />
                  </TelegramShareButton>
                  <WhatsappShareButton url={shareUrl} title={title} className="social__item social__item--whatsapp">
                    <i className="fa fa-whatsapp" aria-hidden="true" />
                  </WhatsappShareButton>
                  <PinterestShareButton
                    url={shareUrl}
                    media={currentTrack.logo_medium}
                    description={title}
                    className="social__item social__item--pinterest"
                  >
                    <i className="fa fa-pinterest" aria-hidden="true" />
                  </PinterestShareButton>
                  <RedditShareButton url={shareUrl} title={title} className="social__item social__item--reddit">
                    <i className="fa fa-reddit" aria-hidden="true" />
                  </RedditShareButton>
                </div>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(TrackDetails);

