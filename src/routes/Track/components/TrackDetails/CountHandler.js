import React, { PropTypes } from 'react';

class CountHandler extends React.Component {
  static propTypes = {
    count: PropTypes.number,
    countUpdate: PropTypes.func,
  }

  componentWillMount() {
    this.props.countUpdate(this.props.count);
  }

  render() {
    return null;
  }
}

export default CountHandler;
