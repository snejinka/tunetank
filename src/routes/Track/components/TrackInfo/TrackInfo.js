import React, { PropTypes } from 'react';
import * as api from 'api';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import TrackInfoImage from 'assets/img/track-img-big1.png';
import moment from 'moment';
import * as common from 'common';
import _ from 'lodash';
import classNames from 'classnames';
import WaveSurfer from 'components/WaveSurfer';
import { AddTrackToFavorites, fetchRemoveTrackFromFavorites, showBuyTrack, openModal} from 'store/user';
import { getCurrentTrack } from '../../modules/currentTrack';
import { CROWN_SVG } from 'assets/js/crown';
import { SHORT_NOTE_SVG } from 'assets/js/short_note';
import { MEDIUM_NOTE_SVG } from 'assets/js/medium_note';
import { LONG_NOTE_SVG } from 'assets/js/long_note';
import { setTrackPath, setPlayStatus, setTrackPlaying } from 'store/audioPlayer';
import { TrackSvg } from 'styles/svgs/svgs.js';
import Like from 'components/Like';
import slug from 'slug';

class TrackInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showVersionsDropdown: false,
      selectedType: common.TrackVersions.main_track,
      is_favorite: props.currentTrack.is_favorite,
    };
    this.showVersions = this.showVersions.bind(this);
    this.hideVersions = this.hideVersions.bind(this);
  }

  showVersions() {
    this.setState({ showVersionsDropdown: true });
  }

  hideVersions() {
    this.setState({ showVersionsDropdown: false });
  }

  AddTrackToFavorites = (event) => {
    this.setState({ is_favorite: true });
    api.fetchAddTrackToFavorites(this.props.currentTrack.id).then((res) => {}).catch(() => {
      this.setState({ is_favorite: false });
    });
  }

  RemoveTrackFromFavorites = (event) => {
    this.setState({ is_favorite: false });
    api.fetchRemoveTrackFromFavorites(this.props.currentTrack.id).then((res) => {}).catch(() => {
      this.setState({ is_favorite: true });
    });
  }

  handlePlay() {
    if(this.props.track_playing.id == this.props.currentTrack.id) {
      this.props.actions.setPlayStatus(common.AudioPlayerStatuses.PLAY);
    } else {
      this.props.actions.setTrackPlaying(this.props.currentTrack);
    }
  }

  handlePause() {
    this.props.actions.setPlayStatus(common.AudioPlayerStatuses.PAUSE);
  }

  renderTrackButton() {
    if (this.props.play_status === common.AudioPlayerStatuses.PAUSE && this.props.track_playing.id == this.props.currentTrack.id) {
      return (
        <button onClick={() => this.handlePlay()} className="btn btn-track-control btn-track-control__play">
          <div className="btn-track-control__container" dangerouslySetInnerHTML={{ __html: TrackSvg.playIcon }} />
        </button>
      );
    } else if (this.props.play_status === common.AudioPlayerStatuses.PLAY && this.props.track_playing.id == this.props.currentTrack.id) {
      return (
        <button onClick={() => this.handlePause()} className="btn btn-track-control btn-track-control__play">
          <div className="btn-track-control__container" dangerouslySetInnerHTML={{ __html: TrackSvg.pauseIcon }} />
        </button>
      );
    }
    return (
      <button onClick={() => this.handlePlay()} className="btn btn-track-control btn-track-control__play">
        <div className="btn-track-control__container" dangerouslySetInnerHTML={{ __html: TrackSvg.playIcon }} />
      </button>
    );
  }

  renderVersions() {
    const props = this.props;
    return (
      <div className="tooltip_versions show">
        <span className="tooltip_versions__text tooltip_versions__title">Versions by lenght</span>
        <span className={classNames('tooltip_versions__text', { hidden: props.currentTrack.versions[common.TrackVersions.main_track].url === null })}>Full Track - {common.timeFormatter(props.currentTrack.versions[common.TrackVersions.main_track].duration)}</span>
        <span className={classNames('tooltip_versions__text', { hidden: props.currentTrack.versions[common.TrackVersions.short_version].url === null })}>Short Track - {common.timeFormatter(props.currentTrack.versions[common.TrackVersions.short_version].duration)}</span>
        <span className={classNames('tooltip_versions__text', { hidden: props.currentTrack.versions[common.TrackVersions.medium_version].url === null })}>Medium Track - {common.timeFormatter(props.currentTrack.versions[common.TrackVersions.medium_version].duration)}</span>
        <span className={classNames('tooltip_versions__text', { hidden: props.currentTrack.versions[common.TrackVersions.long_version].url === null })}>Long Track - {common.timeFormatter(props.currentTrack.versions[common.TrackVersions.long_version].duration)}</span>
      </div>
    );
  }

  renderDownloadButton() {
    if (this.props.loggedIn) {
      return (
        <a
          href={this.props.currentTrack.versions.main_track.url}
          download
          className="btn btn-download"
          dangerouslySetInnerHTML={{ __html: TrackSvg.downloadIcon }}
        />
      );
    }
    return (
      <a
        onClick={() => this.props.actions.openModal('sign_up')}
        className="btn btn-download"
        dangerouslySetInnerHTML={{ __html: TrackSvg.downloadIcon }}
      />
    );
  }

  renderFavorites() {
    return (
      <div className="track-info-actions__item track-info-actions__item-like">
        <Like track={this.props.currentTrack} />
        <div className="track-info-actions__note">
          Add to favourites
        </div>
      </div>
    );
  }

  render() {
    if (!this.props.currentTrack || !this.props.currentTrack.user) { return false; }

    const { showBuyTrack } = this.props.actions;

    const versionUrls = [];
    _.forEach(this.props.currentTrack.versions, (version) => {
      if (version.url != null) {
        versionUrls.push(version.url);
      }
    });

    return (
      <div className="track-info">
        <Grid>
          <Row>
            <Col md={3}>
              <div style={{ backgroundImage: `url(${this.props.currentTrack.logo_medium})` }} className="track-info__image" />
              <div className="track-info__date-wrap">
                <span className="title track-info__date">Created {moment(this.props.currentTrack.created_at).format('DD/MM/YYYY')}</span>
              </div>
            </Col>
            <Col md={9}>
              <Row>
                <Col md={12}>
                  <div className="title track-info__track-name">{this.props.currentTrack.name}</div>
                  <Link to={`/author/${slug(this.props.currentTrack.user.name, { lower: true })}/${this.props.currentTrack.user.id}`} className="title track-info__author">{this.props.currentTrack.user.name}</Link>
                </Col>
                <Col md={2}>
                {this.props.loggedIn && this.props.currentUser.id === this.props.currentTrack.user.id && this.props.currentTrack.status === 0 &&
                    <div className="catalog__filters-wrapper">
                      <div className="track_draft">
                        Draft
                      </div>
                    </div>
                  }
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <div className="track-info__menu-controls">
                    {this.renderTrackButton()}
                    <div className="track-duration track-duration__current">{this.props.selectedType == common.TrackVersions.main_track ? this.props.posInSec : '0:00'}</div>
                    <div className="track-info__menu-controls__wave">
                      <WaveSurfer track={this.props.currentTrack} 
                                  width={654}
                                  height={76}
                                  selectedVersion={common.TrackVersions.main_track}
                      />
                      <div className="track-duration track-duration__left">{common.timeFormatter(~~(this.props.currentTrack.duration))}</div>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <div className="track-info-actions">
                    <div className="track-info-actions__inner-wrap">
                      <div className="track-info-actions__item track-info-actions__item-buy">
                        <a href="#" className="btn btn-primary" onClick={() => showBuyTrack(this.props.currentTrack)}>License</a>
                      </div>
                      {this.renderFavorites()}
                      <div className="track-info-actions__item track-info-actions__item-download">
                        {this.renderDownloadButton()}
                        <div className="track-info-actions__note">
                            Download preview
                        </div>
                      </div>
                    </div>
                    <div className="track-info-actions__item track-info-actions__item-version">
                      {this.state.showVersionsDropdown && this.renderVersions()}
                      <div className="versions-circle" onMouseEnter={this.showVersions} onMouseLeave={this.hideVersions}>
                        <span className="versions-circle__text">{versionUrls.length}</span>
                      </div>
                      <div className="track-info-actions__note">
                      Versions by lenght
                    </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  currentTrack: state.currentTrack.currentTrack,
  track_path: state.currentTrack.currentTrack.main_track,
  track_playing: state.audioPlayer.track_playing,
  posInSec: state.audioPlayer.posInSec,
  play_status: state.audioPlayer.play_status,
  selectedType: state.audioPlayer.selectedType,
  loggedIn: state.user.loggedIn,
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    AddTrackToFavorites,
    getCurrentTrack,
    setTrackPath,
    setPlayStatus,
    setTrackPlaying,
    showBuyTrack,
    openModal,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(TrackInfo);

