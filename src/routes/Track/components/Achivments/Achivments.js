import React from 'react';
import { Row, Col } from 'react-bootstrap';
import NoteGreenIcon from '../../../../assets/img/note-icon-green.png';
import NoteBlueIcon from '../../../../assets/img/note-icon-blue.png';
import HeadsetIcon from '../../../../assets/img/headset-icon.png';
import CakeIcon from '../../../../assets/img/cake-icon.png';

export const Achivments = () => (
  <div className="achivments-list">
    <div className="achivments-list-wrap">
      <div className="achivments-list__item">
        <div className="achivments-list__item-image">
          <img src={NoteGreenIcon} alt="" className="img-responsive" />
        </div>
      </div>
      <div className="achivments-list__item">
        <div className="achivments-list__item-image">
          <img src={CakeIcon} alt="" className="img-responsive" />
        </div>
      </div>
      <div className="achivments-list__item">
        <div className="achivments-list__item-image">
          <img src={NoteBlueIcon} alt="" className="img-responsive" />
        </div>
      </div>
      <div className="achivments-list__item">
        <div className="achivments-list__item-image">
          <img src={HeadsetIcon} alt="" className="img-responsive" />
        </div>
      </div>
      <div className="achivments-list__item">
        <div className="achivments-list__item-image">
          <img src={NoteGreenIcon} alt="" className="img-responsive" />
        </div>
      </div>
      <div className="achivments-list__item">
        <div className="achivments-list__item-image">
          <img src={CakeIcon} alt="" className="img-responsive" />
        </div>
      </div>
      <div className="achivments-list__item">
        <div className="achivments-list__item-image">
          <img src={NoteBlueIcon} alt="" className="img-responsive" />
        </div>
      </div>
    </div>
  </div>
    );

export default Achivments;
