import React from 'react';
import { IndexLink, Link } from 'react-router';
import 'styles/variables.scss';
import { Grid, Row, Col } from 'react-bootstrap';
import classNames from 'classnames';

class DropdownMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleTrackDropdown: false,
    };
    this.handleEvent = this.handleEvent.bind(this);
  }

  componentDidMount() {
    document.body.addEventListener('click', this.handleEvent);
  }

  componentWillUnmount() {
    document.body.removeEventListener('click', this.handleEvent);
  }

  handleEvent(e) {
    this.setState({
      visibleTrackDropdown: false,
    });
  }

  toggleTrackDropdown(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      visibleTrackDropdown: !this.state.visibleTrackDropdown,
    });
  }
  render() {
    return (
      // <div className='track__options' onClick={(e) => this.toggleTrackDropdown(e)}>
      <div>
        <div className="dropdown-menu__icon" dangerouslySetInnerHTML={{ __html: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_2" x="0px" y="0px" width="5px" height="19px" viewBox="27.167 1.083 5 19" enable-background="new 27.167 1.083 5 19" xml:space="preserve"><metadata>{/*?xpacket begin="" id="W5M0MpCehiHzreSzNTczkc9d"?*/}<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.5-c014 79.151739, 2013/04/03-12:12:15 "><rdf:rdf xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"><rdf:description xmlns:xmp="http://ns.adobe.com/xap/1.0/" xmlns:xmpgimg="http://ns.adobe.com/xap/1.0/g/img/" xmlns:xmptpg="http://ns.adobe.com/xap/1.0/t/pg/" xmlns:stdim="http://ns.adobe.com/xap/1.0/sType/Dimensions#" xmlns:xmpg="http://ns.adobe.com/xap/1.0/g/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xmpmm="http://ns.adobe.com/xap/1.0/mm/" xmlns:stref="http://ns.adobe.com/xap/1.0/sType/ResourceRef#" xmlns:stevt="http://ns.adobe.com/xap/1.0/sType/ResourceEvent#" rdf:about><xmp:creatortool>Adobe Illustrator CC (Macintosh)</xmp:creatortool><xmp:createdate>2017-04-07T12:29:26+03:00</xmp:createdate><xmp:metadatadate>2017-04-07T12:29:26+03:00</xmp:metadatadate><xmp:modifydate>2017-04-07T12:29:26+03:00</xmp:modifydate><xmp:thumbnails><rdf:alt><rdf:li rdf:parsetype="Resource"><xmpgimg:width>68</xmpgimg:width><xmpgimg:height>256</xmpgimg:height><xmpgimg:format>JPEG</xmpgimg:format><xmpgimg:image>/9j/4AAQSkZJRgABAgEASABIAAD/7QAsUGhvdG9zaG9wIDMuMAA4QklNA+0AAAAAABAASAAAAAEAAQBIAAAAAQAB/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgBAABEAwERAAIRAQMRAf/EAaIAAAAHAQEBAQEAAAAAAAAAAAQFAwIGAQAHCAkKCwEAAgIDAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAACAQMDAgQCBgcDBAIGAnMBAgMRBAAFIRIxQVEGE2EicYEUMpGhBxWxQiPBUtHhMxZi8CRygvElQzRTkqKyY3PCNUQnk6OzNhdUZHTD0uIIJoMJChgZhJRFRqS0VtNVKBry4/PE1OT0ZXWFlaW1xdXl9WZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3OEhYaHiImKi4yNjo+Ck5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6voRAAICAQIDBQUEBQYECAMDbQEAAhEDBCESMUEFURNhIgZxgZEyobHwFMHR4SNCFVJicvEzJDRDghaSUyWiY7LCB3PSNeJEgxdUkwgJChgZJjZFGidkdFU38qOzwygp0+PzhJSktMTU5PRldYWVpbXF1eX1RlZmdoaWprbG1ub2R1dnd4eXp7fH1+f3OEhYaHiImKi4yNjo+DlJWWl5iZmpucnZ6fkqOkpaanqKmqq6ytrq+v/aAAwDAQACEQMRAD8A9U4qsmmjiXk5p4DucVS+bUZn2T4F/HFUMzu32mLfM1xStBI3GKq0d3cR9HJHgdxihHW9/HKQr/A/bwOKorFXYqsmmWKMu3boPE4qk8sryuXc1J6DwxSsxV2KuxV2KuxVMbC7LfupD8X7J8fbFCNxVLNRm5zcB9lP14qhMUuxV2KuxV2KuxVtWKsGBoRuDiqcfWF+ret/k1p79KffihKHbk7N/MSfvxStxV2KuxV2KuxV2KuxVEeofqPD/L/Clf14oQ5FDQ4pdirsVdirsVdirsVdiqrxP1bl250/DFW7uP07hx2JqPkcUKOKXYq7FXYq7FXYq7FUw+rn9HUp8X2/8/oxQqX9uZU5qPjTt4jFUrxS7FXYq7FXYq7FURZ25mk3+wu7f0xQm2KuxVBXdhyJki+13Xx+WKpeyspIYUI6g4paxV2KuxVEW9nJMa/ZT+Y/wxQmkcaRoEQUAxVdirsVdiqySGKQUdQ368VQ7abAfsll9uuKtDS4u7t+GKqsdlbpuF5HxbfFVfFXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYqptdW69ZB9Br+rFVovLY/wC7B+OKqqujiqsGHsa4q3irsVdirsVULm8SEU+1J2X+uKpZLcTSn422/lHTFVPFLsVbVmU1UkHxGKo231FgQs24/n7/AE4oTAEEAg1B6HFXYqo3dwIYqj7Z2UYqlDMWJZjUnqcUtYq7FXYq7FXYqjLC6KMImPwN9n2OKEyxVKb6UvcMOyfCP44qh8UuxV2KuxV2KuxV2Kpr9ZP1L1f2qU/2XTFCVsSSSepNcUtYq7FXYq7FXYq7FXYqrcz9U4f5dfwxQpMCCQeoNMUtYq7FXYq7FXYq7FXYqrcD9U5/5dPwxQuvoilwx7P8Q/jiqHxS7FXYq7FXYq7FXYqmv1Y/UvS/apX/AGXXFC+7txNFQfbG6nFUoZSpKsKEdRilrFXYq7FXYq7FUZYWpdhKw+Bfs+5xQmWKuxVQubNJhX7MnZv64qlktvNEfjXb+YdMVU8UuxVtVZjRQSfAYqjbfTmJDTbD+Tv9OKEwAAAAFAOgxV2KuxV2KuxVTa1t26xj6BT9WKrRZ2w/3WPxxVVVEQUVQo9hTFW8VdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVWSTRRirsF/XiqHbUoB9kM3v0xVoapF3RvwxVVjvbd9g3E+DbYqr4q7FXYq7FUFd3/EmOL7XdvD5Yql7MzEljUnqTilrFXYq7FURb3kkJp9pP5T/DFCaRyJIgdDUHFV2KoW/uDEnBT8b9/AYqleKXYq7FXYq7FXYqiLO4MMm/2G2b+uKE2xVJ7uT1Lhz2BoPkMVUcUuxV2KuxV2KuxV2Kph9YP6OrX4vsf5/RihLyamuKXYq7FXYq7FXYq7FXYqq8j9W49udfwxVY68XZf5SR92KrcVdirsVdirsVdirsVRHpn6jz/wAv8KU/XihfqMPCbmPsv+vFUJil2KuxV2KuxV2KtqpZgoFSdgMVTj6uv1b0f8mlffrX78ULpoVljKN36HwOKpPLE8TlHFCOh8cUrMVdirsVdirsVTGwtCv72QfF+yPD3xQjcVdiqyaGOVeLivge4xVL5tOmTdPjX8cVQzI6/aUr8xTFK0AnYYqrR2lxJ0QgeJ2GKEdb2EcRDP8AG/bwGKorFXYq/wD/2Q==</xmpgimg:image></rdf:li></rdf:alt></xmp:thumbnails><xmptpg:maxpagesize rdf:parsetype="Resource"><stdim:w>1.763889</stdim:w><stdim:h>6.702778</stdim:h><stdim:unit>Millimeters</stdim:unit></xmptpg:maxpagesize><xmptpg:npages>1</xmptpg:npages><xmptpg:hasvisibletransparency>False</xmptpg:hasvisibletransparency><xmptpg:hasvisibleoverprint>False</xmptpg:hasvisibleoverprint><xmptpg:platenames><rdf:seq><rdf:li>Cyan</rdf:li><rdf:li>Magenta</rdf:li><rdf:li>Yellow</rdf:li></rdf:seq></xmptpg:platenames><xmptpg:swatchgroups><rdf:seq><rdf:li rdf:parsetype="Resource"><xmpg:groupname>Default Swatch Group</xmpg:groupname><xmpg:grouptype>0</xmpg:grouptype></rdf:li></rdf:seq></xmptpg:swatchgroups><dc:format>image/svg+xml</dc:format><xmpmm:derivedfrom rdf:parsetype="Resource"><stref:instanceid>xmp.iid:688e2f85-3d5f-4717-94d8-3872c6715518</stref:instanceid><stref:documentid>xmp.did:688e2f85-3d5f-4717-94d8-3872c6715518</stref:documentid><stref:originaldocumentid>xmp.did:375c040f-05db-49bd-a191-f1cf3f56ad6a</stref:originaldocumentid></xmpmm:derivedfrom><xmpmm:documentid>xmp.did:ef022741-46bd-4c8f-a76a-32a203110ea2</xmpmm:documentid><xmpmm:instanceid>xmp.iid:ef022741-46bd-4c8f-a76a-32a203110ea2</xmpmm:instanceid><xmpmm:originaldocumentid>xmp.did:375c040f-05db-49bd-a191-f1cf3f56ad6a</xmpmm:originaldocumentid><xmpmm:history><rdf:seq><rdf:li rdf:parsetype="Resource"><stevt:action>saved</stevt:action><stevt:instanceid>xmp.iid:375c040f-05db-49bd-a191-f1cf3f56ad6a</stevt:instanceid><stevt:when>2017-04-07T12:22:56+03:00</stevt:when><stevt:softwareagent>Adobe Illustrator CC (Macintosh)</stevt:softwareagent><stevt:changed>/</stevt:changed></rdf:li><rdf:li rdf:parsetype="Resource"><stevt:action>saved</stevt:action><stevt:instanceid>xmp.iid:ef022741-46bd-4c8f-a76a-32a203110ea2</stevt:instanceid><stevt:when>2017-04-07T12:29:26+03:00</stevt:when><stevt:softwareagent>Adobe Illustrator CC (Macintosh)</stevt:softwareagent><stevt:changed>/</stevt:changed></rdf:li></rdf:seq></xmpmm:history></rdf:description></rdf:rdf></x:xmpmeta>{/*?xpacket end="w"?*/}</metadata><g><circle fill="#E9E8E8" cx="29.667" cy="3.583" r="2.5" /><circle fill="#E9E8E8" cx="29.667" cy="10.583" r="2.5" /><circle fill="#E9E8E8" cx="29.667" cy="17.583" r="2.5" /></g></svg>' }} />
        <ul className={classNames('dropdown-menu', { 'dropdown-menu_visible': this.state.visibleTrackDropdown })}>
          <li className="dropdown-menu__item" onClick={() => this.toggleTrackDropdown()}>Add to Favs</li>
          <li className="dropdown-menu__item" onClick={() => this.toggleTrackDropdown()}>Download Preview</li>
          <li className="dropdown-menu__item" onClick={() => this.toggleTrackDropdown()}>License Track</li>
          <li className="dropdown-menu__item" onClick={() => this.toggleTrackDropdown()}>View Artist</li>
          <li className="dropdown-menu__item" onClick={() => this.toggleTrackDropdown()}>View Details</li>
        </ul>
      </div>
    );
  }
}

export default DropdownMenu;
