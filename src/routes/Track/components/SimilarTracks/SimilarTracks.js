import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import TrackList from 'components/TrackList';
import DropdownTrackOptions from '../DropdownTrackOptions';
import TrackInfoImage from 'assets/img/track-img-big1.png';
import MusicWave from 'assets/img/waves.png';
import { TrackSvg } from 'styles/svgs/svgs';
import TrackItem from 'components/TrackItem';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';

class SimilarTracks extends React.Component {
  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.similar_tracks);
    this.props.actions.setTrackPlaying(playTrack);
  }

  renderSimilarTracks() {
    const nodes = [];
    this.props.similar_tracks.forEach((item, i) => {
      nodes.push(
        <Col md={3} sm={6} xs={12} key={i}>
          <TrackItem dropDownStyle="dark" track={item} userInfo={item.user} hasTrackMenu setTracks={playTrack => this.setTracks(playTrack)} />
        </Col>,
      );
    });

    return nodes;
  }

  render() {
    if (!this.props.similar_tracks || this.props.similar_tracks.length == 0) { return false; }

    return (
      <div className="similar-tracks">
        <Grid>
          <Row>
            <Col md={12}>
              <Row>
                <Col md={12}>
                  <div className="title title_primary align_left title_grey">Similar Tracks</div>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  {this.renderSimilarTracks()}
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  similar_tracks: state.currentTrack.currentTrack.similar_tracks,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTracks,
    setTrackPlaying,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(SimilarTracks);

