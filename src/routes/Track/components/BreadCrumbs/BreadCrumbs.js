import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Breadcrumb, Grid, Row, Col } from 'react-bootstrap';


class BreadCrumbs extends React.Component {
  render() {
    return (
      <div className="breadcrumb__wrap">
        <Grid>
          <Row>
            <Col md={12}>
              <Breadcrumb>
                <Breadcrumb.Item href="#">
                  Home
                </Breadcrumb.Item>
                <Breadcrumb.Item href="http://getbootstrap.com/components/#breadcrumbs">
                  Music
                </Breadcrumb.Item>
                <Breadcrumb.Item active>
                  Ambient
                </Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({

  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(BreadCrumbs);

