import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import TrackList from '../../../../components/TrackList';
import AuthorCard from '../AuthorCard';
import SelectizeSimpleSelectTheme from '../SelectizeSimpleSelectTheme';
import PaginationAdvanced from '../../../../components/Pagination';
import { getUserInfo } from 'store/user';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';

class MoreAuthorTracks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allPages: Math.ceil(props.currentTrack.more_tracks.length / 4),
      currentPage: 1,
      perPageTracks: this.sortByDate(props.currentTrack.more_tracks).slice(0, 4),
      sortType: 'Date',
    };
  }

  componentWillMount() {
    this.props.actions.getUserInfo(this.props.currentTrack.user.id);
  }

  static propTypes = {
    currentTrack: PropTypes.object,
  }

  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.currentTrack.more_tracks);
    this.props.actions.setTrackPlaying(playTrack);
  }

  chooseSortType(sortType, array) {
    const offset = this.state.currentPage * 4;
    if (sortType === 'Date') {
      this.setState({
        perPageTracks: this.sortByDate(array).slice(offset - 4, offset),
        sortType,
      });
      return this.sortByDate(array);
    } else if (sortType === 'Alphabet') {
      this.setState({
        perPageTracks: this.sortByAlphabet(array).slice(offset - 4, offset),
        sortType,
      });
      return this.sortByAlphabet(array);
    }
  }

  sortByDate(array) {
    return array.sort((a, b) => Date.parse(b.created_at) - Date.parse(a.created_at));
  }

  sortByAlphabet(array) {
    return array.sort((a, b) => a.name.localeCompare(b.name));
  }

  changePage(current) {
    const offset = current * 4;
    const perSlide = this.chooseSortType(this.state.sortType, this.props.currentTrack.more_tracks).slice(offset - 4, offset);

    this.setState({
      perPageTracks: perSlide,
      currentPage: current,
    });
  }

  render() {
    return (
      <div className="author-tracks">
        <Grid>
          <Row>
          {this.props.currentTrack.more_tracks.length > 1 ? (<Col md={9}>
              <Row>
                <Col md={7}>
                  <div className="title title_primary align_left title_grey">More Author’s Tracks</div>
                </Col>
                <Col md={5}>
                  <div className="sort-panel flexbox justify-end">
                    <span className="sort-panel__title sort-panel__title__nowrap">Sort by</span>
                    <SelectizeSimpleSelectTheme chooseSortType={(sortType, array) => this.chooseSortType(sortType, this.props.currentTrack.user.tracks)} />
                  </div>
                </Col>
              </Row>
              <TrackList
                tracks={this.state.perPageTracks}
                userInfo={this.props.currentTrack.user}
                viewAuthorTracks
                setTracks={playTrack => this.setTracks(playTrack)}
              />
              <PaginationAdvanced changePage={this.changePage.bind(this)} allPages={this.state.allPages} />
            </Col>) : (
              <Col md={9}>
                <Row>
                  <div className="title title_primary align_left title_grey text-midlle">This author doesn’t have other tracks yet</div>
                </Row>
              </Col>
            )
            }
            <Col md={3}>
              <AuthorCard currentTrack={this.props.currentTrack} />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.user.userInfo,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getUserInfo,
    setTracks,
    setTrackPlaying,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(MoreAuthorTracks);

