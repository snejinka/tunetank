import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import BreadCrumbs from '../BreadCrumbs';
import TrackInfo from '../TrackInfo';
import TrackDetails from '../TrackDetails';
import MoreAuthorTracks from '../MoreAuthorTracks';
import SimilarTracks from '../SimilarTracks';
import Spinner from 'components/Spinner';
import { getCurrentTrack } from '../../modules/currentTrack';

class TrackView extends React.Component {
  componentWillMount() {
    this.props.actions.getCurrentTrack(this.props.params.trackSlug, this.props.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.id !== nextProps.params.id) {
      this.props.actions.getCurrentTrack(nextProps.params.trackSlug, nextProps.params.id);
    }
  }

  render() {
    if (!this.props.hasCurrentTrack) { return <Spinner />; }

    return (
      <div className="wrapper-fluid">
        { this.props.hasCurrentTrack &&
        <div className="wrapper-fluid">
          <BreadCrumbs />
          <TrackInfo currentTrack={this.props.currentTrack} />
          <TrackDetails currentTrack={this.props.currentTrack} />
          <MoreAuthorTracks currentTrack={this.props.currentTrack} />
          <SimilarTracks />
        </div>
        }
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  currentTrack: state.currentTrack.currentTrack,
  hasCurrentTrack: Object.keys(state.currentTrack.currentTrack).length > 0,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getCurrentTrack,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(TrackView);
