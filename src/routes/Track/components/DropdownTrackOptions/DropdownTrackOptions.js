import React from 'react';
import { Row, Col, DropdownButton, MenuItem } from 'react-bootstrap';

export const DropdownTrackOptions = () => (
  <DropdownButton title="" id="bg-nested-dropdown" className="dropdown-options dropdown-menu__icon">
    <MenuItem eventKey="1">Add to Favs</MenuItem>
    <MenuItem eventKey="2">Download Preview</MenuItem>
    <MenuItem eventKey="1">License Track</MenuItem>
    <MenuItem eventKey="2">View Artist</MenuItem>
    <MenuItem eventKey="1">View Details</MenuItem>
  </DropdownButton>
     );

export default DropdownTrackOptions;
