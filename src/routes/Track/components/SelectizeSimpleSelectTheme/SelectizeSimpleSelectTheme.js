import React, { PropTypes } from 'react';
import { ReactSelectize, SimpleSelect } from 'react-selectize';
import 'react-selectize/themes/index.css';

class SelectizeSimpleSelectTheme extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {
        label: 'Date',
        value: 'Date',
      },
    };
  }

  static propTypes = {
    chooseSortType: PropTypes.func,
  }

  chooseSortType(sortType, array) {
    this.props.chooseSortType(sortType.value);
  }

  render() {
    const options = ['Date', 'Alphabet', 'Popular'].map(option => ({ label: option, value: option }));
    return (<SimpleSelect
      options={options}
      placeholder=""
      className="selectize-select-theme react-default-template"
      defaultValue={{
        label: 'Date',
        value: 'Date',
      }}
      onValueChange={function (newValue, callback) {
        this.setState({ value: newValue }, callback);
        this.chooseSortType(newValue);
      }.bind(this)}
    />);
  }
    }
export default SelectizeSimpleSelectTheme;

