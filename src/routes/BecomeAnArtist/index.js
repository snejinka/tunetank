import BecomeAnArtist from './BecomeAnArtist';

export default store => ({
  path: 'become-an-artist',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, BecomeAnArtist);
    }, 'BecomeAnArtist');
  },
});
