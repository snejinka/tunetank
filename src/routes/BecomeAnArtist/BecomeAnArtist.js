import React, { PropTypes } from 'react';
import 'styles/variables.scss';
import { Grid, Row, Col } from 'react-bootstrap';


import Scroll from 'react-scroll';
var Link       = Scroll.Link;
var Element    = Scroll.Element;
var Events     = Scroll.Events;
var scroll     = Scroll.animateScroll;
var scrollSpy  = Scroll.scrollSpy;

class BecomeAnArtist extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    Events.scrollEvent.register('begin', function(to, element) {
      console.log("begin", arguments);
    });

    Events.scrollEvent.register('end', function(to, element) {
      console.log("end", arguments);
    });

    scrollSpy.update();
  }

  componentWillUnmount(){
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col md={12}>
            <Row>
              <Col md={2}>
                <div className="scroll-content-nav">
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-200} to="what-is-tunetank" spy={true} smooth={true} duration={500}>What is Tunetank</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-250} to="how-to-use" spy={true} smooth={true} duration={500}>How to use</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-250} to="exclusivity" spy={true} smooth={true} duration={500}> Content exclusivity </Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-210} to="rates-and-payments" spy={true} smooth={true} duration={500}>Rates and Payments</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-150} to="quality-criteria" spy={true} smooth={true} duration={500}>Quality Criteria</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-200} to="responsibilities" spy={true} smooth={true} duration={500}>Responsibilities</Link>
                </div>

              </Col>
              <Col md={8}>
                <div className="scroll-content__container">
                  <h1 className="title title_primary title_grey text-center">Become an Artist</h1>
                  <p className="scroll-content__text">Join Tunetank and start your music journey with us. </p>
                  <p className="scroll-content__text">If you want to become an artist with Tunetank, read this post to find all the answers you’ve been looking for. If you looked through the text, but still can’t find the details you need, contact us - we’d be happy to help you.</p>
                  <div className="scroll-content__separator"></div>
                  <Element name="what-is-tunetank" id="what-is-tunetank" className="element" >
                    <h2 className="scroll-content__title-second">What is Tunetank?</h2>
                    <p className="scroll-content__text">a new free royalty music platform of excellently crafted tracks. We tried our best not to become messy, cumbersome place that gives nothing but a headache and made all tracks easily accessible to save your time and nerves. Tunetank is a new player in the world of free trial music apps, however we offer you a great time together and are really waiting for you.</p>
                    <p className="scroll-content__text">Royalty free is not about being available for free - it means that  the purchases buys one-off licence each time he needs to use the needed track. The revenue from purchasing the licence goes to both Tunetank and the artist (the artist’s rate depends on whether the music is Tunetank-exclusive – take a look at section 2 below).</p>
                  </Element>
                  <Element name="how-to-use" id="how-to-use" className="element" >
                    <h2 className="scroll-content__title-second">How To use</h2>
                    <p className="scroll-content__text">After you’ve decided to sell your music on our website, the first step to take is submitting request to become a seller on Tunetank.  We add your enquiry to the review list, so you can get a response from Tunetank within two weeks after you sent the form.</p>
                  </Element>
                  <Element name="exclusivity" id="exclusivity" className="element" >
                    <h2 className="scroll-content__title-second">Content exclusivity </h2>
                    <p className="scroll-content__text">For artists, exclusive means that your items are exclusively available on one specific website, so we are the only one who can represent your music. It means, that your tracks won’t be available on any other royalty free music library catalog.</p>
                    <p className="scroll-content__text">Tunetank welcomes exclusive music, but soon we’re planning to accept non-exclusive tracks as well. This way, Tunetank will help you increase your portfolio by adding non-exclusive music. But still, right now your Tunetank portfolio must contain exclusive content only.</p>
                  </Element>
                  <Element name="rates-and-payments" id="rates-and-payments" className="element" >
                    <h2 className="scroll-content__title-second">Rates and Payments</h2>
                    <p className="scroll-content__text">We’d try to make our rates as clear as any math hater could understood it. Here’s what we’ve got: </p>
                    <table className="scroll-content__table">
                      <thead>
                        <tr>
                          <td className="scroll-content__text">Exclusive</td>
                          <td className="scroll-content__text">Non-Exclusive</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="scroll-content__text">
                            50%-70% (Depends on the number of sales)
                          </td>
                          <td className="scroll-content__text">
                            35% (Coming soon)
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <p className="scroll-content__text">When I get money for tracks sold? Tunetank’s pay-outs are made every month via PayPal. In order for a pay-out to be made, you must have at least $50 in your Tunetank’s account.</p>
                    <p className="scroll-content__text">Discounts –  To boost prospectives’ engagement, we will offer time-limited discounts (mostly no more than 20%). When tracks are purchased at a discount, your income will be calculated from the discounted total. It means that the discount will be split between yourself and Tunetank.</p>
                  </Element>
                  <Element name="quality-criteria" id="quality-criteria" className="element" >
                    <h2 className="scroll-content__title-second">Quality Criteria</h2>
                    <p  className="scroll-content__text scroll-content__list-title">Tunetank cares about a music reviewing process. Each track that passes the upload stage then will be deeply reviewed by Tunetank experts. To refine the review process, we use a grading system where every track is rated out of 6 areas, which are:</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Production quality</li>
                      <li className="scroll-content__text content-list__item">Mastery of music</li>
                      <li className="scroll-content__text content-list__item">Composition cohesiveness</li>
                      <li className="scroll-content__text content-list__item">Instruments/samples quality </li>
                      <li className="scroll-content__text content-list__item">Emotive behavior </li>
                      <li className="scroll-content__text content-list__item">Commercial relevance / usability</li>
                    </ul>
                    <p  className="scroll-content__text scroll-content__list-title">The rate you earn on selling tracks depends on how many tracks you’ve already sold. First you’re given 50% from a licence sold, then the stake rises. This works the following way:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">if you’ve sold from 501 to 1000 tracks you get 55%;</li>
                      <li className="scroll-content__text content-list__item">if you’ve sold from 1001 to 2000 tracks, you’re given 60%;</li>
                      <li className="scroll-content__text content-list__item">if you’ve sold from 2001 to 3000 tracks, you get 65%;</li>
                      <li className="scroll-content__text content-list__item">If you’ve sold more than 3000 tracks, you’ll get 70% from a licence sold.</li>
                    </ul>
                    <h3 className="scroll-content__title-third">Can I delete an uploaded track?</h3>
                    <p className="scroll-content__text">You can delete any track you've uploaded on Tunetank in 3 months after the date of publication.</p>
                    <h3 className="scroll-content__title-third">How to use advanced search options?</h3>
                    <p className="scroll-content__text">Combine the points above in the Advanced Search to mix genres, moods and instruments. One more option is that you can refine your tracks search by choosing a certain length or tempo, the same goes for other parameters. </p>
                  </Element>
                  <Element name="responsibilities" id="responsibilities" className="element">
                    <h2 className="scroll-content__title-second">As a seller you’re responsible for the following:</h2>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">you use music in accordance with the terms of use; </li>
                      <li className="scroll-content__text content-list__item">the track is of good quality and meets the purpose for which it is “sold”;</li>
                      <li className="scroll-content__text content-list__item">the track matches the description you give on the track preview page;</li>
                      <li className="scroll-content__text content-list__item">your music is 100% original, so you don’t infringe on the valid intellectual property of anyone else. You’re not allowed to sell sound-alike tracks and samples on Tunetank.</li>
                      <li className="scroll-content__text content-list__item">the track and its description do not break any applicable law or regulation (including those governing export control, consumer protection, unfair competition, criminal law, pornography, anti-discrimination, trade practices or fair trading);</li>
                      <li className="scroll-content__text content-list__item">the track is non-virused and doesn’t contain other computer codes, files or programs that destroy your computer data or hardware. </li>
                      <li className="scroll-content__text content-list__item">if the track is is marked as 'supported', they will provide you with the services as outlined in the item support policy.</li>
                      <li className="scroll-content__text content-list__item">the track itself, its and profile description fit well with our website design. </li>
                      <li className="scroll-content__text content-list__item">the track lengths is longer than 90 seconds. </li>
                    </ul>
                  </Element>
                </div>
              </Col>
            </Row>

          </Col>
        </Row>
      </Grid>
    );
  }
}


export default BecomeAnArtist;
