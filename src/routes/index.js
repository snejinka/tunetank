import CoreLayout from '../layouts/CoreLayout';
import Authorization from './Authorization';
import Home from './Home';
import Author from './Author';
import Track from './Track';
import AuthService from 'utils/AuthService';
import UploadItem from './UploadItem';
import Catalog from './Catalog';
import BecomeSeller from './BecomeSeller';
import ShoppingCart from './ShoppingCart';
import Checkout from './Checkout';
import FAQ from './FAQ';
import BecomeAnArtist from './BecomeAnArtist';
import LicenseTerms from './LicenseTerms';

const config = { auth0_client_id: process.env.AUTH0_CLIENT_ID, auth0_domain: process.env.AUTH0_DOMAIN };
export const auth = new AuthService(config.auth0_client_id, config.auth0_domain);

export const requireAuth = (nextState, replace) => {
  if (!auth.loggedIn()) {
    replace({ pathname: '/' });
  }
};

export const createRoutes = store => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Home(store),
  auth,
  childRoutes: [
    Authorization(store),
    Author(store),
    Track(store),
    UploadItem(store),
    Catalog(store),
    BecomeSeller(store),
    ShoppingCart(store),
    Checkout(store),
    FAQ(store),
    BecomeAnArtist(store),
    LicenseTerms(store),
  ],
});

export default createRoutes;
