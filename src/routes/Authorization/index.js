export default store => ({
  path: 'authorization',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const Authorization = require('./components/Authorization').default;
      cb(null, Authorization);
    }, 'Authorization');
  },
});
