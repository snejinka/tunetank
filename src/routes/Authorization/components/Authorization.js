import React from 'react';
import Spinner from '../../../components/Spinner';
import 'styles/authorization.scss';

const Authorization = () => (
  <div className="spinner-block">
    <Spinner />
  </div>
);

export default Authorization;
