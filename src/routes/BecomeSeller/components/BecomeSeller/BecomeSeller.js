import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getCurrentUser, setTab, openModal } from 'store/user';
import slug from 'slug';
import { WORLD_SVG } from 'styles/svgs/svgs.js';
import toastr from 'toastr';
import { Grid, Row, Col } from 'react-bootstrap';
import { fetchBecomeASeller } from '../../../../api';
import SliderSlick from '../SliderSlick';
import flagUsa from '../../../../assets/img/usa-flag.png';
import flagJapan from '../../../../assets/img/japan-flag.png';
import flagGermany from '../../../../assets/img/germany-flag.png';

class BecomeSeller extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
    };
  }

  handleBecomeSeller() {
    this.props.actions.getCurrentUser().then((user) => {
      if(user == null) {
        this.props.actions.openModal('sign_up');
      } else {
        this.setState({ spinner: true });
        fetchBecomeASeller(user.id).then((data) => {
          if (data.status === 200) {
            this.props.actions.setTab('main');
            window.location.href = `/author/${slug(this.props.currentUser.name, { lower: true })}/${this.props.currentUser.id}`;
          }
        }).catch(() => {
          toastr.error('Something went wrong. Please try again.');
          this.setState({ spinner: false });
        });
      }
    });
  }

  render() {
    return (
      <div className="seller">
        <div className="seller-intro">
          <div className="container">
            <h1 className="seller__header">Tank of royalty-free music</h1>
            <div className="seller__header-remark">
              <div className="seller__header-remark_line">
                Make money from your music as a hobby or make it full time
              </div>
              profession without major record labels.
              </div>
            <button
              onClick={this.handleBecomeSeller.bind(this)} style={{
                width: '185px',
              }} className="btn btn-primary btn-primary_role_start"
            >
              <span className="btn-primary__text">
                {!this.state.spinner && 'Start Selling'}
                {this.state.spinner && <i className="fa fa-spinner fa-spin" />}
              </span>
            </button>
          </div>
        </div>
        <div className="seller-incom">
          <div className="container">
            <h4 className="seller-incom__header">Calculate your Income</h4>
            <div className="seller-incom__inner">
              <div className="seller-incom__description">
                Upload your work once and then earn 100 times selling tracks to Tunetank buyers: whether they are from the USA, Japan or Germany.
              </div>
              <div className="map">
                <div className="map__flags-group">
                  <div
                    dangerouslySetInnerHTML={{
                      __html: WORLD_SVG.world_map,
                    }}
                  />
         

                  <img className="flag flag__germany" src={flagGermany}/>
                  
                                    <img className="flag flag__usa" src={flagUsa}/>
                  
                                    <img className="flag flag__japan" src={flagJapan}/> 
                  />
                </div>
              </div>
            </div>
            <div className="sellers-feature">
              <div className="sellers-feature__title">
                <div className="sellers-feature__title-line">Seller gets 50% from each license sold. When you sell more than</div><div className="sellers-feature__title-line">500 tracks, the stake rises. See the way it works</div>
                </div>
              <div className="sellers-statistic-box">
                <div className="sellers-statistic-box__title-group">
                  <div className="sellers-statistic-box__title">
                    Percentage of income earned
                  </div>
                  <div className="sellers-statistic-box__title">
                    Number of downloaded tracks
                  </div>
                </div>
                <div className="sellers-statistic-box__description">
                  <ul className="img-list">
                    <li className="img-list__item">
                      <div className="img-list__item-note">
                        55%
                      </div>
                      <div className="img-list__item-title">
                        501-1000
                      </div>
                    </li>
                    <li className="img-list__item">
                      <div className="img-list__item-note">
                        60%
                      </div>
                      <div className="img-list__item-title">
                        1001-2000
                      </div>
                    </li>
                    <li className="img-list__item">
                      <div className="img-list__item-note">
                        65%
                      </div>
                      <div className="img-list__item-title">
                        2001-3000
                      </div>
                    </li>
                    <li className="img-list__item">
                      <div className="img-list__item-note">
                        70%
                      </div>
                      <div className="img-list__item-title">
                        3001+
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="seller-stock">
          <Grid>
            <div className="seller-stock__header-group">
              <div className="seller-stock__header">
                <div className="seller-stock__header-line">
                  Become an affiliate and
                </div>
                get 2 bonuses
                </div>
              <div className="seller-stock__header-remark">
                Boost your income with Tunetank affiliate program.
              </div>
            </div>
            <Row>
              <Col md={6} className="stock-card">
                <div className="stock-card__count">
                  30%
                </div>
                <div className="stock-card__description">
                  <div className="stock-card__title">
                    Get 30% from 1st sale
                  </div>
                  <div className="stock-card__note">
                    From referred users who sign up and purchase a license with Tunetank.
                  </div>
                </div>
              </Col>
              <Col md={6} className="stock-card">
                <div className="stock-card__count">
                  +1%
                </div>
                <div className="stock-card__description">
                  <div className="stock-card__title">
                    +1% income increase
                  </div>
                  <div className="stock-card__note">
                    For every 50-th customer you bring to Tunetank.
                  </div>
                </div>
              </Col>
            </Row>
          </Grid>

        </div>
        <div className="seller-slider-wrap">
          <Grid>
            <SliderSlick />
          </Grid>
        </div>
      </div>
    );
  }
  }

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getCurrentUser,
    setTab,
    openModal
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(BecomeSeller);
