import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import { SellerSlider } from 'styles/svgs/svgs.js';
import { getCurrentUser, setTab } from 'store/user';
import slug from 'slug';
import toastr from 'toastr';
import { fetchBecomeASeller } from '../../../../api';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class SliderSlick extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
    };
  }

  handleBecomeSeller() {
    this.setState({ spinner: true });
    this.props.actions.getCurrentUser().then(user => fetchBecomeASeller(user.id)).then((data) => {
      if (data.status === 200) {
        this.props.actions.setTab('main');
        window.location.href = `/author/${slug(this.props.currentUser.name, { lower: true })}/${this.props.currentUser.id}`;
      }
    }).catch(() => {
      toastr.error('Something went wrong. Please try again.');
      this.setState({ spinner: false });
    });
  }

  render() {
    return (
      <OwlCarousel className="seller-slider" items={1} dots={false} loop nav>
        <div className="seller-slider__item">
          <div className="seller-slider__item-inner">
            <div
              className="seller-slider__item-image" dangerouslySetInnerHTML={{
                __html: SellerSlider.slide_1,
              }}
            />
            <div className="seller-slider__item-description">
              <div className="seller-slider__item-title">
                Income <span className="bold">Tracking</span>
              </div>
              <div className="seller-slider__item-note">
                Use Tunetank’s dashboard and graphics to take control of earnings and comply with latest market trends.
              </div>
            </div>
          </div>
          <div className="seller-slider__action">
            <button
              onClick={this.handleBecomeSeller.bind(this)} style={{
                width: '185px',
              }} className="btn btn-primary btn-primary_role_start"
            >
              <span className="btn-primary__text">
                {!this.state.spinner && 'Start Selling'}
                {this.state.spinner && <i className="fa fa-spinner fa-spin" />}
              </span>
            </button>
          </div>
        </div>
        <div className="itseller-slider__item">
          <div className="seller-slider__item-inner">
            <div
              className="seller-slider__item-image" dangerouslySetInnerHTML={{
                __html: SellerSlider.slide_2,
              }}
            />
            <div className="seller-slider__item-description">
              <div className="seller-slider__item-title">
                <span className="bold">No</span> License <span className="bold">Limits</span>
              </div>
              <div className="seller-slider__item-note">
                Upload track once and earn money from every purchase of it.
              </div>
            </div>
          </div>
          <div className="seller-slider__action">
            <button
              onClick={this.handleBecomeSeller.bind(this)} style={{
                width: '185px',
              }} className="btn btn-primary btn-primary_role_start"
            >
              <span className="btn-primary__text">
                {!this.state.spinner && 'Start Selling'}
                {this.state.spinner && <i className="fa fa-spinner fa-spin" />}
              </span>
            </button>
          </div>
        </div>
        <div className="seller-slider__item">
          <div className="seller-slider__item-inner">
            <div
              className="seller-slider__item-image" dangerouslySetInnerHTML={{
                __html: SellerSlider.slide_3,
              }}
            />
            <div className="seller-slider__item-description">
              <div className="seller-slider__item-title">
                Music <span className="bold">Quality</span>
              </div>
              <div className="seller-slider__item-note">
                If your track is truly cool, be sure, it won't sink in a pile of poorly-written audios.
              </div>
            </div>
          </div>
          <div className="seller-slider__action">
            <button
              onClick={this.handleBecomeSeller.bind(this)} style={{
                width: '185px',
              }} className="btn btn-primary btn-primary_role_start"
            >
              <span className="btn-primary__text">
                {!this.state.spinner && 'Start Selling'}
                {this.state.spinner && <i className="fa fa-spinner fa-spin" />}
              </span>
            </button>
          </div>
        </div>
      </OwlCarousel>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getCurrentUser,
    setTab,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SliderSlick);
