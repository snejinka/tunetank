import BecomeSeller from './components/BecomeSeller';

export default store => ({
  path: 'become-a-seller',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, BecomeSeller);
    }, 'BecomeSeller');
  },
});
