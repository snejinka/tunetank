import { injectReducer } from '../../store/reducers';
import HomeView from './components/HomeView';

export default store => ({
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, HomeView);
    }, 'HomeView');
  },
});
