import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IndexLink, Link } from 'react-router';
import 'styles/variables.scss';
import { Grid, Row, Col } from 'react-bootstrap';
import classNames from 'classnames';
import Spinner from '../../../../components/Spinner';
import moment from 'moment';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';
import TrackItem from '../../../../components/TrackItem';


class FeaturedTracks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tracks: this.props.tracks.slice(0, 8),
      sortTracks: [],
    };
  }

  sortByDate() {
    return this.props.tracks.sort((a, b) => Date.parse(b.created_at) - Date.parse(a.created_at));
  }

  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.all_featured_tracks);
    this.props.actions.setTrackPlaying(playTrack);
  }

  render() {
    return (
      <div className="featured-tracks">
        <Grid>
          <div className="track__wrap">
            <Row>
              <Col md={12}>
                <h1 className="title title_primary">Featured Tunes</h1>
              </Col>
            </Row>
            {this.props.tracks.length > 0 ?
              <div>
                <Row>
                  { this.sortByDate().map((track, i) => (
                    <Col md={3} sm={6} xs={12} key={i}>
                      <TrackItem dropDownStyle="dark" track={track} hasTrackMenu setTracks={playTrack => this.setTracks(playTrack)} />
                    </Col>
                      ))
                    }
                </Row>
                <Row>
                  <Col md={12}>
                    <Link className="btn btn-primary" to={{ pathname: '/catalog', query: { featured: true } }}>
                        View More Tunes
                      </Link>
                  </Col>
                </Row>
              </div>
            :
              <Spinner />
            }
          </div>

        </Grid>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  tracks: state.tracks.featured.slice(0, 8),
  all_featured_tracks: state.tracks.featured,
  tracks_player: state.audioPlayer.tracks,
  track_playing: state.audioPlayer.track_playing,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTracks, setTrackPlaying,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(FeaturedTracks);
