import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import 'styles/variables.scss';
import Slider from 'react-slick';
import { Grid, Row, Col } from 'react-bootstrap';
import TrackItem from 'components/TrackItem';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';
import { setTab } from 'store/user';
import { browserHistory } from 'react-router';
import slug from 'slug';
import { tabs } from 'common';

const numberOfResultsPerSlide = 8;

class FollowingFeed extends React.Component {

  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.allTracks);
    this.props.actions.setTrackPlaying(playTrack);
  }

  handleViewAlltracks() {
    this.props.actions.setTab(tabs.followingFeed);
    browserHistory.push({ pathname: `/author/${slug(this.props.currentUser.name, { lower: true })}/${this.props.currentUser.id}` });
  }

  renderStatic() {
    return (
      <Col md={12}>
        <Row>
          {this.props.allTracks.map((track, i) => (
            <Col md={3} sm={6} xs={12} key={i}>
              <TrackItem dropDownStyle="medium" track={track} hasTrackMenu setTracks={playTrack => this.setTracks(playTrack)} />
            </Col>
          ))}
        </Row>
      </Col>
    );
  }

  renderSlide() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
    };

    const slides = [];
    let countSlides = Math.ceil(this.props.allTracks.length / numberOfResultsPerSlide);
    if (countSlides > 4) {
      countSlides = 4;
    }

    for (let i = 1; i <= countSlides; i++) {
      const offset = i * numberOfResultsPerSlide;
      slides.push(
        <Col md={12} key={i}>
          <Row>
            {this.props.allTracks.slice(offset - numberOfResultsPerSlide, offset).map((track, i) => (
              <Col md={3} sm={6} xs={12} key={i}>
                <TrackItem dropDownStyle="medium" track={track} hasTrackMenu setTracks={playTrack => this.setTracks(playTrack)} />
              </Col>
            ))}
          </Row>
        </Col>,
      );
    }

    return (
      <Slider {...settings}>
        {slides}
      </Slider>
    );
  }

  renderDescription() {
    if (this.props.is_featured) {
      return <div className="title title_secondary">Want to be updated on tracks of your favourite authors?<br /> Follow them and you’ll see all the recent tracks here and on your page.</div>;
    }
    return <div className="title title_secondary">New tracks from authors you follow</div>;
  }

  render() {
    if (this.props.currentUser !== null) {
      return (
        <div className="following-feed">
          <Grid>
            <div className="track__wrap">
              <Row>
                <Col md={12}>
                  <h1 className="title title_primary">Following Feed</h1>
                  {this.renderDescription()}
                </Col>
              </Row>
              <Row>
                {this.props.allTracks.length < 9 ? this.renderStatic() : this.renderSlide()}
              </Row>

              <Row>
                <Col md={12}>
                  <a className="btn btn-primary" onClick={() => this.handleViewAlltracks()}>
                    View All Tunes
                  </a>
                </Col>
              </Row>
            </div>

          </Grid>
          {(this.props.is_featured) && (<div className="following-feed_disabled" />)}
        </div>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  allTracks: state.followingFeed.followingTracks,
  currentUser: state.user.currentUser,
  is_featured: state.followingFeed.is_featured,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTracks, setTrackPlaying, setTab,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FollowingFeed);
