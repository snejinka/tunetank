import React, { PropTypes } from 'react';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock, Modal, Button, Popover, Tooltip, ButtonToolbar } from 'react-bootstrap';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import slug from 'slug';

class ModalCreateTrack extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
    };
  }

  static propTypes = {
    template: PropTypes.bool,
  }

  showModal = () => {
    this.setState({ show: true });
  };

  hideModal = () => {
    this.setState({ show: false });
  };

  render() {
    const backdropStyle = {
      zIndex: 'auto',
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
    };
    return (
      <ButtonToolbar>
        <Button bsStyle="primary" onClick={this.showModal}>
            Track created
          </Button>
        <Modal
          {...this.props}
          backdropStyle={backdropStyle}
          show={this.state.show}
          onHide={this.hideModal}
          dialogClassName="modal-default modal-message"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg" className="title_primary title_primary__inner">Track {this.props.template && 'and template'} is created!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="modal-note">
              Great, your track {this.props.template && 'and template'} is created. <br />
              It will be going through moderating before being published.
            </div>
          </Modal.Body>
          <Modal.Footer>
            <div className="modal-note">
              You can track it’s status on my <Link to={`/author/${slug(this.props.currentUser.name, { lower: true })}/${this.props.currentUser.id}`} className="modal-note__link"> My tracks page. </Link>
            </div>
          </Modal.Footer>
        </Modal>
      </ButtonToolbar>
    );
  }
}

const mapStateToProps = (state, props) => ({
  currentUser: state.user.currentUser,
});

export default connect(mapStateToProps, {})(ModalCreateTrack);
