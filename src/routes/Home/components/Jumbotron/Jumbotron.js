import React from 'react';
import { Link } from 'react-router';
import 'styles/variables.scss';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import AutosuggestInput from 'components/AutosuggestInput';
import { browserHistory } from 'react-router';
import { FilterFields, changeFilterValue } from 'store/filters';

class Jumbotron extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClickOurCollection() {
    this.props.actions.changeFilterValue(FilterFields.search_string, '');
    browserHistory.push({ pathname: '/catalog', query: {} });
  }

  render() {
    return (
      <div className="jumbotron-home">
        <Grid>
          <div className="jumbotron-home__inner">
            <Row className="jumbotron-home__inner-wrap">
              <Col md={6}>
                <div className="jumbotron-home__inner-right">
                  <Row>
                    <div className="col-md-12">
                      <div className="title title_secondary">Energize your content with top musicians tracks!</div>
                      <h1 className="title title_primary">Royalty Free music for your creative projects &mdash; </h1>
                    </div>
                  </Row>
                  <Row>
                    <Col md={10}>
                      <div>
                        <div className="input_search-wrap">
                          <AutosuggestInput />
                          <Link
                            to={{ pathname: '/catalog', query: { q: this.props.search_string } }}
                            className="btn btn_search"
                          >
                            <i className="fa fa-search" aria-hidden="true" />
                          </Link>
                        </div>
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={2}>
                      <a
                        className="btn btn_angle link-color"
                        onClick={this.handleClickOurCollection.bind(this)}
                      >
                        Our collection
                      </a>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col md={6}>
                <span className="jumbotron-home__img" />
              </Col>
            </Row>
            <Row className="jumbotron-home__btn-group">
              <Col md={2} mdOffset={4} sm={4} smOffset={2}>
                <Link
                  to={{ pathname: '/catalog', query: { sorting: 2 } }}
                  className="btn btn_light"
                >
                  Popular files
                </Link>
              </Col>
              <Col md={2} sm={4}>
                <Link
                  to={{ pathname: '/catalog', query: { sorting: 2, time_selection: 3 } }}
                  className="btn btn_light"
                >
                  Top new files
                </Link>
              </Col>
            </Row>
          </div>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  search_string: state.catalogFilters.filters.search_string,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    changeFilterValue,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Jumbotron);
