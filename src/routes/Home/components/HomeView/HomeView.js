import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './HomeView.scss';
import Jumbotron from '../Jumbotron';
import FeaturedTracks from '../FeaturedTracks';
import FollowingFeed from '../FollowingFeed';
import NewTracks from '../NewTracks';
import Advantages from '../Advantages';
import Newsletter from '../Newsletter';
import AuthService from 'utils/AuthService';
import ModalSignUp from 'components/ModalSignUp';
import ModalCreateTrack from '../ModalCreateTrack';
import * as common from 'common';
import { changeFilterValue,
        FilterFields } from 'store/filters';
import { getFollowingTracks } from 'store/followingFeed.js';
import { getFeaturedTracks, getGenres, fetchNewTracks, getNewTracks } from 'store/tracks';


class HomeView extends React.Component {
  componentWillMount() {
    this.loadData();
  }

  componentDidMount() {
    ReactDOM.findDOMNode(this).scrollTop = 0;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.currentUser && nextProps.currentUser) {
      if (this.props.currentUser.id != nextProps.currentUser.id) {
        this.loadData();
      }
    }
  }

  loadData() {
    this.props.actions.changeFilterValue(FilterFields.search_string, '');
    this.props.actions.getFeaturedTracks();
    this.props.actions.getGenres().then(() => {
      this.props.actions.fetchNewTracks();
    });
    this.props.actions.getFollowingTracks();
  }

  render() {
    return (
      <div className="full-fill">
        <div className="flexbox hidden">
          { this.props.location.query.qrhl && <ModalSignUp />}
          {(this.props.location.query.qct || this.props.location.query.qtt) && <ModalCreateTrack template={!!this.props.location.query.qtt} />}
        </div>
        <Jumbotron />
        <FeaturedTracks />
        <FollowingFeed />
        <NewTracks />
        <Advantages />
        <Newsletter />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getFeaturedTracks,
    getFollowingTracks,
    getGenres,
    fetchNewTracks,
    getNewTracks,
    changeFilterValue,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(HomeView);

