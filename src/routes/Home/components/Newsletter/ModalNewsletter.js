import React, { PropTypes } from 'react';
import { Modal, ButtonToolbar } from 'react-bootstrap';

class ModalNewsletter extends React.Component {
  constructor() {
    super();
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  static propTypes = {
    show: PropTypes.bool,
    close: PropTypes.func,
  }

  handleKeyPress(e) {
    if (e.charCode == 13 || e.charCode == 27) {
      this.props.close();
    }
  }

  render() {
    const backdropStyle = {
      zIndex: 'auto',
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
    };

    return (
      <ButtonToolbar>
        <Modal
          backdropStyle={backdropStyle}
          show={this.props.show}
          onHide={this.props.close}
          onKeyPress={this.handleKeyPress}
          dialogClassName="modal-default"
        >
          <Modal.Header closeButton style={{ paddingBottom: '0' }}>
            <Modal.Title id="contained-modal-title-lg" className="title_primary title_primary__inner">Thank you</Modal.Title>
          </Modal.Header>
          <Modal.Body className="title_primary title_primary__inner" style={{ fontSize: '30px', paddingBottom: '50px' }}>
                    for subscribing to our newsletter!
                </Modal.Body>
          <Modal.Footer>
            <div className="flexbox justify-center align-center">
              <div className="action-link title title-note" style={{ cursor: 'pointer' }} onClick={this.props.close}>Continue browsing!</div>
            </div>
          </Modal.Footer>
        </Modal>
      </ButtonToolbar>
    );
  }
}

export default ModalNewsletter;
