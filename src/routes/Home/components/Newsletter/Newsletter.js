import React from 'react';
import { IndexLink, Link } from 'react-router';
import 'styles/variables.scss';
import { Grid, Row, Col } from 'react-bootstrap';
import ModalNewsletter from './ModalNewsletter';
import * as common from 'common';
import { fetchAddNewsletterEmail } from 'api';
import toastr from 'toastr';

class Newsletter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      email: '',
    };
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeEmail(e) {
    const value = e.target.value;
    this.setState({ email: value });
  }

  handleCloseModal() {
    this.setState({ modal: false });
  }

  handleSubmit() {
    const email = this.state.email;
    if (common.validateEmail(email)) {
      fetchAddNewsletterEmail(email).then(() => {
        this.setState({
          email: '',
          modal: true,
        });
      }).catch((error) => {
        if (error.response.status == 422 && error.response.data.message == 'The following errors were found: Email is already taken') {
          this.setState({ email: '' }, () => toastr.warning('You are already subscribed to the newsletter!'));
        }
      });
    } else {
      toastr.error('Empty or invalid email. Please try again.');
    }
  }

  render() {
    return (
      <div className="newsletter jumbotron-home">
        <Grid>
          <div className="jumbotron-home__inner">
            <Row>
              <Col md={8} mdOffset={2}>
                <div className="title title_primary">Newsletter</div>
                <div className="title title_small">Subscribe for new content, updates, surveys & offers.</div>
                <input
                  type="text"
                  value={this.state.email}
                  onChange={this.handleChangeEmail}
                  className="input-primary form-control"
                  placeholder="E-mail Address"
                />
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                <a className="btn btn-success" onClick={this.handleSubmit}>SUBSCRIBE</a>
              </Col>
            </Row>
          </div>

        </Grid>
        <ModalNewsletter
          show={this.state.modal}
          close={this.handleCloseModal.bind(this)}
        />
      </div>
    );
  }
}

export default Newsletter;
