import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IndexLink, Link } from 'react-router';
import 'styles/variables.scss';
import { Grid, Row, Col, Tab, Collapse, NavItem } from 'react-bootstrap';
import classnames from 'classnames';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';
import DropdownMenu from '../DropdownMenu';
import TrackItem from '../../../../components/TrackItem';
import { findTrackByGenre } from 'store/tracks';
import Spinner from '../../../../components/Spinner';

class NewTracks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: props.activeTab || 0,
      visibleFilter: false,
      spinner: false,
      selectedGenre: null,
    };
    this.handleSelect = this.handleSelect.bind(this);
  }

  setAudioPlayerTracks(playTrack) {
    this.props.setTracks(this.props.newTracks);
    this.props.setTrackPlaying(playTrack);
  }

  toggleFilter() {
    if (this.state.visibleFilter) {
      this.setState({
        visibleFilter: false,
        selectedGenre: null,
      });
    } else {
      this.setState({
        visibleFilter: true,
      });
    }
  }


  handleSelect(selectedTab) {
    this.setState({
      activeTab: selectedTab,
    });
  }

  handleSelectGenre(selectedGenre) {
    this.setState({ spinner: true });
    this.props.findTrackByGenre(selectedGenre).then(() => this.setState({ selectedGenre: selectedGenre.name, spinner: false }));
  }

  renderTracks() {
    const tracks = this.state.selectedGenre == null ? this.props.newTracks : this.props.selectedGenreTracks;
    if (!tracks) { return false; }

    return (
      <Grid className="new-tracks__container">
        <Row>
          <Col md={12}>
            <div className="track__wrap row">
              {tracks.map((newTrack, i) =>
                <Col md={3} sm={6} xs={12} key={i}>
                  <TrackItem dropDownStyle="light" track={newTrack} hasTrackMenu setTracks={(playTrack) => { this.setAudioPlayerTracks(playTrack); }} />
                </Col>,
              )}
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Link
              to={{ pathname: '/catalog', query: { sorting: 1 } }}
              className="btn btn-primary"
            >
                  View More Tunes
            </Link>
          </Col>
        </Row>
      </Grid>
    );
  }

  renderSpinner() {
    return (
      <Grid className="new-tracks__container">
        <Row>
          <Spinner />
        </Row>
      </Grid>
    );
  }

  render() {
    if (!this.props.hasGenres) { return null; }

    return (
      <div className="new-tracks">
        <Grid>
          <div className="track__wrap">
            <Row>
              <Col md={12}>
                <h1 className="title title_primary">New Tunes</h1>
              </Col>
            </Row>
          </div>
        </Grid>
        <Tab.Container id="track__tabs" activeKey={this.state.activeTab} defaultActiveKey={0} onSelect={this.handleSelect}>
          <div>
            <button className="title title__filter" onClick={() => this.toggleFilter()}>Filter</button>
            <Collapse in={this.state.visibleFilter}>
              <div className="nav-tabs__wrap">
                <Grid className="nav-tabs container">
                  {this.props.genres.map((genre, i) =>
                    <NavItem
                      className={classnames({ active: genre.name == this.state.selectedGenre })}
                      eventKey={i}
                      key={i}
                      onClick={() => this.handleSelectGenre(genre)}
                    >
                      {genre.name}
                    </NavItem>,
                    )}
                </Grid>
              </div>
            </Collapse>
            {(this.props.newTracks.length == 0 || this.state.spinner) ? this.renderSpinner() : this.renderTracks()}
          </div>
        </Tab.Container>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  genres: state.tracks.genres,
  hasGenres: state.tracks.genres.length > 0,
  newTracks: state.tracks.newTracks,
  selectedGenreTracks: state.tracks.selectedGenreTracks,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  findTrackByGenre,
  setTracks,
  setTrackPlaying,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(NewTracks);
