import { requireAuth } from '../../routes/index';
import Cart from './components/Cart';

export default () => ({
  path: 'cart',
  onEnter: requireAuth,
  getComponent(nextStatse, cb) {
    require.ensure([], () => {
      cb(null, Cart);
    }, 'Cart');
  },
});
