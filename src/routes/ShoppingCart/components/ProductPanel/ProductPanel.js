import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import pluralize from 'pluralize';
import { Link } from 'react-router';
import CartList from '../CartList';
import accounting from 'accounting';
import { clearCart, calcTotalAmount } from 'store/cart';

const ProductPanel = props => (
  <div className="product-panel">
    <div className="product-panel-header">
      <div className="product-panel-title">
        {pluralize('item', props.items.length, true)} in your cart
      </div>

      <div className="product-panel-header__action-group">
        <Link
          to={'/catalog'}
          className="btn-primary btn-primary__big"
        >
          Keep Browsing
        </Link>
        <button
          className="btn-secondary"
          onClick={props.actions.clearCart}
        >
          Clear Cart
        </button>
      </div>
    </div>
    <div className="product-panel-content">
      <div className="product-panel-content__top">
        <CartList />
      </div>
      <div className="product-panel-content__bottom">
        <button
          className="btn-primary btn-primary__big"
          onClick={() => props.handleCheckout()}
        >
          Checkout
        </button>
        <div className="total-price">
          <div className="total-price__title">
            Total:
          </div>
          <div className="total-price__value">
            {accounting.formatMoney(props.actions.calcTotalAmount())}
          </div>
        </div>
      </div>
    </div>
  </div>
);

const mapStateToProps = state => ({
  items: state.cart.items,
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    clearCart,
    calcTotalAmount,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductPanel);
