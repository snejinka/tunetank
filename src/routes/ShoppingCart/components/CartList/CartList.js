import React from 'react';
import { connect } from 'react-redux';
import CartListItem from '../CartListItem';

const CartList = props => (
  <div className="cart-list">
    {props.items.map(item => <CartListItem key={`track-${item.id}-${item.license.id}`} item={item} />)}
  </div>
);

const mapStateToProps = state => ({
  items: state.cart.items,
});

export default connect(mapStateToProps, null)(CartList);
