import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  licenses,
  increaseItemQuantity,
  reduceItemQuantity,
  removeItemFromCart,
  setLicense,
  setQuantity,
} from 'store/cart';
import { SimpleSelect } from 'react-selectize';
import 'react-selectize/themes/index.css';
import { Link } from 'react-router';
import slug from 'slug';
import { Row, Col, DropdownButton, MenuItem } from 'react-bootstrap';

class CartListItem extends Component {
  constructor(props) {
    super(props);

    this.handleQuantityChange = this.handleQuantityChange.bind(this);
  }

  handleQuantityChange(event) {
    const re = /^[0-9\b]+$/;
    if (event.target.value === '' || re.test(event.target.value)) {
      this.props.actions.setQuantity(event.target.value, this.props.item.id);
    }
  }

  render() {
    const { item, actions } = this.props;
    const options = licenses.map(license => ({ label: license.name, value: license.id }));

    return (
      <div className="cart-list-item">
        <div className="cart-list-item__track">
          <div className="track track__shopping-cart">
            <div
              className="track__img"
              style={item.logo !== null && { backgroundImage: `url(${item.logo})` }}
            />
            <div className="track__info">
              <Link
                to={`/track/${slug(item.name, { lower: true })}/${item.id}`}
                className="track__name"
              >
                { item.name }
              </Link>
              <Link
                to={`/author/${slug(item.user.name, { lower: true })}/${item.user.id}`}
                className="track__author"
              >
                { item.user.name }
              </Link>
            </div>
          </div>
        </div>
        <div className="cart-list-item__counter">
          <div className="counter">
            <div className="counter-remove">
              <button
                className="btn__remove"
                onClick={() => actions.reduceItemQuantity(item.id)}
              />
            </div>
            <div className="counter-current-value">
              <input
                type="text"
                className="counter-field"
                value={item.quantity || 0}
                onChange={this.handleQuantityChange}
              />
            </div>
            <div className="counter-add">
              <button
                className="btn__add"
                onClick={() => actions.increaseItemQuantity(item.id)}
              />
            </div>
          </div>
        </div>
        <div className="cart-list-item__type">
          <SimpleSelect
            options={options}
            defaultValue={options.find(option => option.value === item.license.id)}
            onValueChange={newLicense => this.props.actions.setLicense(newLicense.value, item.id)}
            className="selectize-select-theme react-default-template"
          />
        </div>
        <div className="cart-list-item__help">
          {/* <div className="helper">
            ?
          </div> */}
          <DropdownButton title="?" id="bg-nested-dropdown" className="dropdown-options dropdown-download dropdown-helper">
              <MenuItem eventKey="1">To get more info about the licenses, read the <Link className="link-inside-link" to="http://34.213.133.50/license-terms">license terms </Link></MenuItem>
          </DropdownButton>
        </div>
        <div className="cart-list-item__price">
          {item.amount_in_cents}
        </div>
        <div className="cart-list-item__remove">
          <button
            className="btn-remove"
            onClick={() => actions.removeItemFromCart(item.id)}
          />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    increaseItemQuantity,
    reduceItemQuantity,
    removeItemFromCart,
    setLicense,
    setQuantity,
  }, dispatch),
});

export default connect(null, mapDispatchToProps)(CartListItem);
