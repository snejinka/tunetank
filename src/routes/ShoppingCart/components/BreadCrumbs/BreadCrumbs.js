import React from 'react';
import { Breadcrumb, Grid, Row, Col } from 'react-bootstrap';

const BreadCrumbs = () => (
  <div className="breadcrumb__wrap">
    <Grid>
      <Row>
        <Col md={12}>
          <Breadcrumb>
            <Breadcrumb.Item href="#">
              Home
            </Breadcrumb.Item>
            <Breadcrumb.Item active>
              Shopping Cart
            </Breadcrumb.Item>
          </Breadcrumb>
        </Col>
      </Row>
    </Grid>
  </div>
);

export default BreadCrumbs;
