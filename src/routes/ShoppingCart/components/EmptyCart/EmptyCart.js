import React from 'react';
import { Link } from 'react-router';

const EmptyCart = () => (
  <div className="product-panel">
    <div className="product-panel-content-empty">
      <div className="product-panel-content-empty__title">
        Your shopping cart is empty.
      </div>
      <div className="product-panel-content-empty__description">
        Discover most <Link to="/catalog" className="link link-dark">popular tracks</Link>.
      </div>
    </div>
  </div>
);

export default EmptyCart;
