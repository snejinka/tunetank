import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import toastr from 'toastr';
import BreadCrumbs from '../BreadCrumbs';
import ProductPanel from '../ProductPanel';
import CartSummary from '../CartSummary';
import EmptyCart from '../EmptyCart';
import { openModal } from 'store/user.js';
import { isCartEmpty } from 'store/cart';

class Cart extends Component {
  constructor(props) {
    super(props);

    this.handleCheckout = this.handleCheckout.bind(this);
  }

  handleCheckout() {
    if (this.props.actions.isCartEmpty()) {
      toastr.error('You should select at least one license.');
    } else if (this.props.loggedIn) {
      browserHistory.push({ pathname: '/checkout' });
    } else {
      this.props.actions.openModal('sign_in');
    }
  }

  render() {
    const { props } = this;
    return (
      <div>
        {/* <BreadCrumbs /> */}
        <Grid className="shopping-cart">
          <Row>
            <Col md={12}>
              <div className="shopping-cart__header">
                <div className="title title_primary">
                  Shopping Cart
                </div>
              </div>
            </Col>
          </Row>

          {props.items.length === 0 &&
            <Row>
              <Col md={9}>
                <EmptyCart />
              </Col>
            </Row>
          }
          {props.items.length !== 0 &&
            <Row>
              <Col md={9}>
                <ProductPanel handleCheckout={this.handleCheckout} />
              </Col>
              <Col md={3}>
                <CartSummary handleCheckout={this.handleCheckout} />
              </Col>
            </Row>
          }
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  items: state.cart.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    isCartEmpty,
    openModal,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
