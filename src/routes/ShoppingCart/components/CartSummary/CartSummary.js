import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import accounting from 'accounting';
import { calcTotalAmount } from 'store/cart';
import pluralize from 'pluralize';

const CartSummary = props => (
  <div className="cart-summary">
    <div className="cart-summary__header">
      Shopping Cart Total
    </div>
    <div className="cart-summary__info">
      <div className="cart-summary__value">
        {accounting.formatMoney(props.actions.calcTotalAmount())}
      </div>
      <div className="cart-summary__remark">
        {pluralize('item', props.items.length, true)} in your cart
      </div>
    </div>
    <div className="cart-summary__action">
      <button
        className="btn-primary btn-primary__big"
        onClick={() => props.handleCheckout()}
      >
        Checkout
      </button>
    </div>
    <div className="cart-summary__remark">
      Secure Payment
    </div>
  </div>
);

const mapStateToProps = state => ({
  items: state.cart.items,
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    calcTotalAmount,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CartSummary);
