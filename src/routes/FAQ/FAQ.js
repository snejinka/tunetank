import React, { PropTypes } from 'react';
import 'styles/variables.scss';
import { Grid, Row, Col } from 'react-bootstrap';
import ContentIdImage from '../../assets/img/ContentID.png'
import ContentIdImage2 from '../../assets/img/ContentID2.png'
import CopyrightNotice from '../../assets/img/youtubeCopyright.png'
import CopyrightNotice2 from '../../assets/img/youtubeCopyright2.png'
import CopyrightNotice3 from '../../assets/img/youtubeCopyright3.png'
import ReasonImage from '../../assets/img/reasonImage.png'
import UnlistedVideoImage from '../../assets/img/unlisted.png'


import Scroll from 'react-scroll';
var Link       = Scroll.Link;
var Element    = Scroll.Element;
var Events     = Scroll.Events;
var scroll     = Scroll.animateScroll;
var scrollSpy  = Scroll.scrollSpy;

class FAQ extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    Events.scrollEvent.register('begin', function(to, element) {
      console.log("begin", arguments);
    });

    Events.scrollEvent.register('end', function(to, element) {
      console.log("end", arguments);
    });

    scrollSpy.update();
  }

  componentWillUnmount(){
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col md={12}>
            <Row>
              <Col md={2}>
                <div className="scroll-content-nav">
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-300} to="licensing" spy={true} smooth={true} duration={500}>Licensing</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-250} to="my-account" spy={true} smooth={true} duration={500}>My Account</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-250} to="payments" spy={true} smooth={true} duration={500}>Payments</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-210} to="music-rights" spy={true} smooth={true} duration={500}>Music rights</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-250} to="music-search" spy={true} smooth={true} duration={500}>Music Search</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-150} to="downloading" spy={true} smooth={true} duration={500}>Downloading</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-250} to="you-tube" spy={true} smooth={true} duration={500}>YouTube</Link>
                </div>

              </Col>
              <Col md={8}>
                <div className="scroll-content__container">
                  <h1 className="title title_primary title_grey text-center">FAQ</h1>
                  <Element name="licensing" id="licensing" className="element" >
                    <h2 className="scroll-content__title-second">Licensing</h2>
                    <h3 className="scroll-content__title-third">What does my licence cover?</h3>
                    <p className="scroll-content__text">Your licence lets you add music to one project and covers both sync and dubbing rights. It doesn’t include performing rights - normally hosting platforms like Youtube or broadcasters like TV channels are responsible for that, so they can be cleared directly with performing rights societies.For getting a licence that covers several projects or videos at once, contact our account managers.</p>
                    <h3 className="scroll-content__title-third">I’ve bought and paid for the track I don’t need, what to do?</h3>
                    <p className="scroll-content__text">Contact us and show your email receipt to let us send you the correct one.</p>
                    <h3 className="scroll-content__title-third">How does Tunetank’s licensing work?</h3>
                    <p className="scroll-content__text">We share the licence in accordance with use scenarios. Every licence is valid globally, forever - ? and works on multiple platforms. Take a look at our licences’ description to see which one fits you the best.</p>
                    <h3 className="scroll-content__title-third">Which one do I need? </h3>
                    <p className="scroll-content__text">Read out licences’ description to see which licence is good for you.</p>
                    <h3 className="scroll-content__title-third">What if I need multi-use licence?</h3>
                    <p className="scroll-content__text">All Tunetank's licences are for a single track for a single project. If you need to licence music for more than a single use, buy 2 or more licences for that.</p>
                  </Element>
                  <Element name="my-account" id="my-account" className="element" >
                    <h2 className="scroll-content__title-second">My account</h2>
                    <h3 className="scroll-content__title-third">Can I listen to tracks before buying them?</h3>
                    <p className="scroll-content__text">Sure. Listen to trial tracks to get an idea how complete track versions sound. Register to try this out.</p>
                    <h3 className="scroll-content__title-third">Where can I get my invoice, licence and purchased  tracks?</h3>
                    <p className="scroll-content__text">This all will appear in your account section.</p>
                  </Element>
                  <Element name="payments" id="payments" className="element" >
                    <h2 className="scroll-content__title-second">Payments</h2>
                    <h3 className="scroll-content__title-third">How to purchase tracks?</h3>
                    <p className="scroll-content__text">Choose a track, click on “Licence”, then you go to “Shopping Cart” section, right from where you choose “Checkout” and input your data.</p>
                    <h3 className="scroll-content__title-third">Can I pay with PayPal?</h3>
                    <p className="scroll-content__text">Online customers are able to use paypal or credit card as a payment option.</p>
                    <h3 className="scroll-content__title-third">I don’t like the purchased track, can I get my money back?</h3>
                    <p className="scroll-content__text">We cannot promise a refund if no technical defect founded and 48 hours had passed after you notified us. To ask for a refund, contact us and we’ll help you. The money will be refunded to a credit card you used when purchasing and will processed within 15 working days.</p>
                  </Element>
                  <Element name="music-rights" id="music-rights" className="element" >
                    <h2 className="scroll-content__title-second">Music Rights</h2>
                    <h3  className="scroll-content__title-third">What rights do I need to clear when adding tracks to videos?</h3>
                    <p  className="scroll-content__text scroll-content__list-title">Below are the rights you need to know when adding music to your video content:</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Sync rights - cover the composition and song lyrics and mostly held by music publishers.</li>
                      <li className="scroll-content__text content-list__item">Master rights - cover actual recorded version of the song, mostly held by the record label.</li>
                      <li className="scroll-content__text content-list__item">Mechanical rights - clear these rights in case you’re planning DVD production This is done through rights society or with the publisher.</li>
                      <li className="scroll-content__text content-list__item">Performance rights - clear these rights if you’re planning TV broadcast or online production. TV channel or a website are mostly responsible for that.</li>
                    </ul>
                    <p  className="scroll-content__text scroll-content__list-title">To clear these rights you can try out the following options:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">Commercial music - if you’re going to use a track by your beloved artist, contact his music publisher that holds the sync rights and the record label that owns the master rights. They can refuse your request for getting a licence, so there’s a chance you won’t be able to use the track.</li>
                      <li className="scroll-content__text content-list__item">Cover versions - if you’re planning to record your own version of a song you really love, you still have to clear the sync and mechanical rights with the publisher. The same goes for a character in your production singing part of the song in the background.</li>
                      <li className="scroll-content__text content-list__item">Specially composed music - hiring a composer to write a song for you. Composer’s fee should contain both sync and master rights, but still ensure the composition is truly original to avoid any copyright issues.</li>
                      <li className="scroll-content__text content-list__item">Production music - the most cost effective option. Production music libraries contain as much music genres as commercial music. The libraries hold both sync and master rights, so they can clear them in one licence.</li>
                    </ul>
                  </Element>
                  <Element name="music-search" id="music-search" className="element" >
                    <h2 className="scroll-content__title-second">Music search</h2>
                    <h3  className="scroll-content__title-third">How can I search for new tracks?</h3>
                    <p  className="scroll-content__text scroll-content__list-title">To find a new track for your video, enter strings of words or adjectives into into the Quick Search box. Here is what you can try out: </p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Mood - atmospheric, mellow, dramatic</li>
                      <li className="scroll-content__text content-list__item">Instrument - guitar, organ, violin</li>
                      <li className="scroll-content__text content-list__item">Genre - pop, Indie, dubstep</li>
                    </ul>
                    <h3  className="scroll-content__title-third">How to browse tracks?</h3>
                    <p  className="scroll-content__text scroll-content__list-title">You can browse music in these ways:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">Collections: all the tracks you can sort by tracks’ length and upload date</li>
                      <li className="scroll-content__text content-list__item">Featured tracks: most popular tracks of a week </li>
                      <li className="scroll-content__text content-list__item">New tracks: latest tracks you can filter by your favourite genre</li>
                      <li className="scroll-content__text content-list__item">Artists: find new tracks or listen to favourite ones from your beloved artists </li>
                    </ul>
                    <h3 className="scroll-content__title-third">Can I delete an uploaded track?</h3>
                    <p className="scroll-content__text">You can delete any track you've uploaded on Tunetank in 3 months after the date of publication.</p>
                    <h3 className="scroll-content__title-third">How to use advanced search options?</h3>
                    <p className="scroll-content__text">Combine the points above in the Advanced Search to mix genres, moods and instruments. One more option is that you can refine your tracks search by choosing a certain length or tempo, the same goes for other parameters. </p>
                  </Element>
                  <Element name="downloading" id="downloading" className="element">
                    <h2 className="scroll-content__title-second">Downloading</h2>
                    <h3  className="scroll-content__title-third">How can I download tracks I’ve purchased?</h3>
                    <p className="scroll-content__text scroll-content__list-title">Once you have purchased a track you can log in to your online account and download it by these steps:</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Go to your account page, and click on the “Downloads” link</li>
                      <li className="scroll-content__text content-list__item">In “My downloads” section, click on the “Download” button on the right side of the order you wish to download</li>
                      <li className="scroll-content__text content-list__item">Choose either “Download ZIP” to get tracks or “Download licence” to receive a document with licence you’ve chosen.</li>
                    </ul>
                    <h3 className="scroll-content__title-third">Can I reload a track I’ve accidentally deleted?</h3>
                    <p className="scroll-content__text">Sure. Once you’ve paid for a music file it stays in your account, so you can download it as many times as you need.</p>
                  </Element>
                  <Element name="you-tube" id="you-tube" className="element">
                    <h2 className="scroll-content__title-second">YouTube</h2>
                    <h3  className="scroll-content__title-third">What is YouTube Content ID?</h3>
                    <p className="scroll-content__text">With Content ID copyright owners can determine Youtube content that belongs to them. If you’re a Youtube creator, spend time to understand what music you’re allowed to use in your videos as this might influence the status of your uploaded content.</p>
                    <h3 className="scroll-content__title-third">What is YouTube monetisation?</h3>
                    <p className="scroll-content__text">Monetisation means making money from advertising based on how many people view your videos.</p>
                    <p className="scroll-content__text">To monetize your video, open your Youtube channel homescreen and choose ‘Video Manager’ from the menu. Then find a submenu with the label ‘Monetization’.</p>
                    <p className="scroll-content__text">Monetisation asks you to agree to Youtube’s advertising terms and conditions requires. Once you agreed to them, you can allow ads to be displayed alongside your videos and Youtube will give you a portion of ads revenue. Once you have agreed to these terms, you can allow ads to run alongside your videos and YouTube will give you a portion of the ads generated revenue with you.</p>
                    <h3 className="scroll-content__title-third">What is Google AdSense?</h3>
                    <p className="scroll-content__text">an advertising tool that lets video creators display targeted ads on websites and make money from people’s views and clicks. The video element is integrated into YouTube, so you might know about this as about the way of monetising. Google gives more detailed information about this.</p>
                    <h3 className="scroll-content__title-third">Why am I seeing ads on my video?</h3>
                    <p className="scroll-content__text">Ads mostly appear on videos where copyrighted music is used. The portion of the revenue goes to our composers so they can make more tracks.</p>
                    <p className="scroll-content__text">To disable third-party ads or to monetise your YouTube content, buy a Creator + or Professional licence.</p>
                    <h3 className="scroll-content__title-third">Should I purchase a licence to use music in my Youtube videos?</h3>
                    <p className="scroll-content__text">For personal and non-commercial use you can try out our Individual licence. However if you’re going to earn money on your video content, you can buy Professional or Marketing licence. </p>
                    <h3 className="scroll-content__title-third">What licence I need to purchase for a Youtube advert? </h3>
                    <p className="scroll-content__text">If you going to use your video as an advert, you can try out one of our licences that suits your company’s budget. </p>
                    <h3 className="scroll-content__title-third">Does my Tunetank licence include ad-blocking?</h3>
                    <p className="scroll-content__text">An Individual licence won’t disable ads on your videos. If you intend to disable third party ads or monetise your videos on Youtube, purchase a Professional, Marketing or Enterprise licence.  </p>
                    <h3 className="scroll-content__title-third">Why am I seeing a claim notice on my YouTube account?</h3>
                    <p className="scroll-content__text">YouTube’s Content ID system find videos that use copyrighted stuff and serves a notice after detecting this. If you own the correct licence you can use our YouTube monetisation code to clear this claim. After you’ve entered your code, you are given up to 48 hours for your claim to clear. </p>
                    <p className="scroll-content__text">To get more details about YouTube Content ID see our links section. If you bought an Individual licence and do not want to earn money on your videos then no further action is needed.</p>
                    <p className="scroll-content__text">If you aren’t going to monetise your video, acknowledge the claim and move on.</p>
                    <h3 className="scroll-content__title-third">What is a YouTube monetisation code?</h3>
                    <p className="scroll-content__text">Our YouTube monetisation code is required to clear YouTube copyright claims that occur when you use Tunetank music in your videos but we have developed this code to speed up the process for you.</p>
                    <h3 className="scroll-content__title-third">How to use Youtube monetisation code?</h3>
                    <p className="scroll-content__text scroll-content__list-title">Here are the the instructions you can follow to clear your video with the monetisation code:</p>
                    <ol className="content-list content-list_decimal">
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">In Video Manager, select Copyright Notices.</span></li>
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">Underneath your video, click the link that says Includes copyrighted content</span></li>
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">Click File a Dispute and then choose I have a licence or permission from the proper rights holder to use this material.</span></li>
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">After confirming you wish to dispute, put your code in the Reason for Dispute box</span></li>
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">Your video will be cleared within 48 hours</span></li>
                    </ol>
                    <h3 className="scroll-content__title-third">Where can I find my YouTube monetisation code?</h3>
                    <p className="scroll-content__text scroll-content__list-title">The places where you can get your code are:</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Order confirmation page</li>
                      <li className="scroll-content__text content-list__item">Confirmation email</li>
                    </ul>
                    <p className="scroll-content__text">Codes related to previous purchases can be found under Account > Orders</p>
                    <h3 className="scroll-content__title-third">Some useful links for YouTube Creators</h3>
                    <p className="scroll-content__text scroll-content__list-title">We’ve compiled a list of useful resources you can start using:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">Understanding YouTube monetisation</li>
                      <li className="scroll-content__text content-list__item">Content ID claims explained</li>
                    </ul>
                    <h3 className="scroll-content__title-third">Why do I have ads on my videos </h3>
                    <p className="scroll-content__text">If a video creator uses unauthorized or unlicensed content in a Youtube video, it may have monetized ads placed over it by the YouTube Content ID system. Once you’ve tried Tunetank licence certificate to legally use audios in your videos, these ads will disappear and you’ll be able to earn money on your videos if you want.</p>
                    <p className="scroll-content__text">To get more information about how to clear unlisted video from ads, take a look at “How to clear a YouTube copyright notice” section below.</p>
                    <h3 className="scroll-content__title-third">How to find out whether a track is registered with Content ID or not?</h3>
                    <p className="scroll-content__text">If your audio is registered with Content ID, it will be mentioned in the item description on your individual item page. Here is how it may look like:</p>
                    <img className="scroll-content__image" src={ContentIdImage}/>
                    <p className="scroll-content__text">The item page sidebar will also contain a “YouTube Content ID Registered: Yes/No” section, as well as which partner platform the Content ID is administered by, if any:</p>
                    <img className="scroll-content__image" src={ContentIdImage2}/>
                    <p className="scroll-content__text">Please Note: Adding Content ID information to the existing items might take some time for authors. However, you can see the item description that many authors have already given. You can also connect any author via his profile page to check this out. </p>
                    <h3 className="scroll-content__title-third">How to remove YouTube copyright notices</h3>
                    <p className="scroll-content__text scroll-content__list-title">Choose one of the following ways:</p>
                    <ol className="content-list content-list_decimal">
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">Dispute this claim with Youtube’s built-in form.</span></li>
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">If the music is administered via AdRev, use the AdRev Claim Clearance page to clear a claim. After the claim has been cleared, you’ll receive an email confirmation from AdRev.</span></li>
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">Contact the author via his Tunetank profile page and share a link to your Youtube video to let the author remove a copyright notice in the shortest possible time.</span></li>
                    </ol>
                    <p className="scroll-content__text">To remove a claim via Youtube, let them know that you licenced the copyrighted music, so you legally use it in your videos. Choose to “dispute” the claim by clicking the “matched third party content”copyright notice that is next to the video in your Video Manager, or on your copyright notices page. After clicking “file a dispute” choose the option "I have a licence or written permission from the proper rights holder to use this material".</p>
                    <img className="scroll-content__image" src={CopyrightNotice}/>
                    <img className="scroll-content__image" src={CopyrightNotice2}/>
                    <img className="scroll-content__image" src={CopyrightNotice3}/>
                    <p className="scroll-content__text">In the "Reason for dispute" box, copy/paste the contents from purchased licence certificate(accessible via the Downloads section in your Tunetank account).  You can also add the statement "A licence to use this royalty-free music by [Name of Author] was purchased from Tunetank".</p>
                    <img className="scroll-content__image" src={ReasonImage}/>
                    <p className="scroll-content__text scroll-content__list-title">After submitting the dispute, claims are mostly removed within 1-4 days. If any problem appears, contact the author via his profile page to speed up clearing a claim as well.</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">For better understanding how to find your item purchase code & licence certificate, read this article.</span></li>
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">These articles might be also useful YouTube's support article for users affected by copyright claims as well as YouTube’s support article on submitting additional documentation to claim montezation rights.</span></li>
                    </ul>
                    <h3 className="scroll-content__title-third">Hacks for removing claims in advance</h3>
                    <h3 className="scroll-content__text">Upload early as an “Unlisted” video</h3>
                    <p className="scroll-content__text">If you’ve licenced digitally fingerprinted audios from Tunetank, we offer you to set your Youtube content to “Unlisted” after uploading, until all “matched third-party content” notices will be removed.</p>
                    <p className="scroll-content__text">This will help you remove any copyright notices before you publish your videos and let you  be sure that you can monetize your videos with no issues. Once the copyright notice has been cleared, the video becomes “Public” and can be monetized. </p>
                    <img className="scroll-content__image" src={UnlistedVideoImage}/>
                    <h3 className="scroll-content__title-third">Whitelisting Channels</h3>
                    <p className="scroll-content__text">If you or your client frequently uses Content ID registered music of a certain author in your Youtube videos, or a piece of Content ID registered music in two or more Youtube videos, some YouTube Partner Platforms such as AdRev also support whitelisting an entire Youtube channel. Whitelisting your personal Youtube channel lets you avoid any further copyright notices from appearing on your videos that include tracks from that author.</p>
                    <p className="scroll-content__text scroll-content__list-title">To whitelist your Youtube channel, contact the author via his Tunetank profile page to have them request this for your channel. Here is what to share in your message: </p>
                    <ol className="content-list content-list_decimal">
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">The contents of your purchased Licence Certificate(s)</span></li>
                      <li className="scroll-content__text content-list__item"><span className="scroll-content__text">A link to any relevant channel on Youtube (this might be a channel that belongs to you or your client, for example if you’re making videos them)</span></li>
                    </ol>
                    <p className="scroll-content__text">Please Note: Whitelisting a Youtube channel might be impossible if copyright notices from another author/rights-holder on that channel are not clear. In this case, these must be individually cleared, before whitelisting a channel.</p>
                  </Element>
                </div>
              </Col>
            </Row>

          </Col>
        </Row>
      </Grid>
    );
  }
}

export default FAQ;
