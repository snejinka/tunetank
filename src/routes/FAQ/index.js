import FAQ from './FAQ';

export default store => ({
  path: 'faq',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, FAQ);
    }, 'FAQ');
  },
});
