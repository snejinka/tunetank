import { injectReducer } from '../../store/reducers';
import UploadItemView from './components/UploadItemView';
import { requireAuth } from '../../routes/index';

export default store => ({
  path: 'uploadItem',
  onEnter: requireAuth,
  getComponent(nextStatse, cb) {
    require.ensure([], (require) => {
      const reducerNewTrack = require('./modules/newTrack').default;
      const reducerTrackTemplate = require('./modules/trackTemplate').default;
      injectReducer(store, { key: 'newTrack', reducer: reducerNewTrack });
      injectReducer(store, { key: 'trackTemplate', reducer: reducerTrackTemplate });
      cb(null, UploadItemView);
    }, 'UploadItemView');
  },
});
