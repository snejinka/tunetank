import * as api from 'api';
export const SET_TRACK_TEMPLATES = 'SET_TRACK_TEMPLATES';
export const SET_TRACK_TEMPLATE_NAME = 'SET_TRACK_TEMPLATE_NAME';
export const SET_SELECTED_TRACK_TEMPLATE = 'SET_SELECTED_TRACK_TEMPLATE';
export const CLEAR_TEMPLATE_NAME = 'CLEAR_TEMPLATE_NAME';

const initialState = {
  track_templates_by_user: [],
  track_template_name: '',
  selected_track_template: {},
};


export const setTrackTemplates = userId =>
  dispatch => api.fetchTrackTemplates(userId).then((res) => {
    dispatch({
      type: SET_TRACK_TEMPLATES,
      data: res,
    });
  });

export const createTrackTemplate = (uploadForm, templateName) =>
  dispatch => new Promise((resolve) => {
    const trackTemplate = {
      name: templateName,
      looped: uploadForm.looped,
      vocals: uploadForm.vocals,
      tempo: uploadForm.tempo,
      organization: uploadForm.organization,
      composer: uploadForm.composer,
      publisher: uploadForm.publisher,
      youtube_id_administrated_by: uploadForm.youtube_id_administrated_by,
      bit_rate: uploadForm.bit_rate,
      sample_rate: uploadForm.sample_rate,
      genres: uploadForm.genres,
      instruments: uploadForm.instruments,
      moods: uploadForm.moods,
    };
    return api.fetchCreateTrackTemplate(trackTemplate).then((res) => {
      dispatch({ type: CLEAR_TEMPLATE_NAME });
      resolve(res);
    });
  });

export const setTrackTemplateName = name =>
  (dispatch) => {
    dispatch({
      type: SET_TRACK_TEMPLATE_NAME,
      data: name,
    });
  };

export const setSelectedTrackTemplate = template =>
  (dispatch) => {
    dispatch({
      type: SET_SELECTED_TRACK_TEMPLATE,
      data: template,
    });
  };

const ACTION_HANDLERS = {
  [SET_TRACK_TEMPLATES]: (state, action) => ({
    ...state,
    track_templates_by_user: action.data,
  }),
  [SET_TRACK_TEMPLATE_NAME]: (state, action) => ({
    ...state,
    track_template_name: action.data,
  }),
  [SET_SELECTED_TRACK_TEMPLATE]: (state, action) => ({
    ...state,
    selected_track_template: action.data,
  }),
  [CLEAR_TEMPLATE_NAME]: state => ({
    ...state,
    track_template_name: initialState.track_template_name,
  }),
};

export default function trackTemplateReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
