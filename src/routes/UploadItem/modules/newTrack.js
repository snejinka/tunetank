import toastr from 'toastr';
import * as api from 'api';
import { createTrackTemplate, setTrackTemplates } from './trackTemplate';

export const GET_SERVER_DATA = 'GET_SERVER_DATA';
export const SET_TRACK_NAME = 'SET_TRACK_NAME';
export const SET_TRACK_GENRES = 'SET_TRACK_GENRES';
export const SET_TRACK_MOODS = 'SET_TRACK_MOODS';
export const SET_TRACK_INSTRUMENTS = 'SET_TRACK_INSTRUMENTS';
export const SET_TRACK_LOOPED = 'SET_TRACK_LOOPED';
export const SET_TRACK_TEMPO = 'SET_TRACK_TEMPO';
export const SET_TRACK_VOCALS = 'SET_TRACK_VOCALS';
export const SET_TRACK_BIT_RATE = 'SET_TRACK_BIT_RATE';
export const SET_TRACK_SAMPLE_RATE = 'SET_TRACK_SAMPLE_RATE';
export const SET_TRACK_ORGANIZATION = 'SET_TRACK_ORGANIZATION';
export const SET_TRACK_COMPOSER = 'SET_TRACK_COMPOSER';
export const SET_TRACK_PUBLISHER = 'SET_TRACK_PUBLISHER';
export const SET_TRACK_YOUTUBE_ID_REGISTERED = 'SET_TRACK_YOUTUBE_ID_REGISTERED';
export const SET_TRACK_YOUTUBE_ID_ADMINISTRATED_BY = 'SET_TRACK_YOUTUBE_ID_ADMINISTRATED_BY';
export const SET_REVIEWER_MESSAGE = 'SET_REVIEWER_MESSAGE';
export const SET_FILES_FOR_SELECT = 'SET_FILES_FOR_SELECT';
export const SET_TRACK_SECURE_ID = 'SET_TRACK_SECURE_ID';
export const SET_TRACK_LOGO = 'SET_TRACK_LOGO';
export const SET_TRACK_ARCHIVE = 'SET_TRACK_ARCHIVE';
export const SET_TRACK_MAIN = 'SET_TRACK_MAIN';
export const SET_TRACK_SHORT_VERSION = 'SET_TRACK_SHORT_VERSION';
export const SET_TRACK_MEDIUM_VERSION = 'SET_TRACK_MEDIUM_VERSION';
export const SET_TRACK_LONG_VERSION = 'SET_TRACK_LONG_VERSION';
export const CLEAR_ALL_FIELDS_FORM = 'CLEAR_ALL_FIELDS_FORM';

export const BIT_RATE = {
  rate_320: 0,
};

export const SAMPLE_RATE = {
  rate_16_stereo: 0,
};

const initialState = {
  youtube_id_registered: false,
  files_for_select: [],
  validation: [
    "Name can't be blank",
    'Image is required',
    'Zip archive is required',
    'Main track is required',
    "Genres can't be blank",
    "Moods can't be blank",
    "Instruments can't be blank",
    "Tempo can't be blank",
    'Bitrate should be checked',
    'Sample should be checked',
  ],
  form: {
    name: '',
    logo: '',
    originals_archive: '',
    main_track: '',
    short_version: '',
    medium_version: '',
    long_version: '',
    genres: [],
    moods: [],
    instruments: [],
    looped: false,
    tempo: null,
    vocals: false,
    bit_rate: BIT_RATE.rate_320,
    sample_rate: SAMPLE_RATE.rate_16_stereo,
    organization: '',
    composer: '',
    publisher: '',
    youtube_id_administrated_by: '',
    reviewer_message: '',
    secure_id: '',
  },
  data: {
    genres: [],
    moods: [],
    instruments: [],
  },
};

export const getServerData = () => dispatch => api.fetchCategories().then((res) => {
  dispatch({ type: GET_SERVER_DATA, data: res });
});

export const createNewTrack = (form, templateName) => (dispatch, getState) => {
  const state = getState();
  return new Promise((resolve, reject) => api.fetchCreateNewTrack(form).then((res) => {
    if (templateName !== '') {
      dispatch(createTrackTemplate(form, templateName)).then(() => {
        dispatch({ type: CLEAR_ALL_FIELDS_FORM });
      });
      dispatch(setTrackTemplates(state.user.currentUser.id));
    } else {
      dispatch({ type: CLEAR_ALL_FIELDS_FORM });
    }
    resolve(res);
  }).catch(() => {
    toastr.error('Oops! Something went wrong. Please, try again later.');
    reject();
  }));
};

export const setTrackName = name => (dispatch) => {
  dispatch({ type: SET_TRACK_NAME, data: name });
};

export const setTrackGenres = genres => (dispatch) => {
  dispatch({ type: SET_TRACK_GENRES, data: genres });
};

export const setTrackMoods = moods => (dispatch) => {
  dispatch({ type: SET_TRACK_MOODS, data: moods });
};

export const setTrackInstruments = instruments => (dispatch) => {
  dispatch({ type: SET_TRACK_INSTRUMENTS, data: instruments });
};

export const setTrackLooped = looped => (dispatch) => {
  dispatch({ type: SET_TRACK_LOOPED, data: looped });
};

export const setTrackTempo = tempo => (dispatch) => {
  dispatch({ type: SET_TRACK_TEMPO, data: tempo });
};

export const setTrackVocals = vocals => (dispatch) => {
  dispatch({ type: SET_TRACK_VOCALS, data: vocals });
};

export const setTrackBitrate = bitrate => (dispatch) => {
  dispatch({ type: SET_TRACK_BIT_RATE, data: bitrate });
};

export const setTrackSamplerate = samplerate => (dispatch) => {
  dispatch({ type: SET_TRACK_SAMPLE_RATE, data: samplerate });
};

export const setTrackOrganization = organization => (dispatch) => {
  dispatch({ type: SET_TRACK_ORGANIZATION, data: organization });
};

export const setTrackComposer = composer => (dispatch) => {
  dispatch({ type: SET_TRACK_COMPOSER, data: composer });
};

export const setTrackPublisher = publisher => (dispatch) => {
  dispatch({ type: SET_TRACK_PUBLISHER, data: publisher });
};

export const setTrackYoutubeIdRegistered = value => (dispatch) => {
  dispatch({ type: SET_TRACK_YOUTUBE_ID_REGISTERED, data: value });
};

export const setTrackYoutubeIdAdministratedBy = value => (dispatch) => {
  dispatch({ type: SET_TRACK_YOUTUBE_ID_ADMINISTRATED_BY, data: value });
};

export const setReviewerMessage = message => (dispatch) => {
  dispatch({ type: SET_REVIEWER_MESSAGE, data: message });
};

export const setFilesForSelect = files => (dispatch) => {
  dispatch({ type: SET_FILES_FOR_SELECT, data: files });
};

export const setTrackLogo = value => (dispatch) => {
  dispatch({ type: SET_TRACK_LOGO, data: value });
};

export const setTrackArchive = value => (dispatch) => {
  dispatch({ type: SET_TRACK_ARCHIVE, data: value });
};

export const setTrackMain = value => (dispatch) => {
  dispatch({ type: SET_TRACK_MAIN, data: value });
};

export const setTrackShortVersion = value => (dispatch) => {
  dispatch({ type: SET_TRACK_SHORT_VERSION, data: value });
};

export const setTrackMediumVersion = value => (dispatch) => {
  dispatch({ type: SET_TRACK_MEDIUM_VERSION, data: value });
};

export const setTrackLongVersion = value => (dispatch) => {
  dispatch({ type: SET_TRACK_LONG_VERSION, data: value });
};

export const setTrackSecureId = value => (dispatch) => {
  dispatch({ type: SET_TRACK_SECURE_ID, data: value });
};

const ACTION_HANDLERS = {
  [GET_SERVER_DATA]: (state, action) => ({
    ...state,
    data: action.data,
  }),
  [SET_TRACK_NAME]: (state, action) => {
    const message = 'Name can\'t be blank';
    let validation = [...state.validation];
    if (action.data === '' && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        name: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_GENRES]: (state, action) => {
    const message = 'Genres can\'t be blank';
    let validation = [...state.validation];
    if (action.data.length === 0 && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        genres: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_MOODS]: (state, action) => {
    const message = 'Moods can\'t be blank';
    let validation = [...state.validation];
    if (action.data.length === 0 && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        moods: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_INSTRUMENTS]: (state, action) => {
    const message = 'Instruments can\'t be blank';
    let validation = [...state.validation];
    if (action.data.length === 0 && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        instruments: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_LOOPED]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      looped: action.data,
    },
  }),
  [SET_TRACK_VOCALS]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      vocals: action.data,
    },
  }),
  [SET_TRACK_TEMPO]: (state, action) => {
    const message = 'Tempo can\'t be blank';
    let validation = [...state.validation];
    if (action.data === null && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        tempo: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_BIT_RATE]: (state, action) => {
    const message = 'Bitrate should be checked';
    let validation = [...state.validation];
    if (action.data === null && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1 && action.data !== null) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        bit_rate: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_SAMPLE_RATE]: (state, action) => {
    const message = 'Sample should be checked';
    let validation = [...state.validation];
    if (action.data === null && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1 && action.data !== null) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        sample_rate: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_ORGANIZATION]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      organization: action.data,
    },
  }),
  [SET_TRACK_COMPOSER]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      composer: action.data,
    },
  }),
  [SET_TRACK_PUBLISHER]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      publisher: action.data,
    },
  }),
  [SET_TRACK_YOUTUBE_ID_REGISTERED]: (state, action) => ({
    ...state,
    youtube_id_registered: action.data,
  }),
  [SET_TRACK_YOUTUBE_ID_ADMINISTRATED_BY]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      youtube_id_administrated_by: action.data,
    },
  }),
  [SET_REVIEWER_MESSAGE]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      reviewer_message: action.data,
    },
  }),
  [SET_FILES_FOR_SELECT]: (state, action) => ({
    ...state,
    files_for_select: action.data,
  }),
  [SET_TRACK_LOGO]: (state, action) => {
    const message = 'Image is required';
    let validation = [...state.validation];
    if (action.data === '' && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        logo: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_ARCHIVE]: (state, action) => {
    const message = 'Zip archive is required';
    let validation = [...state.validation];
    if (action.data === '' && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        originals_archive: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_MAIN]: (state, action) => {
    const message = 'Main track is required';
    let validation = [...state.validation];
    if (action.data === '' && !state.validation.includes(message)) {
      validation = validation.concat(message);
    } else {
      const index = validation.indexOf(message);
      if (index !== -1) { validation.splice(index, 1); }
    }

    return ({
      ...state,
      form: {
        ...state.form,
        main_track: action.data,
      },
      validation,
    });
  },
  [SET_TRACK_SHORT_VERSION]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      short_version: action.data,
    },
  }),
  [SET_TRACK_MEDIUM_VERSION]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      medium_version: action.data,
    },
  }),
  [SET_TRACK_LONG_VERSION]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      long_version: action.data,
    },
  }),
  [SET_TRACK_SECURE_ID]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      secure_id: action.data,
    },
  }),
  [CLEAR_ALL_FIELDS_FORM]: state => ({
    ...state,
    form: initialState.form,
  }),
};

export default function newTrackReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler
    ? handler(state, action)
    : state;
}
