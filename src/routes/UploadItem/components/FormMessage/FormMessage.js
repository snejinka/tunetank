import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import { setReviewerMessage } from '../../modules/newTrack';

class FormMessage extends React.Component {
  changeMessage = (event) => {
    this.props.actions.setReviewerMessage(event.target.value);
  }

  render() {
    return (
      <div className="form-wrap">
        <form action="" className="form-default">
          <div className="title_primary tilte_primary__form">Message to Reviewer</div>
          <FormGroup>
            <ControlLabel>
              Comments
            </ControlLabel>
            <FormControl
              componentClass="textarea"
              className="form-control-textarea"
              value={this.props.reviewerMessage}
              onChange={this.changeMessage}
            />
            <HelpBlock>
              If you have something to say ;)
            </HelpBlock>
          </FormGroup>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  reviewerMessage: state.newTrack.form.reviewer_message,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setReviewerMessage,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormMessage);
