import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ChooseUploadTemplate from '../ChooseUploadTemplate';
import FormDescriptionAndFiles from '../FormDescriptionAndFiles';
import FormCategoryAndAttributes from '../FormCategoryAndAttributes';
import FormAffilatedAuthors from '../FormAffilatedAuthors';
import FormYouTubeID from '../FormYouTubeID';
import FormMessage from '../FormMessage';
import FormSaveTemplate from '../FormSaveTemplate';
import { Grid, Row, Col } from 'react-bootstrap';
import { setTrackTemplates, setSelectedTrackTemplate } from '../../modules/trackTemplate';
import { createNewTrack, setTrackName } from '../../modules/newTrack';
import { getCurrentUser } from 'store/user';
import * as common from 'common';
import FormExclusivity from '../FormExclusivity';


class UploadItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      queryParam: common.ModalsQueryParams.qct,
      submited: false,
      validationErrors: false,
      queryParamUnlogged: common.ModalsQueryParams.qrhl,
    };
  }

  componentWillMount() {
    if (this.props.loggedIn) {
      if (this.props.user_id) {
        this.props.actions.setTrackTemplates(this.props.user_id);
      }
    } else {
      this.props.router.push(this.state.queryParamUnlogged);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.loggedIn) {
      if (this.props.user_id !== nextProps.user_id) {
        this.props.actions.setTrackTemplates(nextProps.user_id);
      }
    }
    if (nextProps.track_template_name !== '') {
      this.setState({
        queryParam: common.ModalsQueryParams.qtt,
      });
    } else {
      this.setState({
        queryParam: common.ModalsQueryParams.qct,
      });
    }
  }

  hideErrors() {
    this.setState({ validationErrors: false });
  }

  createNewTrack = (event) => {
    event.preventDefault();
    this.setState({
      submited: true,
    });
    if (
      this.props.form.name !== '' &&
      this.props.form.logo !== '' &&
      this.props.form.originals_archive !== '' &&
      this.props.form.main_track !== '' &&
      this.props.form.genres.length > 0 &&
      this.props.form.moods.length > 0 &&
      this.props.form.instruments.length > 0 &&
      this.props.form.tempo !== null &&
      this.props.form.sample_rate !== null &&
      this.props.form.bit_rate !== null
    ) {
      this.props.actions.createNewTrack(this.props.form, this.props.track_template_name).then((res) => {
        this.props.router.push(this.state.queryParam);
      });
    } else {
      this.setState({
        validationErrors: true,
        submited: false,
      });
      window.scrollTo(0, 0);
    }
  }

  render() {
    return (
      <div>
        <Grid>
          <Row>
            <Col md={12}>
              {this.props.loggedIn &&

                <div className="upload-item-wrap">

                  <ChooseUploadTemplate
                    templates={this.props.track_templates_by_user.length > 0 ? this.props.track_templates_by_user : []}
                    setSelectedTrackTemplate={this.props.actions.setSelectedTrackTemplate}
                    hideErrors={this.hideErrors.bind(this)}
                    errors={this.state.validationErrors}
                  />
                  <FormDescriptionAndFiles submited={this.state.submited} />
                  <FormCategoryAndAttributes selectedTrackTemplate={this.props.selected_track_template} submited={this.state.submited} />
                  {/* FormAffilatedAuthors selectedTrackTemplate={this.props.selected_track_template}/>*/}
                  <FormYouTubeID selectedTrackTemplate={this.props.selected_track_template} />
                  <FormExclusivity />
                  <FormMessage />
                  <FormSaveTemplate />
                  <div className="text-left">
                    <button type="button" disabled={this.state.submited} className="btn btn-primary btn-primary__submit" onClick={this.createNewTrack}>Submit</button>
                  </div>
                </div>
              }

            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  form: state.newTrack.form,
  user_id: state.user.loggedIn && state.user.currentUser.id,
  track_templates_by_user: state.trackTemplate.track_templates_by_user,
  track_template_name: state.trackTemplate.track_template_name,
  selected_track_template: state.trackTemplate.selected_track_template,
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackTemplates,
    getCurrentUser,
    setSelectedTrackTemplate,
    createNewTrack,
    setTrackName,

  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(UploadItemView);
