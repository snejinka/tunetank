import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MultiSelect } from 'react-selectize';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import {
  getServerData,
  setTrackGenres,
  setTrackMoods,
  setTrackInstruments,
  setTrackTags,
  setTrackLooped,
  setTrackVocals,
  setTrackTempo,
  setTrackBitrate,
  setTrackSamplerate,
  SAMPLE_RATE,
  BIT_RATE,
} from '../../modules/newTrack';

import _ from 'lodash';


const MAX_GENRE = 3;
const MAX_MOODS = 5;
const MAX_INSTURMENTS = 10;

class FormCategoryAndAttributes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validateGenres: false,
      validateMoods: false,
      validateInstruments: false,
      validateTags: false,
      validateLooped: false,
      validateTempo: false,
      validateVocals: false,
      validateBitrate: false,
      validateSampleRate: false,
      valueTempo: null,
      valueLooped: false,
      valueVocals: false,
      genreValues: [],
      genreGroups: [],
      genreOptions: [],
      moodValues: [],
      moodOptions: [],
      instrumentValues: [],
      instrumentOptions: [],
      maxGenreReached: false,
      maxMoodsReached: false,
      maxInstrumentsReached: false,
    };
  }

  static propTypes = {
    selectedTrackTemplate: PropTypes.object,
    submited: PropTypes.bool,
  }

  componentWillMount() {
    if (this.props.data.genres.length > 0) {
      this.genresSelectizeProps(this.props.data.genres);
    }
    if (this.props.data.moods.length > 0) {
      this.moodsSelectizeProps(this.props.data.moods);
    }
    if (this.props.data.instruments.length > 0) {
      this.instrumentsSelectizeProps(this.props.data.instruments);
    }
    this.props.actions.setTrackSamplerate(null);
    this.props.actions.setTrackBitrate(null);
  }


  componentDidMount() {
    this.props.actions.getServerData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.trackGenres.length > 0) {
      this.setState({
        validateGenres: true,
      });
    } else {
      this.setState({
        validateGenres: false,
      });
    }
    if (nextProps.trackMoods.length > 0) {
      this.setState({
        validateMoods: true,
      });
    } else {
      this.setState({
        validateMoods: false,
      });
    }
    if (nextProps.trackInstruments.length > 0) {
      this.setState({
        validateInstruments: true,
      });
    } else {
      this.setState({
        validateInstruments: false,
      });
    } if (nextProps.trackLooped !== '') {
      this.setState({
        validateLooped: true,
      });
    } else {
      this.setState({
        validateLooped: false,
      });
    } if (nextProps.trackTempo !== null) {
      this.setState({
        validateTempo: true,
      });
    } else {
      this.setState({
        validateTempo: false,
      });
    } if (nextProps.trackVocals !== '') {
      this.setState({
        validateVocals: true,
      });
    } else {
      this.setState({
        validateVocals: false,
      });
    } if (nextProps.trackBitrate !== null) {
      this.setState({
        validateBitrate: true,
      });
    } else {
      this.setState({
        validateBitrate: false,
      });
    } if (nextProps.trackSamplerate !== null) {
      this.setState({
        validateSampleRate: true,
      });
    } else {
      this.setState({
        validateSampleRate: false,
      });
    }

    if (this.props.selectedTrackTemplate !== nextProps.selectedTrackTemplate) {
      let genreValues = [];
      let moodValues = [];
      let instrumentsValues = [];
      const templateGenres = nextProps.selectedTrackTemplate.genres;
      const templateMoods = nextProps.selectedTrackTemplate.moods;
      const templateInstruments = nextProps.selectedTrackTemplate.instruments;


      if (templateGenres) {
        if (templateGenres.length > 0) {
          const genreOptions = nextProps.data.genres.map(genre => ({
            label: genre.name,
            value: genre.id,
          }));
          genreValues = genreOptions.filter((genre) => {
            const templateGenresIds = templateGenres.map(i => i.id);
            return templateGenresIds.includes(genre.value);
          });
        }
      }
      if (templateMoods) {
        if (templateMoods.length > 0) {
          const moodOptions = [].concat.apply([], nextProps.data.moods.map(mood => ({
            label: mood.name,
            value: mood.id,
          })));

          moodValues = moodOptions.filter((mood) => {
            const templateMoodsIds = templateMoods.map(i => i.id);
            return templateMoodsIds.includes(mood.value);
          });
        }
      }
      if (templateInstruments) {
        if (templateInstruments.length > 0) {
          const instrumentOptions = [].concat.apply([], nextProps.data.instruments.map(instrument => ({
            label: instrument.name,
            value: instrument.id,
          })));

          instrumentsValues = instrumentOptions.filter((instrument) => {
            const templateInstrumentsIds = templateInstruments.map(i => i.id);
            return templateInstrumentsIds.includes(instrument.value);
          });
        }
      }
      this.setState({
        valueTempo: nextProps.selectedTrackTemplate.tempo,
        validateTempo: true,
        valueLooped: nextProps.selectedTrackTemplate.looped,
        valueVocals: nextProps.selectedTrackTemplate.vocals,
        genreValues,
        moodValues,
        instrumentValues: instrumentsValues,
      });
      this.selectTrackItems(genreValues, this.props.actions.setTrackGenres);
      this.selectTrackItems(moodValues, this.props.actions.setTrackMoods);
      this.selectTrackItems(instrumentsValues, this.props.actions.setTrackInstruments);
      this.props.actions.setTrackTempo(nextProps.selectedTrackTemplate.tempo);
    }
    if (nextProps.data.genres.length > 0) {
      this.genresSelectizeProps(nextProps.data.genres);
    }

    if (nextProps.data.moods.length > 0) {
      this.moodsSelectizeProps(nextProps.data.moods);
    }

    if (nextProps.data.instruments.length > 0) {
      this.instrumentsSelectizeProps(nextProps.data.instruments);
    }
  }

  selectTrackItems = (items, action) => {
    const preparedItems = items.map(item => item.value);
    action(preparedItems);
  }

  checkRadioBoxItemLooped = (event, action) => {
    this.setState({
      valueLooped: event.target.value === 'true',
    });
    action(event.target.value === 'true');
  }

  checkRadioBoxItemVocals = (event, action) => {
    this.setState({
      valueVocals: event.target.value === 'true',
    });
    action(event.target.value === 'true');
  }

  checkCheckBoxItem = (event, action) => {
    if (event.target.checked) {
      action(parseInt(event.target.value));
    } else {
      action(null);
    }
  }

  changeTrackTempo = (event) => {
    let tempo = parseInt(event.target.value);

    if (event.target.value.length === 0) {
      tempo = null;
    } else if (isNaN(tempo) || tempo < 1 || tempo > 250) return;

    this.setState({
      valueTempo: tempo,
    });

    this.props.actions.setTrackTempo(tempo);
  }

  genresSelectizeProps(dataGenresArray) {
    const templateGenres = this.props.selectedTrackTemplate.genres;
    let genreValues = [];

    const genreOptions = dataGenresArray.map(genre => ({
      label: genre.name,
      value: genre.id,
    }));

    if (templateGenres) {
      if (templateGenres.length > 0) {
        genreValues = genreOptions.filter((genre) => {
          const templateGenresIds = templateGenres.map(i => i.id);
          return templateGenresIds.includes(genre.value);
        });
      }
    } else {
      genreValues = genreOptions.filter(genre => this.props.trackGenres.includes(genre.value));
    }


    this.setState({
      genreOptions,
      // genreValues: genreValues,
    });
  }

  moodsSelectizeProps(dataMoods) {
    let moodValues = [];
    const templateMoods = this.props.selectedTrackTemplate.moods;
    const moodOptions = dataMoods.map(mood => ({
      label: mood.name,
      value: mood.id,
    }));

    if (templateMoods) {
      if (templateMoods.length > 0) {
        moodValues = moodOptions.filter((mood) => {
          const templateMoodsIds = templateMoods.map(i => i.id);
          return templateMoodsIds.includes(mood.value);
        });
      }
    } else {
      moodValues = moodOptions.filter(mood => this.props.trackMoods.includes(mood.value));
    }

    this.setState({
      moodOptions,
    });
  }

  instrumentsSelectizeProps(dataInstruments) {
    let instrumentValues = [];
    const templateInstruments = this.props.selectedTrackTemplate.instruments;

    const instrumentOptions = dataInstruments.map(instrument => ({
      label: instrument.name,
      value: instrument.id,
    }));

    if (templateInstruments) {
      if (templateInstruments.length > 0) {
        instrumentValues = instrumentOptions.filter((instrument) => {
          const templateInstrumentsIds = templateInstruments.map(i => i.id);
          return templateInstrumentsIds.includes(instrument.value);
        });
      }
    } else {
      instrumentValues = instrumentOptions.filter(instrument => this.props.trackInstruments.includes(instrument.value));
    }

    this.setState({
      instrumentOptions,
    });
  }

  handleClickOption(item, values, selectize) {
    const NewValues = values;
    NewValues.push(item);
    if ((values === this.state.genreValues) && (values.length <= MAX_GENRE)) {
        this.setState({
          genreValues: NewValues,
        });
        this.selectTrackItems(NewValues, this.props.actions.setTrackGenres);
        selectize(this.props.data.genres);
    } else {
      this.setState({
        maxGenreReached: values.length <= MAX_GENRE ? true : false,
      })
    }
    if ((values === this.state.moodValues) && (values.length <= MAX_MOODS)){
      this.setState({
        moodValues: NewValues,
      });
      this.selectTrackItems(NewValues, this.props.actions.setTrackMoods);
      selectize(this.props.data.moods);
    } else {
      this.setState({
        maxMoodsReached: values.length <= MAX_MOODS ? true : false,
      })
    }
    if ((values === this.state.instrumentValues) && (values.length < MAX_INSTURMENTS) ){
      this.setState({
        instrumentValues: NewValues,
      });
      this.selectTrackItems(NewValues, this.props.actions.setTrackInstruments);
      selectize(this.props.data.instruments);
    } else {
      this.setState({
        maxInstrumentsReached: values.length <= MAX_INSTURMENTS ? true : false,
      })
    }
  }

  handleResetButton(values) {
    const resetValues = values;
    resetValues.splice(0, resetValues.length);
    if (values === this.state.genreValues) {
      this.setState({
        genreValues: resetValues,
      });
      this.props.actions.setTrackGenres(resetValues);
    } else if (values === this.state.moodValues) {
      this.setState({
        moodValues: resetValues,
      });
      this.props.actions.setTrackMoods(resetValues);
    } else if (values === this.state.instrumentValues) {
      this.setState({
        instrumentValues: resetValues,
      });
      this.props.actions.setTrackInstruments(resetValues);
    }
  }

  handleCloseValue(item, values) {
    for (let i = 0; i < values.length; i++) {
      if (values[i].value === item.value) {
        const newValues = _.without(values, item);
        const preparedItems = newValues.map(item => item.value);

        if ((values === this.state.genreValues)){
          this.setState({
            genreValues: newValues,
          });
          this.props.actions.setTrackGenres(preparedItems);
        } else if (values === this.state.moodValues) {
          this.setState({
            moodValues: newValues,
          });
          this.props.actions.setTrackMoods(preparedItems);
        } else if (values === this.state.instrumentValues) {
          this.setState({
            instrumentValues: newValues,
          });
          this.props.actions.setTrackInstruments(preparedItems);
        }
      }
    }
  }

  render() {
    const props = this.props;
    return (
      <div className="form-wrap">
        <form action="" className="form-default">
          <div className="title_primary tilte_primary__form">Category & Attributes</div>
          <FormGroup className="form-group__indent-bot-big">
            <ControlLabel>
              Choose Genre(s) *
            </ControlLabel>
            <MultiSelect
              options={this.state.genreOptions}
              values={this.state.genreValues}
              placeholder="Select genres"
              className="react-default-template"
              maxValues={MAX_GENRE}
              renderOption={item => <div className="simple-option" onClick={() => { this.handleClickOption(item, this.state.genreValues, this.genresSelectizeProps.bind(this)); }}>
                <span>{item.label}</span>
              </div>}
              renderValue={item => (<div className="simple-value value-with-close">
                <i className="fa fa-times btn-close" aria-hidden="true" onClick={() => this.handleCloseValue(item, this.state.genreValues)} />
                <span>{item.label}</span>
              </div>)}
              renderResetButton={item => <div className="react-selectize-reset-button-container" onClick={() => { this.handleResetButton(this.state.genreValues); }}>
                <svg className="react-selectize-reset-button" style={{ width: 8, height: 8 }}>
                  <path d="M0 0 L8 8 M8 0 L 0 8" />
                </svg>
              </div>}
            />
            <HelpBlock>
              Please choose not more than 3 genres so that your track would be accepted by moderators
            </HelpBlock>
            {!this.state.validateGenres && this.props.submited &&
            <HelpBlock className="not-valid">
              Genres can't be blank
            </HelpBlock>
            }
          </FormGroup>
          <FormGroup className="form-group__indent-bot-big">
            <ControlLabel>
              Choose Mood(s) *
            </ControlLabel>
            <MultiSelect
              options={this.state.moodOptions}
              values={this.state.moodValues}
              placeholder="Select moods"
              className="react-default-template"
              maxValues={MAX_MOODS}
              renderOption={item => <div className="simple-option" onMouseDown={() => { this.handleClickOption(item, this.state.moodValues, this.moodsSelectizeProps.bind(this)); }}>
                <span>{item.label}</span>
              </div>}
              renderValue={item => (<div className="simple-value value-with-close">
                <i className="fa fa-times btn-close" aria-hidden="true" onClick={() => this.handleCloseValue(item, this.state.moodValues)} />
                <span>{item.label}</span>
              </div>)}
              renderResetButton={item => <div className="react-selectize-reset-button-container" onClick={() => { this.handleResetButton(this.state.moodValues); }}>
                <svg className="react-selectize-reset-button" style={{ width: 8, height: 8 }}>
                  <path d="M0 0 L8 8 M8 0 L 0 8" />
                </svg>
              </div>}
            />
            <HelpBlock>
              Please choose not more than 5 moods so that your track would be accepted by moderators
            </HelpBlock>
            {!this.state.validateMoods && this.props.submited &&
            <HelpBlock className="not-valid">
              Moods can't be blank
            </HelpBlock>
            }
          </FormGroup>
          <FormGroup className="form-group__indent-bot-big">
            <ControlLabel>
              Choose Instrument(s) *
            </ControlLabel>
            <MultiSelect
              options={this.state.instrumentOptions}
              values={this.state.instrumentValues}
              placeholder="Select instruments"
              className="react-default-template"
              maxValues={MAX_INSTURMENTS}
              renderOption={item => <div className="simple-option" onMouseDown={() => { this.handleClickOption(item, this.state.instrumentValues, this.instrumentsSelectizeProps.bind(this)); }}>
                <span>{item.label}</span>
              </div>}
              renderValue={item => (<div className="simple-value value-with-close">
                <i className="fa fa-times btn-close" aria-hidden="true" onClick={() => this.handleCloseValue(item, this.state.instrumentValues)} />
                <span>{item.label}</span>
              </div>)}
              renderResetButton={item => <div className="react-selectize-reset-button-container" onClick={() => { this.handleResetButton(this.state.instrumentValues); }}>
                <svg className="react-selectize-reset-button" style={{ width: 8, height: 8 }}>
                  <path d="M0 0 L8 8 M8 0 L 0 8" />
                </svg>
              </div>}
            />
            <HelpBlock>
              Please choose not more than 10 instruments so that your track would be accepted by moderators
            </HelpBlock>
            {!this.state.validateInstruments && this.props.submited &&
            <HelpBlock className="not-valid">
              Instruments can't be blank
            </HelpBlock>
            }
          </FormGroup>
          <div className="options-box option-box__bottom-indent">
            <Row className="options-box__item">
              <Col md={4}>
                <ControlLabel className="options-box__item-title">
                  Looped *
                </ControlLabel>
                <div className="radio-box">
                  <FormControl
                    type="radio"
                    id="loopedRadio"
                    name="loopedRadioGroup"
                    value="true"
                    checked={this.state.valueLooped}
                    onChange={(event) => {
                      this.checkRadioBoxItemLooped(event, props.actions.setTrackLooped);
                    }}
                  />
                  <ControlLabel htmlFor="loopedRadio" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="loopedRadio" className="radio-box__note">Yes</ControlLabel>
                </div>
                <div className="radio-box">
                  <FormControl
                    type="radio"
                    id="notLoopedRadio"
                    name="loopedRadioGroup"
                    value="false"
                    checked={!this.state.valueLooped}
                    onChange={(event) => {
                      this.checkRadioBoxItemLooped(event, props.actions.setTrackLooped);
                    }}
                  />
                  <ControlLabel htmlFor="notLoopedRadio" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="notLoopedRadio" className="radio-box__note">No</ControlLabel>
                </div>
                {!this.state.validateLooped && this.props.submited &&
                <HelpBlock className="not-valid">
                  Looped is not valid
                </HelpBlock>
                }
              </Col>
              <Col md={4}>
                <ControlLabel>
                  Tempo (BPM) *
                </ControlLabel>
                <FormControl
                  value={this.state.valueTempo === null ? '' : this.state.valueTempo}
                  onChange={this.changeTrackTempo}
                />
                <HelpBlock>
                  Input value between 1-250.
                </HelpBlock>
                {!this.state.validateTempo && this.props.submited &&
                <HelpBlock className="not-valid">
                  Tempo can't be blank
                </HelpBlock>
                }
              </Col>
              <Col md={4} className="text-right">
                <ControlLabel className="options-box__item-title">
                  Vocals *
                </ControlLabel>
                <div className="radio-box-wrap radio-box-wrap__indent">
                  <div className="radio-box">
                    <FormControl
                      type="radio"
                      id="vocalsRadio"
                      name="vocalsRadioGroup"
                      value="true"
                      checked={this.state.valueVocals}
                      onChange={(event) => {
                        this.checkRadioBoxItemVocals(event, props.actions.setTrackVocals);
                      }}
                    />
                    <ControlLabel htmlFor="vocalsRadio" className="radio-box__pseudo-radio" />
                    <ControlLabel htmlFor="vocalsRadio" className="radio-box__note">Yes</ControlLabel>
                  </div>
                  <div className="radio-box">
                    <FormControl
                      type="radio"
                      id="notVocalsRadio"
                      name="vocalsRadioGroup"
                      value="false"
                      checked={!this.state.valueVocals}
                      onChange={(event) => {
                        this.checkRadioBoxItemVocals(event, props.actions.setTrackVocals);
                      }}
                    />
                    <ControlLabel htmlFor="notVocalsRadio" className="radio-box__pseudo-radio" />
                    <ControlLabel htmlFor="notVocalsRadio" className="radio-box__note">No</ControlLabel>
                  </div>
                  {!this.state.validateVocals && this.props.submited &&
                  <HelpBlock className="not-valid">
                    Vocals is not valid
                  </HelpBlock>
                  }
                </div>
              </Col>
            </Row>
            <div className="options-box__item">
              <ControlLabel className="options-box__item-title options-box__item-title__indent-medium">
                Bitrate *
              </ControlLabel>
              <div className="radio-box radio-box__square">
                <FormControl
                  type="checkbox"
                  id="bitrate320Radio"
                  value={BIT_RATE.rate_320}
                  checked={this.props.trackBitrate === 0}
                  onChange={(event) => {
                    this.checkCheckBoxItem(event, props.actions.setTrackBitrate);
                  }}
                />
                <ControlLabel htmlFor="bitrate320Radio" className="radio-box__pseudo-radio" />
                <ControlLabel htmlFor="bitrate320Radio" className="radio-box__note">320 kbps</ControlLabel>
              </div>
              <HelpBlock>
                All MP3 files must have a bitrate 320 kbps.
              </HelpBlock>
              {!this.state.validateBitrate && this.props.submited &&
              <HelpBlock className="not-valid">
                Bitrate should be checked
              </HelpBlock>
              }
            </div>
            <div className="options-box__item">
              <ControlLabel className="options-box__item-title options-box__item-title__indent-medium">
                Sample rate *
              </ControlLabel>
              <div className="radio-box radio-box__square">
                <FormControl
                  type="checkbox"
                  id="samplerate16Radio"
                  value={SAMPLE_RATE.rate_16_stereo}
                  checked={this.props.trackSamplerate === 0}
                  onChange={(event) => {
                    this.checkCheckBoxItem(event, props.actions.setTrackSamplerate);
                  }}
                />
                <ControlLabel htmlFor="samplerate16Radio" className="radio-box__pseudo-radio" />
                <ControlLabel htmlFor="samplerate16Radio" className="radio-box__note">16-Bit Stereo, 44.1
                  kHz</ControlLabel>
              </div>
              <HelpBlock>
                All MP3 files must have a sample rate 16-Bit Stereo, 44.1 kHz.
              </HelpBlock>
              {!this.state.validateSampleRate && this.props.submited &&
              <HelpBlock className="not-valid">
                Sample should be checked
              </HelpBlock>
              }
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  data: state.newTrack.data,
  trackGenres: state.newTrack.form.genres,
  trackMoods: state.newTrack.form.moods,
  trackInstruments: state.newTrack.form.instruments,
  trackLooped: state.newTrack.form.looped,
  trackTempo: state.newTrack.form.tempo,
  trackVocals: state.newTrack.form.vocals,
  trackBitrate: state.newTrack.form.bit_rate,
  trackSamplerate: state.newTrack.form.sample_rate,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getServerData,
    setTrackGenres,
    setTrackMoods,
    setTrackInstruments,
    setTrackTags,
    setTrackLooped,
    setTrackTempo,
    setTrackVocals,
    setTrackBitrate,
    setTrackSamplerate,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormCategoryAndAttributes);
