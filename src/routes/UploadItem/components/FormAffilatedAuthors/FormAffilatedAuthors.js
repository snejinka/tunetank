import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import { setTrackOrganization, setTrackComposer, setTrackPublisher } from '../../modules/newTrack';

class FormAffilatedAuthors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trackOrganization: '',
      trackComposer: '',
      trackPublisher: '',
    };
  }

  static propTypes = {
    selectedTrackTemplate: PropTypes.object,
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedTrackTemplate !== nextProps.selectedTrackTemplate) {
      const templateOrganization = nextProps.selectedTrackTemplate.organization;
      const templateComposer = nextProps.selectedTrackTemplate.composer;
      const templatePublisher = nextProps.selectedTrackTemplate.publisher;

      if (templateOrganization !== '') {
        this.setState({
          trackOrganization: templateOrganization,
        });
      }

      if (templateComposer !== '') {
        this.setState({
          trackComposer: templateComposer,
        });
      }

      if (templatePublisher !== '') {
        this.setState({
          trackPublisher: templatePublisher,
        });
      }
    }
  }

  changeField(event, action) {
    action(event.target.value);
  }

  render() {
    return (
      <div className="form-wrap">
        <form action="" className="form-default">
          <div className="title_primary tilte_primary__form">For P.R.O. Affilated Authors</div>
          <FormGroup className="form-group__indent-bot-big">
            <ControlLabel>
              Organization
            </ControlLabel>
            <FormControl
              value={this.state.trackOrganization}
              onChange={(event) => {
                this.setState({ trackOrganization: event.target.value });
                this.changeField(event, this.props.actions.setTrackOrganization);
              }}
            />
          </FormGroup>
          <FormGroup className="form-group__indent-bot-big">
            <ControlLabel>
              Composer
            </ControlLabel>
            <FormControl
              value={this.state.trackComposer}
              onChange={(event) => {
                this.setState({ trackComposer: event.target.value });
                this.changeField(event, this.props.actions.setTrackComposer);
              }}
            />
            <HelpBlock>
              Enter the name(s) of the persone(s) who composed this track.
            </HelpBlock>
          </FormGroup>
          <FormGroup>
            <ControlLabel>
              Publisher
            </ControlLabel>
            <FormControl
              value={this.state.trackPublisher}
              onChange={(event) => {
                this.setState({ trackPublisher: event.target.value });
                this.changeField(event, this.props.actions.setTrackPublisher);
              }}
            />
            <HelpBlock>
              Enter the name of the publishing company for this track.
            </HelpBlock>
          </FormGroup>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  trackOrganization: state.newTrack.form.organization,
  trackComposer: state.newTrack.form.composer,
  trackPublisher: state.newTrack.form.publisher,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackOrganization,
    setTrackComposer,
    setTrackPublisher,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormAffilatedAuthors);
