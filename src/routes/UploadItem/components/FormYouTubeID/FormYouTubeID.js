import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import { setTrackYoutubeIdAdministratedBy, setTrackYoutubeIdRegistered } from '../../modules/newTrack';

class FormYouTubeID extends React.Component {

  static propTypes = {
    selectedTrackTemplate: PropTypes.object,
  }


  componentWillReceiveProps(nextProps) {
    if (this.props.selectedTrackTemplate !== nextProps.selectedTrackTemplate) {
      const templateYoutubeIdAdministratedBy = nextProps.selectedTrackTemplate.youtube_id_administrated_by;

      if (templateYoutubeIdAdministratedBy !== '') {
        this.props.actions.setTrackYoutubeIdAdministratedBy(templateYoutubeIdAdministratedBy);
        this.props.actions.setTrackYoutubeIdRegistered(true);
      }
    }
  }


  specifyYouTubeAdministration = (event) => {
    this.props.actions.setTrackYoutubeIdAdministratedBy(event.target.value);
  }

  changeYouTubeIdRegistrationFlag = (event) => {
    const flag = event.target.value === 'true';
    this.props.actions.setTrackYoutubeIdRegistered(flag);
    if (flag === false) this.props.actions.setTrackYoutubeIdAdministratedBy('');
  }

  render() {
    return (
      <div className="form-wrap">
        <div className="form-default">
          <div className="title_primary tilte_primary__form tilte_primary__form-ompressed">YouTube Content ID Registered
          </div>
          <div className="options-box">
            <Row className="options-box__item">
              <Col md={12}>
                <ControlLabel className="options-box__item-title">
                  YouTube Content ID Registered
                </ControlLabel>
                <div className="radio-box">
                  <FormControl
                    type="radio"
                    id="registeredRadio"
                    name="radioGroup"
                    value="true"
                    checked={this.props.youtubeIdRegistered}
                    onChange={this.changeYouTubeIdRegistrationFlag}
                  />
                  <ControlLabel htmlFor="registeredRadio" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="registeredRadio" className="radio-box__note">Yes</ControlLabel>
                </div>
                <div className="radio-box">
                  <FormControl
                    type="radio"
                    id="notRegisteredRadio"
                    name="radioGroup"
                    value="false"
                    checked={!this.props.youtubeIdRegistered}
                    onChange={this.changeYouTubeIdRegistrationFlag}
                  />
                  <ControlLabel htmlFor="notRegisteredRadio" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="notRegisteredRadio" className="radio-box__note">No</ControlLabel>
                </div>
                <HelpBlock>
                  Select “Yes” if your track is registered with YouTube Content ID. Otherwise, please, select “No”.
                </HelpBlock>
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                <FormGroup>
                  <ControlLabel>
                    YouTube Content ID Administrated by
                  </ControlLabel>
                  <FormControl
                    disabled={!this.props.youtubeIdRegistered}
                    value={this.props.youtubeIdAdministratedBy}
                    onChange={this.specifyYouTubeAdministration}
                  />
                  <HelpBlock>
                    Specify the platform that you’ve registered your music with, e.g. “AdRev”.
                  </HelpBlock>
                </FormGroup>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  youtubeIdAdministratedBy: state.newTrack.form.youtube_id_administrated_by,
  youtubeIdRegistered: state.newTrack.youtube_id_registered,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackYoutubeIdAdministratedBy,
    setTrackYoutubeIdRegistered,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormYouTubeID);
