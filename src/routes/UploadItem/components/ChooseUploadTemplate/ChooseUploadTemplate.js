import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Alert } from 'react-bootstrap';
import SelectizeSimpleSelectThemeOrange from '../SelectizeSimpleSelectThemeOrange';


class ChooseUploadTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alertVisible: this.props.submited,
      selectedTemplate: {},
    };
  }

  static propTypes = {
    templates: PropTypes.array,
    setSelectedTrackTemplate: PropTypes.func,
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.errors !== nextProps.errors) {
      this.setState({ alertVisible: nextProps.errors });
    }
  }

  hideAlert() {
    this.setState({ alertVisible: false });
    this.props.hideErrors();
  }

  getSelectTemplateById(id) {
    for (let i = 0; i < this.props.templates.length; i++) {
      if (this.props.templates[i].id === id) {
        this.setState({
          selectedTemplate: this.props.templates[i],
        });
      }
    }
  }

  setTemplate(template) {
    this.props.setSelectedTrackTemplate(template);
  }

  getAlert() {
    if (this.state.alertVisible && this.props.validation.length) {
      return (
        <Alert className="upload-item-alert" bsStyle="danger" onDismiss={this.hideAlert.bind(this)}>
          <h4>Oh snap! You got an error!</h4>
          <ul>
            {this.props.validation.map((error, index) => (<li key={index}>{error}</li>))}
          </ul>
        </Alert>
      );
    }
    return null;
  }

  render() {
    return (
      <div className="select-box">
        {this.getAlert()}
        <Row>
          <Col md={12}>
            <div className="title_primary text-left">Upload an Item</div>
          </Col>
        </Row>
        <Row>
          <Col md={7} className="nopadding-right">
            <div className="title title-note text-left">
              Fill the form below and your file will be reviewed by our moderators.
              You can create your own template and use it for your future uploads.
            </div>
          </Col>
          <Col md={5}>
            <div className="panel-wrap flexbox justify-start flex-wrap">
              <div className="sort-panel sort-panel__indent-bottom">
                <SelectizeSimpleSelectThemeOrange
                  options={this.props.templates}
                  getSelectTemplateById={id => this.getSelectTemplateById(id)}
                />
              </div>
              <button
                type="button" className="btn btn-primary"
                onClick={() => this.setTemplate(this.state.selectedTemplate)}
              >Apply Template</button>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  validation: state.newTrack.validation,
});

export default connect(mapStateToProps)(ChooseUploadTemplate);
