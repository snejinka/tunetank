import React, { PropTypes } from 'react';
import { ReactSelectize, SimpleSelect } from 'react-selectize';
import '../../../../../node_modules/react-selectize/themes/index.css';

class SelectizeSimpleSelectThemeOrange extends React.Component {
  static propTypes = {
    options: PropTypes.array,
    getSelectTemplateById: PropTypes.func,
  }

  selectTemplate = (template) => {
    this.props.getSelectTemplateById(template.id);
  }

  render() {
    const options = this.props.options.map(option => ({ label: option.name, value: option.name, id: option.id }));
    return (<SimpleSelect
      options={options}
      placeholder=""
      className="selectize-select-theme simple-select__statick-width simple-select__border-orange react-default-template"
      onValueChange={(value) => {
        this.selectTemplate(value);
      }}
    />);
  }
}
export default SelectizeSimpleSelectThemeOrange;

