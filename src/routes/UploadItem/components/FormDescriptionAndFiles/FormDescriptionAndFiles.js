import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import { SimpleSelect } from 'react-selectize';
import ReactS3Uploader from 'react-s3-uploader';
import { SoundMimeTypes, ArchiveMimeTypes, ImageMimeTypes } from 'common';
import _ from 'lodash';
import {
  setTrackName,
  setTrackLogo,
  setTrackArchive,
  setTrackMain,
  setTrackShortVersion,
  setTrackMediumVersion,
  setTrackLongVersion,
  setFilesForSelect,
  setTrackSecureId,
} from '../../modules/newTrack';
import { ProgressBar } from 'react-bootstrap';
import { API_SERVER_URL } from 'api';
import toastr from 'toastr';


class FormDescriptionAndFiles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validateName: false,
      validateMainTrack: false,
      validateZip: false,
      validateImageTrack: false,
      fileOptionsImage: [],
      fileOptionsZip: [],
      fileOptionsAudio: [],
      fileOptionsAudioShort: [],
      fileOptionsAudioMedium: [],
      fileOptionsAudioLong: [],
      valueMainTrack: undefined,
      valueShortTrack: undefined,
      valueMediumTrack: undefined,
      valueLongTrack: undefined,
      uploadProgress: 0,
      uploadFileNames: [],
      invalidSymbol: false,
    };
  }

  static propTypes ={
    submited: PropTypes.bool,
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.mainTrack !== '') {
      this.setState({
        validateMainTrack: true,
      });
    } else {
      this.setState({
        validateMainTrack: false,
      });
    }
    if (nextProps.originalsArchive !== '') {
      this.setState({
        validateZip: true,
      });
    } else {
      this.setState({
        validateZip: false,
      });
    }
    if (nextProps.trackLogo !== '') {
      this.setState({
        validateImageTrack: true,
      });
    } else {
      this.setState({
        validateImageTrack: false,
      });
    }
  }

  changeTrackName = (event) => {
    const cyrillicPattern = /[\u0400-\u04FF]/;
    const value = event.target.value;

    if (value !== '') {
      if ((!cyrillicPattern.test(value))) {
        this.setState({
          validateName: true,
          invalidSymbol: false,
        }, () => {
          this.props.actions.setTrackName(value);
        });
      } else {
        this.setState({
          invalidSymbol: true,
        })
      }
    } else {
      this.setState({
        validateName: false,
      }, () => {
        this.props.actions.setTrackName(value);
      });
    }
  }

  onFileUploadFinish = (res, file) => {
    const fileOptionsImage = this.state.fileOptionsImage;
    const props = this.props;
    const filesForSelect = props.filesForSelect.slice();
    const fileUrl = res.signedUrl.split('?')[0];

    for (let i = 0; i < filesForSelect.length; ++i) {
      if (filesForSelect[i].name === res.fileName) {
        filesForSelect.splice(i, 1);
      }
    }
    filesForSelect.push({ name: res.fileName, url: fileUrl, type: file.type });

    props.actions.setFilesForSelect(filesForSelect);
    props.actions.setTrackSecureId(res.secureId);


    if (ImageMimeTypes.includes(file.type)) {
      fileOptionsImage.push({ label: file.name, value: fileUrl });
      this.setState({
        fileOptionsImage,
      });
    }
    if (ArchiveMimeTypes.includes(file.type) || (file.name.slice(-3) === 'zip')) {
      this.state.fileOptionsZip.push({ label: file.name, value: fileUrl });
    }
    if (SoundMimeTypes.includes(file.type)) {
      this.state.fileOptionsAudio.push({ label: file.name, value: fileUrl });
    }
  }

  selectFile = (option, prevValue, action, type) => {
    if (option) {
      if (type === 'main') {
        this.setState({
          valueMainTrack: option,
        });
        if (this.state.valueShortTrack === option) {
          this.props.actions.setTrackShortVersion(null);
          this.setState({
            valueShortTrack: undefined,
          });
        } else if (this.state.valueMediumTrack === option) {
          this.props.actions.setTrackMediumVersion(null);
          this.setState({
            valueMediumTrack: undefined,
          });
        } else if (this.state.valueLongTrack === option) {
          this.props.actions.setTrackLongVersion(null);
          this.setState({
            valueLongTrack: undefined,
          });
        }
      } else if (type == 'short') {
        this.setState({
          valueShortTrack: option,
        });
        if (this.state.valueMainTrack === option) {
          this.props.actions.setTrackMain(null);
          this.setState({
            valueMainTrack: undefined,
          });
        } else if (this.state.valueMediumTrack === option) {
          this.props.actions.setTrackMediumVersion(null);
          this.setState({
            valueMediumTrack: undefined,
          });
        } else if (this.state.valueLongTrack === option) {
          this.props.actions.setTrackLongVersion(null);
          this.setState({
            valueLongTrack: undefined,
          });
        }
      } else if (type == 'medium') {
        this.setState({
          valueMediumTrack: option,
        });
        if (this.state.valueMainTrack === option) {
          this.props.actions.setTrackMain(null);
          this.setState({
            valueMainTrack: undefined,
          });
        }
        if (this.state.valueShortTrack === option) {
          this.props.actions.setTrackShortVersion(null);
          this.setState({
            valueShortTrack: undefined,
          });
        } else if (this.state.valueLongTrack === option) {
          this.props.actions.setTrackLongVersion(null);
          this.setState({
            valueLongTrack: undefined,
          });
        }
      } else if (type == 'long') {
        this.setState({
          valueLongTrack: option,
        });
        if (this.state.valueMainTrack === option) {
          this.props.actions.setTrackMain(null);
          this.setState({
            valueMainTrack: undefined,
          });
        }
        if (this.state.valueShortTrack === option) {
          this.props.actions.setTrackShortVersion(null);
          this.setState({
            valueShortTrack: undefined,
          });
        } else if (this.state.valueMediumTrack === option) {
          this.props.actions.setTrackMediumVersion(null);
          this.setState({
            valueMediumTrack: undefined,
          });
        }
      }
    }

    const newValue = option ? option.value : null;
    action(newValue);
  }

  onProgress(progress) {
    this.setState({
      uploadProgress: progress,
    });
  }

  preProcess(file, next) {
    if (ImageMimeTypes.includes(file.type) && (file.size > 10485760)) {
      toastr.error('The image must be less than 10 mb');
      return false;
    }
    next(file);
    const filename = this.state.uploadFileNames;
    filename.unshift(file.name);
    this.setState({
      uploadFileNames: filename,
    });
  }

  render() {
    const props = this.props;
    // const ReactS3Uploader = require('react-s3-uploader');
    const fileOptions = props.filesForSelect.map(file => ({
      label: file.name,
      value: file.url,
    }));

    const acceptMimeTypes = _.union(SoundMimeTypes, ArchiveMimeTypes, ImageMimeTypes);

    return (
      <div className="form-wrap">
        <form action="" className="form-default">
          <div className="title_primary tilte_primary__form">
            Description and Files
          </div>
          <FormGroup className="form-group__indent-bot-big">
            <ControlLabel>
              Name*
            </ControlLabel>
            <FormControl
              value={props.trackName}
              maxLength={100}
              onChange={this.changeTrackName}
            />
            <HelpBlock>
              Maximum 100 characters.
            </HelpBlock>
            {!this.state.validateName && this.props.submited &&
            <HelpBlock className="not-valid">
              Name can't be blank
            </HelpBlock>
            }
            { this.state.invalidSymbol && 
              <HelpBlock className="not-valid">
                Please use only Latin letters and digits
              </HelpBlock>
            }
          </FormGroup>
          <FormGroup className="upload-files upload-files__bottom-indent form-group__indent-bot-big">
            <ControlLabel>
              Upload All Project Files *
            </ControlLabel>
            <div className="file-upload">
              <ReactS3Uploader
                signingUrl="/api/v1/s3/track_signed_url"
                signingUrlMethod="POST"
                preprocess={(file, next) => this.preProcess(file, next)}
                onProgress={event => this.onProgress(event)}
                onFinish={this.onFileUploadFinish}
                signingUrlQueryParams={{ secureId: props.trackSecureId }}
                server={API_SERVER_URL}
              />
              <button type="button" className="btn btn-primary btn-primary__choose" disabled={this.state.uploadFileNames.length > 0 && this.state.uploadProgress < 100}>Choose File</button>
            </div>
            {this.state.uploadProgress > 0 && this.state.uploadProgress < 100 &&
              <div className="upload-progress-bar">
                <ProgressBar active now={this.state.uploadProgress} />
              </div>
            }
            <div className="upload-filenames">
              {this.state.uploadFileNames.map((filename, i) => <span className="upload-filenames__item" key={i}>{filename}</span>)}
            </div>

            <div className="text-block text-block__uplad-note">
              <p>Track preview image: JPG and PNG files, 270x270 pixels</p>
              <p>ZIP archive: All files for buyers, including MP3 and WAV</p>
              <p>Audio Previews: it is allowed to upload up to 4 versions of your track: main version</p>
              <p>(necessarily) and up to 3 versions with different length. File format should be MP3 320 kbit/s.</p>
            </div>
          </FormGroup>
          <Row className="form-group form-group__indent-bot-big">
            <Col md={6}>
              <ControlLabel>
                Image Preview *
              </ControlLabel>
              <div>
                <SimpleSelect
                  options={this.state.fileOptionsImage}
                  placeholder="Select file"
                  className="react-default-template selectize-select-theme"
                  onValueChange={(value) => {
                    this.selectFile(value, props.trackLogo, this.props.actions.setTrackLogo, this.state.fileOptionsImage);
                  }}
                />
              </div>
              {!this.state.validateImageTrack && this.props.submited &&
              <HelpBlock className="not-valid">
                Image is required
              </HelpBlock>
              }
            </Col>
            <Col md={6}>
              <ControlLabel>
                ZIP Archive *
              </ControlLabel>
              <SimpleSelect
                options={this.state.fileOptionsZip}
                placeholder="Select file"
                className="react-default-template selectize-select-theme"
                onValueChange={(value) => {
                  this.selectFile(value, props.originalsArchive, this.props.actions.setTrackArchive, this.state.fileOptionsZip);
                }}
              />
              {!this.state.validateZip && this.props.submited &&
              <HelpBlock className="not-valid">
                Zip archive is required
              </HelpBlock>
              }
            </Col>
          </Row>
          <Row className="form-group form-group__indent-bot-big">
            <Col md={6}>
              <ControlLabel>
                Main Track Preview*
              </ControlLabel>
              <SimpleSelect
                options={this.state.fileOptionsAudio}
                placeholder="Select file"
                className="react-default-template selectize-select-theme"
                value={this.state.valueMainTrack}
                onValueChange={(value) => {
                  this.selectFile(value, props.mainTrack, this.props.actions.setTrackMain, 'main');
                }}
              />
              {!this.state.validateMainTrack && this.props.submited &&
              <HelpBlock className="not-valid">
                Main track is required
              </HelpBlock>
              }
            </Col>
            <Col md={6}>
              <ControlLabel>
                Short Version Preview
              </ControlLabel>
              <SimpleSelect
                options={this.state.fileOptionsAudio}
                placeholder="Select file"
                value={this.state.valueShortTrack}
                className="react-default-template selectize-select-theme"
                onValueChange={(value) => {
                  this.selectFile(value, props.shortVersion, this.props.actions.setTrackShortVersion, 'short');
                }}
              />
            </Col>
          </Row>
          <Row className="form-group form-group__indent-bot-big">
            <Col md={6}>
              <ControlLabel>
                Medium Version Preview
              </ControlLabel>
              <SimpleSelect
                options={this.state.fileOptionsAudio}
                placeholder="Select file"
                className="react-default-template selectize-select-theme"
                value={this.state.valueMediumTrack}
                onValueChange={(value) => {
                  this.selectFile(value, props.mediumVersion, this.props.actions.setTrackMediumVersion, 'medium');
                }}
              />
            </Col>
            <Col md={6}>
              <ControlLabel>
                Long Version Preview
              </ControlLabel>
              <SimpleSelect
                options={this.state.fileOptionsAudio}
                placeholder="Select file"
                value={this.state.valueLongTrack}
                className="react-default-template selectize-select-theme"
                onValueChange={(value) => {
                  this.selectFile(value, props.longVersion, this.props.actions.setTrackLongVersion, 'long');
                }}
              />
            </Col>
          </Row>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  filesForSelect: state.newTrack.files_for_select,
  trackSecureId: state.newTrack.form.secure_id,
  trackName: state.newTrack.form.name,
  trackLogo: state.newTrack.form.logo,
  originalsArchive: state.newTrack.form.originals_archive,
  mainTrack: state.newTrack.form.main_track,
  shortVersion: state.newTrack.form.short_version,
  mediumVersion: state.newTrack.form.medium_version,
  longVersion: state.newTrack.form.long_version,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackName,
    setFilesForSelect,
    setTrackLogo,
    setTrackArchive,
    setTrackMain,
    setTrackShortVersion,
    setTrackMediumVersion,
    setTrackLongVersion,
    setTrackSecureId,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormDescriptionAndFiles);
