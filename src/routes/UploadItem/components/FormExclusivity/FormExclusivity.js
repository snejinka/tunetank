import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';

class FormExclusivity extends React.Component {

    render() {
        return (
            <div className="form-wrap">
        <div className="form-default">
          <div className="title_primary tilte_primary__form tilte_primary__form-ompressed">Terms of exclusivity
          </div>
          <div className="options-box">
            <Row className="options-box__item">
              <Col md={12}>
                <ControlLabel className="options-box__item-title">
                This track is exclusive
                </ControlLabel>
                <div className="radio-box">
                  <FormControl
                    type="radio"
                    id="pervoe"
                    name="radioGroup1"
                    value="true"
                    checked="true"
                  />
                  <ControlLabel htmlFor="pervoe" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="pervoe" className="radio-box__note">Yes</ControlLabel>
                </div>
                <div className="radio-box">
                  <FormControl
                    type="radio"
                    id="vtoroe"
                    name="radioGroup1"
                    value="false"
                  />
                  <ControlLabel htmlFor="vtoroe" className="radio-box__pseudo-radio" />
                  <ControlLabel htmlFor="vtoroe" className="radio-box__note disabledDiv">No</ControlLabel>
                </div>
                <HelpBlock>
                    Select “Yes” if your track will only be available on TuneTank.com. Otherwise, please, select “No”
                </HelpBlock>
              </Col>
            </Row>
          </div>
        </div>
      </div>
        )
    }
}
    
export default FormExclusivity