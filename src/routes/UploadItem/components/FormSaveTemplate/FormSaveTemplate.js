import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import { setTrackTemplateName } from '../../modules/trackTemplate';


class FormSaveTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validateTrackTemplateName: false,
      saveTemplate: false,
      validateTrackTemplateNameAlreadyHave: false,
    };
  }

  changeTrackTemplateName = (event) => {
    if (event.target.value !== '' && this.state.saveTemplate) {
      const templateNames = this.props.track_templates_by_user.map(i => i.name);
      if (templateNames.includes(event.target.value)) {
        this.setState({
          validateTrackTemplateNameAlreadyHave: true,
        });
      } else {
        this.setState({
          validateTrackTemplateName: true,
          validateTrackTemplateNameAlreadyHave: false,
        });
        this.props.actions.setTrackTemplateName(event.target.value);
      }
    } else {
      this.setState({
        validateTrackTemplateName: false,
      });
    }
  }

  checkSaveTemplate = (event) => {
    this.setState({
      saveTemplate: !this.state.saveTemplate,
    });
  }

  render() {
    return (
      <div className="form-wrap form-wrap__indent-large">
        <div className="form-default">
          <FormGroup className="form-group__indent-bot-big">
            <div className="radio-box radio-box__square">
              <FormControl
                type="checkbox"
                id="radioGroup5"
                onChange={this.checkSaveTemplate} value={this.state.saveTemplate}
              />
              <ControlLabel htmlFor="radioGroup5" className="radio-box__pseudo-radio" />
              <ControlLabel htmlFor="radioGroup5" className="radio-box__note"> Save as Template</ControlLabel>
            </div>
            <HelpBlock>
              Template saves information from Category&Attributes, P.R.O. Affilated Authors,
              YouTube Content ID Registrated section.
            </HelpBlock>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Template Name</ControlLabel>
            <FormControl
              onChange={this.changeTrackTemplateName}
            />
            {!this.state.validateTrackTemplateName && this.state.saveTemplate &&
            <HelpBlock className="not-valid">
              Template name can't be blank if you want to create template
            </HelpBlock>
            }
            {this.state.validateTrackTemplateNameAlreadyHave &&
            <HelpBlock className="not-valid">
              You already have template with such name
            </HelpBlock>
            }
          </FormGroup>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  track_templates_by_user: state.trackTemplate.track_templates_by_user,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTrackTemplateName,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormSaveTemplate);

