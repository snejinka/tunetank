import React from 'react';
import { ReactSelectize, SimpleSelect } from 'react-selectize';
import '../../../../../node_modules/react-selectize/themes/index.css';

class SelectizeSimpleSelectTheme extends React.Component {

  render() {
    const options = ['Popular', 'Best', 'Latest', 'Unknown', 'All'].map(fruit => ({ label: fruit, value: fruit }));
    return <SimpleSelect options={options} placeholder="" className="selectize-select-theme react-default-template" />;
  }
    }
export default SelectizeSimpleSelectTheme;

