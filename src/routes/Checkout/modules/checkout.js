import axios from 'axios';
import payform from 'payform';
import toastr from 'toastr';
import multiDownload from 'multi-download';
import { browserHistory } from 'react-router';
import { fetchGetCurrentUserBillingInfo, API_SERVER_URL } from 'api';
import { clearCart } from 'store/cart';

const PAYPAL_CHECKOUT_API_URL = `${API_SERVER_URL}/api/v1/orders/express_checkout`;
const PAYPAL_CHECKOUT_EXECUTE_API_URL = `${API_SERVER_URL}/api/v1/orders/execute`;
const CREDIT_CARD_CHECKOUT_API_URL = `${API_SERVER_URL}/api/v1/orders/credit_card_checkout`;

export const SET_WAITING = 'SET_WAITING';
export const UNSET_WAITING = 'UNSET_WAITING';
export const SET_FIELD = 'SET_FIELD';
export const SET_BILLING = 'SET_BILLING';
export const SET_INVALID_FIELDS = 'SET_INVALID_FIELDS';
export const REMOVE_INVALID_FIELD = 'REMOVE_INVALID_FIELD';

function formItemsArray(items) {
  return items.filter(item => item.quantity > 0).map(item => (
    {
      track_id: item.id,
      author_id: item.user.id,
      name: item.name,
      description: 'license desscription',
      quantity: item.quantity,
      amount_in_cents: item.license.price_in_cents,
    }
  ));
}

function calcTotalAmount(items) {
  let totalAmount = 0;

  items.forEach((item) => {
    totalAmount += item.license.price_in_cents * item.quantity;
  });
  return totalAmount / 100;
}

function totalAmountInCents(items) {
  return calcTotalAmount(items) * 100;
}

export const setWaiting = () => (dispatch) => {
  dispatch({ type: SET_WAITING });
};

export const unsetWaiting = () => (dispatch) => {
  dispatch({ type: UNSET_WAITING });
};

export const setCardField = (value, fieldName) => (dispatch) => {
  dispatch({ type: SET_FIELD, form: 'creditCard', value, fieldName });
};

export const setBillingField = (value, fieldName) => (dispatch) => {
  dispatch({ type: SET_FIELD, form: 'billing', value, fieldName });
};

export const setCardInvalidFields = fields => (dispatch) => {
  dispatch({ type: SET_INVALID_FIELDS, form: 'creditCard', fields });
};

export const setBillingInvalidFields = fields => (dispatch) => {
  dispatch({ type: SET_INVALID_FIELDS, form: 'billing', fields });
};

export const removeCardInvalidField = field => (dispatch) => {
  dispatch({ type: REMOVE_INVALID_FIELD, form: 'creditCard', field });
};

export const removeBillingInvalidField = field => (dispatch) => {
  dispatch({ type: REMOVE_INVALID_FIELD, form: 'billing', field });
};

export const paypalCheckout = () => (dispatch, getState) => {
  dispatch(setWaiting());
  const { cart, checkout } = getState();

  axios.post(PAYPAL_CHECKOUT_API_URL, {
    order: {
      amount_in_cents: totalAmountInCents(cart.items),
      items: formItemsArray(cart.items),
    },
    billing: checkout.billing,
  })
  .then((resp) => {
    window.location.replace(resp.data.redirect_url);
  })
  .catch((res) => {
    toastr.error(res.error || res.response.data.error);
    dispatch(unsetWaiting());
  });
};

export const creditCardCheckout = () => (dispatch, getState) => {
  dispatch(setWaiting());
  const { cart, checkout } = getState();

  axios.post(CREDIT_CARD_CHECKOUT_API_URL, {
    order: {
      amount_in_cents: totalAmountInCents(cart.items),
      items: formItemsArray(cart.items),
    },
    credit_card: { ...checkout.creditCard, type: payform.parseCardType(checkout.creditCard.number) },
    billing: checkout.billing,
  })
  .then((res) => {
    multiDownload(res.data.download_urls);
    dispatch(unsetWaiting());
    dispatch(clearCart());
    browserHistory.push({ pathname: '/' });
  })
  .catch((res) => {
    toastr.error(res.error || res.response.data.error);
    dispatch(unsetWaiting());
  });
};

export const completePayment = paymentInfo => (dispatch) => {
  dispatch(setWaiting());

  axios.post(PAYPAL_CHECKOUT_EXECUTE_API_URL, {
    payment_info: paymentInfo,
  })
  .then((res) => {
    multiDownload(res.data.download_urls);
    dispatch(unsetWaiting());
    dispatch(clearCart());
    browserHistory.push({ pathname: '/' });
  })
  .catch((res) => {
    toastr.error(res.error || res.response.data.error);
    dispatch(unsetWaiting());
  });
};

export const getBillingInfo = () => dispatch => fetchGetCurrentUserBillingInfo().then((res) => {
  const user = res.data.data.current_user;
  dispatch({
    type: SET_BILLING,
    billing: {
      email: user.email,
      emailConfirm: user.email,
      name: user.name,
      company: user.company,
      country: user.country,
      address_first_line: user.address_first_line,
      address_second_line: user.address_second_line,
      city: user.city,
      state: user.state,
      zip: user.zip,
      vat: user.vat,
      invalidFields: [],
    },
  });
});

const initialState = {
  creditCard: {
    name: '',
    number: '',
    exp_month: '',
    exp_year: '',
    cvv: '',
    invalidFields: [],
  },
  billing: {
    email: '',
    emailConfirm: '',
    name: '',
    company: '',
    country: '',
    address_first_line: '',
    address_second_line: '',
    city: '',
    state: '',
    zip: '',
    vat: '',
    invalidFields: [],
  },
  waiting: false,
};

const ACTION_HANDLERS = {
  [SET_WAITING]: state => ({
    ...state,
    waiting: true,
  }),
  [UNSET_WAITING]: state => ({
    ...state,
    waiting: false,
  }),
  [SET_FIELD]: (state, action) => ({
    ...state,
    [action.form]: {
      ...state[action.form],
      [action.fieldName]: action.value,
    },
  }),
  [SET_BILLING]: (state, action) => ({
    ...state,
    billing: action.billing,
  }),
  [SET_INVALID_FIELDS]: (state, action) => ({
    ...state,
    [action.form]: {
      ...state[action.form],
      invalidFields: action.fields,
    },
  }),
  [REMOVE_INVALID_FIELD]: (state, action) => {
    const fields = state[action.form].invalidFields.filter(field => field !== action.field);
    return {
      ...state,
      [action.form]: {
        ...state[action.form],
        invalidFields: fields,
      },
    };
  },
};

export default function checkoutReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
