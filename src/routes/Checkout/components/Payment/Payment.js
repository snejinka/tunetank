import React from 'react';
import { Row, Col, Tab, Nav, NavItem } from 'react-bootstrap';
import Disclaimer from '../Disclaimer';

const Payment = () => (
  <div className="payment-cards">
    <Tab.Container id="tabs-inner" defaultActiveKey="first">
      <Row className="clearfix">
        <Col sm={12}>
          <div className="payment-methotd-menu-wrap">
            <div className="payment-methotd-title">
              Select a payment method
            </div>
            <Nav className="payment-methotd-menu flexbox">
              <NavItem eventKey="first" className="tabs-menu__item">
                <div className="payment-cards__item">
                  <div className="payment-cards__item-description">
                    PayPal
                  </div>
                </div>
              </NavItem>
              <NavItem eventKey="second" className="tabs-menu__item">
                <div className="payment-cards__item ">
                  <div className="payment-cards__item-description">
                    Credit Card
                  </div>
                </div>
              </NavItem>
            </Nav>
          </div>
        </Col>
        <Col md={12} sm={12}>
          <Tab.Content className="tab-wrap-inner tab-wrap-inner__noindent tab-wrap-inner__with-border" animation>
            <Tab.Pane eventKey="first">
              <Row>
                <Col md={12}>
                  <Disclaimer type="PayPal" />
                </Col>
              </Row>
            </Tab.Pane>
            <Tab.Pane eventKey="second">
              <Row>
                <Col md={12}>
                  <Disclaimer type="CreditCard" />
                </Col>
              </Row>
            </Tab.Pane>
          </Tab.Content>
        </Col>
      </Row>
    </Tab.Container>
  </div>
);

export default Payment;
