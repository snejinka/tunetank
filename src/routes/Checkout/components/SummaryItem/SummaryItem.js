import React from 'react';
import accounting from 'accounting';

const SummaryItem = ({ item }) => (
  <div className="order-summary-list__item">
    <div className="order-summary-list__item-title">
      {`${item.name} (${item.quantity})`}
    </div>
    <div className="order-summary-list__item-value">
      {accounting.formatMoney((item.license.price_in_cents * item.quantity) / 100)}
    </div>
  </div>
);

export default SummaryItem;
