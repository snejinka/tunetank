import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col } from 'react-bootstrap';
import accounting from 'accounting';
import payform from 'payform';
import toastr from 'toastr';
import {
  paypalCheckout,
  creditCardCheckout,
  setCardInvalidFields,
  setBillingInvalidFields,
} from '../../modules/checkout';
import { calcTotalAmount } from 'store/cart';
import { validateEmail } from 'common';
import SavedCardsList from '../SavedCardsList';
import CreditCardForm from '../CreditCardForm';

class Disclaimer extends Component {
  constructor(props) {
    super(props);

    this.creditCardCheckout = this.creditCardCheckout.bind(this);
    this.payPalCheckout = this.payPalCheckout.bind(this);
  }

  validateCreditCardForm() {
    const { creditCard, actions } = this.props;
    const invalidFields = [];

    if (!creditCard.name || creditCard.name.length === 0) {
      invalidFields.push('name');
    }

    if (!payform.validateCardNumber(creditCard.number)) {
      invalidFields.push('number');
    }

    if (!payform.validateCardExpiry(creditCard.exp_month, creditCard.exp_year)) {
      invalidFields.push('expiration');
    }

    if (!payform.validateCardCVC(creditCard.cvv)) {
      invalidFields.push('cvv');
    }

    actions.setCardInvalidFields(invalidFields);

    if (invalidFields.length === 0) {
      return true;
    }

    toastr.error('Credit card is invalid.');
    return false;
  }

  validateBillingForm() {
    const { billing, actions } = this.props;
    const invalidFields = [];

    if (!validateEmail(billing.email)) {
      invalidFields.push('email');
    }

    if (billing.email !== billing.emailConfirm || !validateEmail(billing.emailConfirm)) {
      invalidFields.push('emailConfirm');
    }

    if (!billing.name || billing.name.length === 0) {
      invalidFields.push('name');
    }

    if (!billing.address_first_line || billing.address_first_line.length === 0) {
      invalidFields.push('address_first_line');
    }

    if (!billing.country || billing.country.length === 0) {
      invalidFields.push('country');
    }

    if (!billing.city || billing.city.length === 0) {
      invalidFields.push('city');
    }

    if (!billing.zip || billing.zip.length === 0) {
      invalidFields.push('zip');
    }

    actions.setBillingInvalidFields(invalidFields);

    if (invalidFields.length === 0) {
      return true;
    }

    toastr.error('Billing info is invalid.');
    return false;
  }

  creditCardCheckout() {
    if (this.validateCreditCardForm() && this.validateBillingForm()) {
      this.props.actions.creditCardCheckout();
    }
  }

  payPalCheckout() {
    if (this.validateBillingForm()) {
      this.props.actions.paypalCheckout();
    }
  }

  render() {
    const { waiting, type, actions } = this.props;
    switch (type) {
      case ('PayPal'):
        return (
          <div>
            <div className="disclaimer disclaimer-checkout">
              <div className="disclaimer__description">
                <div className=" text-block">
                  <div className="disclaimer__note">
                    {"After payment via payPal's secure checkout. We'll send you a link to download your files."}
                  </div>
                </div>
              </div>
            </div>
            <Row>
              <Col md={12}>
                <div className="checkout-concluding">
                  <div className="checkout-concluding__value">
                    Total {accounting.formatMoney(actions.calcTotalAmount())}
                  </div>
                  <div className="flexbox justify-start">
                    <button
                      className="btn btn-primary btn-primary__big"
                      onClick={this.payPalCheckout}
                      disabled={waiting}
                    >
                      Checkout with PayPal
                    </button>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        );
      case ('CreditCard'):
        return (
          <div>
            <div className="disclaimer disclaimer-checkout">
              <Row>
                <Col md={6}>
                  <CreditCardForm />
                </Col>
                {/* <Col md={6}>
                  <SavedCardsList />
                </Col> */}
              </Row>
            </div>
            <Row>
              <Col md={12}>
                <div className="checkout-concluding">
                  <div className="checkout-concluding__value">
                    Total {accounting.formatMoney(actions.calcTotalAmount())}
                  </div>
                  <div className="flexbox justify-start">
                    <button
                      className="btn btn-primary btn-primary__big"
                      onClick={this.creditCardCheckout}
                      disabled={waiting}
                    >
                      Make Payment
                    </button>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        );
      default:
        return (<div />);
    }
  }
}

const mapStateToProps = state => ({
  cartItems: state.cart.items,
  creditCard: state.checkout.creditCard,
  billing: state.checkout.billing,
  waiting: state.checkout.waiting,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    calcTotalAmount,
    paypalCheckout,
    creditCardCheckout,
    setCardInvalidFields,
    setBillingInvalidFields,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Disclaimer);
