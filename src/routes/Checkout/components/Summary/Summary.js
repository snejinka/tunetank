import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import accounting from 'accounting';
import { calcTotalAmount } from 'store/cart';
import SummaryItem from '../SummaryItem';

const Summary = ({ cartItems, actions }) => (
  <div className="order-summary">
    <div className="order-summary__header">
      Order Summary
    </div>
    <div className="order-summary-list">
      {cartItems.filter(item => item.quantity > 0).map(item => <SummaryItem key={`track-${item.id}-${item.license.id}`} item={item} />)}
    </div>
    <div className="tunetank-total">
      <div className="tunetank-total__title">
        Total
      </div>
      <div className="tunetank-total__value">
        {accounting.formatMoney(actions.calcTotalAmount())}
      </div>
    </div>
  </div>
);

const mapStateToProps = state => ({
  cartItems: state.cart.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    calcTotalAmount,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Summary);
