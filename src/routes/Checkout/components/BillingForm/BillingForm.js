import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
} from 'react-bootstrap';
import { setBillingField, removeBillingInvalidField, getBillingInfo } from '../../modules/checkout';
import ReactCountrySelect from '../../../../components/ReactCountrySelect';

class BillingForm extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
  }

  componentWillMount() {
    this.props.actions.getBillingInfo();
  }

  handleChange(event) {
    const fieldName = event.target.name;
    const fleldVal = event.target.value;
    this.props.actions.setBillingField(fleldVal, fieldName);
    this.props.actions.removeBillingInvalidField(fieldName);
  }

  handleCountryChange(option) {
    this.props.actions.setBillingField(option ? option.value : '', 'country');
    this.props.actions.removeBillingInvalidField('country');
  }

  render() {
    const { billing } = this.props;
    const { invalidFields } = billing;

    return (
      <div className="form-wrap form-wrap__large-right-gap">
        <form action="" className="form-default">
          <div className="form-default__header">
            <div className="title_primary title_primary__small">
            Billing Details
          </div>
          </div>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('email') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              E-mail *
            </ControlLabel>
            <FormControl
              name="email"
              value={billing.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('emailConfirm') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              Confirm e-mail *
            </ControlLabel>
            <FormControl
              name="emailConfirm"
              value={billing.emailConfirm}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('name') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              Name *
            </ControlLabel>
            <FormControl
              name="name"
              value={billing.name}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('company') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              Company Name (optional)
            </ControlLabel>
            <FormControl
              name="company"
              value={billing.company}
              onChange={this.handleChange}
            />
            <HelpBlock>
              If your want your invoices addressed to a company.
            </HelpBlock>
          </FormGroup>

          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('country') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              Country *
            </ControlLabel>
            {billing.country === '' &&
              <ReactCountrySelect
                multi={false}
                onSelect={this.handleCountryChange}
                value={billing.country}
                flagImagePath="/flags/"
              />
            }
            {billing.country !== '' &&
              <ReactCountrySelect
                multi={false}
                onSelect={this.handleCountryChange}
                value={billing.country}
                flagImagePath="/flags/"
              />
            }
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('address_first_line') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              Address line 1 *
            </ControlLabel>
            <FormControl
              name="address_first_line"
              value={billing.address_first_line}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('address_second_line') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              Address line 2 (optional)
            </ControlLabel>
            <FormControl
              name="address_second_line"
              value={billing.address_second_line}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('city') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              City *
            </ControlLabel>
            <FormControl
              name="city"
              value={billing.city}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('state') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              State / Provice / Region
            </ControlLabel>
            <FormControl
              name="state"
              value={billing.state}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('zip') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              ZIP / Postal Code *
            </ControlLabel>
            <FormControl
              name="zip"
              value={billing.zip}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            className={`form-group__indent-bot-big ${invalidFields.includes('vat') ? 'has-error' : ''}`}
          >
            <ControlLabel>
              VAT number (if applicable )
            </ControlLabel>
            <FormControl
              name="vat"
              value={billing.vat}
              onChange={this.handleChange}
            />
          </FormGroup>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  billing: state.checkout.billing,
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setBillingField,
    removeBillingInvalidField,
    getBillingInfo,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(BillingForm);
