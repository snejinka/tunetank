import React from 'react';
import SavedCard from '../SavedCard';

const SavedCardsList = () => (
  <div className="saved-cards-wrap">
    <div className="form-default__header">
      <div className="title_primary title_primary__small">
        Saved Cards
      </div>
    </div>
    <div className="cards-list cards-list__small">
      <SavedCard />
      <SavedCard />
    </div>
  </div>
);

export default SavedCardsList;
