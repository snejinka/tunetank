import React from 'react';
import VisaIcon from '../../../../assets/img/visa-icon.png';
import MasterCardIcon from '../../../../assets/img/master-card-icon.png';

const SavedCard = () => (
  <div className="cards-list__item-wrap">
    <div className="cards-list__item">
      <div className="cards-list__item-image">
        <img src={MasterCardIcon} alt="" className="img-responsive" />
      </div>
      <div className="cards-list__item-description">
        <div className="cards-list__item-note">
          <ul className="list-inline">
            <li>****</li>
            <li>****</li>
            <li>****</li>
            <li>1234</li>
          </ul>
        </div>
        <div className="cards-list__item-title">
          Master Card
        </div>
      </div>
      <div className="cards-list__item-date">
        (10/18)
      </div>
    </div>
  </div>
);

export default SavedCard;
