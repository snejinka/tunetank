import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import Spinner from 'components/Spinner';
import { isCartEmpty } from 'store/cart';
import { completePayment } from '../../modules/checkout';
import BreadCrumbs from '../BreadCrumbs';
import BillingForm from '../BillingForm';
import Summary from '../Summary';
import Payment from '../Payment';
import EmptyCart from '../../../ShoppingCart/components/EmptyCart/EmptyCart';

class CheckoutView extends Component {
  componentDidMount() {
    const token = this.props.location.query.token;
    const paymentId = this.props.location.query.paymentId;
    const payerId = this.props.location.query.PayerID;

    const paymentInfo = {
      payment_id: paymentId,
      payer_id: payerId,
      token,
    };

    if (token && paymentId && payerId) {
      this.props.actions.completePayment(paymentInfo);
    }
  }

  render() {
    if (this.props.waiting) { return <Spinner />; }
    if (this.props.actions.isCartEmpty()) {
      return (
        <div>
          <BreadCrumbs />
          <Grid className="checkout">
            <Row>
              <Col md={9}>
                <EmptyCart />
              </Col>
            </Row>
          </Grid>
        </div>
      );
    }

    return (
      <div>
        <BreadCrumbs />
        <Grid className="checkout">
          <Row>
            <Col md={12}>
              <div className="checkout__header">
                <div className="title title_primary">
                  Checkout
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={9}>
              <BillingForm />
            </Col>
            <Col md={3}>
              <Summary />
            </Col>
          </Row>
          <Row>
            <Col md={9}>
              <Payment />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cartItems: state.cart.items,
  waiting: state.checkout.waiting,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    completePayment,
    isCartEmpty,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutView);
