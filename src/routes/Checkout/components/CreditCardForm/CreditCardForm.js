import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
} from 'react-bootstrap';
import NumberFormat from 'react-number-format';
import { SimpleSelect } from 'react-selectize';
import VisaIcon from '../../../../assets/img/visa-icon.png';
import MasterCardIcon from '../../../../assets/img/master-card-icon.png';
import { setCardField, removeCardInvalidField } from '../../modules/checkout';

class CreditCardForm extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const fieldName = event.target.name;
    const fleldVal = event.target.value;
    this.props.actions.setCardField(fleldVal, fieldName);
    this.props.actions.removeCardInvalidField(fieldName);
  }

  handleExpirationChange(option, fieldName) {
    this.props.actions.setCardField(option.value, fieldName);
    this.props.actions.removeCardInvalidField('expiration');
  }

  render() {
    const { creditCard } = this.props;
    const { invalidFields } = creditCard;
    const currenYear = new Date().getFullYear();
    const years = [currenYear, currenYear + 1, currenYear + 2, currenYear + 3];
    const months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    return (
      <div>
        <FormGroup className={`form-group__indent-bot-medium ${invalidFields.includes('name') ? 'has-error' : ''}`}>
          <ControlLabel>
            Name on card
          </ControlLabel>
          <FormControl
            name="name"
            value={creditCard.name}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup className={`form-group__indent-bot-medium ${invalidFields.includes('number') ? 'has-error' : ''}`}>
          <ControlLabel>
            Card Number
          </ControlLabel>
          <NumberFormat
            name="number"
            customInput={FormControl}
            format="#### #### #### ####"
            value={creditCard.number}
            onChange={this.handleChange}
          />
          <div className="card-icon-list flexbox align-center">
            <div className="card-icon-list__item">
              <div className="card-icon-list__item-image">
                <img src={VisaIcon} alt="" className="img-responsive" />
              </div>
            </div>
            <div className="card-icon-list__item">
              <div className="card-icon-list__item-image">
                <img src={MasterCardIcon} alt="" className="img-responsive" />
              </div>
            </div>
          </div>
        </FormGroup>
        <Row>
          <Col md={6}>
            <FormGroup
              className={`form-group__indent-bot-big ${invalidFields.includes('expiration') ? 'has-error' : ''}`}
            >
              <ControlLabel>
                Exp Month
              </ControlLabel>
              <SimpleSelect
                options={months.map(month => ({ label: month.toString(), value: month }))}
                defaultValue={months.find(month => month.value === creditCard.exp_month)}
                onValueChange={newOption => this.handleExpirationChange(newOption, 'exp_month')}
                className="selectize-select-theme react-default-template simple-select__border-dark"
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup
              className={`form-group__indent-bot-big ${invalidFields.includes('expiration') ? 'has-error' : ''}`}
            >
              <ControlLabel>
                Exp Year
              </ControlLabel>
              <SimpleSelect
                options={years.map(year => ({ label: year.toString(), value: year }))}
                defaultValue={years.find(year => year.value === creditCard.exp_year)}
                onValueChange={newOption => this.handleExpirationChange(newOption, 'exp_year')}
                className="selectize-select-theme react-default-template simple-select__border-dark"
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <FormGroup className={`form-group__indent-bot-medium ${invalidFields.includes('cvv') ? 'has-error' : ''}`}>
              <ControlLabel>
                Security Code
              </ControlLabel>
              <NumberFormat
                className="form-control__short-field"
                type="password"
                name="cvv"
                customInput={FormControl}
                format="###"
                value={creditCard.cvv}
                onChange={this.handleChange}
              />
              <HelpBlock className>
                Last 3 digits on back of your card.
              </HelpBlock>
            </FormGroup>
          </Col>
        </Row>
        {/* <Row>
          <Col md={12}>
            <FormGroup>
              <div className="radio-box radio-box__square radio-box__nogap">
                <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
                <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
                <ControlLabel htmlFor="radioGroup121" className="radio-box__note radio-box__note_light">
                  Save card for the next time
                </ControlLabel>
              </div>
            </FormGroup>
          </Col>
        </Row> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  creditCard: state.checkout.creditCard,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setCardField,
    removeCardInvalidField,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreditCardForm);
