import { injectReducer } from '../../store/reducers';
import { requireAuth } from '../../routes/index';
import CheckoutView from './components/CheckoutView';

export default store => ({
  path: 'checkout',
  onEnter: requireAuth,
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const reducer = require('./modules/checkout').default;
      injectReducer(store, { key: 'checkout', reducer });
      cb(null, CheckoutView);
    }, 'Checkout');
  },
});
