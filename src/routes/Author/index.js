import AuthorView from './components/AuthorView';
import { injectReducer } from '../../store/reducers';

export default store => ({
  path: 'author/:userSlug/:id',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const reducer = require('./modules/withdraw').default;
      injectReducer(store, { key: 'withdraw', reducer });
      cb(null, AuthorView);
    }, 'AuthorView');
  },
});