import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import WithdrawalsDetailsTable from '../WithdrawalsDetailsTable';
import WithdrawalsPayment from '../WithdrawalsPayment';
import { requestLastWithdraws } from '../../modules/withdraw';
import accounting from 'accounting';
import _ from 'lodash';
import moment from 'moment';

const MIN_WITHDRAW_AMOUNT_IN_CENTS = 5000;

class Withdrawals extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showWithdrawal: false,
      selectedPaymentType: 'all',
    };
  }

  componentWillMount(){
    this.props.actions.requestLastWithdraws();
  }

  showWithdrawal() {
    this.setState({ showWithdrawal: true });
  }

  hideWithdrawl() {
    this.setState({
      showWithdrawal: false,
    })
  }

  switchPaymentType(type) {
    this.setState({ selectedPaymentType: type });
  }

  hasWithdrawThisMonth() {
    const withdrawArrayLength = this.props.withdrawals.length;
    if (withdrawArrayLength > 0) {
      let anyItem = _.filter(this.props.withdrawals, (item) => {
        return item.status === 0 && moment(item.created_at).isBetween(moment().startOf('month'), moment().endOf('month'))
      });
      return anyItem.length > 0 ? true : false
    }
    else {
      return false;
    } 
  }

  getWithdrawalPayment() {
    if (this.state.showWithdrawal) {
      return (
        <Row>
          <Col md={12}>
            <div className="tab__header tab__header-with-border">
              <div className="title_primary title_primary__inner title_primary__indent-bot">New Withdrawals</div>
              <div className="title_sub-note align_left">
                You can withdraw up to {accounting.formatMoney(this.props.currentUser.balance_in_cents / 100)}
              </div>
            </div>
            <WithdrawalsPayment
              active={this.state.selectedPaymentType}
              switchType={this.switchPaymentType.bind(this)}
              hideWithdrawl={this.hideWithdrawl.bind(this)}
            />
          </Col>
        </Row>
      );
    }
  }

  render() {
    const disabledWithdraw = this.props.currentUser.balance_in_cents < MIN_WITHDRAW_AMOUNT_IN_CENTS || this.hasWithdrawThisMonth();

    return (
      <div className="">
        <Row>
          <Col md={12}>
            {this.props.withdrawals.length > 0 &&
              <div>
                <div className="title_sub-wrap">
                  <div className="title_sub-note title_sub-note__withdrawals align_left">
                    You curently have withdrawals pending and queued for processing.
                  </div>
                </div>
                <WithdrawalsDetailsTable withdrawals={this.props.withdrawals}/>
              </div>
            }
            {disabledWithdraw && 
              <div className="title_sub-wrap">
                <div className="title_sub-note title_sub-note__withdrawals align_left">
                  { this.hasWithdrawThisMonth() ?
                    ('You have already requested withdrawal this month!') :
                    (
                      `You have insufficient funds to withdraw. Minimum withdraw amount is ${accounting.formatMoney(MIN_WITHDRAW_AMOUNT_IN_CENTS / 100)}.`)
                  }
                </div>
              </div>
            }
            <div className="buttons-group-wrap flexbox justify-start align-center">
              <div className="btn-primary-wrap">
                <button className="btn btn-primary btn-primary__big" disabled={disabledWithdraw} onClick={this.showWithdrawal.bind(this)}>
                  Make a Withdrawal
                </button>
              </div>
            </div>
          </Col>
        </Row>
        {this.getWithdrawalPayment()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  withdrawals: state.withdraw.lastWithdraws,
  currentUser: state.user.currentUser
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    requestLastWithdraws
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Withdrawals);
