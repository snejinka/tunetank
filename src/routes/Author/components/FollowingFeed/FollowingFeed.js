import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PaginationAdvanced from 'components/Pagination';
import TrackList from 'components/TrackList';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';
import { Link } from 'react-router';

class FollowingFeed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allPages: Math.ceil(props.followingTracks.length / 5),
      currentPage: 1,
      perPageTracks: props.followingTracks.slice(0, 5)
    };

    this.changePage = this.changePage.bind(this);
  }

  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.followingTracks);
    this.props.actions.setTrackPlaying(playTrack);
  }

  changePage(current) {
    const offset = current * 5;
    const perSlide = this.props.followingTracks.slice(offset - 5, offset);

    this.setState({
      perPageTracks: perSlide,
      currentPage: current,
    });
  }

  render() {
    if (this.props.is_featured) {
      return (
        <div className="list-box">
          <div className="tab__header flexbox justify-between align-end">
            <div className="title_primary title_primary__inner">
                My Following Feed
            </div>
          </div>
          <div className="author-panel__disabled">
            <div className="author-panel__disabled-bg">
              <div className="author-panel__disabled-header author-panel__disabled-header--following">
                  You aren't following anyone yet
                </div>
              <Link href="/catalog" className="author-panel__disabled-subheader">
                  Find people to follow
              </Link>
            </div>
          </div>
        </div>
      );
    }

    let showPagination = true;
    if (this.state.perPageTracks.length == 0 || this.state.currentPage == 0) { showPagination = false; }

    return (
      <div className="list-box">
        <div className="tab__header flexbox justify-between align-end">
          <div className="title_primary title_primary__inner">
            My Following Feed
          </div>
        </div>
        <TrackList
          tracks={this.state.perPageTracks}
          setTracks={playTrack => this.setTracks(playTrack)}
          viewAuthorTracks={false}
        />
        {showPagination && <PaginationAdvanced changePage={this.changePage} allPages={this.state.allPages} />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  followingTracks: state.followingFeed.followingTracks,
  is_featured: state.followingFeed.is_featured
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTracks,
    setTrackPlaying,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FollowingFeed);
