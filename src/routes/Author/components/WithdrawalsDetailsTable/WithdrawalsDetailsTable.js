import React, { PropTypes } from 'react';
import { Row, Col } from 'react-bootstrap';
import moment from 'moment';

const STATUSES = {
  0: 'Pending',
  1: 'Pending',
  2: 'Completed',
  3: 'Failed',
}

class WithdrawalsDetailsTable extends React.Component {
  static propTypes = {
    withdrawals: PropTypes.array
  }

  render() {
    return(
      <div className="table table-default">
        <Row className="table-default__header">
          <Col md={3}>Date Requested</Col>
          <Col md={5}>Details</Col>
          <Col md={4}>Status</Col>
        </Row>
        {this.props.withdrawals.map((item, i) => {
          return(
            <Row className="table-default__row">
              <Col md={3}>
                <div className="table-default__title">
                    {moment(item.created_at).format('D MMM YYYY, ddd')}
                </div>
              </Col>
              <Col md={5}>
                <div className="table-default__title">${item.amount_in_cents / 100}</div>
                <div className="table-default__note">to {item.paypal_receiver} via PayPal</div>
              </Col>
              <Col md={4}>
                <div className="table-default__title">
                  {STATUSES[item.status]}
                </div>
                <div className="table-default__note">
                  Updated at {moment(item.upadted_at).format('D MMM YYYY')}
                </div>
              </Col>
            </Row>
          );
        })}
      </div>
    );
  }
}

export default WithdrawalsDetailsTable;
