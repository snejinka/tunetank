import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import TrackList from '../../../../components/TrackList';
import PaginationAdvanced from '../../../../components/Pagination';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';
import SelectizeSimpleSelectTheme from 'components/SelectizeSimpleSelectTheme';

const PER_PAGE_TRACKS_COUNT = 15;

class MyDownloads extends React.Component {
  static sortByDate(array) {
    return array.sort((a, b) => Date.parse(b.created_at) - Date.parse(a.created_at));
  }

  constructor(props) {
    super(props);

    const { boughtTracks } = props;
    this.state = {
      boughtTracks,
      allPages: boughtTracks && Math.ceil(boughtTracks.length / PER_PAGE_TRACKS_COUNT),
      currentPage: 1,
      perPageTracks: boughtTracks && MyDownloads.sortByDate(boughtTracks).slice(0, PER_PAGE_TRACKS_COUNT),
    };
    this.changePage = this.changePage.bind(this);
  }

  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.favoriteTracks);
    this.props.actions.setTrackPlaying(playTrack);
  }

  changePage(current) {
    const offset = current * PER_PAGE_TRACKS_COUNT;
    const perSlide = MyDownloads.sortByDate(this.state.boughtTracks).slice(offset - PER_PAGE_TRACKS_COUNT, offset);

    this.setState({
      perPageTracks: perSlide,
      currentPage: current,
    });
  }

  render() {
    const { userInfo, boughtTracks } = this.props;
    const { perPageTracks } = this.state;

    if (!boughtTracks || boughtTracks.length === 0) {
      return (
        <div className="list-box">
          <div className="tab__header flexbox justify-between align-end">
            <div className="title_primary title_primary__inner">
              My Downloads
            </div>
            <div className="sort-panel sort-panel__indent flexbox align-center">
              <span className="sort-panel__title sort-panel__title__nowrap" >Sort by</span>
              <SelectizeSimpleSelectTheme/>
            </div>
          </div>
          <div className="author-panel__disabled">
            <div className="author-panel__disabled-bg">
              <div className="author-panel__disabled-header author-panel__disabled-header--downloads">
                You haven’t downloaded anything yet.
              </div>
              <Link to="/catalog" className="author-panel__disabled-subheader">
                Find some awesome tracks
              </Link>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="list-box">
        <div className="tab__header flexbox justify-between align-end">
          <div className="title_primary title_primary__inner">
            My Downloads 
          </div>
          <div className="sort-panel sort-panel__indent flexbox align-center">
            <span className="sort-panel__title sort-panel__title__nowrap" >Sort by</span>
            <SelectizeSimpleSelectTheme/>
          </div>
        </div>

        <TrackList
          tracks={perPageTracks}
          userInfo={userInfo}
          setTracks={playTrack => this.setTracks(playTrack)}
          isDownload
          isBought
        />

        <PaginationAdvanced
          changePage={this.changePage}
          allPages={this.state.allPages}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.user.userInfo,
  boughtTracks: state.user.userInfo.bought_tracks,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTracks,
    setTrackPlaying,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyDownloads);
