import React from 'react';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';


export const WithdrawalsDisclaimer = ({ type }) => {
  switch (type) {
    case ('paypal'):
      return (
        <div className="disclaimer">
          <div className="disclaimer__header-wrap">
            <div className="title_primary title_primary__thin">
                Your PayPal Account
            </div>
          </div>

          <div className="disclaimer__description">
            <div className=" text-block">
              <div className="disclaimer__note">
                  Get payd by credit or debit card, PayPal transfer or even via bank account in just a few clicks. All you need is your e-mail address.
              </div>
              <div className="disclaimer__note">
                <a href="https://www.paypal.com/us/home" target="_blank" className="action-link action-link__bold">More about PayPal / Create account</a>.
              </div>
            </div>
          </div>
        </div>
      );
    case ('swift'):
      return (
        <div className="disclaimer">
          <div className="disclaimer__header-wrap">
            <div className="title_primary title_primary__thin">
                            Add a New SWIFT Account
                        </div>
          </div>

          <div className="disclaimer__description">
            <div className=" text-block">
              <div className="disclaimer__note">
                                Due to limitations placed on SWIFT withdrawals by financial instructions, fields must <b>NOT</b> contain
                                the following special characters.
                            </div>
              <div className="disclaimer__note-exaxmple">
                                _  ,  #  &  $  @  %  /  (  )  *  !
                            </div>
            </div>
          </div>
        </div>
      );
    case ('payoneer'):
      return (
        <div className="disclaimer">
          <div className="disclaimer__header-wrap">
            <div className="title_primary title_primary__thin">
                            Add a New SWIFT Account
                        </div>
          </div>

          <div className="disclaimer__description">
            <div className=" text-block">
              <div className="disclaimer__note">
                                Due to limitations placed on SWIFT withdrawals by financial instructions, fields must <b>NOT</b> contain
                                the following special characters.
                            </div>
              <div className="disclaimer__note-exaxmple">
                                _  ,  #  &  $  @  %  /  (  )  *  !
                            </div>
            </div>
          </div>
        </div>
      );
    default:

  }
};

export default WithdrawalsDisclaimer;
