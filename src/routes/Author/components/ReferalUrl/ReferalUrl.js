import React from 'react';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';

export const ReferalUrl = () => (
  <div>
    <div className="tab__header tab__header-with-note">
      <div className="title_primary title_primary__inner">Referral URL</div>
      <div className="title_sub-note title_sub-note__top-indent">
                        Learn more about our referral program on <a href="" className="link-away">this page</a>.
                    </div>
    </div>
    <div className="form-wrap form-wrap__large-right-gap">
      <div className="form-wrap__header form-wrap__header-top-indent">
        <div className="title_primary title_primary__small title_primary__letter-indent title-note__ref-align align_left">
                            Generate a referral URL
                        </div>
        <div className="title title-note  title-note__ref-align align_left">
                            Entern your usermane and paste any website URL below to generate a referral link.
                        </div>
      </div>

      <form action="" className="form-default">
        <FormGroup className="form-group__indent-bot-big">
          <ControlLabel>
                                Your username *
                            </ControlLabel>
          <FormControl />
        </FormGroup>
        <FormGroup className="form-group__indent-bot-big">
          <ControlLabel>
                                Website page *
                            </ControlLabel>
          <FormControl placeholder="http://websitename.com/" />
        </FormGroup>
        <FormGroup className="form-group__beyond">
          <ControlLabel>
                                Referral link
                            </ControlLabel>
          <FormControl />
          <HelpBlock className="help-block__color-black">
                                Copy this referral link and paste it anywhere on your website.
                            </HelpBlock>
        </FormGroup>
      </form>
    </div>
  </div>
         );

export default ReferalUrl;
