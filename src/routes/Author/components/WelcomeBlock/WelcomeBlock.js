import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import 'styles/variables.scss';
import WelcomeBlockBackground from 'assets/img/production-bg-2.png';
import moment from 'moment';
import ContactAuthor from '../ContactAuthor';
import { Grid, Row, Col, Tooltip, OverlayTrigger, ButtonToolbar, Button, ProgressBar } from 'react-bootstrap';
import {
  setUserSecureId,
  setUserAvatar,
  setUserBanner,
  followAuthor,
  unfollowAuthor,
  updateExactFealds,
} from 'store/user';
import NoCover from 'assets/img/no-cover.png';
import NoAvatar from 'assets/img/no-avatar.png';
import classNames from 'classnames';
import { openModal } from 'store/user.js';
import { API_SERVER_URL } from 'api';

class WelcomeBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      createdAt: moment(props.userInfo.created_at).format('MMMM YYYY'),
      followed: false,
      followers: props.userInfo.followers,
      hoverAvatar: false,
      uploadProgress: 0,
    };
  }
  static propTypes = {
    userInfo: PropTypes.object,
    loggedIn: PropTypes.bool,
    openModal: PropTypes.func,
  }

  componentWillMount() {
    if (this.props.loggedIn) {
      const followings = this.props.currentUser.followings;
      for (const i in followings) {
        if (followings[i].id === this.props.userInfo.id) {
          this.setState({
            followed: true,
          });
        }
      }
    }
  }

  submitContactForm(title) {
  }

  onProgress(progress) {
    this.setState({
      uploadProgress: progress,
    });
  }

  follow() {
    if (this.props.loggedIn) {
      this.setState({
        followed: true,
      });
      this.props.actions.followAuthor(this.props.userInfo.id);
    } else {
      this.props.actions.openModal('sign_up');
    }
  }

  unfollow() {
    if (this.props.loggedIn) {
      this.setState({
        followed: false,
      });
      this.props.actions.unfollowAuthor(this.props.userInfo.id);
    } else {
      this.props.actions.openModal('sign_up');
    }
  }

  onAvatarUploadFinish = (res) => {
    const fileUrl = res.signedUrl.split('?')[0];
    this.props.actions.setUserAvatar(fileUrl);
    this.props.actions.setUserSecureId(res.secureId);
    this.uploadUserPhoto(this.props.userInfo.avatar);
  }

  onBannerUploadFinish = (res) => {
    const fileUrl = res.signedUrl.split('?')[0];
    this.props.actions.setUserBanner(fileUrl);
    this.props.actions.setUserSecureId(res.secureId);
    this.uploadUserBanner();
  }

  uploadUserPhoto = () => {
    this.props.actions.updateExactFealds(this.props.userInfo.id, { avatar: this.props.userInfo.avatar });
  }

  uploadUserBanner = () => {
    this.props.actions.updateExactFealds(this.props.userInfo.id, { banner: this.props.userInfo.banner });
  }

  onMouseOut(e) {
    if (this.refs.avatar.contains(e.relatedTarget)) {
      this.setState({
        hoverAvatar: true,
      });
    } else {
      this.setState({
        hoverAvatar: false,
      });
    }
  }

  getAuthorButtons() {
    if (this.props.userInfo.is_current) {
      return null;
    }
    return (
      <div className="follow-box__btn-group">
        {this.state.followed ?
          <button type="button" className="btn btn-primary" onClick={() => this.unfollow()}>Unfollow
                  Author</button>
                :
          <button type="button" className="btn btn-primary" onClick={() => this.follow()}>Follow
                  Author</button>
            }
        <ContactAuthor />
      </div>
    );
  }


  render() {
    const ReactS3Uploader = require('react-s3-uploader');
    const bgAvatar = this.props.userInfo.banner ? this.props.userInfo.banner : NoCover;
    const tooltip = (
      <Tooltip id="tooltip">For best quality please use 2545x450 pictures</Tooltip>
    );
    const avatar = this.props.userInfo.avatar ? this.props.userInfo.avatar : NoAvatar;
    return (
      <div className="welcom-block wrapper-fluid">
        <div className="welcom-block__background-image" style={{ backgroundImage: `url(${bgAvatar})` }}></div>
        <Grid className="welcom-block__bottom-align">
          <div className="welcom-block-wrap" onMouseOut={this.onMouseOut.bind(this)}>
            <Row>
              <Col md={12}>
                <div className="follow-box flexbox">
                  <div className="follow-box__photo" ref="avatar" style={{ backgroundImage: `url(${avatar})` }}>
                    {this.props.userInfo.is_current &&

                      <label className="download-new-photo">
                        <ReactS3Uploader
                          signingUrl="/api/v1/s3/user_signed_url"
                          signingUrlMethod="POST"
                          onFinish={this.onAvatarUploadFinish}
                          onProgress={event => this.onProgress(event)}
                          signingUrlQueryParams={{ secureId: this.props.userInfo.secure_id }}
                          server={API_SERVER_URL}
                        />
                        <span className="download-new-photo__text">Upload photo</span>
                      </label>
                                  }


                  </div>
                  <div className={classNames('follow-box__inner-group', { 'follow-box__inner-group_flex': (this.props.loggedIn && this.props.userInfo.is_current) })}>
                    <div className="follow-box__header-wrap">
                      <div className="follow-box__header">
                        {this.props.userInfo.name}
                      </div>
                      <div className="follow-box__header-small">
                                            Registred since {this.state.createdAt}
                      </div>
                    </div>

                    {this.getAuthorButtons()}

                  </div>
                </div>
              </Col>
            </Row>
            {this.props.userInfo.is_current &&

              <OverlayTrigger placement="bottom" overlay={tooltip} trigger="hover">
                <label className={classNames('download-new-photo download-new-photo_banner btn btn-primary')}>
                  <ReactS3Uploader
                    signingUrl="/api/v1/s3/user_signed_url"
                    signingUrlMethod="POST"
                    onProgress={event => this.onProgress(event)}
                    onFinish={this.onBannerUploadFinish}
                    signingUrlQueryParams={{ secureId: this.props.userInfo.secure_id }}
                    server={API_SERVER_URL}
                  />
                  <span className="download-new-photo__text">Upload background</span>
                </label>
              </OverlayTrigger>
                  }
          </div>
        </Grid>
        {this.state.uploadProgress > 0 && this.state.uploadProgress < 100 &&
          <div className="upload-progress-bar__hack">
            <ProgressBar active now={this.state.uploadProgress} />
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
  userInfo: state.user.userInfo,
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setUserSecureId,
    setUserAvatar,
    setUserBanner,
    followAuthor,
    unfollowAuthor,
    updateExactFealds,
    openModal,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeBlock);
