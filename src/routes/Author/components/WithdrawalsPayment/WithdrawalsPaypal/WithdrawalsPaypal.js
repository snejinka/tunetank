import React, { PropTypes } from 'react';
import { Tab, FormGroup, ControlLabel, FormControl, Row, Col, HelpBlock } from 'react-bootstrap';
import WithdrawalsDisclaimer from '../../WithdrawalsDisclaimer';
import * as common from 'common';

class WithdrawalsPaypal extends React.Component {
  constructor(props){
      super(props);
      this.state = {
        email: '',
        confirm_email: '',
        submitted: true
      }
      this.handleChangeInput = this.handleChangeInput.bind(this);
  }

  handleChangeInput(e){
    var value = e.target.value;
    var name = e.target.name;
    this.setState({[name]: value});
  }

  renderValidationErrors(){
        var isValidEmail = common.validateEmail(this.state.email);
        var isValidConfirmEmail = common.validateEmail(this.state.confirm_email);
        if(!this.state.submitted)
            return false;

        if(this.state.email == '' || this.state.confirm_email == '')
            return false;

        if(isValidEmail && isValidConfirmEmail)
            return false;

        return(
              <HelpBlock className="not-valid" style={{textAlign: 'center'}}>
                Oops! That email / confirm email combination is not valid.
            </HelpBlock>
        );
  }

  render() {
    return(
        <Tab.Pane eventKey="first">
            <Row>
                <Col md={12}>
                    <WithdrawalsDisclaimer type="paypal" />
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <div className="form-wrap form-wrap__large-right-gap">
                        <form className="form-default">
                            {this.renderValidationErrors()}
                            <FormGroup className="form-group__indent-bot-big">
                                <ControlLabel>
                                    E-mail Adress *
                                </ControlLabel>
                                <FormControl
                                  type="email"
                                  name="email"
                                  value={this.state.email}
                                  onChange={this.handleChangeInput}
                                />
                            </FormGroup>
                            <FormGroup>
                                <ControlLabel>
                                    Confirm e-mail Adress *
                                </ControlLabel>
                                <FormControl
                                  type="email"
                                  name="confirm_email"
                                  value={this.state.confirm_email}
                                  onChange={this.handleChangeInput}
                                />
                            </FormGroup>
                        </form>
                    </div>
                    <div className="text-block text-block__indent-bot">
                        <p>This will be your default withdrawal account. You can change this at any time.</p>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                <div className="form-wrap form-wrap__large-right-gap">
                    <form className="form-default ">
                    <div className="title title__form-empty">
                        You need to register a PayPal account to continue. <a href="" className="action-link action-link__bold">Click here to register</a>.
                    </div>
                    </form>
                </div>
                </Col>
            </Row>
            <div className="flexbox justify-start">
                <button className="btn btn-primary btn-primary__big disabled">Submit Request</button>
            </div>
        </Tab.Pane>
    );
  }
}

export default WithdrawalsPaypal;