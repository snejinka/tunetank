import React, { PropTypes } from 'react';
import { Row, Col, FormGroup, Tabs, Tab, Nav, NavItem, MenuItem, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import WithdrawalsDisclaimer from '../WithdrawalsDisclaimer';
import SelectizeSimpleSelectTheme from '../SelectizeSimpleSelectTheme';
import WithdrawalsPaypal from './WithdrawalsPaypal';
import { requestWithdraw } from '../../modules/withdraw';
import confirm from '../../../../components/confirm/confirm';
import { getCurrentUser } from 'store/user.js';

class WithdrawalsPayment extends React.Component {
  constructor(props){
    super(props);
    this.requestWithdraw = this.requestWithdraw.bind(this);
    this.state = {
      email: this.props.currentUser.paypal_account,
      value: null,
    }
    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.handleMoneyInput = this.handleMoneyInput.bind(this);
  }

  static propTypes = {
    active: PropTypes.string,
    switchType: PropTypes.func,
  }

  requestWithdraw(){
    const confirmAmountTxt = this.props.active === 'all' ? +(this.props.currentUser.balance_in_cents / 100) : this.state.value;

    confirm(`Are you sure you want to withdraw $${confirmAmountTxt} ?`,
      { title: 'Withdraw confrimation',
        btnText: 'Withdraw',
      }
    ).then(() => {
      this.props.actions.requestWithdraw({
        email: this.state.email,
        value: this.state.value,
        type: this.props.active
      }).then((res) => {
        this.props.actions.getCurrentUser();
        this.setState({
          value: null
        }, () => {
          this.props.hideWithdrawl();
        })
      })
    });
  }

  handleMoneyInput(e) {
    const value = e.target.value;
    const name = e.target.name;
  
    if (Number.isInteger(+value) && (value >= 0)) {
      this.setState({[name]: value});
    } else {
      e.target.value = this.state.value;
    }
  }

  handleChangeInput(e){
    var value = e.target.value;
    var name = e.target.name;
    this.setState({[name]: value});
  }

  render(){
    var disabledSubmit = false;
    if(this.props.active == 'single' && (this.state.value < 30 || this.state.value > this.props.currentUser.balance_in_cents / 100)) {
      disabledSubmit = true;
    }
    
    return(
      <div className="payment-cards">
        <Row className="payment-cards__item-wrap flexbox flex-wrap">
          <Col md={12} className="payment-cards__title">
            1. Select a payment type *
          </Col>
          <Col md={4}>
            <div className={classNames('payment-cards__item', { 'payment-cards__item__active': this.props.active === 'single' })}
              onClick={() => this.props.switchType('single')}
            >
              <div className="payment-cards__item-header">
                  Single Payment
              </div>
              <div className="payment-cards__item-description">
                <FormGroup className="form-default form-default__payment-field">
                  <ControlLabel>$</ControlLabel>
                  <FormControl
                    placeholder="0"
                    value={this.props.value}
                    name="value"
                    onChange={this.handleMoneyInput}
                  />
                </FormGroup>
              </div>
              <div className="payment-cards__item-note">
                Minimum values apply $30
              </div>
            </div>
          </Col>
          <Col md={4}>
            <div
              className={classNames('payment-cards__item', { 'payment-cards__item__active': this.props.active === 'all' })}
              onClick={() => this.props.switchType('all')}
            >
              <div className="payment-cards__item-header">
                  Single Payment
              </div>
              <div className="payment-cards__item-description">
                  All earnings
              </div>
            </div>
          </Col>
        </Row>
        {/* <div className="payment-cards__item-wrap">
          <div className="payment-cards__title">
              2. Select a payment method *
          </div>
          <Tab.Container id="tabs-inner" defaultActiveKey="first">
            <Row className="clearfix">
              <Col sm={12}>
                <Nav className="payment-methotd-menu flexbox justify-between">
                  <NavItem eventKey="first" className="tabs-menu__item">
                    <div className="payment-cards__item">
                      <div className="payment-cards__item-description">
                        PayPal
                      </div>
                      <div className="payment-cards__item-note">
                        Minimum $5.00
                      </div>
                    </div>
                  </NavItem>
                </Nav>
              </Col>
              <Col md={12} sm={12}>
                <Tab.Content className="tab-wrap-inner tab-wrap-inner__small-inent hidden" animation>
                  <Tab.Pane eventKey="second">
                    <Row>
                      <Col md={12}>
                        <WithdrawalsDisclaimer type="swift" />
                      </Col>
                    </Row>
                    <Row>
                      <Col md={12}>
                        <div className="form-wrap form-wrap__large-right-gap">
                          <form className="form-default">
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Name
                              </ControlLabel>
                              <FormControl />
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Billing Address Line 1 *
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Street adress
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Billing Address Line 2 *
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Level, unit or room nomber
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Billing Address Line 3 *
                              </ControlLabel>
                              <FormControl />
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  City *
                              </ControlLabel>
                              <FormControl />
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  State
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Up to 4 letters, numbers or spaces, e.g. Illinoise becomes IL
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Postcode *
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Up to 8 letters or numbers
                              </HelpBlock>
                            </FormGroup>
                            <ControlLabel>
                                Country *
                            </ControlLabel>
                            <FormGroup >
                              <SelectizeSimpleSelectTheme />
                            </FormGroup>
                          </form>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={12}>
                        <div className="form-wrap form-wrap__large-right-gap">
                          <form className="form-default">
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Bank Account Holder’s Name *
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Your full name appears on your bank account statement
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Bank Account Number / IBAN *
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Up to 34 letters and numbers. Australian account numbers should include the BSB number
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  SWIFT Code *
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Either 8 or 11 characters e.g. ABNAUS33 or 1234567891
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Bank Name in full *
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Up to 30 letters, numbers or spaces
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Bank Branch Country *
                              </ControlLabel>
                              <SelectizeSimpleSelectTheme />
                            </FormGroup>
                          </form>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={12}>
                        <div className="form-wrap form-wrap__indent-bot form-wrap__large-right-gap">
                          <form className="form-default">
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Intermediary Bank - Bank Code
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Either 8 or 11 characters e.g. ABNAUS33 or 1234567891
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Intermediary Bank - Name
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  e.g. Citybank
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup className="form-group__indent-bot-big">
                              <ControlLabel>
                                  Intermediary Bank - City
                              </ControlLabel>
                              <FormControl />
                              <HelpBlock>
                                  Up to 12 letters, numbers or spaces
                              </HelpBlock>
                            </FormGroup>
                            <FormGroup>
                              <ControlLabel>
                                  Intermediary Bank - Country
                              </ControlLabel>
                              <SelectizeSimpleSelectTheme />
                              <HelpBlock>
                                  Up to 12 letters, numbers or spaces
                              </HelpBlock>
                            </FormGroup>
                          </form>
                        </div>
                        <div className="text-block text-block__indent-bot">
                          <p className="text-block__color-lighter">Please note that $25.00 transaction fee is charged on all Bank Transfers.</p>
                          <p className="text-block__color-lighter">[service] cannot be held responsive for delays, extra costs or financial loss
                                                            that arises from being provided incorrect account information, so please ensure that you double check the details with
                                                            your financial institution prior to submitting a request for a Bank Transfer.</p>
                          <p>This will be your default withdrawal account. You can change this at any time.</p>
                          <p>Payment amount after fee: $0.00</p>
                        </div>
                      </Col>
                    </Row>
                    <div className="flexbox justify-start">
                      <button className="btn btn-primary btn-primary__big">Submit Request</button>
                    </div>
                  </Tab.Pane>
                  <Tab.Pane eventKey="third">
                    <Row>
                      <Col md={12} className="hidden">
                        <WithdrawalsDisclaimer type="payoneer" />
                      </Col>
                    </Row>
                    <Row>
                      <Col md={12}>
                        <div className="form-wrap form-wrap__large-right-gap">
                          <form className="form-default ">
                            <div className="title title__form-empty">
                                You need to register a Payoneer account to continue. <a href="" className="action-link action-link__bold">Click here to register</a>.
                            </div>
                          </form>
                        </div>
                      </Col>
                    </Row>
                    <div className="flexbox justify-start">
                      <button className="btn btn-remote btn-primary btn-primary__big">Submit Request</button>
                    </div>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </div> */}
        <WithdrawalsDisclaimer type="paypal" />
        <Row>
          <Col md={12}>
            <div className="form-wrap form-wrap__large-right-gap">
              <form className="form-default">
                <FormGroup className="form-group__indent-bot-big">
                  <ControlLabel>
                    Your PayPal email address *
                  </ControlLabel>
                  <FormControl
                    type="email"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChangeInput}
                  />
                </FormGroup>
              </form>
            </div>
            <div className="text-block text-block__indent-bot">
              <p>This will be your default withdrawal account. You can change this at any time.</p>
            </div>
          </Col>
        </Row>
        <div className="flexbox justify-start">
          <button 
            className="btn btn-remote btn-primary btn-primary__big"
            onClick={this.requestWithdraw}
            disabled={disabledSubmit}
          >
            Submit Request
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    requestWithdraw,
    getCurrentUser
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(WithdrawalsPayment);
