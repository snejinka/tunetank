import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock, Modal, Button, Popover, Tooltip, ButtonToolbar } from 'react-bootstrap';
import * as api from 'api';
import { openModal } from 'store/user.js';

class ContactAuthor extends React.Component {
  constructor() {
    super();
    this.state = {
      show: false,
      subject: '',
      message: '',
    };
  }

  static propTypes = {
    disabled: PropTypes.bool,
    loggedIn: PropTypes.bool,
    openModal: PropTypes.func,
  }

  showModal = () => {
    if (this.props.loggedIn) {
      this.setState({ show: true });
    } else {
      this.props.actions.openModal('sign_up');
    }
  };

  hideModal = () => {
    this.setState({ show: false });
  };

  sentContactForm = (event) => {
    this.hideModal();
    api.fetchContactForm(this.props.userInfo.id, this.state.subject, this.state.message).then((res) => {
    });
  }

  setSubject = (event) => {
    this.setState({
      subject: event.target.value,
    });
  }

  setMessage = (event) => {
    this.setState({
      message: event.target.value,
    });
  }

  render() {
    const backdropStyle = {
      zIndex: 'auto',
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
    };
    return (
      <ButtonToolbar className="btn-toolbar_contact">
        <Button
          className="btn btn_line btn_role_contact"
          onClick={this.showModal} disabled={this.props.disabled}
        >
            Contact Author
          </Button>
        <Modal
          {...this.props}
          backdropStyle={backdropStyle}
          show={this.state.show}
          onHide={this.hideModal}
          dialogClassName="modal-default"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg" className="title_primary title_primary__inner">Contact Author</Modal.Title>
            <div className="modal-note">You can drop a line to author by email.</div>
          </Modal.Header>
          <Modal.Body>
            <form action="" className="form-default">
              <FormGroup className="form-group__indent-bot-big">
                <ControlLabel>
                    Topic
                  </ControlLabel>
                <FormControl onChange={this.setSubject} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>
                    Your message
                  </ControlLabel>
                <FormControl
                  componentClass="textarea"
                  className="form-control-textarea form-control-textarea_contact"
                  onChange={this.setMessage}
                />
              </FormGroup>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <div className="modal-default__submit-wrap">
              <Button onClick={this.sentContactForm} className="btn-primary btn-primary__form-submit">Sent</Button>
            </div>
          </Modal.Footer>
        </Modal>
      </ButtonToolbar>
    );
  }
}
const mapStateToProps = state => ({
  userInfo: state.user.userInfo,
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    openModal,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactAuthor);
