import React from 'react';
import { ReactSelectize, SimpleSelect } from 'react-selectize';
import '../../../../../node_modules/react-selectize/themes/index.css';

class SelectizeSimple extends React.Component {
  render() {
    const options = ['Zimbabve', 'Russia', 'USA', 'Germany', 'Spain'].map(fruit => ({ label: fruit, value: fruit }));
    return <SimpleSelect options={options} placeholder="" className="react-default-template" />;
  }
    }
export default SelectizeSimple;
