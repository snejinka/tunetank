import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import 'styles/variables.scss';
import { StickyContainer, Sticky } from 'react-sticky';
import { tabs } from 'common';
import { setTab } from 'store/user';
import AuthorsTracks from '../AuthorsTracks';
import Achivments from '../Achivments';
import Biography from '../Biography';
import SocialLinks from '../SocialLinks';
import Statistics from '../Statistics';
import SidebarFollowers from '../SidebarFollowers';
import SidebarFollowing from '../SidebarFollowing';
import SimilarArtists from '../SimilarArtists';
import FollowingFeed from '../FollowingFeed';
import MyDownloads from '../MyDownloads';
import Favourites from '../Favourites';
import Settings from '../Settings';
import ReferalUrl from '../ReferalUrl';
import Withdrawals from '../Withdrawals';
import Statements from '../Statements';
import Earnings from '../Earnings';

import {
  Grid,
  Row,
  Col,
  Tab,
  TabPane,
  Nav,
  NavItem,
} from 'react-bootstrap';

class AuthorInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stickyHeight: 1300,
    };
    this.handleSelect = this.handleSelect.bind(this);
  }
  
  static propTypes = {
    loggedIn: PropTypes.bool,
    userBiography: PropTypes.string,
    userInfo: PropTypes.object.isRequired,
  }

  handleSelect(key) {
    this.props.actions.setTab(key);
  }

  updateSidebarHeight(height) {
    if (height > this.state.stickyHeight) { this.setState({ stickyHeight: height }); }
  }

  getNavItems() {
    if (this.props.userInfo.role === 'seller' || this.props.userInfo.role === 'admin') {
      return (
        <Nav className="tabs-menu tabs-menu_no-border tabs-menu__left-indent tabs-menu__beyond flexbox">
          <NavItem eventKey={tabs.main} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.main)}>Main
          </NavItem>
          <NavItem eventKey={tabs.followingFeed} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.followingFeed)}>Following Feed
          </NavItem>
          <NavItem eventKey={tabs.downloads} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.downloads)}>Downloads
          </NavItem>
          <NavItem eventKey={tabs.favourites} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.favourites)}>Favourites
          </NavItem>
          <NavItem eventKey={tabs.settings} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.settings)}>Settings
          </NavItem>
          <NavItem eventKey={tabs.referralUrl} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.referralUrl)}>Referral URL
          </NavItem>
          <NavItem eventKey={tabs.earnings} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.earnings)}>Earnings
          </NavItem>
          <NavItem eventKey={tabs.statements} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.statements)}>Statements
          </NavItem>
          <NavItem eventKey={tabs.withdrawals} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.withdrawals)}>Withdrawals
          </NavItem>
        </Nav>
      );
    }
    return (
      <Nav className="tabs-menu tabs-menu_no-border tabs-menu__left-indent tabs-menu__beyond flexbox">
        <NavItem eventKey={tabs.followingFeed} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.followingFeed)}>Following Feed
        </NavItem>
        <NavItem eventKey={tabs.downloads} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.downloads)}>Downloads
        </NavItem>
        <NavItem eventKey={tabs.favourites} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.favourites)}>Favourites
        </NavItem>
        <NavItem eventKey={tabs.settings} className="tabs-menu__item" onClick={() => this.props.actions.setTab(tabs.settings)}>Settings
        </NavItem>
      </Nav>
    );
  }

  getSimilarArtist() {
    if (this.props.userInfo.role === 'seller' || this.props.userInfo.role === 'admin') {
      return (
        <Row>
          <Col md={12}>
            <SimilarArtists />
          </Col>
        </Row>
      );
    }
    return null;
  }

  getTabContent() {
    if (this.props.userInfo.role === 'seller' || this.props.userInfo.role === 'admin' ) {
      return (
        <Tab.Content className="tabs-wrap" animation={true} mountOnEnter={true}>
          <TabPane eventKey={tabs.main} mountOnEnter={true}>
            <AuthorsTracks userInfo={this.props.userInfo} viewAuthorTracks  />
          </TabPane>
          <TabPane eventKey={tabs.followingFeed} mountOnEnter={true}>
            {this.props.loggedIn && this.props.hasFollowingTracks() && <FollowingFeed loggedIn={this.props.loggedIn}/>}
          </TabPane>
          <TabPane eventKey={tabs.downloads} mountOnEnter={true}>
            <MyDownloads />
          </TabPane>
          <TabPane eventKey={tabs.favourites} mountOnEnter={true}>
            {this.props.loggedIn && <Favourites />}
          </TabPane>
          <TabPane eventKey={tabs.settings} mountOnEnter={true}>
            <Settings />
          </TabPane>
          <TabPane eventKey={tabs.referralUrl} mountOnEnter={true}>
            <ReferalUrl />
          </TabPane>
          <TabPane eventKey={tabs.earnings} mountOnEnter={true}>
            <Earnings />
          </TabPane>
          <TabPane eventKey={tabs.statements} mountOnEnter={true}>
            <Statements />
          </TabPane>
          <TabPane eventKey={tabs.withdrawals} mountOnEnter={true}>
            <Withdrawals />
          </TabPane>
        </Tab.Content>
      );
    }
    return (
      <Tab.Content className="tabs-wrap" animation={true} mountOnEnter={true}>
        <TabPane eventKey={tabs.followingFeed} mountOnEnter={true}>
          {this.props.loggedIn && this.props.hasFollowingTracks() && <FollowingFeed />}
        </TabPane>
        <TabPane eventKey={tabs.downloads} mountOnEnter={true}>
          <MyDownloads />
        </TabPane>
        <TabPane eventKey={tabs.favourites} mountOnEnter={true}>
          {this.props.loggedIn && <Favourites />}
        </TabPane>
        <TabPane eventKey={tabs.settings} mountOnEnter={true}>
          <Settings />
        </TabPane>
      </Tab.Content>
    );
  }

  getSidebar() {
    const { totalTracks } = this.props;
    if (totalTracks < 15) {
      return (
        <aside className="sidebar">
          <Row>
            <Col md={12}>
              <Achivments userInfo={this.props.userInfo} />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <Biography userBiography={this.props.userBiography} />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <SocialLinks />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <SidebarFollowers userInfo={this.props.userInfo} />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              {this.props.userInfo.followings.length > 0 && <SidebarFollowing userInfo={this.props.userInfo} />}
            </Col>
          </Row>
          {this.getSimilarArtist()}
        </aside>
      );
    }
    return (
      <aside className="sidebar">
          <Row>
            <Col md={12}>
              <Achivments userInfo={this.props.userInfo} />
            </Col>
          </Row>
        <Row>
          <Col md={12}>
            <Biography userBiography={this.props.userBiography} />
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <SocialLinks />
          </Col>
        </Row>
        <StickyContainer
          style={{
            height: this.state.stickyHeight,
          }}
        >
          <Sticky>
            {({
              style,
              calculatedHeight,
            }) => (
              <div
                id="sticky-sidebar" style={{
                  left: style.left,
                  width: style.width,
                  position: style.position,
                  top: style.top,
                  transform: style.transform,
                  height: calculatedHeight,
                }}
              >
                <Row>
                  <Col md={12}>
                    <SidebarFollowers userInfo={this.props.userInfo} />
                  </Col>
                </Row>
                <Row>
                  <Col md={12}>
                    {this.props.userInfo.followings.length > 0 && <SidebarFollowing userInfo={this.props.userInfo} />}
                  </Col>
                </Row>
                {this.getSimilarArtist()}
              </div>
            )
          }
          </Sticky>
        </StickyContainer>
      </aside>
    );
  }

  render() {
    this.getSidebar();
    return (
      <div>
        <Grid>
          <Tab.Container id="tabs" onSelect={this.handleSelect} activeKey={this.props.tab}>
            <Row>
              <Col md={12}>
                <Row className="clearfix">
                  {this.props.userInfo.is_current && <Col sm={12}>
                    {this.getNavItems()}
                  </Col>
                  }
                </Row>
                <Row id="tabs-content">
                  <Col md={9} sm={9}>
                    {this.props.userInfo.is_current
                      ? this.getTabContent()
                      : this.props.userInfo.tracks.length > 0 && (
                        <div className="content-without-menu">
                          <AuthorsTracks updateHeight={this.updateSidebarHeight.bind(this)} userInfo={this.props.userInfo} viewAuthorTracks />
                        </div>
                      )
                    }
                  </Col>

                  <Col md={3} sm={3} className="sidebar-wrap">
                    {this.getSidebar()}
                  </Col>
                </Row>
              </Col>
            </Row>

          </Tab.Container>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  hasFollowingTracks: () => state.followingFeed.followingTracks.length > 0,
  tab: state.user.tab,
  totalTracks: state.user.userInfo.total_tracks,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTab
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthorInfo);
