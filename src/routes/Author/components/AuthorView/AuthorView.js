import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import WelcomeBlock from '../WelcomeBlock';
import AuthorInfo from '../AuthorInfo';
import { getUserInfo, getSimilarArtists } from 'store/user';
import { getFollowingTracks } from 'store/followingFeed.js';
import Spinner from 'components/Spinner';
import { requestWithdraw } from '../../modules/withdraw';

class AuthorView extends React.PureComponent {
  componentWillMount() {
    if (this.props.loggedIn) {
      this.props.actions.getFollowingTracks();
    }
    this.props.actions.getUserInfo(this.props.params.id, this.props.params.userSlug);
    this.props.actions.getSimilarArtists(this.props.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params.id !== this.props.params.id) {
      this.props.actions.getUserInfo(nextProps.params.id, nextProps.params.userSlug);
      this.props.actions.getSimilarArtists(nextProps.params.id);
    }
  }

  render() {
    if (!this.props.hasUserInfo) { return <Spinner />; }

    return (
      <div className="author-page wrapper-fluid">
        { this.props.hasUserInfo && this.props.userInfo.id && <WelcomeBlock userInfo={this.props.userInfo} />}
        { this.props.hasUserInfo && this.props.userInfo.id && <AuthorInfo loggedIn={this.props.loggedIn} userInfo={this.props.userInfo} userBiography={this.props.userBiography} />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.user.userInfo,
  hasUserInfo: Object.keys(state.user.userInfo).length > 0,
  userBiography: state.user.userInfo.biography,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getUserInfo,
    getSimilarArtists,
    requestWithdraw,
    getFollowingTracks
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthorView);
