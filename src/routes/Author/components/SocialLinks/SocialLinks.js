import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class SocialLinks extends React.Component {

  renderIcon(type) {
    if (type == 1) {
      return (
        <i className="fa fa-soundcloud" aria-hidden="true" />
      );
    } else if (type == 2) {
      return (
        <i className="fa fa-twitter" aria-hidden="true" />
      );
    } else if (type == 3) {
      return (
        <i className="fa fa-instagram" aria-hidden="true" />
      );
    } else if (type == 4) {
      return (
        <i className="fa fa-facebook-official" aria-hidden="true" />
      );
    } else if (type == 5) {
      return (
        <i className="fa fa-google-plus" aria-hidden="true" />
      );
    } else if (type == 6) {
      return (
        <i className="fa fa-linkedin" aria-hidden="true" />
      );
    } else if (type == 7) {
      return (
        <i className="fa fa-youtube" aria-hidden="true" />
      );
    } else if (type == 8) {
      return (
        <i className="fa fa-vimeo" aria-hidden="true" />
      );
    }
  }

  render() {
    if (!this.props.social_links || this.props.social_links.length == 0) { return false; }

    return (
      <div className="social-links">
        <div className="social-links__title">Social Profiles</div>
        <div className="social-links-wrap">
          {this.props.social_links.map((link, i) => {
            if (link.url == '') { return false; }

            return (
              <a href={link.url} className="btn btn_social social-links__item" key={i} target="_blank">
                {this.renderIcon(link.type)}
              </a>
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  social_links: state.user.userInfo.social_links,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(SocialLinks);
