import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Row, Col, FormGroup, FormControl, HelpBlock } from 'react-bootstrap';
import {
  getUserInfo,
  setSocialLinks,
  SOCIAL_LINK_TYPE,
  updateExactFealds,
}
  from 'store/user';

class Social extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newSocialLinksArray: [],
      validationErrors: {
        facebook: false,
        google_plus: false,
        linked_in: false,
        twitter: false,
        youtube: false,
        sound_cloud: false,
        instagram: false,
        vimeo: false,
      },
    };
  }

  isValid(socialLink, url) {
    switch (socialLink) {
      case 'facebook':
        return this.comparePath(socialLink, url, 'https://www.facebook.com/');
      case 'google_plus':
        return this.comparePath(socialLink, url, 'https://plus.google.com/');
      case 'linked_in':
        return this.comparePath(socialLink, url, 'https://www.linkedin.com/');
      case 'twitter':
        return this.comparePath(socialLink, url, 'https://twitter.com/');
      case 'youtube':
        return this.comparePath(socialLink, url, 'https://www.youtube.com/');
      case 'sound_cloud':
        return this.comparePath(socialLink, url, 'https://soundcloud.com/');
      case 'instagram':
        return this.comparePath(socialLink, url, 'https://www.instagram.com/');
      case 'vimeo':
        return this.comparePath(socialLink, url, 'https://vimeo.com/');
    }
  }

  comparePath(socialLink, url, validatePath) {
    if (url.indexOf(validatePath) === 0 || url === '') {
      this.setState({ validationErrors: Object.assign({}, this.state.validationErrors, { [socialLink]: false }) });
      return true;
    }
    this.setState({ validationErrors: Object.assign({}, this.state.validationErrors, { [socialLink]: true }) });
    return false;
  }

  submitSocialNetworks = (event) => {
    this.props.actions.updateExactFealds(
      this.props.currentUser.id, {
        social_links: this.state.newSocialLinksArray,
      }).then((res) => {
        this.props.actions.setSocialLinks(this.state.newSocialLinksArray);
      });
  }

  changeSocialLink(type, value) {
    const socialLinks = this.props.social_links;
    const isReplaced = this.findAndReplaceLink(socialLinks, type, value);
    if (!isReplaced) {
      this.setState({
        newSocialLinksArray: socialLinks.push({ type, url: value }),
      });
    }
  }

  findAndReplaceLink(links, type, url) {
    for (const i in links) {
      if (links[i].type === type) {
        links[i].url = url;
        this.setState({
          newSocialLinksArray: links,
        });
        return true;
      }
    }
    return false;
  }

  findLinkByType(links, type) {
    const link = links.find(link => link.type === type);
    return link ? link.url : '';
  }

  render() {
    const socialLinks = this.props.social_links;

    return (
      <div>
        <div className="tab-wrap-inner-header">
          <div className="title_primary title_primary__inner">Social Networks</div>
        </div>
        <div className="form-wrap form-wrap__notifications">
          <form action="" className="form-default form-default__notifications">
            <div className="form-default__header">
              <div className="title_primary title_primary__small">
                Your Social Networks
              </div>
            </div>
            <Row>
              <Col md={8}>
                <FormGroup>
                  <div className="flexbox align-center">
                    <div className="form-group__image">
                      <div className="facebook-icon" />
                    </div>
                    <FormControl
                      className="form-control__low"
                      defaultValue={this.findLinkByType(socialLinks, SOCIAL_LINK_TYPE.facebook)}
                      placeholder="Facebook link"
                      onChange={(event) => {
                        if (this.isValid('facebook', event.target.value)) {
                          this.changeSocialLink(SOCIAL_LINK_TYPE.facebook, event.target.value);
                        }
                      }}
                    />
                  </div>
                  <div className="flexbox align-center">
                    {this.state.validationErrors.facebook && (
                    <HelpBlock className="not-valid social__validation">
                          Link must start with https://www.facebook.com/
                        </HelpBlock>
                    )}
                  </div>
                </FormGroup>
                <FormGroup>
                  <div className="flexbox align-center">
                    <div className="form-group__image">
                      <div className="google-plus-icon" />
                    </div>
                    <FormControl
                      className="form-control__low"
                      defaultValue={this.findLinkByType(socialLinks, SOCIAL_LINK_TYPE.google_plus)}
                      placeholder="Google+ link"
                      onChange={(event) => {
                        if (this.isValid('google_plus', event.target.value)) {
                          this.changeSocialLink(SOCIAL_LINK_TYPE.google_plus, event.target.value);
                        }
                      }}
                    />
                  </div>
                  <div className="flexbox align-center">
                    {this.state.validationErrors.google_plus && (
                    <HelpBlock className="not-valid social__validation">
                          Link must start with https://plus.google.com/
                        </HelpBlock>
                    )}
                  </div>
                </FormGroup>
                <FormGroup>
                  <div className="flexbox align-center">
                    <div className="form-group__image">
                      <div className="linked-in-icon" />
                    </div>
                    <FormControl
                      className="form-control__low"
                      defaultValue={this.findLinkByType(socialLinks, SOCIAL_LINK_TYPE.linked_in)}
                      placeholder="LinkedIn link"
                      onChange={(event) => {
                        if (this.isValid('linked_in', event.target.value)) {
                          this.changeSocialLink(SOCIAL_LINK_TYPE.linked_in, event.target.value);
                        }
                      }}
                    />
                  </div>
                  <div className="flexbox align-center">
                    {this.state.validationErrors.linked_in && (
                    <HelpBlock className="not-valid social__validation">
                          Link must start with https://www.linkedin.com/
                        </HelpBlock>
                    )}
                  </div>
                </FormGroup>
                <FormGroup>
                  <div className="flexbox align-center">
                    <div className="form-group__image">
                      <div className="twitter-icon" />
                    </div>
                    <FormControl
                      className="form-control__low"
                      defaultValue={this.findLinkByType(socialLinks, SOCIAL_LINK_TYPE.twitter)}
                      placeholder="Twitter link"
                      onChange={(event) => {
                        if (this.isValid('twitter', event.target.value)) {
                          this.changeSocialLink(SOCIAL_LINK_TYPE.twitter, event.target.value);
                        }
                      }}
                    />
                  </div>
                  <div className="flexbox align-center">
                    {this.state.validationErrors.twitter && (
                    <HelpBlock className="not-valid social__validation">
                          Link must start with https://twitter.com/
                        </HelpBlock>
                    )}
                  </div>
                </FormGroup>
                <FormGroup>
                  <div className="flexbox align-center">
                    <div className="form-group__image">
                      <div className="youTube-icon" />
                    </div>
                    <FormControl
                      className="form-control__low"
                      defaultValue={this.findLinkByType(socialLinks, SOCIAL_LINK_TYPE.youtube)}
                      placeholder="Youtube link"
                      onChange={(event) => {
                        if (this.isValid('youtube', event.target.value)) {
                          this.changeSocialLink(SOCIAL_LINK_TYPE.youtube, event.target.value);
                        }
                      }}
                    />
                  </div>
                  <div className="flexbox align-center">
                    {this.state.validationErrors.youtube && (
                    <HelpBlock className="not-valid social__validation">
                          Link must start with https://www.youtube.com/
                        </HelpBlock>
                    )}
                  </div>
                </FormGroup>
                <FormGroup>
                  <div className="flexbox align-center">
                    <div className="form-group__image">
                      <div className="sound-clound-icon" />
                    </div>
                    <FormControl
                      className="form-control__low"
                      defaultValue={this.findLinkByType(socialLinks, SOCIAL_LINK_TYPE.sound_cloud)}
                      placeholder="Sound Cloud link"
                      onChange={(event) => {
                        if (this.isValid('sound_cloud', event.target.value)) {
                          this.changeSocialLink(SOCIAL_LINK_TYPE.sound_cloud, event.target.value);
                        }
                      }}
                    />
                  </div>
                  <div className="flexbox align-center">
                    {this.state.validationErrors.sound_cloud && (
                    <HelpBlock className="not-valid social__validation">
                          Link must start with https://soundcloud.com/
                        </HelpBlock>
                    )}
                  </div>
                </FormGroup>

                <FormGroup>
                  <div className="flexbox align-center">
                    <div className="form-group__image">
                      <div className="instagram-icon" />
                    </div>
                    <FormControl
                      className="form-control__low"
                      defaultValue={this.findLinkByType(socialLinks, SOCIAL_LINK_TYPE.instagram)}
                      placeholder="Instagram link"
                      onChange={(event) => {
                        if (this.isValid('instagram', event.target.value)) {
                          this.changeSocialLink(SOCIAL_LINK_TYPE.instagram, event.target.value);
                        }
                      }}
                    />
                  </div>
                  <div className="flexbox align-center">
                    {this.state.validationErrors.instagram && (
                    <HelpBlock className="not-valid social__validation">
                          Link must start with https://www.instagram.com/
                        </HelpBlock>
                    )}
                  </div>
                </FormGroup>

                <FormGroup>
                  <div className="flexbox align-center">
                    <div className="form-group__image">
                      <div className="vimeo-icon" />
                    </div>
                    <FormControl
                      className="form-control__low"
                      defaultValue={this.findLinkByType(socialLinks, SOCIAL_LINK_TYPE.vimeo)}
                      placeholder="Vimeo link"
                      onChange={(event) => {
                        if (this.isValid('vimeo', event.target.value)) {
                          this.changeSocialLink(SOCIAL_LINK_TYPE.vimeo, event.target.value);
                        }
                      }}
                    />
                  </div>
                  <div className="flexbox align-center">
                    {this.state.validationErrors.vimeo && (
                    <HelpBlock className="not-valid social__validation">
                          Link must start with https://vimeo.com/
                        </HelpBlock>
                    )}
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <button
              type="button" className="btn btn-primary btn-primary__submit btn-primary__submit-top-indent"
              disabled={
                      this.state.validationErrors.facebook === true ||
                      this.state.validationErrors.google_plus === true ||
                      this.state.validationErrors.linked_in === true ||
                      this.state.validationErrors.twitter === true ||
                      this.state.validationErrors.youtube === true ||
                      this.state.validationErrors.sound_cloud === true ||
                      this.state.validationErrors.instagram === true ||
                      this.state.validationErrors.vimeo === true
                    }
              onClick={this.submitSocialNetworks}
            >Save Changes
            </button>
          </form>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  social_links: state.user.social_links,
  userInfo: state.user.userInfo,
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getUserInfo,
    setSocialLinks,
    updateExactFealds,
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(Social);
