import React from 'react';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import SelectizeSimpleSelectTheme from '../../SelectizeSimpleSelectTheme';
import VisaIcon from '../../../../../assets/img/visa-icon.png';
import MasterCardIcon from '../../../../../assets/img/master-card-icon.png';

export const SavedCreditCards = () => (
  <div>
    <div className="tab-wrap-inner-header">
      <div className="title_primary title_primary__inner">Saved Credit Cards</div>
    </div>
    <div className="form-wrap form-wrap__medium-right-gap">
      <form action="" className="form-default">
        <div className="form-default__header">
          <div className="title_primary title_primary__small">
                            Saved Cards
                        </div>
        </div>
        <div className="cards-list">
          <div className="cards-list__item-wrap">
            <div className="cards-list__item">
              <div className="cards-list__item-image">
                <img src={MasterCardIcon} alt="" className="img-responsive" />
              </div>
              <div className="cards-list__item-description">
                <div className="cards-list__item-note">
                  <ul className="list-inline">
                    <li>****</li>
                    <li>****</li>
                    <li>****</li>
                    <li>1234</li>
                  </ul>
                </div>
                <div className="cards-list__item-title">
                                        Master Card
                                    </div>
              </div>
              <div className="cards-list__item-date">
                                    (10/18)
                                </div>
            </div>
            <button className="btn-edit btn-edit__left-gap">
                                Edit
                            </button>
            <button className="btn-remove btn-remove__left-gap" />
          </div>
          <div className="cards-list__item-wrap">
            <div className="cards-list__item">
              <div className="cards-list__item-image">
                <img src={VisaIcon} alt="" className="img-responsive" />
              </div>
              <div className="cards-list__item-description">
                <div className="cards-list__item-note">
                  <ul className="list-inline">
                    <li>****</li>
                    <li>****</li>
                    <li>****</li>
                    <li>5678</li>
                  </ul>
                </div>
                <div className="cards-list__item-title">
                                        Visa
                                    </div>
              </div>
              <div className="cards-list__item-date">
                                    (09/19)
                                </div>
            </div>
            <button className="btn-edit btn-edit__left-gap">
                                Edit
                            </button>
            <button className="btn-remove btn-remove__left-gap" />
          </div>
        </div>
      </form>
    </div>
    <div className="form-wrap form-wrap__large-right-gap">
      <form action="" className="form-default">
        <div className="form-default__header">
          <div className="title_primary title_primary__small">
                            Add New Card
                        </div>
        </div>
        <FormGroup className="form-group__indent-bot-medium">
          <ControlLabel>
                            Name on card
                        </ControlLabel>
          <FormControl />
        </FormGroup>
        <FormGroup className="form-group__indent-bot-medium">
          <ControlLabel>
                            Card Number
                        </ControlLabel>
          <FormControl />
          <div className="card-icon-list flexbox align-center">
            <div className="card-icon-list__item">
              <div className="card-icon-list__item-image">
                <img src={VisaIcon} alt="" className="img-responsive" />
              </div>

            </div>
            <div className="card-icon-list__item">
              <div className="card-icon-list__item-image">
                <img src={MasterCardIcon} alt="" className="img-responsive" />
              </div>
            </div>

          </div>
        </FormGroup>
        <Row>
          <Col md={6}>
            <FormGroup className="form-group__indent-bot-big">
              <ControlLabel>
                                    Exp Month
                                </ControlLabel>
              <SelectizeSimpleSelectTheme />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup className="form-group__indent-bot-big">
              <ControlLabel>
                                    Exp Year
                                </ControlLabel>
              <SelectizeSimpleSelectTheme />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <FormGroup className="form-group__indent-bot-large">
              <ControlLabel>
                                    Security Code
                                </ControlLabel>
              <FormControl className="form-control__short-field" />
              <HelpBlock className>
                                    Last 3 digits on back of your card.
                                </HelpBlock>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={3}>
            <button className="btn btn-primary btn-primary__block">
                                Submit
                            </button>
          </Col>
        </Row>
      </form>
    </div>
  </div>
        );
export default SavedCreditCards;
