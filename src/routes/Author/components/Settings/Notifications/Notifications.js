import React from 'react';
import { Row, Col, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';

export const Notifications = () => (
  <div>
    <div className="tab-wrap-inner-header">
      <div className="title_primary title_primary__inner">Notifications Settings</div>
    </div>
    <div className="form-wrap form-wrap__notifications">
      <form action="" className="form-default form-default__notifications">
        <div className="form-default__header">
          <div className="title_primary title_primary__small">
                        Your Social Networks
                    </div>
        </div>
        <FormGroup>
          <div className="radio-box radio-box__square radio-box__nogap">
            <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__note"> Rating reminders</ControlLabel>
          </div>
          <HelpBlock>
                        Send email reminding me to rate an item a week after purchase.
                    </HelpBlock>
        </FormGroup>
        <FormGroup>
          <div className="radio-box radio-box__square radio-box__nogap">
            <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__note"> Item update notifications</ControlLabel>
          </div>
          <HelpBlock>
                        Send me an email when an item I’ve purchased is updated.
                    </HelpBlock>
        </FormGroup>
        <FormGroup>
          <div className="radio-box radio-box__square radio-box__nogap">
            <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__note">Item comment notifications</ControlLabel>
          </div>
          <HelpBlock>
                        Send me an email when someone comments on one of my items.
                    </HelpBlock>
        </FormGroup>
        <FormGroup>
          <div className="radio-box radio-box__square radio-box__nogap">
            <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__note"> Team comment notifications</ControlLabel>
          </div>
          <HelpBlock>
                       Send me an email when someone comments on one of my team items.
                    </HelpBlock>
        </FormGroup>
        <FormGroup>
          <div className="radio-box radio-box__square radio-box__nogap">
            <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__note"> Item review notifications</ControlLabel>
          </div>
          <HelpBlock>
                        Send me an email when my items are approved or rejected.
                    </HelpBlock>
        </FormGroup>
        <FormGroup>
          <div className="radio-box radio-box__square radio-box__nogap">
            <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__note"> Buyer review notifications</ControlLabel>
          </div>
          <HelpBlock>
                        Send me an email when someone leaves a review with their rating.
                    </HelpBlock>
        </FormGroup>
        <FormGroup>
          <div className="radio-box radio-box__square radio-box__nogap">
            <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__note">Expiring support notifications</ControlLabel>
          </div>
          <HelpBlock>
                       Send me an email showing my soon to expire support entitlements.
                    </HelpBlock>
        </FormGroup>
        <FormGroup>
          <div className="radio-box radio-box__square radio-box__nogap">
            <FormControl type="checkbox" id="radioGroup121" name="radioGroup" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__pseudo-radio" />
            <ControlLabel htmlFor="radioGroup121" className="radio-box__note"> Daily summary emails</ControlLabel>
          </div>
          <HelpBlock>
                        Send me a daily summary of all items approved or rejected.
                    </HelpBlock>
        </FormGroup>
        <button className="btn btn-primary btn-primary__submit btn-primary__submit-top-indent">Save Changes</button>
      </form>
    </div>
  </div>
    );
export default Notifications;
