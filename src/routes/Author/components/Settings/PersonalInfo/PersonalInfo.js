import React from 'react';
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactCountrySelect from '../../../../../components/ReactCountrySelect';
import * as common from 'common';

import {
  setUserSecureId,
  setUserName,
  setCountry,
  setAddressFirstLine,
  setAddressSecondLine,
  setCity,
  setState,
  setZip,
  setVat,
  setEmail,
  setPassword,
  updateUserInfo,
  updateExactFealds,
} from 'store/user';


class PersonalInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      disabledEmail: true,
      disabledPassword: true,
    };
  }

  submitFormPersonalInfo = (event) => {
    const userInfo = { ...this.props.userInfo };
    delete userInfo.role;
    this.props.actions.updateUserInfo(userInfo);
  }

  submitEmail = (event) => {
    this.setState({ disabledEmail: true });
    this.props.actions.updateExactFealds(this.props.userInfo.id, { email: this.props.email });
  }

  submitPassword = (event) => {
    this.setState({ disabledPassword: true });
    this.props.actions.updateExactFealds(this.props.userInfo.id, { password: this.props.password });
  }

  changeUserName = (event) => {
    this.props.actions.setUserName(event.target.value);
  }

  selectCountry = (option) => {
    this.props.actions.setCountry(option.value);
  }

  changeAddressFirstLine = (event) => {
    this.props.actions.setAddressFirstLine(event.target.value);
  }

  changeAddressSecondLine = (event) => {
    this.props.actions.setAddressSecondLine(event.target.value);
  }

  changeCity = (event) => {
    this.props.actions.setCity(event.target.value);
  }

  changeState = (event) => {
    this.props.actions.setState(event.target.value);
  }

  changeZIP = (event) => {
    this.props.actions.setZip(event.target.value);
  }

  changeVat = (event) => {
    this.props.actions.setVat(event.target.value);
  }

  changeEmail = (event) => {
    this.props.actions.setEmail(event.target.value);
  }

  changePassword = (event) => {
    this.props.actions.setPassword(event.target.value);
  }

  validatePass(password) {
    if (password.length > 254) { return false; }

    if (password.length < 6) { return false; }

    return true;
  }

  validateEmail(email) {
    if (!email) { return false; }

    if (email.length > 254) { return false; }

    const valid = this.emailRegexp.test(email);
    if (!valid) { return false; }

    const parts = email.split('@');
    if (parts[0].length > 64) { return false; }

    const domainParts = parts[1].split('.');
    if (domainParts.some(part => part.length > 63)) { return false; }

    return true;
  }

  render() {
    const isValidEmail = common.validateEmail(this.props.email);
    const isValidPass = common.validatePass(this.props.password);

    return (
      <div>
        <div className="tab-wrap-inner-header">
          <div className="title_primary title_primary__inner">Personal Info</div>
        </div>
        <div className="form-wrap form-wrap__large-right-gap">
          <form action="" className="form-default">
            <FormGroup className="form-group__indent-bot-big">
              <ControlLabel>
                E-mail
              </ControlLabel>
              <div className="form-edit-box">
                <FormControl
                  placeholder="email@gmail.com"
                  onChange={this.changeEmail}
                  disabled={this.state.disabledEmail}
                  value={this.props.email ? this.props.email : ''}
                  name="input-email"
                />
                {this.state.disabledEmail ?
                  <button
                    type="button"
                    className="btn-edit btn-edit__left-gap"
                    onClick={() => this.setState({ disabledEmail: false })}
                  >Edit
                  </button>
                :
                  <button
                    type="button"
                    className="btn-edit btn-edit__left-gap"
                    disabled={!isValidEmail}
                    onClick={this.submitEmail}
                  >Save
                  </button>
                }
              </div>
              {!this.state.disabledEmail && this.props.email == '' &&
                <HelpBlock className="not-valid">
                  E-mail can't be blank
                </HelpBlock>
              }
              {!this.state.disabledEmail && !isValidEmail && this.props.email != '' &&
                <HelpBlock className="not-valid">
                  Invalid format
                </HelpBlock>
              }
              <HelpBlock>This address will appear on your License.</HelpBlock>
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                Password
              </ControlLabel>
              <div className="form-edit-box">
                <FormControl
                  type={this.state.disabledPassword ? 'password' : 'text'}
                  placeholder="*********"
                  onChange={this.changePassword}
                  name="input-password"
                  value={this.props.password ? this.props.password : ''}
                  disabled={this.state.disabledPassword}
                />
                {this.state.disabledPassword ?
                  <button
                    type="button"
                    className="btn-edit btn-edit__left-gap"
                    onClick={() => this.setState({ disabledPassword: false })}
                  >Edit
                  </button>
                  :
                  <button
                    type="button"
                    className="btn-edit btn-edit__left-gap"
                    disabled={!isValidPass}
                    onClick={this.submitPassword}
                  >Save
                  </button>
                }
              </div>
              {!this.state.disabledPassword && !isValidPass &&
                <HelpBlock className="not-valid">
                  The password does not match with the minimum password length.
                </HelpBlock>
              }
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                Name
              </ControlLabel>
              <FormControl
                onChange={this.changeUserName}
                value={this.props.userInfo.name ? this.props.userInfo.name : ''}
                name="input-name"
                maxLength="160"
              />
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                Country
              </ControlLabel>
              <ReactCountrySelect
                multi={false}
                onSelect={this.selectCountry}
                value={this.props.userInfo.country}
                flagImagePath="/flags/"
              />
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                Adress Line 1
              </ControlLabel>
              <FormControl
                onChange={this.changeAddressFirstLine}
                value={this.props.userInfo.address_first_line ? this.props.userInfo.address_first_line : ''}
                name="input-address-1"
              />
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                Adress Line 2 (optional)
              </ControlLabel>
              <FormControl
                onChange={this.changeAddressSecondLine}
                value={this.props.userInfo.address_second_line ? this.props.userInfo.address_second_line : ''}
                name="input-adress-2"
              />
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                City
              </ControlLabel>
              <FormControl
                onChange={this.changeCity}
                value={this.props.userInfo.city ? this.props.userInfo.city : ''}
                name="input-city"
              />
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                State / Province / Region
              </ControlLabel>
              <FormControl
                onChange={this.changeState}
                value={this.props.userInfo.state ? this.props.userInfo.state : ''}
                name="input-state"
              />
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                ZIP / Postal Code
              </ControlLabel>
              <FormControl
                onChange={this.changeZIP}
                value={this.props.userInfo.zip ? this.props.userInfo.zip : ''}
                name="input-zip"
              />
            </FormGroup>
            <FormGroup
              className="form-group__indent-bot-big"
            >
              <ControlLabel>
                VAT number (if applicable)
              </ControlLabel>
              <FormControl
                onChange={this.changeVat}
                value={this.props.userInfo.vat ? this.props.userInfo.vat : ''}
                name="input-email"
              />
            </FormGroup>
            <button type="button" className="btn btn-primary btn-primary__submit" onClick={this.submitFormPersonalInfo}>
              Save Changes
            </button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.user.userInfo,
  email: state.user.email,
  password: state.user.password,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setUserSecureId,
    setUserName,
    setCountry,
    setAddressFirstLine,
    setAddressSecondLine,
    setCity,
    setState,
    setZip,
    setVat,
    setEmail,
    setPassword,
    updateUserInfo,
    updateExactFealds,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfo);
