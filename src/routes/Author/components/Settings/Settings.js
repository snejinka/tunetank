import React from 'react';
import {
  Row,
  Col,
  Tab,
  Nav,
  NavItem,
} from 'react-bootstrap';
import PersonalInfo from './PersonalInfo';
import SavedCreditCards from './SavedCreditCards';
import Notifications from './Notifications';
import Social from './Social';

export const Settings = () => (
  <Tab.Container id="tabs-inner" defaultActiveKey="first">
    <Row className="clearfix">
      <Col sm={12}>
        <Nav className="tabs-menu tabs-menu__inner flexbox">
          <NavItem eventKey="first" className="tabs-menu__item">
            Personal Info
          </NavItem>
          {/* <NavItem eventKey="second" className="tabs-menu__item">
                            Saved Credit Cards
                        </NavItem>
                        <NavItem eventKey="third" className="tabs-menu__item">
                            Notifications
                        </NavItem>*/}
          <NavItem eventKey="fourth" className="tabs-menu__item">
            Social Networks
          </NavItem>
        </Nav>
      </Col>
      <Col md={12} sm={12}>
        <Tab.Content className="tab-wrap-inner" animation>
          <Tab.Pane eventKey="first">
            <PersonalInfo />
          </Tab.Pane>
          {/* <Tab.Pane eventKey="second">
                            <SavedCreditCards/>
                        </Tab.Pane>
                        <Tab.Pane eventKey="third">
                            <Notifications/>
                        </Tab.Pane>*/}
          <Tab.Pane eventKey="fourth">
            <Social />
          </Tab.Pane>
        </Tab.Content>
      </Col>
    </Row>
  </Tab.Container>
);

export default Settings;
