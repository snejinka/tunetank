import React from 'react';
import { Row, Col } from 'react-bootstrap';
import NoteGreenIcon from '../../../../assets/img/note-icon-green.png';
import NoteBlueIcon from '../../../../assets/img/note-icon-blue.png';
import HeadsetIcon from '../../../../assets/img/headset-icon.png';
import CakeIcon from '../../../../assets/img/cake-icon.png';
import { AchievementsSvg } from 'styles/svgs/svgs.js';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';
import moment from 'moment';
import _ from 'lodash';

class Achivments extends React.Component {
  constructor(props) {
    super(props);

  }

  renderTracksAchivment(total_tracks) {
    let svgName,
        tooltip_track_number = 0;

    switch(true) {
      case total_tracks > 0 && total_tracks < 10:
        svgName = 'tracks_number_1';
        break;
      case total_tracks >= 10 && total_tracks < 24:
        svgName = 'tracks_number_10';
        tooltip_track_number = 10;
        break;        
      case total_tracks >= 25 && total_tracks < 49:
        svgName = 'tracks_number_25';
        tooltip_track_number = 25;        
        break;        
      case total_tracks >= 50 && total_tracks < 99:
        svgName = 'tracks_number_50';
        tooltip_track_number = 50;        
        break;        
      case total_tracks >= 100 && total_tracks < 249:
        svgName = 'tracks_number_100';
        tooltip_track_number = 100;        
        break;        
      case total_tracks >= 250 && total_tracks < 499:
        svgName = 'tracks_number_250';
        tooltip_track_number = 250;        
        break;        
      case total_tracks >= 500 && total_tracks < 749:
        svgName = 'tracks_number_500';
        tooltip_track_number = 500;        
        break;        
      case total_tracks >= 750 && total_tracks < 999:
        svgName = 'tracks_number_750'
        tooltip_track_number = 750;        
        break;
      case total_tracks >= 1000:
        svgName = 'tracks_number_1k'
        tooltip_track_number = 1000;        
      default:
        break;
    }

    const text = (total_tracks > 0 && total_tracks < 10) ? 
    'Has uploaded more than 1 track to the site (Well done! The more you upload, the more you sell!)' :
    `Has uploaded ${tooltip_track_number} or more tracks to the site (Good progress, way to go!)`    
    const tooltip = <Tooltip id={1}>
      {text}
    </Tooltip>;

    return <div className="achivments-list__item">
      <OverlayTrigger  
        overlay={tooltip} placement="top"
        delayShow={300} delayHide={150}
      >
        <div className="achivments-list__item-image" dangerouslySetInnerHTML={{ __html: AchievementsSvg[svgName] }} />
      </OverlayTrigger>
    </div>
  }

  renderYearsAchivment(created_at) {
    let svgName,
        years = moment().diff(created_at, 'years');

    switch(true) {
      case years < 1:
        svgName = 'newbie';
        break;
      case years  === 1:
        svgName = 'one_year'
        break;
      case years  === 2:
        svgName = 'two_year'
        break;
      case years  === 3:
        svgName = 'three_year'
        break;
      case years  === 4:
        svgName = 'four_year'
        break;
      case years  === 5:
        svgName = 'five_year'
        break;
      case years  === 6:
        svgName = 'six_year'
        break;
      case years  === 7:
        svgName = 'seven_year'
        break;
      default:
        break;
    }
    const text = 'TuneTank member less than 1 year (Happy Birthday!)'
    const tooltip = <Tooltip id={2}>
      {text}
    </Tooltip>;

    return <div className="achivments-list__item">
      <OverlayTrigger  
        overlay={tooltip} placement="top"
        delayShow={300} delayHide={150}
      >
        <div className="achivments-list__item-image" dangerouslySetInnerHTML={{ __html: AchievementsSvg[svgName] }} />
      </OverlayTrigger>      
    </div>
  }

  renderSellerAchivment() {
    const text = 'Seller on TuneTank. (Now you can upload tracks and use our referral program. Go ahead!)'
    const tooltip = <Tooltip id={3}>
      {text}
    </Tooltip>;
    return <div className="achivments-list__item">
      <OverlayTrigger  
        overlay={tooltip} placement="top"
        delayShow={300} delayHide={150}
      >
        <div className="achivments-list__item-image" dangerouslySetInnerHTML={{ __html: AchievementsSvg.seller }} />
      </OverlayTrigger>
    </div>
  }
  
  renderFeaturedAchivment() {
    const text = 'The author had a track featured on the main page. (Exemplary work!)'
    const tooltip = <Tooltip id={4}>
      {text}
    </Tooltip>;
    return <div className="achivments-list__item">
      <OverlayTrigger  
        overlay={tooltip} placement="top"
        delayShow={300} delayHide={150}
      >
        <div className="achivments-list__item-image" dangerouslySetInnerHTML={{ __html: AchievementsSvg.featured_track }} />
      </OverlayTrigger>      
    </div>
  }
  
  render() {
    
    const {
      role,
      total_tracks,
      total_featured_tracks,
      created_at,
      tracks
    } = this.props.userInfo;
    
    return (
      <div className="achivments-list">
        <div className="header__primary flexbox align-center">
          Achievements
        </div>
        <div className="achivments-list-wrap">
          {this.renderYearsAchivment(created_at)}
          { (role === 'seller' || role === 'admin') && this.renderSellerAchivment()}
          {total_tracks > 0 && this.renderTracksAchivment(total_tracks)}
          {total_featured_tracks > 0 && this.renderFeaturedAchivment()}
        </div>

      </div>
    );
  }
}

export default Achivments;
