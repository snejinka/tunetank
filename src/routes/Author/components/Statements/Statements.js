import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Pagination from '../Pagination';
import Datepicker from '../Datepicker';

class Statements extends React.Component {

  render() {
    return (
      <div>
        <div className="tab__header flexbox justify-between align-end">
          <div className="title_primary title_primary__inner">Sales</div>
        </div>
        <Datepicker />
        <div className="table table-statements">
          <div className="table__row table__header">
            <div className="table__cell" />
            <div className="table__cell">
                        Date
                    </div>
            <div className="table__cell">
                        Order #
                    </div>
            <div className="table__cell">
                        License
                    </div>
            <div className="table__cell">
                        Project name
                    </div>
            <div className="table__cell">
                        Price
                    </div>
            <div className="table__cell">
                        Your cut
                    </div>
            <div className="table__cell">
                        Earnings
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__plus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000547
                    </div>
            <div className="table__cell">
                        E
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link">Inspiring piano</a>
            </div>
            <div className="table__cell">
                        $12
                    </div>
            <div className="table__cell">
                        62.50%
                    </div>
            <div className="table__cell">
                        $6.50
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__minus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000234
                    </div>
            <div className="table__cell">
                        R
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link">Nothing Else Metters Nothing</a>
            </div>
            <div className="table__cell">
                        $18
                    </div>
            <div className="table__cell">
                        50%
                    </div>
            <div className="table__cell">
                        $5.20
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__plus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000783
                    </div>
            <div className="table__cell">
                        R
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link"> Enter Sandman</a>
            </div>
            <div className="table__cell">
                        $100
                    </div>
            <div className="table__cell">
                        50%
                    </div>
            <div className="table__cell">
                        $2.35
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__plus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000547
                    </div>
            <div className="table__cell">
                        E
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link">Inspiring piano</a>
            </div>
            <div className="table__cell">
                        $12
                    </div>
            <div className="table__cell">
                        62.50%
                    </div>
            <div className="table__cell">
                        $6.50
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__minus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000234
                    </div>
            <div className="table__cell">
                        R
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link">Nothing Else Metters Nothing</a>
            </div>
            <div className="table__cell">
                        $18
                    </div>
            <div className="table__cell">
                        50%
                    </div>
            <div className="table__cell">
                        $5.20
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__plus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000783
                    </div>
            <div className="table__cell">
                        R
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link"> Enter Sandman</a>
            </div>
            <div className="table__cell">
                        $100
                    </div>
            <div className="table__cell">
                        50%
                    </div>
            <div className="table__cell">
                        $2.35
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__plus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000547
                    </div>
            <div className="table__cell">
                        E
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link">Inspiring piano</a>
            </div>
            <div className="table__cell">
                        $12
                    </div>
            <div className="table__cell">
                        62.50%
                    </div>
            <div className="table__cell">
                        $6.50
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__minus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000234
                    </div>
            <div className="table__cell">
                        R
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link">Nothing Else Metters Nothing</a>
            </div>
            <div className="table__cell">
                        $18
                    </div>
            <div className="table__cell">
                        50%
                    </div>
            <div className="table__cell">
                        $5.20
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__plus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000783
                    </div>
            <div className="table__cell">
                        R
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link"> Enter Sandman</a>
            </div>
            <div className="table__cell">
                        $100
                    </div>
            <div className="table__cell">
                        50%
                    </div>
            <div className="table__cell">
                        $2.35
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__plus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000547
                    </div>
            <div className="table__cell">
                        E
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link">Inspiring piano</a>
            </div>
            <div className="table__cell">
                        $12
                    </div>
            <div className="table__cell">
                        62.50%
                    </div>
            <div className="table__cell">
                        $6.50
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__minus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000234
                    </div>
            <div className="table__cell">
                        R
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link">Nothing Else Metters Nothing</a>
            </div>
            <div className="table__cell">
                        $18
                    </div>
            <div className="table__cell">
                        50%
                    </div>
            <div className="table__cell">
                        $5.20
                    </div>
          </div>
          <div className="table__row">
            <div className="table__cell table__cell__align-center">
              <button className="btn-circle btn-circle__plus" />
            </div>
            <div className="table__cell">
                        5 Apr 2017
                    </div>
            <div className="table__cell">
                        1000783
                    </div>
            <div className="table__cell">
                        R
                    </div>
            <div className="table__cell">
              <a href="javascript:;" className="table__cell-fixed table__link"> Enter Sandman</a>
            </div>
            <div className="table__cell">
                        $100
                    </div>
            <div className="table__cell">
                        50%
                    </div>
            <div className="table__cell">
                        $2.35
                    </div>
          </div>
        </div>
        <Pagination />
      </div>
    );
  }
}

export default Statements;
