import React from 'react';

export const Statistics = () => (
  <div className="statistics">
    <div className="statistics__header">
      <div className="title statistics__header-icon" />
            Statistics
        </div>
    <div className="statistics-box">
      <div className="statistics-box__item">
        <div className="statistics-box__item-title">
                    Sold this week
                </div>
        <div className="statistics-box__item-count">
                    68
                </div>
        <div className="statistics-box__item-note">
                    5,236 in total
                </div>
      </div>
      <div className="statistics-box__item statistics-box__item__wide">
        <div className="statistics-box__item-title">
                    Earning this week
                </div>
        <div className="statistics-box__item-count">
                    $ 1,593
                </div>
      </div>
    </div>
  </div>
    );

export default Statistics;
