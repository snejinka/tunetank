import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  followAuthor,
  unfollowAuthor,
} from 'store/user';
import FollowingItem from './FollowingItem';
import _ from 'lodash';

class SidebarFollowing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      followings: props.userInfo.followings.slice(0, 3),
      followingIds: props.loggedIn ? props.currentUser.followings.map(i => i.id) : null,
    };
  }
  static propTypes = {
    userInfo: PropTypes.object,
  }

  componentDidMount() {
    if (this.props.loggedIn) {
      this.setState({
        followingIds: this.props.currentUser.followings.map(i => i.id),
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.loggedIn) {
      if (!_.isEqual(nextProps.currentUser.followings, this.props.currentUser.followings)) {
        this.setState({
          followingIds: nextProps.currentUser.followings.map(i => i.id),
        });
      }
    }
  }

  setStateFollow(following_id) {
    const newFollowingIds = _.clone(this.state.followingIds);
    newFollowingIds.push(following_id);
    this.setState({
      followingIds: newFollowingIds,
    });
  }

  setStateUnfollow(following_id) {
    const newFollowingIds = _.clone(this.state.followingIds);
    newFollowingIds.splice(this.state.followingIds.indexOf(following_id), 1);
    this.setState({
      followingIds: newFollowingIds,
    });
  }

  follow(following_id) {
    this.setStateFollow(following_id);
    this.props.actions.followAuthor(following_id).catch((err => this.setStateUnfollow(following_id)));
  }

  unfollow(following_id) {
    this.setStateUnfollow(following_id);
    this.props.actions.unfollowAuthor(following_id).catch((err => this.setStateFollow(following_id)));
  }

  render() {
    if (this.props.userInfo.followings.length === 0) { return null; }
    return (
      <div className="follow-list follow-list__column">
        <div className="header__primary">
          Following
          <div className="header__primary-note">
            {this.props.userInfo.followings.length}
          </div>
        </div>
        <div className="follow-list-wrap">
          {this.state.followings.map((following, i) => {
            let isCurrent = false;
            if (this.props.loggedIn) {
              isCurrent = following.id === this.props.currentUser.id;
            } else {
              isCurrent = false;
            }
            return (
              <FollowingItem key={i} isCurrent={isCurrent} following={following} followingIds={this.state.followingIds} follow={this.follow.bind(this)} unfollow={this.unfollow.bind(this)} />
            );
          })}

        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  currentUser: state.user.currentUser,
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    followAuthor,
    unfollowAuthor,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarFollowing);
