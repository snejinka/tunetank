import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AuthorPanel from 'components/AuthorPanel';
import NoAvatar from 'assets/img/no-avatar.png';
import slug from 'slug';
import { Link } from 'react-router';
import {openModal } from 'store/user';

class FollowingItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hoverTrack: false,
      openAuthorPanel: false,
    };
  }

  static propTypes = {
    userInfo: PropTypes.object,
    isCurrent: PropTypes.bool,
    following: PropTypes.object,
    followingIds: PropTypes.array,
    follow: PropTypes.func,
    unfollow: PropTypes.func,
  }

  onMouseEnter(e) {
    e.stopPropagation();
    this.setState({
      hoverTrack: true,
      openAuthorPanel: true,
    });
  }

  onMouseLeave(e) {
    e.stopPropagation();
    this.setState({
      hoverTrack: false,
      openAuthorPanel: false,
    });
  }

  toggleAuthorPanel() {
    this.setState({
      openAuthorPanel: false,
    });
  }

  renderAuthorPanel() {
      return (
        <AuthorPanel
          user={this.props.following} openAuthorPanel={this.state.openAuthorPanel}
          toggleAuthorPanel={this.toggleAuthorPanel.bind(this)}
        />
      );
  }

  render() {
    return (
      <div className="follow-list__item followings-item__hack"  onMouseLeave={this.onMouseLeave.bind(this)}>
        <div className="follow-list__item-wrap">
          <Link to={`/author/${slug(this.props.following.name, { lower: true })}/${this.props.following.id}`} className="follow-list__item-image">
            <img src={this.props.following.avatar !== null ? this.props.following.avatar : NoAvatar} onMouseEnter={this.onMouseEnter.bind(this)} className="img-responsive" alt="follower" />
          </Link>
          <div className="follow-list__item-description">
            <Link to={`/author/${slug(this.props.following.name, { lower: true })}/${this.props.following.id}`} className="follow-list__item-title">
              {this.props.following.name}
            </Link>
            <div className="follow-list__item-note">
              <div className="follow-list__item-note-icon" />
              {this.props.following.total_tracks}
            </div>
          </div>
        </div>
        {this.props.loggedIn && !this.props.isCurrent && (this.props.followingIds.includes(this.props.following.id) ?
          <button
            className="btn btn-action btn-action__small"
            onClick={() => this.props.unfollow(this.props.following.id)}
          >
            Unfollow
          </button>
          :
          <button
            className="btn btn-action btn-action__small"
            onClick={() => this.props.follow(this.props.following.id)}
          >
            Follow
          </button>)
        }
        {!this.props.loggedIn &&
          <button
            className="btn btn-action btn-action__small"
            onClick={() => this.props.actions.openModal('sign_up')}
          >
            Follow
          </button>
        }
        {this.renderAuthorPanel()}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  loggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    openModal
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FollowingItem);
