import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { getUserInfo } from 'store/user';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';
import InfiniteScroll from 'react-infinite-scroller';
import { fetchTrackByUser } from 'api';
import { ReactHeight } from 'react-height';
import TrackList from '../../../../components/TrackList';
import SelectizeSimpleSelectTheme from 'components/SelectizeSimpleSelectTheme';

class AuthorsTracks extends React.Component {
  static propTypes = {
    userInfo: PropTypes.object,
    viewAuthorTracks: PropTypes.bool,
  }

  constructor(props) {
    super(props);
    this.state = {
      tracks: [],
      tracksPerLoad: 10,
      offset: 0,
      totalTracks: null,
      sortType: 1,
      loading: false,
    };

    this.loadTracks = this.loadTracks.bind(this);
    this.handleSorting = this.handleSorting.bind(this);
  }

  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.userInfo.tracks);
    this.props.actions.setTrackPlaying(playTrack);
  }

  handleSorting(select) {
    this.setState({
      sortType: +select.value,
      tracks: [],
      offset: 0,
    });
  }
  loadTracks() {
    if (this.state.loading) {
      return;
    }
    this.setState({ loading: true });

    const { tracksPerLoad, offset, tracks, sortType } = this.state;
    if (!tracks.length) {
      fetchTrackByUser(this.props.userInfo.id, tracksPerLoad, offset, sortType).then((user) => {
        this.setState({
          tracks: user.tracks,
          offset: +this.state.offset + user.tracks.length,
          totalTracks: user.total_tracks,
          loading: false,
        });
      });
    } else {
      fetchTrackByUser(this.props.userInfo.id, tracksPerLoad, offset, sortType).then((user) => {
        this.setState({
          tracks: [...this.state.tracks, ...user.tracks],
          offset: +this.state.offset + user.tracks.length,
          loading: false,
          totalTracks: user.total_tracks,
        });
      });
    }
  }

  removeTrack(id) {
    const tracks = this.state.tracks;
    const i = tracks.findIndex(item => item.id === id);
    if (i !== -1) {
      tracks.splice(i,1);
    }
    this.setState({
      tracks: tracks,
    })
  }

  render() {
    const { tracks, totalTracks } = this.state;

    if (this.props.userInfo.tracks.length === 0 && this.props.userInfo.is_current) {
      return (
        <div className="list-box">
          <div className="tab__header flexbox justify-between align-end">
            <div className="title_primary title_primary__inner">
              My Tracks
            </div>
          </div>
          <div className="author-panel__disabled">
            <div className="author-panel__disabled-bg">
              <div className="author-panel__disabled-header author-panel__disabled-header--upload" style={{ marginBottom: '23px' }}>
                You haven’t uploaded any tracks yet.
              </div>
              <Link to="/uploadItem" className="btn btn-primary"> Upload Your Track</Link>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="list-box">
        <div className="list-box__header-wrap flexbox justify-between align-end">
          <div className="title_primary title_primary__inner">
            <span className="count-big">{totalTracks}</span> author’s tracks
          </div>
          <div className="sort-panel sort-panel__indent flexbox align-center">
            <span className="sort-panel__title sort-panel__title__nowrap" >Sort by</span>
            <SelectizeSimpleSelectTheme
              options={{ 1: 'Date', 0: 'Alphabet', 2: 'Popular' }}
              defaultValue={{ label: 'Date', value: 1 }}
              onSelect={this.handleSorting}
            />
          </div>
        </div>
        <ReactHeight onHeightReady={height => this.props.updateHeight(height)}>

          <InfiniteScroll
            pageStart={0}
            loadMore={this.loadTracks}
            hasMore={!tracks.length || tracks.length < totalTracks}
            loader={<div className="loader">Loading ...</div>}
          >
            <TrackList
              userInfo={this.props.userInfo}
              tracks={tracks}
              setTracks={playTrack => this.setTracks(playTrack)}
              viewAuthorTracks={this.props.viewAuthorTracks}
              removeTrack={this.removeTrack.bind(this)}
            />
          </InfiniteScroll>

        </ReactHeight>

      </div>
    );
  }
}

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    getUserInfo,
    setTracks,
    setTrackPlaying,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthorsTracks);
