import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TrackList from '../../../../components/TrackList';
import { Link } from 'react-router';
import PaginationAdvanced from '../../../../components/Pagination';
import { setTracks, setTrackPlaying } from 'store/audioPlayer';

class Favourites extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allPages: Math.ceil(props.favoriteTracks.length / 5),
      currentPage: 1,
      perPageTracks: props.favoriteTracks.slice(0, 5)
    };

    this.changePage = this.changePage.bind(this);
  }

  setTracks(playTrack) {
    this.props.actions.setTracks(this.props.favoriteTracks);
    this.props.actions.setTrackPlaying(playTrack);
  }

  changePage(current) {
    const offset = current * 5;
    const perSlide = this.props.favoriteTracks.slice(offset - 5, offset);

    this.setState({
      perPageTracks: perSlide,
      currentPage: current,
    });
  }

  render() {
    const { favoriteTracks } = this.props;

    if (!favoriteTracks) { return false; }

    if (favoriteTracks.length === 0) {
      return (
        <div className="list-box">
          <div className="tab__header flexbox justify-between align-end">
            <div className="title_primary title_primary__inner">
              My Favourites
            </div>
          </div>
          <div className="author-panel__disabled">
            <div className="author-panel__disabled-bg">
              <div className="author-panel__disabled-header author-panel__disabled-header--favourites">
                You haven’t liked anything yet.
              </div>
              <Link to="/catalog" className="author-panel__disabled-subheader">
                Find some lovely tracks
              </Link>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="list-box">
        <div className="tab__header flexbox justify-between align-end">
          <div className="title_primary title_primary__inner">
            My Favourites ({favoriteTracks.length})
          </div>
        </div>
        <TrackList
          tracks={this.state.perPageTracks}
          userInfo={this.props.userInfo}
          setTracks={playTrack => this.setTracks(playTrack)}
        />
        <PaginationAdvanced changePage={this.changePage} allPages={this.state.allPages} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  favoriteTracks: state.user.userInfo.favorite_tracks,
  userInfo: state.user.userInfo,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setTracks,
    setTrackPlaying,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Favourites);
