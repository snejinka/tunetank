import React from 'react';
import AuthorImage from '../../../../assets/img/wil.png';

class AuthorPanel extends React.Component {
  render() {
    return (
      <div className="author-panel">
        <div className="author-panel__image">
          <img src={AuthorImage} alt="" />
        </div>
        <div className="author-panel__name">
          Will Iam
        </div>
        <div className="author-panel__description">
          <p>Warsaw</p>
          <p>United Kingdom</p>
        </div>
        <div className="author-panel__count">
          <div className="music-icon music-icon__indent-right" />
          215
        </div>
        <button className="btn btn-action btn-action__role-follow">
          follow
        </button>
      </div>
    );
  }
}

export default AuthorPanel;
