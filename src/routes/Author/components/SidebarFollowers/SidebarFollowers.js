import React, { PropTypes } from 'react';
import NoAvatar from 'assets/img/no-avatar.png';
import AuthorPanel from 'components/AuthorPanel';
import FollowerItem from './FollowerItem';
class SidebarFollowers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      followers: this.props.userInfo.followers.slice(0, 5),
      hoverTrack: false,
      openAuthorPanel: false,
    };
  }

  static propTypes = {
    userInfo: PropTypes.object,
  }

  onMouseEnter(e) {
    e.stopPropagation();
    this.setState({
      hoverTrack: true,
      openAuthorPanel: true,
    });
  }

  onMouseLeave(e) {
    e.stopPropagation();
    this.setState({
      hoverTrack: false,
      openAuthorPanel: false,
    });
  }

  toggleAuthorPanel() {
    this.setState({
      openAuthorPanel: false,
    });
  }

  renderAuthorPanel(user) {
    if (this.state.hoverTrack) {
      return (
        <AuthorPanel
          user={user} openAuthorPanel={this.state.openAuthorPanel}
          toggleAuthorPanel={this.toggleAuthorPanel.bind(this)} isFollower
        />
      );
    }
    return null;
  }

  render() {
    if (this.props.userInfo.followers.length === 0) { return null; }
    return (
      <div className="follow-list">
        <div className="header__primary">
          Followers
          <div className="header__primary-note">
            {this.props.userInfo.followers.length}
          </div>
        </div>
        <div className="follow-list-wrap">
          {this.state.followers.map((follower, i) => <FollowerItem key={i} follower={follower} />)}
        </div>
      </div>
    );
  }
}

export default SidebarFollowers;
