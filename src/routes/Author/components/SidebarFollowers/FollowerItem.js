import React, { PropTypes } from 'react';
import AuthorPanel from 'components/AuthorPanel';
import NoAvatar from 'assets/img/no-avatar.png';
import slug from 'slug';
import { Link } from 'react-router';


class FollowerItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hoverTrack: false,
      openAuthorPanel: false,
    };
  }

  static propTypes = {
    follower: PropTypes.object,
  }

  onMouseEnter(e) {
    e.stopPropagation();
    this.setState({
      hoverTrack: true,
      openAuthorPanel: true,
    });
  }

  onMouseLeave(e) {
    e.stopPropagation();
    this.setState({
      hoverTrack: false,
      openAuthorPanel: false,
    });
  }

  toggleAuthorPanel() {
    this.setState({
      openAuthorPanel: false,
    });
  }

  renderAuthorPanel() {
    return (
      <AuthorPanel
        user={this.props.follower} openAuthorPanel={this.state.openAuthorPanel}
        toggleAuthorPanel={this.toggleAuthorPanel.bind(this)} isFollower
      />
    );
  }

  render() {
    const { follower } = this.props;
    return (
      <div className="follow-list__item" onMouseLeave={this.onMouseLeave.bind(this)}>
        <Link to={`/author/${slug(follower.name, { lower: true })}/${follower.id}`} className="follow-list__item-image">
          <img
            src={follower.avatar !== null ? follower.avatar : NoAvatar}
            className="img-responsive"
            onMouseEnter={this.onMouseEnter.bind(this)}
            alt="follower"
          />
        </Link>
        {this.renderAuthorPanel(follower)}        
      </div>
    );
  }
}

export default FollowerItem;
