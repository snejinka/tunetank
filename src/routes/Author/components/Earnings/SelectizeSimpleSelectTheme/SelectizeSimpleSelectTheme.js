import React from 'react';
import { ReactSelectize, SimpleSelect } from 'react-selectize';
import 'react-selectize/themes/index.css';

class SelectizeSimpleSelectTheme extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '2016',
    };
  }

  render() {
    const options = ['2016', '2017', '1000', '2499'].map(option => ({ label: option, value: option }));
    return (<SimpleSelect
      options={options}
      placeholder=""
      className="selectize-select-theme react-default-template selectize-select-theme-filter"
      defaultValue={{
        label: '2016',
        value: '2016',
      }}
      onValueChange={function (newValue, callback) {
        this.setState({ value: newValue }, callback);
      }}
    />);
  }
}
export default SelectizeSimpleSelectTheme;

