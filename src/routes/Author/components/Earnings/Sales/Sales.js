import React from 'react';
import { connect } from 'react-redux';
import accounting from 'accounting';
import TableTotal from '../TableTotal';
import ChartDashboard from '../ChartDashboard';

const Sales = props => (
  <div>
    <div className="tab__header flexbox align-center justify-between">
      <div className="title_primary title_primary__inner">Sales</div>
      <div className="panel-notification panel-notification_balance flexbox align-center justify-between">
        <div className="panel-notification__description panel-notification__description_balance">
          <div className="title_primary title_primary__small">
            {accounting.formatMoney(props.currentUser.balance_in_cents / 100)}
          </div>
          <div className="title title-note">
            Your ballance
          </div>
        </div>
        <button className="btn btn-primary btn-primary__small">
          Withdrawal money
        </button>
      </div>
    </div>
    <ChartDashboard />
    <TableTotal />
  </div>
);

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
});

export default connect(mapStateToProps, null)(Sales);
