import React, { PropTypes } from 'react';
import { Row, Col } from 'react-bootstrap';

class TableTotal extends React.Component {
  render() {
    return (
      <div className="table table-total">
        <Row className="table__header ">
          <Col md={4}>
                    Time Frame
                </Col>
          <Col md={5}>
                    Sales count
                </Col>
          <Col md={3}>
                    Earnings
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    January
                </Col>
          <Col md={5}>
                    65
                </Col>
          <Col md={3}>
                    $650
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    February
                </Col>
          <Col md={5}>
                    36
                </Col>
          <Col md={3}>
                    $520
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    March
                </Col>
          <Col md={5}>
                    75
                </Col>
          <Col md={3}>
                    $235
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    April
                </Col>
          <Col md={5}>
                    34
                </Col>
          <Col md={3}>
                    $750
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    May
                </Col>
          <Col md={5}>
                    62
                </Col>
          <Col md={3}>
                    $356
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    June
                </Col>
          <Col md={5}>
                    119
                </Col>
          <Col md={3}>
                    $90
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    July
                </Col>
          <Col md={5}>
                    65
                </Col>
          <Col md={3}>
                    $335
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    August
                </Col>
          <Col md={5}>
                    98
                </Col>
          <Col md={3}>
                    $256
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    September
                </Col>
          <Col md={5}>
                    90
                </Col>
          <Col md={3}>
                    $870
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    October
                </Col>
          <Col md={5}>
                    97
                </Col>
          <Col md={3}>
                    $1270
                </Col>
        </Row>
        <Row className="table__row">
          <Col md={4}>
                    November
                </Col>
          <Col md={5}>
                    34
                </Col>
          <Col md={3}>
                    $639
                </Col>
        </Row>
        <Row className="table__footer">
          <Col md={4}>
                    Total
                </Col>
          <Col md={5}>
                    598
                </Col>
          <Col md={3}>
                    $3515.60
                </Col>
        </Row>
      </div>
    );
  }
}

export default TableTotal;
