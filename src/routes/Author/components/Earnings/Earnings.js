import React from 'react';
import {
  Row,
  Col,
  Tab,
  Nav,
  NavItem,
} from 'react-bootstrap';
import Sales from './Sales';

export const Earnings = () => (
  <Tab.Container id="tabs-inner" defaultActiveKey="first">
    <Row className="clearfix">
      <Col sm={12}>
        <Nav className="tabs-menu tabs-menu__inner  flexbox">
          <NavItem eventKey="first" className="tabs-menu__item tabs-menu__item__gap-right">
            Sales
          </NavItem>
          <NavItem eventKey="second" className="tabs-menu__item tabs-menu__item__gap-right">
            Referals
          </NavItem>
        </Nav>
      </Col>
      <Col md={12} sm={12}>
        <Tab.Content className="tab-wrap-inner" animation>
          <Tab.Pane eventKey="first">
            <Sales />
          </Tab.Pane>
          <Tab.Pane eventKey="second">
            tab referals content
          </Tab.Pane>
        </Tab.Content>
      </Col>
    </Row>
  </Tab.Container>
);

export default Earnings;
