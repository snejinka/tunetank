import React from 'react';
import SelectizeSimpleSelectTheme from '../SelectizeSimpleSelectTheme';
import ChartBars from '../ChartBars';

export const ChartDashboard = () => (
  <div className="chart-dashboard">
    <div className="chart-dashboard__header">
      <div className="chart-dashboard__header-inner-wrap">
        <div className="chart-dashboard-title-wrap">
          <div className="title title-note">
                            Show report for
                        </div>
        </div>
        <div className="nav-filter">
          <div className="nav-filter-group">
            <button className="btn btn-filter btn-filter__active">Month</button>
            <button className="btn btn-filter">Year</button>
            <button className="btn btn-filter">All time</button>
          </div>
          <SelectizeSimpleSelectTheme />
        </div>
      </div>
      <div className="flexbox align-center">
        <div className="globe-icon globe-icon__right-gap" />
        <a href="" className="action-link action-link__view-all action-link__underlined">Statistic by countries</a>
      </div>
    </div>
    <div className="chart-dashboard__content">
      <div className="chart-wrapper">
        <ChartBars />
      </div>
      <div className="summary">
        <div className="summary__item">
          <div className="summary__item-hedear">
                            $3515.00
                        </div>
          <div className="summary__item-name">
                            Total Earnings
                        </div>
          <div className="summary__item-note">
                            Year 2016
                        </div>
        </div>
        <div className="summary__item">
          <div className="summary__item-hedear">
                            598
                        </div>
          <div className="summary__item-name">
                            Total Sales
                        </div>
          <div className="summary__item-note">
                            Year 2016
                        </div>
        </div>
        <div className="summary__item">
          <div className="summary__item-hedear">
                            $292.73
                        </div>
          <div className="summary__item-name">
                            Montly Earnings
                        </div>
          <div className="summary__item-note">
                            Average
                        </div>
        </div>
        <div className="summary__item">
          <div className="summary__item-hedear">
                            37
                        </div>
          <div className="summary__item-name">
                            Montly Sales
                        </div>
          <div className="summary__item-note">
                            Average
                        </div>
        </div>
      </div>
    </div>
  </div>
    );

export default ChartDashboard;
