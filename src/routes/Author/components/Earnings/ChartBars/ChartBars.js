import React, { PropTypes } from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
  datasets: [{
    label: 'My First dataset',
    backgroundColor: 'rgba(255,204,0,1)',
    borderColor: 'rgba(255,204,0,1)',
    borderWidth: 0,
    hoverBackgroundColor: 'rgba(255,204,0,1)',
    hoverBorderColor: 'rgba(255,204,0,1)',
    data: [10650, 5901, 8022, 8144, 11560, 11550, 4066, 8866, 1111, 3300, 7611, 4111],
  }],
};
const ChartBars = React.createClass({
  render() {
    return <Bar data={data} />;
  },
});

export default ChartBars;
