import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { setBiography, updateExactFealds } from 'store/user';
import classNames from 'classnames';
import { ReactHeight } from 'react-height';


class Biography extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editBiography: false,
      expandedBiography: false,
      biographyHeight: 0,
    };
  }

  changeBiography = (event) => {
    this.props.actions.setBiography(event.target.value);
  }

  submitBiography = (event) => {
    this.setState({ editBiography: false });
    this.props.actions.updateExactFealds(this.props.userInfo.id, { biography: this.props.userInfo.biography });
  }

  renderBiography() {
    if (this.state.editBiography === false) {
      return (
        <ReactHeight
          onHeightReady={height => this.setState({ biographyHeight: height })}
          onClick={() => this.props.userInfo.is_current && this.setState({ editBiography: true })}
        >
          <p className={classNames('biography__text', { biography__text_expanded: this.state.expandedBiography })}>
            {this.props.userInfo.biography ? this.props.userInfo.biography : 'Type your biography here...'}
          </p>
        </ReactHeight>

      );
    }
    return (
      <FormGroup controlId="formControlsTextarea">
        <FormControl
          componentClass="textarea"
          autoFocus
          className="biography__edit-area"
          placeholder="Type your biography"
          onChange={this.changeBiography}
          onBlur={this.submitBiography}
          value={this.props.userInfo.biography}
        />
      </FormGroup>
    );
  }

  render() {
    if ((this.props.userInfo.biography === null || this.props.userInfo.biography === '') && !this.props.userInfo.is_current) {
      return null;
    }
    return (
      <div className="text-block biography">
        <div className="header__primary">
          <span>Biography</span>
        </div>
        {this.renderBiography()}
        {this.state.biographyHeight >= 137 && this.state.editBiography === false &&
          <button
            className={classNames('btn-link btn-link__more btn-link__more-with-arrow', { 'btn-link_hide': this.state.expandedBiography })}
            onClick={() => this.setState({ expandedBiography: !this.state.expandedBiography })}
          >{this.state.expandedBiography ? 'Hide' : 'Read more'}
          </button>
          }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.user.userInfo,
  social_links: state.user.social_links,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setBiography, updateExactFealds,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Biography);

