import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';
import classNames from 'classnames';
import _ from 'lodash';
import NoAvatar from 'assets/img/no-avatar.png';
import slug from 'slug';

class SimilarArtists extends React.Component {
  renderItems() {
    const nodes = [];
    this.props.similarArtists.forEach((item, i) => {
      const avatar = item.avatar == null ? NoAvatar : item.avatar;
      nodes.push(
        <Link key={i} to={`/author/${slug(item.name, { lower: true })}/${item.id}`} className="card-list__item">
          <div className="card-list__item-image">
            <img src={avatar} className="img-responsive" alt="fergie <3" />
          </div>
          <div className="card-list__item-title">
            {item.name}
          </div>
        </Link>,
            );
    });
    nodes.splice(0, 2);
    return nodes;
  }

  render() {
    return (

     <div className="card-list">
      {this.props.similarArtists.length > 0 && 
      <div>
        <div className="header__primary">
            Similar Artists
        </div>
        <div className="card-list-wrap">
          {this.renderItems()}
        </div>
      </div>
      }
    </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  similarArtists: state.user.similarArtists,
});

export default connect(mapStateToProps, null)(SimilarArtists);

