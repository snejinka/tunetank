import React from 'react';
import Selectize from './SelectizeSimpleSelectTheme';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import '../../../../../node_modules/react-datepicker/src/stylesheets/datepicker.scss';

class Datepicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment(),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.setState({
      startDate: date,
    });
  }
  render() {
    return (
      <div className="flexbox align-center filter-panel">
        <div className="datepicker">
          <DatePicker
            selected={this.state.startDate}
            selectsStart
            startDate={this.state.startDate}
            todayButton="Today"
            endDate={this.state.endDate}
            onChange={this.handleChangeStart}
          />
          <div className="flexbox align-center chain-word">to</div>
          <DatePicker
            selected={this.state.endDate}
            selectsEnd
            startDate={this.state.startDate}
            todayButton="Today"
            endDate={this.state.endDate}
            onChange={this.handleChangeEnd}
          />
        </div>
        <div className="chain-word">show</div>
        <div className="select-wrap">
          <Selectize />
        </div>
        <button className="btn btn-primary">
                    Refresh
                </button>
      </div>
    );
  }
}

export default Datepicker;
