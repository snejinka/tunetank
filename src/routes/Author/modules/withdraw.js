import axios from 'axios';
import toastr from 'toastr';
import { browserHistory } from 'react-router';
import { API_SERVER_URL } from 'api';

const PAYPAL_LAST_WITHDRAWS_API_URL = `${API_SERVER_URL}/api/v1/withdraws/last_withdraws`;
const PAYPAL_REQUEST_WITHDRAW_API_URL = `${API_SERVER_URL}/api/v1/withdraws/request_withdraw`;
const PAYPAL_CHANGE_ACCOUNT_API_URL = `${API_SERVER_URL}/api/v1/withdraws/change_paypal_account`;

export const SET_LAST_WITHDRAWS = 'SET_LAST_WITHDRAWS';

function totalAmountInCents(item) {
    return item * 100;
}

export const requestLastWithdraws = () => (dispatch, getState) => {  
    axios.get(PAYPAL_LAST_WITHDRAWS_API_URL)
    .then((resp) => {
      dispatch(setLastWithdraws(resp.data.withdraws))
    })
    .catch((res) => {
      toastr.error(res.error);
    });
};

export const requestWithdraw = ({email, value, type}) => (dispatch, getState) => {
    const params = type === 'single' ? { email: email, amount_in_cents: totalAmountInCents(value) } : { email: email }
    return axios.post(PAYPAL_REQUEST_WITHDRAW_API_URL, {'withdraw': params})
    .then((resp) => {
      dispatch(requestLastWithdraws());
    })
    .catch((res) => {
      toastr.error(res.error);
    });
};

export const requestChangePaypalAccount = (email, confirm_email) => (dispatch, getState) => {  
    axios.post(PAYPAL_CHANGE_ACCOUNT_API_URL, {
        email: email,
        confirm_email: confirm_email,
    })
    .then((resp) => {
      console.log(resp);
    })
    .catch((res) => {
      toastr.error(res.error);
    });
};

export const setLastWithdraws = (result) => (dispatch) => {
  dispatch({ type: SET_LAST_WITHDRAWS, payload: result });
};

const ACTION_HANDLERS = {
    [SET_LAST_WITHDRAWS]: (state, action) => ({
      ...state,
      lastWithdraws: action.payload,
    })
};

const initialState = {
    lastWithdraws: []
};

export default function withdrawReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}