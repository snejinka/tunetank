import React, { PropTypes } from 'react';
import 'styles/variables.scss';
import { Grid, Row, Col } from 'react-bootstrap';



import Scroll from 'react-scroll';
var Link       = Scroll.Link;
var Element    = Scroll.Element;
var Events     = Scroll.Events;
var scroll     = Scroll.animateScroll;
var scrollSpy  = Scroll.scrollSpy;

class LicenseTerms extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    Events.scrollEvent.register('begin', function(to, element) {
      console.log("begin", arguments);
    });

    Events.scrollEvent.register('end', function(to, element) {
      console.log("end", arguments);
    });

    scrollSpy.update();
  }

  componentWillUnmount(){
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col md={12}>
            <Row>
              <Col md={2}>
                <div className="scroll-content-nav">
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-100} to="individual" spy={true} smooth={true} duration={500}>Individual</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-250} to="professional" spy={true} smooth={true} duration={500}>Professional</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-250} to="marketing" spy={true} smooth={true} duration={500}>Marketing</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-210} to="enterprise" spy={true} smooth={true} duration={500}>Enterprise</Link>
                  <Link activeClass="active" className="scroll-content-nav__item" offset={-210} to="universal" spy={true} smooth={true} duration={500}>Universal</Link>
                </div>

              </Col>
              <Col md={8}>
                <div className="scroll-content__container">
                  <h1 className="title title_primary title_grey text-center">Licence terms</h1>
                  <Element name="individual" id="individual" className="element" >
                    <h2 className="scroll-content__title-second">Individual</h2>
                    <p  className="scroll-content__text scroll-content__list-title">For personal use, informational and educational purposes.</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Personal videos on Youtube channel</li>
                      <li className="scroll-content__text content-list__item">Homevideos</li>
                      <li className="scroll-content__text content-list__item">Slideshows</li>
                      <li className="scroll-content__text content-list__item">Non profit apps and games</li>
                    </ul>
                    <p  className="scroll-content__text scroll-content__list-title">Examples:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">You’re making travel videos for your non-commercial Youtube channel</li>
                      <li className="scroll-content__text content-list__item">You’re making a video of your baby’s first birthday party and want to share it with your friends and family</li>
                      <li className="scroll-content__text content-list__item">You’re creating your graduation slideshow and need music that matches to the pace</li>
                    </ul>
                  </Element>
                  <Element name="professional" id="professional" className="element" >
                    <h2 className="scroll-content__title-second">Professional</h2>
                    <p  className="scroll-content__text scroll-content__list-title">All the benefits of our Individual licence plus suits early-stage business outcome goals.</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Wedding day films</li>
                      <li className="scroll-content__text content-list__item">TV and Radio shows</li>
                      <li className="scroll-content__text content-list__item">Low budget crowdfunding goals, reels, tradeshows and games </li>
                    </ul>
                    <p  className="scroll-content__text scroll-content__list-title">Examples:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">For videographers who create commercial wedding day content</li>
                      <li className="scroll-content__text content-list__item">For those who want to start their own Talk show</li>
                      <li className="scroll-content__text content-list__item">For business newcomers who look for funding for their first mobile game</li>
                    </ul>
                  </Element>
                  <Element name="marketing" id="marketing" className="element" >
                    <h2 className="scroll-content__title-second">Marketing</h2>
                    <p  className="scroll-content__text scroll-content__list-title">Suitable for promoting products, services or brands.</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Internet, TV and Radio commercials</li>
                    </ul>
                    <p  className="scroll-content__text scroll-content__list-title">Examples:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">You’re promoting your client’s business </li>
                      <li className="scroll-content__text content-list__item">You’re looking for a catchy track for your product’s TV commercial</li>
                      <li className="scroll-content__text content-list__item">You’re an advertising company creating a Radio commercial for a client</li>
                    </ul>
                  </Element>
                  <Element name="enterprise" id="enterprise" className="element" >
                    <h2 className="scroll-content__title-second">Enterprise</h2>
                    <p  className="scroll-content__text scroll-content__list-title">Suitable for large companies. Covers broader advertising campaigns.</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">High budget crowdfunding goals, reels and tradeshows</li>
                      <li className="scroll-content__text content-list__item">Feature films with low budget</li>
                    </ul>
                    <p  className="scroll-content__text scroll-content__list-title">Examples:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">You’re a filmmaker who needs background tracks for future movie</li>
                      <li className="scroll-content__text content-list__item">You need funds from common people to start working on your creative project</li>
                      <li className="scroll-content__text content-list__item">You’re planning large tradeshow exhibition </li>
                    </ul>
                  </Element>
                  <Element name="universal" id="universal" className="element" >
                    <h2 className="scroll-content__title-second">Universal</h2>
                    <p  className="scroll-content__text scroll-content__list-title">All use cases and scenarios. No budget and audience limitations</p>
                    <ul className="content-list content-list_circle">
                      <li className="scroll-content__text content-list__item">Multimillion dollars blockbusters</li>
                      <li className="scroll-content__text content-list__item"> Giant companies and projects</li>
                    </ul>
                    <p  className="scroll-content__text scroll-content__list-title">Examples:</p>
                    <ul className="content-list content-list_tick">
                      <li className="scroll-content__text content-list__item">You’re Guy Ritchie searching for a cool track for your next masterpiece</li>
                      <li className="scroll-content__text content-list__item">You’re a top manager in BMW and need background tracks for your next tradeshow</li>
                    </ul>
                  </Element>
                </div>
              </Col>
            </Row>

          </Col>
        </Row>
      </Grid>
    );
  }
}


export default LicenseTerms;
