import LicenseTerms from './LicenseTerms';

export default store => ({
  path: 'license-terms',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, LicenseTerms);
    }, 'LicenseTerms');
  },
});
